package com.unalarabe.urpc.meta;

import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportEnum;
import com.unalarabe.urpc.converter.structure.ExportException;
import com.unalarabe.urpc.converter.structure.ExportField;
import com.unalarabe.urpc.converter.structure.ExportMethod;
import com.unalarabe.urpc.converter.structure.ExportNativeClass;
import com.unalarabe.urpc.converter.structure.ExportParameter;
import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.util.ServiceUtil;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class Generator {

    private final String javaNamespace;

//    private ExportEnum errorsEnum;
    public Generator(String javaNamespace) {
        this.javaNamespace = javaNamespace;
    }

    public ExportMethod generateRpcMehod(String namespace, ExportService exportService, ExportMethod method) {
        ExportMethod result = new ExportMethod();
        result.name = method.name;
        result.ignoredGenerators.addAll(method.ignoredGenerators);
        String requsetClassName = namespace + ".Request" + Util.firstLetterToUpperCase(method.name);
        ExportClass requestClass = new ExportClass(requsetClassName, true, false);
        for (ExportParameter parameter : method.parameters) {
            ExportField field = new ExportField();
            field.required = parameter.required;
            field.name = parameter.name;
            field.type = parameter.type;
            requestClass.fields.add(field);
        }

        String responseClassName = namespace + ".Response" + Util.firstLetterToUpperCase(method.name);
        ExportClass responseClass = new ExportClass(responseClassName, false, true);
        if (!"void".equals(method.type.name)) {
            ExportField responseField = new ExportField();
            responseField.name = "result";
            responseField.type = method.type;
            responseClass.fields.add(responseField);
        }
        if (!method.exceptions.isEmpty()) {
            ExportField errorField = new ExportField();
            errorField.name = "errorValue";
            errorField.type = getErrorsEnum(namespace, exportService, method, null);
            responseClass.fields.add(errorField);
            ExportField errorTextField = new ExportField();
            errorTextField.name = "errorText";
            errorTextField.type = getStringClass(namespace, method);
            responseClass.fields.add(errorTextField);
        }

        ExportParameter request = new ExportParameter();
        request.name = "request";
        request.type = requestClass;

        result.parameters.add(request);
        result.type = responseClass;

        return result;
    }

    public ExportMethod generateRpcMehod(String namespace, ExportService exportService, List<ExportMethod> methods) {
        ExportMethod result = new ExportMethod();
        String methodName = ServiceUtil.first(methods).name;
        for (ExportMethod method : methods) {
            if (!methodName.equals(method.name)) {
                throw new IllegalArgumentException("Maethods has different names: " + methods);
            }
        }

        result.name = methodName;

        String requsetClassName = namespace + ".Request" + Util.firstLetterToUpperCase(methodName);
        ExportClass requestClass = new ExportClass(requsetClassName, true, false);
        String responseClassName = namespace + ".Response" + Util.firstLetterToUpperCase(methodName);
        ExportClass responseClass = new ExportClass(responseClassName, false, true);

        for (int i = 0; i < methods.size(); i++) {
            ExportMethod method = methods.get(i);
            String requsetSubClassName = namespace + ".Request" + Util.firstLetterToUpperCase(method.name) + i;
            ExportClass requestSubClass = new ExportClass(requsetSubClassName, true, false);
            for (ExportParameter parameter : method.parameters) {
                ExportField field = new ExportField();
                field.required = parameter.required;
                field.name = parameter.name;
                field.type = parameter.type;
                requestSubClass.fields.add(field);
            }
            ExportField field = new ExportField();
            field.required = false;
            field.name = "request" + i;
            field.type = requestSubClass;

            requestClass.fields.add(field);
        }

        for (int index = 0; index < methods.size(); index++) {
            ExportMethod method = methods.get(index);
            String responseSubClassName = namespace + ".Response" + Util.firstLetterToUpperCase(method.name) + index;
            ExportClass responseSubClass = new ExportClass(responseSubClassName, false, true);
            if (!"void".equals(method.type.name)) {
                ExportField responseField = new ExportField();
                responseField.name = "result";
                responseField.type = method.type;
                responseSubClass.fields.add(responseField);
            }
            if (!method.exceptions.isEmpty()) {
                ExportField errorField = new ExportField();
                errorField.name = "errorValue";
                errorField.type = getErrorsEnum(namespace, exportService, method, String.valueOf(index));
                responseSubClass.fields.add(errorField);
                ExportField errorTextField = new ExportField();
                errorTextField.name = "errorText";
                errorTextField.type = getStringClass(namespace, method);
                responseSubClass.fields.add(errorTextField);
            }
            ExportField field = new ExportField();
            field.required = false;
            field.name = "response" + index;
            field.type = responseSubClass;

            responseClass.fields.add(field);
        }
        ExportParameter request = new ExportParameter();
        request.name = "request";
        request.type = requestClass;

        result.parameters.add(request);
        result.type = responseClass;

        return result;
    }

    private ExportEnum getErrorsEnum(String namespace, ExportService exportService, ExportMethod method, String methodSuffix) {
        ExportEnum errorsEnum = new ExportEnum(exportService.name + Util.firstLetterToUpperCase(method.name) + (methodSuffix != null ? methodSuffix : "") + "ErrorEnum", true);
        ExportEnum result = errorsEnum;
        Set<String> shortNames = new HashSet<>();
        Set<String> fullNames = new HashSet<>();
        for (ExportException exception : method.exceptions) {
            String shortName = exception.name;
            int lastIndexOfDot = shortName.lastIndexOf(".");
            if (lastIndexOfDot > -1) {
                shortName = shortName.substring(lastIndexOfDot + 1);
            }
            shortNames.add(shortName);
            fullNames.add(exception.name.replaceAll("\\.", "_"));
        }
        if (shortNames.size() == method.exceptions.size()) {
//            result.entries.addAll(shortNames);
            result.entries.addAll(fullNames);
        } else if (fullNames.size() == method.exceptions.size()) {
            result.entries.addAll(fullNames);
        } else {
            throw new IllegalStateException("Can't export exceptions");
        }
        return result;
    }

    private ExportClass getStringClass(String namespace, ExportMethod method) {
        ExportClass result = new ExportNativeClass(String.class);
        return result;
    }

    public ExportService generateRpcService(ExportService service) {
        ExportService result = new ExportService(service.name);
        Set<String> methodNames = service.getMethodNames();
        ExportClass rpcRequest = new ExportClass(service.name + "Request", true, false);
        ExportClass rpcResponse = new ExportClass(service.name + "Response", false, true);
        ExportField correlationIdField = new ExportField();
        correlationIdField.name = "correlationId";
        correlationIdField.type = new ExportNativeClass(String.class);
        ExportField replyToField = new ExportField();
        replyToField.name = "replyTo";
        replyToField.type = new ExportNativeClass(String.class);
        ExportField versionField = new ExportField();
        versionField.name = "version";
        versionField.type = new ExportNativeClass(String.class);
        rpcRequest.fields.add(correlationIdField);
        rpcRequest.fields.add(replyToField);
        rpcRequest.fields.add(versionField);
        rpcResponse.fields.add(correlationIdField);
        for (String methodName : methodNames) {
            ExportMethod exportMethod;
            List<ExportMethod> methods = service.getMethods(methodName);
            if (methods.size() == 1) {
                ExportMethod method = ServiceUtil.first(methods);
                exportMethod = generateRpcMehod(Util.toNamespace(service.name), service, method);
            } else {
                exportMethod = generateRpcMehod(Util.toNamespace(service.name), service, methods);
            }
            result.addMethod(exportMethod.name, exportMethod);

            ExportField rpcRequestField = new ExportField();
            rpcRequestField.ignoredGenerators = new HashSet<>(exportMethod.ignoredGenerators);
            rpcRequestField.name = methodName;
            rpcRequestField.type = ServiceUtil.first(exportMethod.parameters).type;
            rpcRequest.fields.add(rpcRequestField);
            ExportField rpcResponseField = new ExportField();
            rpcResponseField.ignoredGenerators = new HashSet<>(exportMethod.ignoredGenerators);
            rpcResponseField.name = methodName;
            rpcResponseField.type = exportMethod.type;
            rpcResponse.fields.add(rpcResponseField);
        }
        result.getExtraClasses().add(rpcRequest);
        result.getExtraClasses().add(rpcResponse);
        return result;
    }
}
