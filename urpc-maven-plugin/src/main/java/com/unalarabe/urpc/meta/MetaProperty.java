package com.unalarabe.urpc.meta;

import com.google.protobuf.ByteString;
import com.squareup.protoparser.DataType;
import com.squareup.protoparser.FieldElement;
import java.util.List;
import java.util.Map;
import org.apache.avro.Schema;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MetaProperty implements Comparable<MetaProperty> {

    public final String type;
    public final String name;
    public final int order;
    public final boolean required;
    public final boolean javaType;

    public MetaProperty(String clazz, String name, int order, boolean javaType, boolean required) {
        this.type = clazz;
        this.name = name;
        this.order = order;
        this.javaType = javaType;
        this.required = required;
    }

    public MetaProperty(FieldElement fieldElement) {
        DataType dataType = fieldElement.type();
        this.required = fieldElement.label() == FieldElement.Label.REQUIRED;
        boolean list = fieldElement.label() == FieldElement.Label.REPEATED;
        this.type = resolveType(dataType, list, this.required);
        if (this.type == null) {
            throw new NullPointerException("Can't resolve field type");
        }
        this.name = fieldElement.name();
        this.order = fieldElement.tag();
        this.javaType = isJavaType(dataType);
    }

    public MetaProperty(Schema.Field fieldElement) {
        Schema schemaType = fieldElement.schema();
        this.required = false;
//        boolean list = fieldElement.label() == FieldElement.Label.REPEATED;
//        if (this.type == null) {
//            throw new NullPointerException("Can't resolve field type");
//        }
        this.type = resolveType(schemaType, schemaType.getType() == Schema.Type.ARRAY, false);
        this.name = fieldElement.name();
        this.order = fieldElement.pos();
        this.javaType = isJavaType(schemaType);
    }

    private Boolean isJavaType(DataType type) {
        return type instanceof DataType.ScalarType;
    }

    private Boolean isSimpleJavaType(Schema type) {
        Schema.Type schemaType = type.getType();
        return schemaType == Schema.Type.BOOLEAN
               || schemaType == Schema.Type.DOUBLE
               || schemaType == Schema.Type.FLOAT
               || schemaType == Schema.Type.INT
               || schemaType == Schema.Type.LONG
               || schemaType == Schema.Type.NULL
               || schemaType == Schema.Type.STRING;
    }

    private Boolean isJavaType(Schema type) {
        Schema.Type schemaType = type.getType();
        return isSimpleJavaType(type)
               || schemaType == Schema.Type.ARRAY && isSimpleJavaType(type.getElementType())
               || schemaType == Schema.Type.MAP && isSimpleJavaType(type.getValueType())
               || (schemaType == Schema.Type.UNION && type.getTypes().size() == 2
                   && type.getTypes().get(0).getType() == Schema.Type.NULL
                   && isSimpleJavaType(type.getTypes().get(1)));
    }

    private String resolveType(DataType type, boolean list, boolean required) {
        String result = null;
        if (type instanceof DataType.NamedType) {
            DataType.NamedType namedType = (DataType.NamedType) type;
            String baseType = namedType.name();
            if (list) {
                result = List.class.getCanonicalName() + " <" + baseType + ">";
            } else {
                result = baseType;
            }
        } else if (type instanceof DataType.ScalarType) {
            DataType.ScalarType scalarType = (DataType.ScalarType) type;
            String baseType = resolveProtoType(scalarType, required).getCanonicalName();
            if (list) {
                result = List.class.getCanonicalName() + "<" + baseType + ">";
            } else {
                result = baseType;
            }
        } else if (type instanceof DataType.MapType) {
            DataType.MapType mapType = (DataType.MapType) type;
            String keyType = resolveType(mapType.keyType(), false, false);
            String valueType = resolveType(mapType.valueType(), false, false);
            result = Map.class.getCanonicalName() + "<" + keyType + ", " + valueType + ">";
        } else {
            throw new IllegalStateException("Can't be here");
        }
        return result;
    }

    private String resolveType(Schema type, boolean list, boolean required) {
        String result = null;
        if (isSimpleJavaType(type)) {
            return resolveAvroType(type, required).getCanonicalName();
        } else if (type.getType() == Schema.Type.RECORD) {
            String baseType = type.getFullName();
            if (list) {
                result = List.class.getCanonicalName() + " <" + baseType + ">";
            } else {
                result = baseType;
            }
        } else if (type.getType() == Schema.Type.UNION && type.getTypes().size() == 2) {
            Schema baseType = type.getTypes().get(1);
            String baseTypeName = resolveType(baseType, baseType.getType() == Schema.Type.ARRAY, false);
            if (list) {
                result = List.class.getCanonicalName() + " <" + baseTypeName + ">";
            } else {
                result = baseTypeName;
            }
        } else if (type.getType() == Schema.Type.ARRAY) {
            String baseType = resolveAvroType(type.getElementType(), required).getCanonicalName();
            if (list) {
                result = List.class.getCanonicalName() + "<" + baseType + ">";
            } else {
                result = baseType;
            }
        } else if (type.getType() == Schema.Type.MAP) {
            String keyType = "java.lang.String";
            String valueType = resolveType(type.getValueType(), false, false);
            result = Map.class.getCanonicalName() + "<" + keyType + ", " + valueType + ">";
        } else if (type.getType() == Schema.Type.ENUM) {
            result = type.getFullName();
        } else {
            throw new IllegalStateException("Can't be here");
        }
        return result;
    }

    @Override
    public int compareTo(MetaProperty o) {
        return this.order - o.order;
    }

    @Override
    public String toString() {
        return "ProtoProperty{" + "clazz=" + type + ", name=" + name + ", order=" + order + ", required=" + required + '}';
    }

    private Class resolveProtoType(DataType.ScalarType type, boolean required) {
        switch (type) {
            case ANY:
                return Object.class;
            case BOOL:
                return required ? Boolean.TYPE : Boolean.class;
            case BYTES:
                return ByteString.class;
            case DOUBLE:
                return required ? Double.TYPE : Double.class;
            case FIXED32:
                return required ? Integer.TYPE : Integer.class;
            case FIXED64:
                return required ? Long.TYPE : Long.class;
            case FLOAT:
                return required ? Float.TYPE : Float.class;
            case INT32:
                return required ? Integer.TYPE : Integer.class;
            case INT64:
                return required ? Long.TYPE : Long.class;
            case SFIXED32:
                return required ? Integer.TYPE : Integer.class;
            case SFIXED64:
                return required ? Long.TYPE : Long.class;
            case SINT32:
                return required ? Integer.TYPE : Integer.class;
            case SINT64:
                return required ? Long.TYPE : Long.class;
            case STRING:
                return String.class;
            case UINT32:
                return required ? Integer.TYPE : Integer.class;
            case UINT64:
                return required ? Long.TYPE : Long.class;
            default:
                throw new IllegalStateException("Can't be here");
        }
    }

    private Class resolveAvroType(Schema type, boolean required) {
        switch (type.getType()) {
            case RECORD:
                return Object.class;
            case BOOLEAN:
                return required ? Boolean.TYPE : Boolean.class;
            case BYTES:
                return ByteString.class;
            case DOUBLE:
                return required ? Double.TYPE : Double.class;
            case FIXED:
                return required ? Integer.TYPE : Integer.class;
            case FLOAT:
                return required ? Float.TYPE : Float.class;
            case INT:
                return required ? Long.TYPE : Integer.class;
            case LONG:
                return required ? Long.TYPE : Long.class;
            case STRING:
                return String.class;
            default:
                throw new IllegalStateException("Can't be here");
        }
    }

}
