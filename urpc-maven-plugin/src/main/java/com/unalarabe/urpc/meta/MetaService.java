package com.unalarabe.urpc.meta;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MetaService {
    
    public String name;
    public List<MetaMethod> methods = new ArrayList<>();

    @Override
    public String toString() {
        return "ProtoService{" + "name=" + name + ", methods=" + methods + '}';
    }
    
}
