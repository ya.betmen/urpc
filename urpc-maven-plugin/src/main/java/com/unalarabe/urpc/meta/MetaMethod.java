package com.unalarabe.urpc.meta;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MetaMethod {

    public String name;
    public String originalName;
    public String resultType;
    public String parameterType;
    public List<String> errors = new ArrayList<>();

    @Override
    public String toString() {
        return "ProtoMethod{"
               + "name=" + name
               + ", resultType=" + resultType
               + ", parameterType=" + parameterType
               + ", errors=" + errors + '}';
    }

}
