package com.unalarabe.urpc.meta;

import com.squareup.protoparser.EnumConstantElement;
import com.squareup.protoparser.EnumElement;
import com.squareup.protoparser.FieldElement;
import com.squareup.protoparser.MessageElement;
import com.squareup.protoparser.TypeElement;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.avro.Schema;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MetaClass {

    public final String packet;
    public final String name;
    public final SortedSet<MetaProperty> properties = new TreeSet<>();
    public final boolean rpcResponse;
    public final boolean rpcRequest;
    public final SortedSet<String> entries = new TreeSet<>();

    public MetaClass(String packet, String name, boolean rpcResponse, boolean rpcRequest) {
        this.packet = packet;
        this.name = name;
        this.rpcResponse = rpcResponse;
        this.rpcRequest = rpcRequest;
    }

    public MetaClass(TypeElement typeElement, Map<String, TypeElement> types, boolean rpcResponse, boolean rpcRequest) {
        String qualifiedName = typeElement.qualifiedName();
        int lastIndexOfDot = qualifiedName.lastIndexOf(".");
        if (lastIndexOfDot > -1) {
            this.packet = qualifiedName.substring(0, lastIndexOfDot);
            this.name = qualifiedName.substring(lastIndexOfDot + 1);
        } else {
            this.packet = "";
            this.name = qualifiedName;
        }
        if (typeElement instanceof MessageElement) {
            MessageElement messageElement = (MessageElement) typeElement;
            for (FieldElement fieldElement : messageElement.fields()) {
                properties.add(new MetaProperty(fieldElement));
            }
        }
        if (typeElement instanceof EnumElement) {
            EnumElement enumElement = (EnumElement) typeElement;
            for (EnumConstantElement constant : enumElement.constants()) {
                entries.add(constant.name());
            }
        }
        this.rpcResponse = rpcResponse;
        this.rpcRequest = rpcRequest;
    }

    public MetaClass(Schema typeElement, Map<String, Schema> types, boolean rpcResponse, boolean rpcRequest) {
        String qualifiedName = typeElement.getFullName();
        int lastIndexOfDot = qualifiedName.lastIndexOf(".");
        if (lastIndexOfDot > -1) {
            this.packet = qualifiedName.substring(0, lastIndexOfDot);
            this.name = qualifiedName.substring(lastIndexOfDot + 1);
        } else {
            this.packet = "";
            this.name = qualifiedName;
        }
        System.out.println("-----------------------");
        System.out.println("typeElement = " + typeElement);

        if (typeElement.getType() == Schema.Type.RECORD) {
            List<Schema.Field> fields = typeElement.getFields();
            for (Schema.Field fieldElement : fields) {
                properties.add(new MetaProperty(fieldElement));
            }
        }
        if (typeElement.getType() == Schema.Type.ENUM) {
            for (String entry : typeElement.getEnumSymbols()) {
                entries.add(entry);
            }
        }
        this.rpcResponse = rpcResponse;
        this.rpcRequest = rpcRequest;
    }

    public String getFullName() {
        return packet.isEmpty() ? name : packet + "." + name;
    }

    @Override
    public String toString() {
        return "ProtoClass{" + "packet=" + packet + ", name=" + name + ", properties=" + properties + ", enties=" + entries + '}';
    }

}
