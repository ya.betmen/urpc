package com.unalarabe.urpc.maven.plugin.proto2.writer;

import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportField;
import com.unalarabe.urpc.maven.plugin.Util;
import java.util.HashSet;
import java.util.Set;

import static com.unalarabe.urpc.maven.plugin.Util.getterFrom;
import static com.unalarabe.urpc.maven.plugin.Util.getterTo;
import static com.unalarabe.urpc.maven.plugin.Util.setterFrom;
import static com.unalarabe.urpc.maven.plugin.Util.protoSetterTo;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class SimpleWriter extends ConverterWriter {

    private static final String TEMPLATE_COPY_TO = "th.{setter}({prefix}{converterToName}(original.{getter}()){suffix});";
    private static final String TEMPLATE_COPY_TO_ISSET = "if (original.{getter}() != null) { th.{setter}({prefix}{converterToName}(original.{getter}()){suffix}); }";
    private static final String TEMPLATE_COPY_FROM = "original.{setter}({converterFromName}({prefix}th.{getter}(){suffix}));";
    private static final String TEMPLATE_COPY_FROM_ISSET = "if (th.{isset}()) { original.{setter}({converterFromName}({prefix}th.{getter}(){suffix})); }";
    private static final String TEMPLATE_COPY_SIMPLE_TO = "th.{setter}({prefix}original.{getter}(){suffix});";
    private static final String TEMPLATE_COPY_SIMPLE_TO_ISSET = "if (original.{getter}() != null) { th.{setter}({prefix}original.{getter}(){suffix}); }";
    private static final String TEMPLATE_COPY_SIMPLE_FROM = "original.{setter}({prefix}th.{getter}(){suffix});";
    private static final String TEMPLATE_COPY_SIMPLE_FROM_ISSET = "if (th.{isset}()) { original.{setter}({prefix}th.{getter}(){suffix}); }";
    private static final String TEMPLATE_TO_TRIFT
        = "    public static {proto2Class} {converterToName}({originalClass} original) {\n"
        + "        if (original == null) {\n"
        + "            return null;\n"
        + "        }\n"
        + "        {proto2Class}.Builder th = {proto2Class}.newBuilder();\n"
        + "{bodyTo}"
        + "        return th.build();\n"
        + "    }\n";
    private static final String TEMPLATE_FROM_TRIFT
        = "    public static {originalClass} {converterFromName}({proto2Class} th) {\n"
        + "        if (th == null) {\n"
        + "            return null;\n"
        + "        }\n"
        + "        {originalClass} original = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createObject({originalClass}.class);\n"
        + "{bodyFrom}"
        + "        return original;\n"
        + "    }\n";
    private final ExportClass exportClass;

    public SimpleWriter(ExportClass exportClass, WriterFactory processed) {
        super(processed);
        this.exportClass = exportClass;
    }

    private String makeIsset(ExportField field) {
        return "has" + field.name.substring(0, 1).toUpperCase() + field.name.substring(1);
    }

    @Override
    public String write() {
        StringBuilder bodyTo = new StringBuilder();
        StringBuilder bodyFrom = new StringBuilder();
        StringBuilder result = new StringBuilder();
        Set<String> methods = new HashSet<>();
        for (ExportField field : exportClass.fields) {
            if (!field.isIgnored("proto2")) {
                String getterFrom = getterFrom(field);
                String getterTo = getterTo(field);
                String setterFrom = setterFrom(field);
                String setterTo = protoSetterTo(field);
                String converterToName = writeConverterToName(field.type);
                String converterFromName = writeConverterFromName(field.type);
                String prefixTo = "";
                String suffixTo = "";
                String prefixFrom = "";
                String suffixFrom = "";
                if (Util.isChar(field.type.name)) {
                    if (Util.isCollection(field.type)) {
                        prefixTo = "convertListOfCharsToIntegers(";
                        suffixTo = ")";
                        prefixFrom = "convertListOfIntegersToChars(";
                        suffixFrom = ")";
                    } else {
                        prefixTo = "(int)";
                        suffixTo = "";
                        prefixFrom = "(char)";
                        suffixFrom = "";
                    }
                } else if (Util.isByte(field.type.name)) {
                    if (Util.isCollection(field.type)) {
                        prefixTo = "convertListOfBytesToIntegers(";
                        suffixTo = ")";
                        prefixFrom = "convertListOfIntegersToBytes(";
                        suffixFrom = ")";
                    } else {
                        prefixTo = "(int)";
                        suffixTo = "";
                        prefixFrom = "(byte)";
                        suffixFrom = "";
                    }
                } else if (Util.isDate(field.type.name)) {
                    if (Util.isCollection(field.type)) {
                        prefixTo = "convertListOfDateToTimestamps(";
                        suffixTo = ")";
                        prefixFrom = "convertListOfTimestampToDates(";
                        suffixFrom = ")";
                    } else {
                        prefixTo = "convertDateToTimestamp(";
                        suffixTo = ")";
                        prefixFrom = "convertTimestampToDate(";
                        suffixFrom = ")";
                    }
                }
                String templateTo;
                String templateFrom;
                String isset = makeIsset(field);
                if (!Util.isCollection(field.type) && Util.isExistsInProto2(field.type)) {
                    templateTo = TEMPLATE_COPY_SIMPLE_TO;
                    templateFrom = TEMPLATE_COPY_SIMPLE_FROM;
                    if (!field.required && !Util.isSimple(field.type)) {
                        templateTo = TEMPLATE_COPY_SIMPLE_TO_ISSET;
                        templateFrom = TEMPLATE_COPY_SIMPLE_FROM_ISSET;
                    }
                } else {
                    ConverterWriter writer = createWriter(field.type);
                    if (writer != null) {
                        result.append(writer.write());
                    }
                    templateTo = TEMPLATE_COPY_TO;
                    templateFrom = TEMPLATE_COPY_FROM;
                    if (!field.required && !Util.isSimple(field.type) && !Util.isCollection(field.type)) {
                        templateTo = TEMPLATE_COPY_TO_ISSET;
                        templateFrom = TEMPLATE_COPY_FROM_ISSET;
                    }
                }
                String methodTo = Util.format(templateTo, "setter", setterTo, "getter", getterTo, "converterToName", converterToName, "prefix", prefixTo, "suffix", suffixTo, "isset", isset);
                System.out.println("SimpleWriter.methodTo = " + converterToName);
                String methodFrom = Util.format(templateFrom, "setter", setterFrom, "getter", getterFrom, "converterFromName", converterFromName, "prefix", prefixFrom, "suffix", suffixFrom, "isset", isset);
                if (!methods.contains(methodTo)) {
                    methods.add(methodTo);
                    bodyTo.append("        ").append(methodTo);
                    bodyTo.append("\n");
                }
                if (!methods.contains(methodFrom)) {
                    methods.add(methodFrom);
                    bodyFrom.append("        ").append(methodFrom);
                    bodyFrom.append("\n");
                }
            }
        }
        String converterToName = writeConverterToName(exportClass);
        String converterFromName = writeConverterFromName(exportClass);
        boolean addSeparator = false;

        if (!isConverterMethoExists(converterToName)) {
            addConverterMethod(converterToName);
            result.append(Util.format(TEMPLATE_TO_TRIFT,
                "className", toName(exportClass.name),
                "proto2Class", toProto2Class(exportClass),
                "originalClass", toOriginalClass(exportClass),
                "converterToName", converterToName,
                "converterFromName", converterFromName,
                "bodyTo", bodyTo,
                "bodyFrom", bodyFrom));
        }

        if (!isConverterMethoExists(converterFromName)) {
            addConverterMethod(converterFromName);
            if (addSeparator) {
                result.append("\n");
            }
            result.append(Util.format(TEMPLATE_FROM_TRIFT,
                "className", toName(exportClass.name),
                "proto2Class", toProto2Class(exportClass),
                "originalClass", toOriginalClass(exportClass),
                "converterToName", converterToName,
                "converterFromName", converterFromName,
                "bodyTo", bodyTo,
                "bodyFrom", bodyFrom));
        }

        return result.toString();
    }

}
