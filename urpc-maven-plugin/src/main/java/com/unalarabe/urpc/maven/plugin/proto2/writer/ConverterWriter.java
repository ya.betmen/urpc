package com.unalarabe.urpc.maven.plugin.proto2.writer;

import java.util.Set;
import com.unalarabe.urpc.converter.structure.ExportArray;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportCollection;
import com.unalarabe.urpc.converter.structure.ExportField;
import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.converter.structure.ExportSet;
import com.unalarabe.urpc.maven.plugin.Util;

import static com.unalarabe.urpc.maven.plugin.Util.format;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public abstract class ConverterWriter {

    public static final String TEMPLATE_FROM_PROTO2_NAME = "convert{className}FromProto2";
    public static final String TEMPLATE_LIST_TO_PROTO2_NAME = "convertListOf{className}ToProto2";
    public static final String TEMPLATE_LIST_FROM_PROTO2_NAME = "convertListOf{className}FromProto2";
    public static final String TEMPLATE_SET_TO_PROTO2_NAME = "convertSetOf{className}ToProto2";
    public static final String TEMPLATE_SET_FROM_PROTO2_NAME = "convertSetOf{className}FromProto2";
    public static final String TEMPLATE_ARRAY_TO_PROTO2_NAME = "convertArrayOf{className}ToProto2";
    public static final String TEMPLATE_ARRAY_FROM_PROTO2_NAME = "convertArrayOf{className}FromProto2";
    public static final String TEMPLATE_MAP_TO_PROTO2_NAME = "convertMapOf{keyClassName}To{valueClassName}ToProto2";
    public static final String TEMPLATE_MAP_FROM_PROTO2_NAME = "convertMapOf{keyClassName}To{valueClassName}FromProto2";
    public static final String TEMPLATE_TO_PROTO2_NAME = "convert{className}ToProto2";

    private final WriterFactory factory;

    public ConverterWriter(WriterFactory processed) {
        this.factory = processed;
    }

    protected ConverterWriter createWriter(ExportClass exportClass) {
        return this.factory.createWriter(exportClass);
    }

    public static String toName(String className) {
        StringBuilder sb = new StringBuilder();
        String[] parts = className.replace("[]", "").split("\\.");
        for (String part : parts) {
            sb.append(part.substring(0, 1).toUpperCase());
            if (part.length() > 1) {
                sb.append(part.substring(1));
            }
        }
        return sb.toString();
    }

    public static String writeConverterToName(ExportClass ec) {
        String converterToName;
        if (ec instanceof ExportList) {
            converterToName = format(TEMPLATE_LIST_TO_PROTO2_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportSet) {
            converterToName = format(TEMPLATE_SET_TO_PROTO2_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportArray) {
            converterToName = format(TEMPLATE_ARRAY_TO_PROTO2_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportMap) {
            ExportMap em = (ExportMap) ec;
            converterToName = format(TEMPLATE_MAP_TO_PROTO2_NAME, "keyClassName", toName(em.keyType), "valueClassName", toName(em.name));
        } else {
            converterToName = format(TEMPLATE_TO_PROTO2_NAME, "className", toName(ec.name));
        }
        return converterToName;
    }

    public static String writeConverterFromName(ExportClass ec) {
        String converterFromName;
        if (ec instanceof ExportList) {
            converterFromName = format(TEMPLATE_LIST_FROM_PROTO2_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportSet) {
            converterFromName = format(TEMPLATE_SET_FROM_PROTO2_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportArray) {
            converterFromName = format(TEMPLATE_ARRAY_FROM_PROTO2_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportMap) {
            ExportMap em = (ExportMap) ec;
//            if (Util.isExistsInProto2(em.keyType)) {
//                converterFromName = format(TEMPLATE_MAP_FROM_PROTO2_NAME, "keyClassName", toName(em.keyType), "valueClassName", toName(em.name));
//            } else {
            converterFromName = format(TEMPLATE_MAP_FROM_PROTO2_NAME, "keyClassName", toName(em.keyType), "valueClassName", toName(em.name));
//            }
        } else {
            converterFromName = format(TEMPLATE_FROM_PROTO2_NAME, "className", toName(ec.name));
        }
        return converterFromName;
    }

    public String getClassName(String className) {
        int lastIndexOfDot = className.lastIndexOf('.');
        return className.substring(lastIndexOfDot + 1);
    }

    public String toProto2Class(String className) {
        return getProto2Package() + "." + className;
    }

    public String toProto2Class(ExportClass ec) {
        if (Util.isCollection(ec)) {
            String proto2Class = ec.name;
            if (!Util.isExistsInProto2(ec)) {
                proto2Class = toProto2Class(ec.name);
            }
            if (ec instanceof ExportArray) {
                return "java.util.List<" + proto2Class + ">";
            } else if (ec instanceof ExportList) {
                return "java.util.List<" + proto2Class + ">";
            } else if (ec instanceof ExportSet) {
                return "java.util.Collection<" + proto2Class + ">";
            } else if (ec instanceof ExportMap) {
                ExportMap em = (ExportMap) ec;
                String keyClass = em.keyType;
                proto2Class = em.name;
                if (!Util.isExistsInProto2(keyClass)) {
                    keyClass = toProto2Class(keyClass);
                }
                if (!Util.isExistsInProto2(proto2Class)) {
                    proto2Class = toProto2Class(proto2Class);
                }
                return "java.util.Map<" + keyClass + ", " + proto2Class + ">";
            } else if (ec instanceof ExportCollection) {
                return "java.util.Collection<" + proto2Class + ">";
            } else {
                throw new IllegalStateException(ec.toString());
            }
        } else if (Util.isExistsInProto2(ec)) {
            return ec.name;
        } else {
            return toProto2Class(ec.name);
        }
    }

    public static String toOriginalClass(ExportClass ec) {
        if (ec instanceof ExportArray) {
            return ec.name + "[]";
        } else if (ec instanceof ExportList) {
            return "java.util.List<" + ec.name + ">";
        } else if (ec instanceof ExportSet) {
            return "java.util.Set<" + ec.name + ">";
        } else if (ec instanceof ExportMap) {
            return "java.util.Map<" + ((ExportMap) ec).keyType + ", " + ec.name + ">";
        } else if (ec instanceof ExportCollection) {
            return "java.util.Collection<" + ec.name + ">";
        } else {
            return ec.name;
        }
    }

    public String getProto2Package() {
        return factory.getProto2Package();
    }

    public void addConverterMethod(String converterMethod) {
        factory.addConverterMethod(converterMethod);
    }

    public boolean isConverterMethoExists(String converterMethod) {
        return factory.isConverterMethoExists(converterMethod);
    }

    abstract public String write();

}
