package com.unalarabe.urpc.maven.plugin.proto2.writer;

import com.unalarabe.urpc.converter.structure.ExportArray;
import com.unalarabe.urpc.maven.plugin.Util;

import static com.unalarabe.urpc.maven.plugin.Util.format;
import static com.unalarabe.urpc.maven.plugin.proto2.writer.SimpleWriter.toName;
import static com.unalarabe.urpc.maven.plugin.proto2.writer.SimpleWriter.writeConverterFromName;
import static com.unalarabe.urpc.maven.plugin.proto2.writer.SimpleWriter.writeConverterToName;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ArrayWriter extends ConverterWriter {

    private static final String TEMPLATE_COPY_ARRAY_TO
        = "        for ({originalItemClass} o : original){\n"
        + "            th.add({converterToName}(o));\n"
        + "        }\n";
    private static final String TEMPLATE_COPY_ARRAY_FROM
        = "        for (int i = 0; i < th.size(); i++) {\n"
        + "            original[i] = {converterFromName}(th.get(i));\n"
        + "        }\n";
    private static final String TEMPLATE_COPY_ARRAY_SIMPLE_TO
        = "        for ({originalItemClass} o : original){\n"
        + "            th.add({prefix}o{postfix});\n"
        + "        }\n";
    private static final String TEMPLATE_COPY_ARRAY_SIMPLE_FROM
        = "        for (int i = 0; i < th.size(); i++) {\n"
        + "            original[i] = {prefix}th.get(i){postfix};\n"
        + "        }\n";

    private static final String TEMPLATE_ARRAY_TO_TRIFT
        = "    public static java.util.List<{proto2ItemClass}> {converterToName}({originalClass} original) {\n"
        + "        if (original == null) {\n"
        + "            return null;\n"
        + "        }\n"
        + "        {proto2Class} th = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createCollection({collectionClass}.class, {proto2ItemClass}.class);\n"
        + "{bodyTo}"
        + "        return th;\n"
        + "    }\n";
    private static final String TEMPLATE_ARRAY_FROM_TRIFT
        = "    public static {originalClass} {converterFromName}({proto2Class} th) {\n"
        + "        if (th == null) {\n"
        + "            return null;\n"
        + "        }\n"
        + "        {originalClass} original = new {originalBaseClass}[th.size()];\n"
        + "{bodyFrom}"
        + "        return original;\n"
        + "    }\n";

    private final ExportArray exportArray;

    public ArrayWriter(ExportArray exportArray, WriterFactory processed) {
        super(processed);
        this.exportArray = exportArray;
    }

    @Override
    public String write() {
        String collectionClass = "java.util.List";
        String prefixTo = "";
        String prefixFrom = "";
        String postfixTo = "";
        String postfixFrom = "";
        String proto2ItemClass;
        String originalItemClass = exportArray.name;
        if (Util.isSimple(exportArray)) {
            proto2ItemClass = Util.toComplex(exportArray.name);
        } else if (!Util.isExistsInProto2(exportArray.name)) {
            proto2ItemClass = toProto2Class(exportArray.name);
//        } else if (Util.isByte(exportArray.name)) {
//            proto2ItemClass = "java.lang.Integer";
//            if (Util.isSimple(exportArray)) {
//                prefixTo = "(int)";
//            } else {
//                postfixTo = ".intValue()";
//            }
//            postfixFrom = ".byteValue()";
//        } else if (Util.isChar(exportArray.name)) {
//            proto2ItemClass = "java.lang.Integer";
//            prefixTo = "(int)";
//            prefixFrom = "(char)";
//            postfixFrom = ".intValue()";
        } else {
            proto2ItemClass = exportArray.name;
        }
        String proto2Class = collectionClass + "<" + proto2ItemClass + ">";
        String templateCopyTo;
        String templateCopyFrom;
        String converterItemToName = format(TEMPLATE_TO_PROTO2_NAME, "className", toName(exportArray.name));
        String converterItemFromName = format(TEMPLATE_FROM_PROTO2_NAME, "className", toName(exportArray.name));
        if (Util.isExistsInProto2(exportArray.name)) {
            templateCopyTo = TEMPLATE_COPY_ARRAY_SIMPLE_TO;
            templateCopyFrom = TEMPLATE_COPY_ARRAY_SIMPLE_FROM;
        } else {
            templateCopyTo = TEMPLATE_COPY_ARRAY_TO;
            templateCopyFrom = TEMPLATE_COPY_ARRAY_FROM;
        }
        String result = "";
        String converterToName = writeConverterToName(exportArray);
        String converterFromName = writeConverterFromName(exportArray);
        if (!isConverterMethoExists(converterToName)) {
            addConverterMethod(converterToName);
            String bodyTo = format(templateCopyTo, "converterToName", converterItemToName, "originalItemClass", originalItemClass, "prefix", prefixTo, "postfix", postfixTo);
            result += Util.format(TEMPLATE_ARRAY_TO_TRIFT,
                "className", toName(exportArray.name),
                "proto2Class", proto2Class,
                "originalBaseClass", exportArray.name,
                "proto2ItemClass", proto2ItemClass,
                "originalClass", toOriginalClass(exportArray),
                "converterToName", converterToName,
                "converterFromName", converterFromName,
                "collectionClass", collectionClass,
                "bodyTo", bodyTo,
                "bodyFrom", "");
        }

        if (!isConverterMethoExists(converterFromName)) {
            addConverterMethod(converterFromName);
            if (!result.isEmpty()) {
                result += "\n";
            }
            String bodyFrom = format(templateCopyFrom, "converterFromName", converterItemFromName, "proto2ItemClass", proto2ItemClass, "prefix", prefixFrom, "postfix", postfixFrom);
            result += Util.format(TEMPLATE_ARRAY_FROM_TRIFT,
                "className", toName(exportArray.name),
                "proto2Class", proto2Class,
                "originalBaseClass", exportArray.name,
                "proto2ItemClass", proto2ItemClass,
                "originalClass", toOriginalClass(exportArray),
                "converterToName", converterToName,
                "converterFromName", converterFromName,
                "collectionClass", collectionClass,
                "bodyTo", "",
                "bodyFrom", bodyFrom);
        }
        return result;
    }

}
