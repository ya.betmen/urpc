package com.unalarabe.urpc.maven.plugin.proto2;

import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.exception.PluginException;
import com.unalarabe.urpc.meta.Generator;
import com.squareup.protoparser.ProtoFile;
import com.squareup.protoparser.ProtoParser;
import com.unalarabe.urpc.meta.MetaService;

import static com.unalarabe.urpc.maven.plugin.Util.path;
import static com.unalarabe.urpc.maven.plugin.Util.SEPARATOR;

import com.unalarabe.urpc.maven.plugin.proto2.writer.ConvertersWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import com.unalarabe.urpc.converter.ServiceConverter;
import com.unalarabe.urpc.converter.ConverterException;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.meta.MetaClass;
import com.unalarabe.urpc.maven.plugin.proto2.writer.ClientClassWriter;
import com.unalarabe.urpc.maven.plugin.proto2.writer.ClientWriter;
import com.unalarabe.urpc.maven.plugin.proto2.writer.Proto2Writer;
import com.unalarabe.urpc.maven.plugin.proto2.writer.ProxyWriter;
import com.unalarabe.urpc.maven.plugin.proto2.writer.JmsServiceWriter;
import com.unalarabe.urpc.maven.plugin.proto2.writer.KafkaServiceWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FilenameUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.VersionRange;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.StringUtils;

/**
 * @phase generate-sources
 * @goal compile-proto2
 * @requiresDependencyResolution compile
 * @author Petr Zalyautdinov (Заляутдинов Пётр) <zalyautdinov.petr@gmail.com>
 */
public class ProtobufGeneratorMojo extends AbstractMojo {

    /**
     * @parameter default-value="${basedir}/src/main/java"
     */
    protected File basedir;
    /**
     * @parameter default-value="${basedir}/src/main/java"
     */
    protected File servicedir;
    /**
     * @parameter default-value="${basedir}/src/main"
     */
    protected File protoBaseDir;
    /**
     * @parameter default-value="services"
     */
//    protected String protobufFileName;
    /**
     * @parameter default-value={"**\/*.proto"}
     */
    protected String[] protobufFiles;
    /**
     * @parameter default-value="proto2"
     */
    protected String protobufNamespace;
    /**
     * @parameter default-value={"**\/*"}
     */
    protected String[] includes;
    /**
     * @parameter default-value={}
     */
    protected String[] excludes;
    /**
     * @parameter default-value={}
     */
    protected String[] extraLangs;
    /**
     * @parameter default-value={}
     */
    protected String[] protobufExtraPaths;
    /**
     * @parameter default-value="/usr/bin/protoc"
     */
    protected String protobufPath;
    /**
     * The output directory for bundles.
     *
     * @parameter default-value="${project.build.directory}/generated-sources"
     */
    protected File outputDirectory;
    /**
     * The Maven project.
     *
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    protected MavenProject project;
    /**
     * Used to look up Artifacts in the remote repository.
     *
     * @component
     */
    protected ArtifactFactory factory;
    /**
     * Used to look up Artifacts in the remote repository.
     *
     * @component
     */
    protected ArtifactResolver resolver;
    /**
     * Location of the local repository.
     *
     * @parameter expression="${localRepository}"
     * @readonly
     * @required
     */
    protected ArtifactRepository local;
    /**
     * List of Remote Repositories used by the resolver.
     *
     * @parameter expression="${project.remoteArtifactRepositories}"
     * @readonly
     * @required
     */
    protected List<?> remoteRepos;

//    private static final String ANNOTATION_PROTO2_SERVICE_CLASS = Proto2Service.class.getCanonicalName();
//    private static final String ANNOTATION_PROTO2_METHOD = Proto2Method.class.getCanonicalName();
//    private static final String ANNOTATION_PROTO2_OPTIONAL = Optional.class.getCanonicalName();
    private ClassLoader classLoader = null;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            File generatedSourcesDir = outputDirectory;
            String generatedSourcesDirName = generatedSourcesDir.getAbsolutePath();
            if (generatedSourcesDir.exists()) {
                generatedSourcesDir.delete();
            }
            generatedSourcesDir.mkdirs();

            String[] files = Util.scanDirectory(servicedir, includes, excludes);
            ConvertersWriter sw = new ConvertersWriter(protobufNamespace, "com.unalarabe.urpc.plugin.util.proto2");
            Generator generator = new Generator(protobufNamespace);
            List<ExportService> services = new ArrayList<>();
            Set<ExportClass> classesForConvert = new HashSet<>();
            File servicesDir = createDir(path(generatedSourcesDirName, "java-service"));
            File proto2Dir = createDir(path(generatedSourcesDirName, "proto2"));
            for (String file : files) {
                String clazz = file.replaceAll(SEPARATOR, ".").replaceAll(".java", "");
                ServiceConverter cc = new ServiceConverter(getClassLoader(), servicedir, basedir);
                ExportService service = cc.export(clazz);
                if (service != null) {
                    classesForConvert.addAll(Util.extractClasses(service));
                    ExportService rpcService = generator.generateRpcService(service);
                    Proto2Writer pw = new Proto2Writer(protobufNamespace, path(generatedSourcesDirName, "proto2"));
                    File proto2File = pw.writeService(rpcService);
                    Set<File> messagesProtoFiles = pw.getMessagesProtoFiles();
                    String result = "";
                    try {
                        List<String> cmd = Util.toList(protobufPath,
                                                       "-I", proto2Dir.getAbsolutePath(),
                                                       "--java_out", servicesDir.getAbsolutePath(), proto2File.getAbsolutePath());
                        messagesProtoFiles.forEach((messagesProtoFile) -> cmd.add(messagesProtoFile.getAbsolutePath()));
                        getLog().info(String.join(" ", cmd));
                        result = Util.executeCommand(proto2File.getParentFile(), cmd.toArray(new String[cmd.size()]));
                    } catch (Exception ex) {
                        getLog().error(ex.getMessage());
                        throw new IllegalStateException(ex);
                    }
                    getLog().info(result);

                    ProxyWriter proxyWriter = new ProxyWriter(protobufNamespace, service);
                    writeFile(path(generatedSourcesDirName, "java-service", (protobufNamespace + "." + Util.toNamespace(service.name)).replaceAll("\\.", SEPARATOR), proxyWriter.proxyClassName() + ".java"), proxyWriter.write());

                    JmsServiceWriter jmsServiceWriter = new JmsServiceWriter(protobufNamespace, rpcService);
                    writeFile(path(generatedSourcesDirName, "java-service", (protobufNamespace + "." + Util.toNamespace(service.name)).replaceAll("\\.", SEPARATOR), jmsServiceWriter.getJmsServiceName() + ".java"), jmsServiceWriter.write());

                    KafkaServiceWriter kafkaServiceWriter = new KafkaServiceWriter(protobufNamespace, rpcService);
                    writeFile(path(generatedSourcesDirName, "java-service", (protobufNamespace + "." + Util.toNamespace(service.name)).replaceAll("\\.", SEPARATOR), kafkaServiceWriter.getJmsServiceName() + ".java"), kafkaServiceWriter.write());

                    services.add(rpcService);
                }
            }
            File converterUtils = writeFile(path(generatedSourcesDirName, "java-service", "com", "unalarabe", "urpc", "plugin", "util", "proto2", "Proto2ConverterUtils.java"), sw.writeServiceConverters("Proto2ConverterUtils", classesForConvert));
            File objectFactory = writeFile(path(generatedSourcesDirName, "java-service", "com", "unalarabe", "urpc", "plugin", "util", "proto2", "ObjectFactoryImpl.java"), sw.writeObjectFactoryImpl());

            String[] protoFiles = Util.scanDirectory(protoBaseDir, protobufFiles, new String[0]);
            for (String protoFile : protoFiles) {
                if (protoFile.endsWith(".proto")) {
                    createDir(path(generatedSourcesDirName, "java-client"));
                    protoFile = protoBaseDir.getAbsolutePath() + SEPARATOR + protoFile;
                    String result = "";
                    try {
                        File f = new File(protoFile);
                        List<String> protoIncludes = loadIncludes(f);
                        if (protobufExtraPaths != null) {
                            protoIncludes.addAll(Arrays.asList(protobufExtraPaths));
                        }
                        List<File> protoFileIncludes = convertNamesToFiles(protoIncludes);
                        ProtoFileParser parsed = new ProtoFileParser(f, protoFileIncludes);

                        List<MetaService> protoServices = parsed.getServices();
                        List<MetaClass> protoClasses = parsed.getClasses();
                        String javaPackage = parsed.getJavaPackage();
                        boolean muliplyFiles = parsed.isMuliplyFiles();
                        List<String> cmd = Util.toList(protobufPath, "--java_out", path(generatedSourcesDirName, "java-client"));

                        String relativePath = parsed.getJavaPackage().replaceAll("\\.", SEPARATOR) + SEPARATOR + f.getName();
                        String absolutePath = f.getAbsolutePath();
                        if (absolutePath.endsWith(relativePath)) {
                            cmd.add("-I");
                            cmd.add(absolutePath.substring(0, absolutePath.length() - relativePath.length()));
                        } else {
                            cmd.add("-I");
                            cmd.add(f.getParentFile().getAbsolutePath());
                        }

                        for (File protoFileInclude : protoFileIncludes) {
                            cmd.add("-I");
                            cmd.add(protoFileInclude.getAbsolutePath());
                        }

                        cmd.add(f.getAbsolutePath());
                        getLog().info(String.join(" ", cmd));
                        result = Util.executeCommand(f.getParentFile(), cmd.toArray(new String[cmd.size()]));
                        Map<String, ClientWriter> writesMap = new HashMap<>();
                        ClientWriter lastCw = null;
                        for (MetaService protoService : protoServices) {
                            ClientWriter cw = new ClientWriter(javaPackage, protoService, protoClasses, !muliplyFiles);
                            writeFile(path(generatedSourcesDirName, "java-client", javaPackage.replaceAll("\\.", SEPARATOR), cw.getClassName() + ".java"), cw.writeService());
                            writeFile(path(generatedSourcesDirName, "java-client", javaPackage.replaceAll("\\.", SEPARATOR), cw.getProviderClassName() + ".java"), cw.writeServiceProvider());
                            for (String usedClass : cw.getUsedClasses()) {
                                writesMap.put(usedClass, cw);
                            }
                        }
                        for (MetaClass protoClass : protoClasses) {
                            ClientWriter clientWriter = writesMap.get(protoClass.getFullName());
                            ClientClassWriter ccw = new ClientClassWriter(protoClass, clientWriter != null ? clientWriter : lastCw);
                            writeFile(path(generatedSourcesDirName, "java-client", ccw.getPackage().replaceAll("\\.", SEPARATOR), ccw.getName() + ".java"), ccw.writeClass());
                            lastCw = clientWriter;
                        }
                    } catch (Exception ex) {
                        getLog().error(ex.getMessage());
                        throw new IllegalStateException(ex);
                    }
                    getLog().info(result);
                }
            }
        } catch (PluginException | ConverterException ex) {
            getLog().error(ex.getMessage(), ex);
        }
    }

    private List<File> convertNamesToFiles(List<String> fileNames) {
        List<File> result = new ArrayList<>();
        for (String fileName : fileNames) {
            result.add(new File(fileName));
        }
        return result;
    }

    private List<String> loadIncludes(File f) throws PluginException {
        List<String> result = new ArrayList<>();
        File propertiesFile = findPropertiesFile(f);
        Properties properties = getProperties(propertiesFile);
        if (properties != null) {
            String extrapaths = properties.getProperty("extrapaths");
            if (extrapaths != null) {
                String[] parts = extrapaths.split(" ");
                for (String path : parts) {
                    File extraProtoFile = resolvePath(f.getParentFile(), path);
                    if (extraProtoFile == null) {
                        throw new PluginException("Can't find " + path + " listed in " + propertiesFile.getAbsolutePath());
                    }
                    result.add(extraProtoFile.getAbsolutePath());
                }
            }
        }
        return result;
    }

    private Set<String> findProtoDependencies(File f) {
        Set<String> allDependencies = new HashSet<>();
        try {
            ProtoFile parsed = ProtoParser.parseUtf8(f);
            String absolutePath = f.getAbsolutePath();
            String protoPath = parsed.packageName().replaceAll("\\.", SEPARATOR) + SEPARATOR + f.getName();
            if (!absolutePath.endsWith(protoPath)) {
                return allDependencies;
            }
            String basePath = absolutePath.substring(0, absolutePath.length() - protoPath.length());

            Set<String> dependencies = new HashSet<>();
            dependencies.addAll(parsed.dependencies());
            dependencies.addAll(parsed.publicDependencies());
            dependencies.removeAll(dependencies);

        } catch (IOException ex) {
            getLog().error(ex);
        }
        return null;
    }

    private File resolvePath(File baseDir, String path) throws PluginException {
        try {
            return new File(baseDir, path).getCanonicalFile();
        } catch (IOException ex) {
            throw new PluginException(ex);
        }
    }

    private File findPropertiesFile(File protoFile) throws PluginException {
        try {
            String baseName = FilenameUtils.getBaseName(protoFile.getName());
            File propertiesFile = new File(path(protoFile.getParentFile().getAbsolutePath(), baseName + ".properties"));
            if (propertiesFile.exists()) {
                return propertiesFile;
            }
            return null;
        } catch (Exception ex) {
            throw new PluginException(ex);
        }
    }

    private Properties getProperties(File propertiesFile) throws PluginException {
        try {
            Properties properties = new Properties();
            if (propertiesFile != null) {
                properties.load(new FileInputStream(propertiesFile));
            }
            return properties;
        } catch (Exception ex) {
            throw new PluginException(ex);
        }
    }

    private File writeFile(String path, String content) {
        try {
            File file = new File(path);
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.append(content);
            fileWriter.flush();
            return file;
        } catch (IOException ex) {
            getLog().error(ex);
        }
        return null;
    }

    private File createDir(String path) {
        File file = new File(path);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
        return file;
    }

    protected ClassLoader getClassLoader() {
        if (classLoader == null) {
            try {
                List<Dependency> dependencies = project.getDependencies();
                List<URL> url = new ArrayList<>();
                url.add(outputDirectory.toURI().toURL());
                for (Dependency d : dependencies) {
                    String scope = d.getScope();
                    if (!Artifact.SCOPE_TEST.equals(scope)) {
                        Artifact artifact = getArtifact(d.getGroupId(), d.getArtifactId(), d.getVersion(), d.getType(), d.getClassifier());
                        url.add(artifact.getFile().toURI().toURL());
                    }
                }

                URL[] classpath = url.toArray(new URL[url.size()]);

                URLClassLoader uc = new URLClassLoader(classpath, this.getClass().getClassLoader()) {
                    @Override
                    public Class<?> loadClass(String name) throws ClassNotFoundException {
                        getLog().debug("Loading Class for compile [" + name + "]");
                        Class<?> c = super.loadClass(name);
                        getLog().debug("Loading Class for compile [" + name + "] found [" + c + "]");
                        return c;
                    }

                    @Override
                    protected Class<?> findClass(String name) throws ClassNotFoundException {
                        getLog().debug("Finding Class for compile [" + name + "]");
                        Class<?> c = super.findClass(name);
                        getLog().debug("Finding Class for compile [" + name + "] found [" + c + "]");
                        return c;
                    }
                };

                return uc;

            } catch (MalformedURLException | MojoExecutionException ex) {
                getLog().error(ex);
            }
        }
        return classLoader;
    }

    protected Artifact getArtifact(String groupId, String artifactId, String version, String type, String classifier) throws MojoExecutionException {
        Artifact artifact;
        VersionRange vr;

        try {
            vr = VersionRange.createFromVersionSpec(version);
        } catch (InvalidVersionSpecificationException e) {
            vr = VersionRange.createFromVersion(version);
        }

        if (StringUtils.isEmpty(classifier)) {
            artifact = factory.createDependencyArtifact(groupId, artifactId, vr, type, null,
                                                        Artifact.SCOPE_COMPILE);
        } else {
            artifact = factory.createDependencyArtifact(groupId, artifactId, vr, type,
                                                        classifier, Artifact.SCOPE_COMPILE);
        }
        try {
            resolver.resolve(artifact, remoteRepos, local);
        } catch (ArtifactResolutionException e) {
            throw new MojoExecutionException("Unable to resolve artifact.", e);
        } catch (ArtifactNotFoundException e) {
            throw new MojoExecutionException("Unable to find artifact.", e);
        }
        return artifact;
    }
}
