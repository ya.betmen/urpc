package com.unalarabe.urpc.maven.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.unalarabe.urpc.converter.ConverterException;
import com.unalarabe.urpc.converter.structure.ExportArray;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportCollection;
import com.unalarabe.urpc.converter.structure.ExportField;
import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.converter.structure.ExportMethod;
import com.unalarabe.urpc.converter.structure.ExportParameter;
import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.converter.structure.ExportSet;

import static com.unalarabe.urpc.converter.util.Util.getSortedIncludeFiles;

import org.codehaus.plexus.util.DirectoryScanner;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class Util {

    public static final String SEPARATOR = System.getProperty("file.separator");

    private static final Logger LOGGER = Logger.getLogger(Util.class.getName());

    public static final Set PROTO2_TYPES;
    public static final Set JAVA_SIMPLE_TYPES;

    static {
        Set set = new HashSet();
        set.add(Double.TYPE.getCanonicalName());
        set.add(Double.class.getCanonicalName());
        set.add(Float.TYPE.getCanonicalName());
        set.add(Float.class.getCanonicalName());
        set.add(Integer.TYPE.getCanonicalName());
        set.add(Integer.class.getCanonicalName());
        set.add(Long.TYPE.getCanonicalName());
        set.add(Long.class.getCanonicalName());
        set.add(Boolean.TYPE.getCanonicalName());
        set.add(Boolean.class.getCanonicalName());
        set.add(String.class.getCanonicalName());

        set.add(Character.TYPE.getCanonicalName());
        set.add(Character.class.getCanonicalName());
        set.add(Byte.TYPE.getCanonicalName());
        set.add(Byte.class.getCanonicalName());

        set.add(Date.class.getCanonicalName());

        PROTO2_TYPES = Collections.unmodifiableSet(set);
    }

    static {
        Set set = new HashSet();
        set.add(Byte.TYPE.getCanonicalName());
        set.add(Short.TYPE.getCanonicalName());
        set.add(Integer.TYPE.getCanonicalName());
        set.add(Long.TYPE.getCanonicalName());
        set.add(Float.TYPE.getCanonicalName());
        set.add(Double.TYPE.getCanonicalName());
        set.add(Character.TYPE.getCanonicalName());
        set.add(Boolean.TYPE.getCanonicalName());
        JAVA_SIMPLE_TYPES = Collections.unmodifiableSet(set);
    }

    public static Set<ExportClass> extractClasses(ExportService service) {
        HashSet<ExportClass> result = new HashSet<>();
        Set<String> methodNames = service.getMethodNames();
        for (String methodName : methodNames) {
            List<ExportMethod> methods = service.getMethods(methodName);
            for (ExportMethod method : methods) {
                result.add(method.type);
                for (ExportParameter parameter : method.parameters) {
                    result.add(parameter.type);
                }
            }
        }
        result.addAll(service.getExtraClasses());
        return result;
    }

    public static Set<ExportClass> extractClasses(List<ExportService> services) {
        HashSet<ExportClass> result = new HashSet<>();
        for (ExportService service : services) {
            result.addAll(extractClasses(service));
        }
        return result;
    }

    public static String[] scanDirectory(File directory, String[] includes, String[] excludes) throws ConverterException {
        DirectoryScanner ds = null;
        if (directory != null && directory.exists() && directory.isDirectory()) {
            ds = new DirectoryScanner();
            ds.setIncludes(includes);
            ds.setExcludes(excludes);
            ds.setBasedir(directory);
            ds.setCaseSensitive(true);
            ds.scan();
        }
        if (ds != null) {
            return getSortedIncludeFiles(ds);
        } else {
            return new String[0];
        }
    }

    public static boolean isSimple(ExportClass ec) {
        return JAVA_SIMPLE_TYPES.contains(ec.name);
    }

    public static boolean isSimple(String className) {
        return JAVA_SIMPLE_TYPES.contains(className);
    }

    public static boolean isCollection(ExportClass ec) {
        return ec instanceof ExportArray
               || ec instanceof ExportCollection
               || ec instanceof ExportList
               || ec instanceof ExportMap
               || ec instanceof ExportSet;
    }

    public static boolean isExistsInProto2(ExportClass ec) {
        return PROTO2_TYPES.contains(ec.name);
    }

    public static boolean isExistsInProto2(String className) {
        return PROTO2_TYPES.contains(className);
    }

    public static boolean isExistsInAvro(ExportClass ec) {
        return PROTO2_TYPES.contains(ec.name);
    }

    public static boolean isExistsInAvro(String className) {
        return PROTO2_TYPES.contains(className);
    }

    public static String format(String template, Object... values) {
        if (values.length % 2 != 0) {
            throw new IllegalArgumentException();
        }
        String result = template;
        for (int i = 0; i < values.length; i += 2) {
            String key = String.valueOf(values[i]);
            String value = String.valueOf(values[i + 1]);
            result = result.replaceAll("\\{" + key + "\\}", value);
        }
        return result;
    }

    public static String readStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        while ((line = reader.readLine()) != null) {
            output.append(line).append("\n");
        }
        return output.toString();
    }

    public static String executeCommand(File workingDir, String... command) throws Exception {
        try {
            Process p;
            p = Runtime.getRuntime().exec(command, null, workingDir);
            p.waitFor();
            if (p.exitValue() == 0) {
                return readStream(p.getInputStream());
            } else {
                throw new Exception(readStream(p.getErrorStream()));
            }
        } catch (InterruptedException | IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            throw new Exception(ex);
        }
    }

    public static String toComplex(String simpleType) {
        switch (simpleType) {
            case "byte":
                return Byte.class.getCanonicalName();
            case "short":
                return Short.class.getCanonicalName();
            case "int":
                return Integer.class.getCanonicalName();
            case "long":
                return Long.class.getCanonicalName();
            case "float":
                return Float.class.getCanonicalName();
            case "double":
                return Double.class.getCanonicalName();
            case "boolean":
                return Boolean.class.getCanonicalName();
            case "char":
                return Character.class.getCanonicalName();
            default:
                throw new IllegalArgumentException(simpleType);
        }
    }

    public static String getterFrom(ExportField field) {
        String prefix;
        String suffix = "";
        prefix = "get";
        if (Util.isCollection(field.type)) {
            if (field.type instanceof ExportMap) {
                suffix = "Map";
            } else {
                suffix = "List";
            }
        }
        return prefix + field.name.substring(0, 1).toUpperCase() + field.name.substring(1) + suffix;
    }

    public static String getterTo(ExportField field) {
        String prefix;
        if ("boolean".equals(field.type.name) && !Util.isCollection(field.type)) {
            prefix = "is";
        } else {
            prefix = "get";
        }
        return prefix + field.name.substring(0, 1).toUpperCase() + field.name.substring(1);
    }

    public static String setterFrom(ExportField field) {
        return "set" + field.name.substring(0, 1).toUpperCase() + field.name.substring(1);
    }

    public static String protoSetterTo(ExportField field) {
        if (Util.isCollection(field.type)) {
            if (field.type instanceof ExportMap) {
                return "putAll" + field.name.substring(0, 1).toUpperCase() + field.name.substring(1);
            } else {
                return "addAll" + field.name.substring(0, 1).toUpperCase() + field.name.substring(1);
            }
        } else {
            return "set" + field.name.substring(0, 1).toUpperCase() + field.name.substring(1);
        }
    }

    public static String setterTo(ExportField field) {
        return "set" + field.name.substring(0, 1).toUpperCase() + field.name.substring(1);
    }

    public static boolean isChar(String type) {
        return Character.TYPE.getCanonicalName().equals(type) || Character.class.getCanonicalName().equals(type);
    }

    public static boolean isString(String type) {
        return String.class.getCanonicalName().equals(type);
    }

    public static String path(String... path) {
        return String.join(SEPARATOR, path);
    }

    public static boolean isByte(String type) {
        return Byte.TYPE.getCanonicalName().equals(type) || Byte.class.getCanonicalName().equals(type);
    }

    public static boolean isDate(String type) {
        return Date.class.getCanonicalName().equals(type);
    }

    public static String firstLetterToUpperCase(String text) {
        return text != null && !text.isEmpty() ? (text.substring(0, 1).toUpperCase() + text.substring(1)) : text;
    }

    public static String getClassName(String name) {
        return name != null && name.contains(".") ? (name.substring(name.lastIndexOf(".") + 1)) : name;
    }

    public static <T> List<T> toList(T... items) {
        List<T> result = new ArrayList<>();
        for (T item : items) {
            result.add(item);
        }
        return result;
    }

    public static String toNamespace(String type) {
        int lastIndexOfDot = type.lastIndexOf(".");
        return lastIndexOfDot >= 0 ? type.substring(0, lastIndexOfDot) : type;
    }

}
