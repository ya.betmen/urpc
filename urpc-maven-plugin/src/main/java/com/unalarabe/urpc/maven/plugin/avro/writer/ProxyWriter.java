package com.unalarabe.urpc.maven.plugin.avro.writer;

import java.util.HashSet;
import java.util.Set;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportField;
import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.converter.structure.ExportMethod;
import com.unalarabe.urpc.converter.structure.ExportParameter;
import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.converter.structure.ExportSet;
import com.unalarabe.urpc.exception.ConvertException;
import com.unalarabe.urpc.exception.MethodCallException;
import com.unalarabe.urpc.service.ServiceProxy;
import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.service.RpcMessage;
import com.unalarabe.urpc.util.RpcParser;
import com.unalarabe.urpc.util.ServiceUtil;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ProxyWriter {

    private static final String TEMPLATE
        = "package {javaNamespace};\n"
          + "\n"
          + "import " + IOException.class.getCanonicalName() + ";\n"
          + "import " + ConvertException.class.getCanonicalName() + ";\n"
          + "import " + MethodCallException.class.getCanonicalName() + ";\n"
          + "import " + ServiceProxy.class.getCanonicalName() + ";\n"
          + "import " + RpcMessage.class.getCanonicalName() + ";\n"
          + "import " + ServiceUtil.class.getCanonicalName() + ";\n"
          + "{imports}\n"
          + "import com.unalarabe.urpc.plugin.util.avro.AvroConverterUtils;\n"
          + "\n"
          + "public class {serviceProxyName} extends ServiceProxy<{fullServiceName}> implements {serviceName} {\n"
          + "\n"
          + "    public {serviceProxyName}({fullServiceName} service) {\n"
          + "        super(service, {fullServiceName}.class);\n"
          + "    }\n"
          + ""
          + "\n"
          + "{methods}\n"
          + "\n"
          + "    @Override\n"
          + "    protected Object callMethod(String method, Object request) throws " + MethodCallException.class.getSimpleName() + " {\n"
          + "        switch (method) {\n"
          + "{castCases}"
          + "            default: {\n"
          + "                throw new IllegalStateException(\"Unknown method: \" + method);\n"
          + "            }\n"
          + "        }\n"
          + "    }\n"
          + ""
          + "\n"
          + "    @Override\n"
          + "    protected byte[] callMethod(String method, byte[] data) throws ConvertException, MethodCallException {\n"
          + "        try {\n"
          + "            switch (method) {\n"
          + "{cases}"
          + "                default: {\n"
          + "                    throw new IllegalStateException(\"Unknown method: \" + method);\n"
          + "                }\n"
          + "            }\n"
          + "        } catch (IOException ex) {\n"
          + "            throw new ConvertException(ex);\n"
          + "        }\n"
          + "    }\n"
          + "\n"
          + "}\n";
    private static final String TEMPLATE_CASE
        = "                case \"{methodName}\": {\n"
          + "                    return {methodName}({requestClass}.getDecoder().decode(data)).toByteBuffer().array();\n"
          + "                }\n";
    private static final String TEMPLATE_CAST_CASE
        = "           case \"{methodName}\": {\n"
          + "               return {methodName}(({requestClass})request);\n"
          + "           }\n";
    private static final String TEMPLATE_METHOD
        = "    @Override\n"
          + "    public {responseClass} {methodName}({requestClass} request) {\n"
          + "        try {\n"
          + "{parameters}"
          + "\n"
          + "            {methodResultClass} result{initResult};\n"
          + "            result = getService().{methodName}({invokeParameters});\n"
          + "            {responseClass}.Builder builder = {responseClass}.newBuilder();\n"
          + "            {nullCheckResult}{\n"
          + "                builder.{setterTo}({prefix}result{suffix});\n"
          + "            }\n"
          + "            return builder.build();\n"
          + "        } catch (Throwable th) {\n"
          + "            throw new MethodCallException(th);\n"
          + "        }\n"
          + "    }\n";

    private static final String TEMPLATE_OVERLOADED_METHOD
        = "    @Override\n"
          + "    public {responseClass} {methodName}({requestClass} request) {\n"
          + "        try {\n"
          + "            {responseClass}.Builder builder = {responseClass}.newBuilder();\n"
          + "{cases}"
          + "            return builder.build();\n"
          + "        } catch (Throwable th) {\n"
          + "            throw new MethodCallException(th);\n"
          + "        }\n"
          + "    }\n";

    private static final String TEMPLATE_METHOD_VOID
        = "    @Override\n"
          + "    public {responseClass} {methodName}({requestClass} request) {\n"
          + "        try {\n"
          + "{parameters}"
          + "\n"
          + "            getService().{methodName}({invokeParameters});\n"
          + "            {responseClass}.Builder builder = {responseClass}.newBuilder();\n"
          + "            return builder.build();\n"
          + "        } catch (Throwable th) {\n"
          + "            throw new MethodCallException(th);\n"
          + "        }\n"
          + "    }\n";

    private final String javaNamespace;
    private final ExportService exportService;

    public ProxyWriter(String javaNamespace, ExportService exportService) {
        this.javaNamespace = javaNamespace;
        this.exportService = exportService;
    }

    public String proxyClassName() {
        return Util.getClassName(exportService.name) + "Proxy";
    }

    public String write() {
        StringBuilder methods = new StringBuilder();
        StringBuilder cases = new StringBuilder();
        StringBuilder castCases = new StringBuilder();
        StringBuilder ifelseCases = new StringBuilder();
        Set<String> imports = new HashSet<>();

        Set<String> methodNames = exportService.getMethodNames();
        for (Iterator<String> iterator = methodNames.iterator(); iterator.hasNext();) {
            String methodName = iterator.next();
            List<ExportMethod> exportMethods = exportService.getMethods(methodName);
            for (Iterator<ExportMethod> methodIterator = exportMethods.iterator(); methodIterator.hasNext();) {
                ExportMethod exportMethod = methodIterator.next();
                if (exportMethod.ignoredGenerators.contains("avro") || exportMethod.ignoredGenerators.contains("all")) {
                    methodIterator.remove();
                }
            }
            if (exportMethods.isEmpty()) {
                iterator.remove();
            }
        }

        for (String methodName : methodNames) {
            if (methods.length() > 0) {
                methods.append("\n");
            }
            List<ExportMethod> exportMethods = exportService.getMethods(methodName);
            if (exportMethods.isEmpty()) {
                throw new IllegalStateException("Shouldn't be here");
            } else if (exportMethods.size() == 1) {
                ExportMethod method = exportMethods.get(0);
                String ClassNameSuffix = Util.firstLetterToUpperCase(method.name);
                StringBuilder invokeParameters = new StringBuilder();
                StringBuilder parameters = new StringBuilder();
                for (ExportParameter parameter : method.parameters) {
                    if (parameters.length() != 0) {
                        parameters.append("\n");
                        invokeParameters.append(", ");
                    }
                    String suffix = "";// Util.isCollection(parameter.type) ? "List" : "";
                    parameters.append("            ").append(ConverterWriter.toOriginalProto2Class(parameter.type)).append(" ").append(parameter.name).append(" = ");
                    boolean checkFieldInit = !parameter.required && !Util.isCollection(parameter.type);
                    if (checkFieldInit) {
                        parameters.append("request.get").append(Util.firstLetterToUpperCase(parameter.name)).append(suffix).append("() == null ? null : ");
                    }
                    if (Util.isExistsInAvro(parameter.type) && !(parameter.type instanceof ExportSet) && !(parameter.type instanceof ExportList)) {
                        parameters.append("request.get").append(Util.firstLetterToUpperCase(parameter.name)).append(suffix).append("();");
//                    } else if (Util.isString(parameter.type.name) && (parameter.type instanceof ExportSet)) {
//                        parameters.append("AvroConverterUtils.convertCollectionOfCharSequenceToSetOfStrings(request.get")
//                            .append(Util.firstLetterToUpperCase(parameter.name)).append(suffix).append("()").append(");");
//                    } else if (Util.isString(parameter.type.name) && (parameter.type instanceof ExportSet)) {
//                        parameters.append("AvroConverterUtils.convertCollectionOfCharSequenceToListOfStrings(request.get")
//                            .append(Util.firstLetterToUpperCase(parameter.name)).append(suffix).append("()").append(");");
                    } else if ((parameter.type instanceof ExportMap) && Util.isString(((ExportMap) parameter.type).keyType)) {
                        parameters.append("AvroConverterUtils.").append(ConverterWriter.writeConverterToName(parameter.type))
                            .append("(request.get").append(Util.firstLetterToUpperCase(parameter.name)).append(suffix).append("()").append(");");
                    } else {
                        parameters.append("AvroConverterUtils.").append(ConverterWriter.writeConverterFromName(parameter.type))
                            .append("(request.get").append(Util.firstLetterToUpperCase(parameter.name)).append(suffix).append("()").append(");");
                    }
                    invokeParameters.append(parameter.name);
                    if (!Util.isExistsInAvro(parameter.type)) {
                        imports.add("import " + parameter.type.name + ";");
                    }
                }

                ExportField exportField = new ExportField();
                exportField.name = "result";
                exportField.type = method.type;
                String setterTo = Util.setterTo(exportField);

                String prefix = "";
                String suffix = "";
                if (!Util.isExistsInAvro(method.type)) {
                    prefix = Util.format("AvroConverterUtils.{converterTo}(", "converterTo", ConverterWriter.writeConverterToName(method.type));
                    suffix = ")";
                }
//                if (Util.isString(method.type.name)) {
//                    if (method.type instanceof ExportSet) {
//                        prefix = Util.format("AvroConverterUtils.convertCollectionOfStringsToListOfCharSequence(");
//                        suffix = ")";
//                    }
//                    if (method.type instanceof ExportList) {
//                        prefix = Util.format("AvroConverterUtils.convertCollectionOfStringsToListOfCharSequence(");
//                        suffix = ")";
//                    }
//                }
                if (method.type instanceof ExportSet) {
                    prefix = "AvroConverterUtils." + ConverterWriter.writeConverterToName(method.type) + "(";
                    suffix = ")";
                }
                if (method.type instanceof ExportList) {
                    prefix = "AvroConverterUtils." + ConverterWriter.writeConverterToName(method.type) + "(";
                    suffix = ")";
                }
                if ((method.type instanceof ExportMap) && Util.isString(((ExportMap) method.type).keyType)) {
                    prefix = "AvroConverterUtils." + ConverterWriter.writeConverterToName(method.type) + "(";
                    suffix = ")";
                }

                cases.append(Util.format(TEMPLATE_CASE,
                                         "methodName", method.name,
                                         "requestClass", "Request" + ClassNameSuffix,
                                         "responseClass", "Response" + ClassNameSuffix
                ));
                castCases.append(Util.format(TEMPLATE_CAST_CASE,
                                             "methodName", method.name,
                                             "requestClass", "Request" + ClassNameSuffix,
                                             "responseClass", "Response" + ClassNameSuffix
                ));
                if (ifelseCases.length() > 0) {
                    ifelseCases.append(" else ");
                } else {
                    ifelseCases.append("                ");
                }
                ifelseCases.append("if (rpcRequest.get").append(Util.firstLetterToUpperCase(methodName)).append("() != null) {\n")
                    .append("                    method = \"").append(methodName).append("\";\n")
                    .append("                    request = rpcRequest.get").append(Util.firstLetterToUpperCase(methodName)).append("();\n                }");
                methods.append(Util.format("void".equals(method.type.name) ? TEMPLATE_METHOD_VOID : TEMPLATE_METHOD,
                                           "methodName", method.name,
                                           "requestClass", "Request" + ClassNameSuffix,
                                           "responseClass", "Response" + ClassNameSuffix,
                                           "invokeParameters", invokeParameters,
                                           "methodResultClass", ConverterWriter.toOriginalProto2Class(method.type),
                                           "nullCheckResult", Util.isSimple(method.type) ? "" : "if (result != null) ",
                                           "prefix", prefix,
                                           "suffix", suffix,
                                           "setterTo", setterTo,
                                           "initResult", " = " + getDefaultValue(method.type),
                                           "parameters", parameters
                ));
            } else {
                String ClassNameSuffix = Util.firstLetterToUpperCase(methodName);
                StringBuilder ifelse = new StringBuilder();
                cases.append(Util.format(TEMPLATE_CASE,
                                         "methodName", methodName,
                                         "requestClass", "Request" + ClassNameSuffix,
                                         "responseClass", "Response" + ClassNameSuffix
                ));
                castCases.append(Util.format(TEMPLATE_CAST_CASE,
                                             "methodName", methodName,
                                             "requestClass", "Request" + ClassNameSuffix,
                                             "responseClass", "Response" + ClassNameSuffix
                ));
                for (int number = 0; number < exportMethods.size(); number++) {
                    ExportMethod method = exportMethods.get(number);
                    if (ifelse.length() != 0) {
                        ifelse.append(" else ");
                    } else {
                        ifelse.append("            ");
                    }
                    StringBuilder variables = new StringBuilder();
                    String localRequestClass = "Request" + ClassNameSuffix + number;

                    StringBuilder invokeParameters = new StringBuilder();
                    StringBuilder parameters = new StringBuilder();
                    for (ExportParameter parameter : method.parameters) {
                        if (parameters.length() != 0) {
                            parameters.append("\n");
                            invokeParameters.append(", ");
                        }
                        String suffix = "";//Util.isCollection(parameter.type) ? "List" : "";
                        parameters.append("                ").append(ConverterWriter.toOriginalProto2Class(parameter.type)).append(" ").append(parameter.name).append(" = ");
                        boolean checkFieldInit = !parameter.required && !Util.isCollection(parameter.type);
                        if (checkFieldInit) {
                            parameters.append("request").append(number).append(".get").append(Util.firstLetterToUpperCase(parameter.name)).append(suffix).append("() == null ? null : ");
                        }
                        if (Util.isExistsInAvro(parameter.type)) {
                            parameters.append("request").append(number).append(".get").append(Util.firstLetterToUpperCase(parameter.name)).append(suffix).append("();");
                        } else {
                            parameters.append("AvroConverterUtils.").append(ConverterWriter.writeConverterFromName(parameter.type))
                                .append("(request.get").append(Util.firstLetterToUpperCase(parameter.name)).append(suffix).append("()").append(");");
                        }
                        invokeParameters.append(parameter.name);
                        if (!Util.isExistsInAvro(parameter.type)) {
                            imports.add("import " + parameter.type.name + ";");
                        }
                    }

                    ifelse.append(Util.format("if (request.getRequest{number}() != null) {\n"
                                              + "                {responseClass}{number}.Builder builder{number} = {responseClass}{number}.newBuilder();\n"
                                              + "                {requestClass} request{number} = request.getRequest{number}();\n"
                                              + "{parameters}\n"
                                              + "                builder{number}.setResult(getService().{method}({invokeParameters}));\n"
                                              + "                builder.setResponse{number}Builder(builder{number});\n"
                                              + "            }",
                                              "number", number,
                                              "method", method.name,
                                              "requestClass", localRequestClass,
                                              "invokeParameters", invokeParameters,
                                              "responseClass", "Response" + ClassNameSuffix,
                                              "parameters", parameters));
                }
                ifelse.append("\n");

                methods.append(Util.format(TEMPLATE_OVERLOADED_METHOD,
                                           "methodName", methodName,
                                           "cases", ifelse,
                                           "responseClass", "Response" + ClassNameSuffix,
                                           "requestClass", "Request" + ClassNameSuffix
                ));
            }
        }

        String namespace = javaNamespace + "." + Util.toNamespace(exportService.name);
        return Util.format(TEMPLATE,
                           "javaNamespace", namespace,
                           "serviceName", Util.getClassName(exportService.name),
                           "serviceProxyName", proxyClassName(),
                           "methods", methods,
                           "ifelseCases", ifelseCases,
                           "castCases", castCases,
                           "cases", cases,
                           "fullServiceName", exportService.name,
                           "imports", String.join("\n", imports)
        );
    }

    public static String getDefaultValue(ExportClass ec) {
        if (Util.isSimple(ec) && !Util.isCollection(ec)) {
            switch (ec.name) {
                case "byte":
                case "short":
                case "int":
                case "long":
                case "float":
                case "double":
                    return "0";
                case "boolean":
                    return "false";
                case "char":
                    return "''";
                default:
                    throw new IllegalArgumentException(ec.name);
            }
        } else {
            return "null";
        }
    }
}
