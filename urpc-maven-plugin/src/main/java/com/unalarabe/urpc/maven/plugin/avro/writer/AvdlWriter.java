package com.unalarabe.urpc.maven.plugin.avro.writer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.unalarabe.urpc.converter.structure.ExportArray;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportEnum;
import com.unalarabe.urpc.converter.structure.ExportException;
import com.unalarabe.urpc.converter.structure.ExportField;
import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.converter.structure.ExportMethod;
import com.unalarabe.urpc.converter.structure.ExportParameter;

import static com.unalarabe.urpc.maven.plugin.Util.format;

import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.converter.structure.ExportSet;
import com.unalarabe.urpc.exception.PluginException;
import com.unalarabe.urpc.maven.plugin.Util;
import java.util.Iterator;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class AvdlWriter {

    public static String TEMPLATE
        = "@namespace(\"{javaNamespace}\")\n"
          + "protocol {serviceName} {\n"
          + "\n"
          + "{imports}"
          + "{messages}"
          + "\n"
          + "{methods}"
          + "}\n"
          + "\n";
    public static String TEMPLATE_PARAMETER
        = "{parameterType} {parameterName}";
    public static String TEMPLATE_METHOD
        = "{resultType} {methodName}({parameters}){throws};";
    public static String TEMPLATE_STRUCT
        = "    record {className} {\n"
          + "{fields}"
          + "    }\n";
    public static String TEMPLATE_ENUM
        = "    enum {className} {\n"
          + "{entries}"
          + "    }\n"
          + "\n";
    public static String TEMPLATE_FILED
        = "    {modifierPrefix}{avroType}{modifierSuffix} {fieldName}{fieldNameSuffix};";

    private final String baseDir;
    private final String avroNamespace;
    private final List<File> messageFiles = new ArrayList<>();
    private final Set<String> processed = new HashSet<>();
    private final Map<String, String> typesMap = new HashMap<>();

    public AvdlWriter(String avroNamespace, String baseDir) {
        this.baseDir = baseDir;
        this.avroNamespace = avroNamespace;
    }

    private String toPath(String namespace) {
        return namespace.replaceAll("\\.", File.separator);
    }

    public String writeStructBody(ExportClass ec) {
        String result;
        if (Util.isExistsInAvro(ec)) {
            result = null;
        } else if (Util.isCollection(ec)) {
            processed.remove(ec.name);
            ExportClass simpleClass = new ExportClass(ec.name);
            simpleClass.fields.addAll(ec.fields);
            result = writeStructBody(simpleClass);
        } else if (ec instanceof ExportEnum) {
            result = "\n" + format(TEMPLATE_ENUM, "className", toClassName(ec), "entries", writeEntries((ExportEnum) ec));
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("\n").append(format(TEMPLATE_STRUCT, "className", toClassName(ec), "fields", writeFields(ec)));
            result = sb.toString();
        }
        return result;
    }

    public String writeImport(ExportClass ec) {
        String namespace = avroNamespace + "." + toNamespace(ec.name);
        return "    import idl \"" + Util.path(toPath(namespace), toClassName(ec) + ".avdl") + "\";\n";
    }

    public String writeStruct(ExportClass ec) throws PluginException {
        try {
            System.out.println("writeStruct: " + ec.name + " => " + ec.fields);
            String namespace = avroNamespace + "." + toNamespace(ec.name);
            String toClassName = toClassName(ec);
            String messageFilePath = Util.path(toPath(namespace), toClassName + ".avdl");

            String result;
            if ("void".equals(ec.name) || Object.class.getCanonicalName().equals(ec.name) || Util.isExistsInAvro(ec)) {
                result = "";
            } else if (processed.contains(ec.name) && !Util.isExistsInAvro(ec)) {
                result = writeImport(ec);
            } else {
                result = writeImport(ec);
                processed.add(ec.name);
                typesMap.put(ec.name, toClassName);
                String body = writeStructBody(ec);
                if (body != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("@namespace(\"").append(namespace).append("\")\n");
                    sb.append("protocol _ {\n");
                    for (ExportField field : ec.fields) {
                        if (!field.isIgnored("avro")) {
                            sb.append(writeStruct(field.type));
                        }
                    }
                    sb.append(body);
                    sb.append("}\n");
                    File messageAvroFile = new File(baseDir, messageFilePath);
                    FileUtils.writeStringToFile(messageAvroFile, sb.toString(), "UTF-8");
                    messageFiles.add(messageAvroFile);
                }
            }
            return result;
        } catch (Exception ex) {
            throw new PluginException(ex);
        }
    }

    public String writeEntries(ExportEnum ee) {
        List<String> entries = new ArrayList<>(ee.entries);
        return "        " + String.join(",\n        ", entries) + "\n";
    }

    public String writeFields(ExportClass ec) {
        StringBuilder sb = new StringBuilder();
        for (ExportField field : ec.fields) {
            if (!field.isIgnored("avro")) {
                String modifierPrefix;
                String modifierSuffix = "";
                String fieldNameSuffix = "";
                if (field.required || Util.isSimple(field.type)) {
                    modifierPrefix = "";
                } else {
                    modifierPrefix = "union { null, ";
                    modifierSuffix = " }";
                    fieldNameSuffix = " = null";
                }
                if (Util.isCollection(field.type)) {
                    modifierPrefix = "";
                    modifierSuffix = "";
                }
                if (field.type instanceof ExportMap) {
                    modifierPrefix = "";
                    modifierSuffix = "";
                }
                boolean full = false;
                if (!Util.isExistsInAvro(field.type) && !toNamespace(ec.name).equals(toNamespace(field.type.name))) {
                    System.out.println(field.type.name);
                    full = true;
                }
                String avroType = toAvroType(field.type, full);
                sb.append("    ").append(format(TEMPLATE_FILED,
                                                "modifierPrefix", modifierPrefix,
                                                "modifierSuffix", modifierSuffix,
                                                "fieldNameSuffix", fieldNameSuffix,
                                                "avroType", avroType,
                                                "fieldName", field.name)).append("\n");
            }
        }
        return sb.toString();
    }

    public File writeService(ExportService service) throws PluginException {
        try {
            StringBuilder methods = new StringBuilder();

            Set<String> imports = new HashSet<>();
            StringBuilder messages = new StringBuilder();

            Set<String> methodNames = service.getMethodNames();

            for (Iterator<String> iterator = methodNames.iterator(); iterator.hasNext();) {
                String methodName = iterator.next();
                List<ExportMethod> methodSet = service.getMethods(methodName);
                for (Iterator<ExportMethod> methodIterator = methodSet.iterator(); methodIterator.hasNext();) {
                    ExportMethod exportMethod = methodIterator.next();
                    if (exportMethod.ignoredGenerators.contains("avro") || exportMethod.ignoredGenerators.contains("all")) {
                        methodIterator.remove();
                    }
                }
                if (methodSet.isEmpty()) {
                    iterator.remove();
                }
            }

            List<ExportClass> allClasses = new ArrayList<>();
            for (String methodName : methodNames) {
                List<ExportMethod> exportMethods = service.getMethods(methodName);
                if (exportMethods.size() != 1) {
                    throw new IllegalStateException("Overloaded methods: " + exportMethods);
                }
                for (ExportMethod exportMethod : exportMethods) {
                    for (ExportParameter parameter : exportMethod.parameters) {
                        allClasses.add(0, parameter.type);
                    }
                    allClasses.remove(exportMethod.type);
                    allClasses.add(0, exportMethod.type);
                }
            }
            allClasses.addAll(service.getExtraClasses());

            List<ExportClass> newClasses = new ArrayList<>();
            do {
                newClasses.clear();
                for (ExportClass clazz : allClasses) {
                    for (ExportField field : clazz.fields) {
                        newClasses.remove(field.type);
                        newClasses.add(0, field.type);
                    }
                }
                newClasses.removeAll(allClasses);
                allClasses.addAll(0, newClasses);
            } while (!newClasses.isEmpty());

            for (ExportClass clazz : allClasses) {
                if (clazz.rpcRequest || clazz.rpcResponse || clazz.rpcError) {
                    String body = writeStructBody(clazz);
                    if (body != null) {
                        messages.append(body);
                    }
                } else {
                    imports.add(writeStruct(clazz));
                }
            }

            for (String methodName : methodNames) {
                if ("error".equals(methodName)) {
                    throw new PluginException("Invalid method name: 'error'");
                }
                List<ExportMethod> exportMethods = service.getMethods(methodName);
                if (exportMethods.size() == 1) {
                    ExportMethod method = exportMethods.get(0);
                    StringBuilder parameters = new StringBuilder();
                    for (int i = 0; i < method.parameters.size(); i++) {
                        ExportParameter parameter = method.parameters.get(i);
                        boolean full = !toNamespace(parameter.type.name).equals(service.name);
                        if (i > 0) {
                            parameters.append(", ");
                        }
                        parameters.append(format(TEMPLATE_PARAMETER,
                                                 "number", (i + 1),
                                                 "parameterType", toClassName(parameter.type, false),
                                                 "parameterName", parameter.name));
                    }
                    StringBuilder throwsErrors = new StringBuilder();
                    for (ExportException exception : method.exceptions) {
                        if (throwsErrors.length() > 0) {
                            throwsErrors.append(", ");
                        }
                        throwsErrors.append(toClassName(exception.name, true));
                    }
                    boolean full = !toNamespace(method.type.name).equals(service.name);
                    methods.append("    ").append(format(TEMPLATE_METHOD,
                                                         "resultType", toClassName(method.type, false),
                                                         "methodName", method.name,
                                                         "parameters", parameters,
                                                         "throws", throwsErrors)).append("\n\n");
                } else {
                    System.out.println("MULTI METHOD: " + methodName);
                }
            }
            String namespace = avroNamespace + "." + toNamespace(service.name);
            String content = format(TEMPLATE,
                                    "javaNamespace", namespace,
                                    "serviceName", toServiceName(service),
                                    "imports", String.join("", imports),
                                    "messages", messages,
                                    "methods", methods);
            File avdlFile = new File(baseDir, namespace + "." + toServiceName(service) + ".avdl");
            FileUtils.writeStringToFile(avdlFile, content, "UTF-8");
            return avdlFile;
        } catch (IOException ex) {
            throw new PluginException(ex);
        }
    }

    public Set<File> getMessagesAvroFiles() {
        return new HashSet<>(messageFiles);
    }

    public static String toAvroTypeBasic(String type) {
        switch (type) {
            case "boolean": {
                return "bool";
            }
            case "java.lang.Boolean": {
                return "bool";
            }
            case "byte": {
                return "int";
            }
            case "java.lang.Byte": {
                return "int";
            }
            case "char": {
                return "int";
            }
            case "java.lang.Character": {
                return "int";
            }
            case "int": {
                return "int";
            }
            case "java.lang.Integer": {
                return "int";
            }
            case "long": {
                return "long";
            }
            case "java.lang.Long": {
                return "long";
            }
            case "float": {
                return "float";
            }
            case "java.lang.Float": {
                return "float";
            }
            case "double": {
                return "double";
            }
            case "java.lang.Double": {
                return "double";
            }
            case "java.lang.String": {
                return "string";
            }
            case "java.util.Date": {
                return "date";
            }
            default: {
                return type;
//                    throw new IllegalStateException("");
            }
        }
    }

    public String toAvroTypeBasic(ExportClass ec, boolean full) {
        if (Util.isExistsInAvro(ec)) {
            return AvdlWriter.toAvroTypeBasic(ec.name);
        } else {
            return toClassName(ec, full);
        }
    }

    public String toAvroType(ExportClass ec) {
        return toAvroType(ec, false);
    }

    public String toAvroType(ExportClass ec, boolean full) {
        String avroTypeBasic = toAvroTypeBasic(ec, full);
        if (Util.isCollection(ec)) {
            if (ec instanceof ExportArray) {
                return format("array<{of}>", "of", avroTypeBasic);
            } else if (ec instanceof ExportList) {
                return format("array<{of}>", "of", avroTypeBasic);
            } else if (ec instanceof ExportSet) {
                return format("array<{of}>", "of", avroTypeBasic);
            } else if (ec instanceof ExportMap) {
                ExportMap em = (ExportMap) ec;
                String keyType;
                if (!Util.isSimple(em.keyType) && !String.class.getCanonicalName().equals(em.keyType)) {
                    throw new IllegalStateException("Only simple types allowed as map keys");
                }
                return format("map<{value}>", "value", avroTypeBasic);
            } else {
                throw new IllegalStateException();
            }
        } else {
            return avroTypeBasic;
        }
    }

    public static String toServiceName(ExportService es) {
        int lastIndexOfDot = es.name.lastIndexOf(".");
        return es.name.substring(lastIndexOfDot + 1);
    }

    public String toClassName(String type, boolean full) {
//        full = false;
        String result;
        if (Util.isExistsInAvro(type)) {
            result = AvdlWriter.toAvroTypeBasic(type);
        } else if (full) {
            result = avroNamespace + "." + type;
        } else {
            int lastIndexOfDot = type.lastIndexOf(".");
            result = type.substring(lastIndexOfDot + 1);
        }
        return result;
    }

    public static String toNamespace(String type) {
        int lastIndexOfDot = type.lastIndexOf(".");
        return lastIndexOfDot >= 0 ? type.substring(0, lastIndexOfDot) : type;
    }

    public String toClassName(ExportClass ec) {
        return toClassName(ec, false);
    }

    public String toClassName(ExportClass ec, boolean full) {
        return toClassName(ec.name, full);
    }

}
