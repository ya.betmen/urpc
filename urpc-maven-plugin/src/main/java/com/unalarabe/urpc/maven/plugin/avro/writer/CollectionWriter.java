package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportCollection;
import com.unalarabe.urpc.maven.plugin.Util;
import java.util.Collection;

import static com.unalarabe.urpc.maven.plugin.Util.format;
import static com.unalarabe.urpc.maven.plugin.avro.writer.ConverterWriter.toName;
import static com.unalarabe.urpc.maven.plugin.avro.writer.ConverterWriter.writeConverterFromName;
import static com.unalarabe.urpc.maven.plugin.avro.writer.ConverterWriter.writeConverterToName;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public abstract class CollectionWriter extends ConverterWriter {

    public static final String TEMPLATE_COLLECTION_TO_AVRO
        = "    public static {avroClass} {converterToName}(" + Collection.class.getCanonicalName() + "<{avroItemClass}> original) {\n"
          + "        if (original == null) {\n"
          + "            return null;\n"
          + "        }\n"
          + "        {avroClass} th = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection({avroCollectionClass}.class, {avroItemClass}.class);\n"
          + "{bodyTo}"
          + "        return th;\n"
          + "    }\n";
    public static final String TEMPLATE_COLLECTION_FROM_AVRO
        = "    public static {originalClass} {converterFromName}(" + Collection.class.getCanonicalName() + "<{originalItemClass}> th) {\n"
          + "        if (th == null) {\n"
          + "            return null;\n"
          + "        }\n"
          + "        {originalClass} original = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection({collectionClass}.class, {originalItemClass}.class);\n"
          + "{bodyFrom}"
          + "        return original;\n"
          + "    }\n";

    protected final ExportCollection exportCollection;

    public CollectionWriter(ExportCollection exportCollection, WriterFactory processed) {
        super(processed);
        this.exportCollection = exportCollection;
    }

    @Override
    public String write() {
        String templateCopyTo = getTemplateCopyTo();
        String templateCopyFrom = getTemplateCopyFrom();
        String avroItemClass;
        String originalItemClass = exportCollection.name;
        String avroClass = toAvroClass(exportCollection);
        String avroCollectionClass = avroClass.substring(0, avroClass.indexOf("<"));
        if (!Util.isExistsInAvro(exportCollection.name)) {
            avroItemClass = toAvroClass(exportCollection.name);
        } else {
            avroItemClass = exportCollection.name;
        }
        String converterItemToName = format(TEMPLATE_TO_AVRO_NAME, "className", toName(exportCollection.name));
        String converterItemFromName = format(TEMPLATE_FROM_AVRO_NAME, "className", toName(exportCollection.name));
        String bodyTo = format(templateCopyTo, "converterToName", converterItemToName, "originalItemClass", originalItemClass);
        String bodyFrom = format(templateCopyFrom, "converterFromName", converterItemFromName, "avroItemClass", avroItemClass);

        StringBuilder sb = new StringBuilder();
        ExportClass simpleClass = new ExportClass(exportCollection.name);
        simpleClass.fields.addAll(exportCollection.fields);
        simpleClass.imports.addAll(exportCollection.imports);

        ConverterWriter simpleWriter = this.createWriter(simpleClass);
        if (simpleWriter != null && !Util.isExistsInAvro(simpleClass)) {
            sb.append(simpleWriter.write());
        }

        boolean addSeparator = false;
        String converterToName = writeConverterToName(exportCollection);
        String converterFromName = writeConverterFromName(exportCollection);
        if (!isConverterMethoExists(converterToName)) {
            addConverterMethod(converterToName);
            String converterTo = Util.format(TEMPLATE_COLLECTION_TO_AVRO,
                                             "className", toName(exportCollection.name),
                                             "avroClass", avroClass,
                                             "avroCollectionClass", avroCollectionClass,
                                             "originalClass", toOriginalProto2Class(exportCollection),
                                             "avroItemClass", avroItemClass,
                                             "originalItemClass", originalItemClass,
                                             "converterToName", converterToName,
                                             "converterFromName", converterFromName,
                                             "collectionClass", getCollectionClass(),
                                             "bodyTo", bodyTo,
                                             "bodyFrom", bodyFrom);
            sb.append(converterTo);
            addSeparator = true;
        }

        if (!isConverterMethoExists(converterFromName)) {
            addConverterMethod(converterFromName);
            if (addSeparator) {
                sb.append("\n");
            }
            String converterFrom = Util.format(TEMPLATE_COLLECTION_FROM_AVRO,
                                               "className", toName(exportCollection.name),
                                               "avroClass", avroClass,
                                               "originalClass", toOriginalProto2Class(exportCollection),
                                               "avroItemClass", avroItemClass,
                                               "originalItemClass", originalItemClass,
                                               "converterToName", converterToName,
                                               "converterFromName", converterFromName,
                                               "collectionClass", getCollectionClass(),
                                               "bodyTo", bodyTo,
                                               "bodyFrom", bodyFrom);
            sb.append(converterFrom);
        }
        return sb.toString();
    }

    protected String getCollectionClass() {
        return exportCollection.collectionClass;
    }

    abstract protected String getTemplateCopyTo();

    abstract protected String getTemplateCopyFrom();

}
