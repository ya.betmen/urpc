package com.unalarabe.urpc.maven.plugin.avro;

import com.unalarabe.urpc.meta.MetaClass;
import com.unalarabe.urpc.meta.MetaMethod;
import com.unalarabe.urpc.meta.MetaService;
import com.unalarabe.urpc.meta.MetaProperty;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.apache.avro.Protocol;
import org.apache.avro.Schema;
import org.apache.avro.compiler.idl.Idl;
import org.apache.avro.compiler.idl.ParseException;
import org.json.JSONObject;


/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class AvdlFileParser {

    private static final Logger LOG = Logger.getLogger(AvdlFileParser.class.getName());

//    private final Map<String, ProtoFile> includes = new HashMap<>();
    private Protocol protocol;
    private Map<String, MetaClass> classes;
    private ArrayList<MetaService> services;

    public AvdlFileParser(File file, List<File> includes) throws IOException, ParseException {
        System.out.println("====================");
        System.out.println(file.getAbsolutePath());
        List<URL> runtimeUrls = new ArrayList<>();
        runtimeUrls.add(file.getParentFile().toURI().toURL());
        URLClassLoader avdlPathLoader = new URLClassLoader(runtimeUrls.toArray(new URL[0]), Thread.currentThread().getContextClassLoader());
        Idl idl = new Idl(file, avdlPathLoader);
        protocol = idl.CompilationUnit();
    }

    public List<MetaService> getServices() throws IOException {
        if (services == null) {
            services = new ArrayList<>();
            String packageName = protocol.getNamespace();

            MetaService protoService = new MetaService();
            protoService.name = protocol.getName();

            Map<String, Protocol.Message> messages = protocol.getMessages();
            for (Map.Entry<String, Protocol.Message> entry : messages.entrySet()) {
                MetaMethod protoMethod = new MetaMethod();
                protoMethod.name = entry.getKey();
                JSONObject jsonObject = new JSONObject(entry.getValue().toString());
                protoMethod.resultType = jsonObject.getString("response");
                protoMethod.parameterType = jsonObject.getJSONArray("request").getJSONObject(0).getString("type");
                protoService.methods.add(protoMethod);
            }
            services.add(protoService);

            Map<String, MetaClass> classesMap = getClassesMap();
            for (MetaService service : services) {
                for (MetaMethod protoMethod : service.methods) {
                    MetaClass protoClass = classesMap.get(protoMethod.resultType);
                    if (protoClass.rpcResponse) {
                        for (MetaProperty property : protoClass.properties) {
                            if ("errorValue".equals(property.name)) {
                                MetaClass errorEnum = classesMap.get(packageName + "." + property.type);
                                protoMethod.errors.addAll(errorEnum.entries);
                                break;
                            }
                        }
                    }
                }
            }
        }
        return services;
    }

    public Map<String, MetaClass> getClassesMap() throws IOException {
        if (classes == null) {
            Set<MetaClass> allClasses = new HashSet<>();
            List<MetaService> services = getServices();
            Set<String> allResponses = new HashSet<>();
            Set<String> allRequests = new HashSet<>();
            Map<String, Set<String>> serviceToClassMap = new HashMap<>();

            for (MetaService service : services) {
                allRequests.add(service.name + "Request");
                allResponses.add(service.name + "Response");
            }

            for (MetaService service : services) {
                Set<String> serviceResponses = new HashSet<>();
                for (MetaMethod method : service.methods) {
                    serviceResponses.add(method.resultType);
                    allResponses.add(method.resultType);
                    allRequests.add(method.parameterType);
                }
                serviceToClassMap.put(service.name, serviceResponses);
            }

            Map<String, Schema> types = new HashMap<>();
            Collection<Schema> typeElements = protocol.getTypes();
            Set<String> subRequests = new HashSet<>();
            Set<String> subResponses = new HashSet<>();
            for (Schema typeElement : typeElements) {
                for (String request : allRequests) {
                    if (typeElement.getName().matches(request + "[0-9]+")) {
                        subRequests.add(typeElement.getName());
                        break;
                    }
                }
                for (String response : allResponses) {
                    if (typeElement.getName().matches(response + "[0-9]+")) {
                        subResponses.add(typeElement.getName());
                        break;
                    }
                }
                types.put(typeElement.getFullName(), typeElement);
            }
            allRequests.addAll(subRequests);
            allResponses.addAll(subResponses);

            for (String name : types.keySet()) {
                Schema typeElement = types.get(name);
                MetaClass protoClass = new MetaClass(typeElement, types, allResponses.contains(typeElement.getName()), allRequests.contains(typeElement.getName()));
                
                allClasses.add(protoClass);
            }

            classes = new HashMap<>();
            for (MetaClass protoClass : allClasses) {
                classes.put(protoClass.packet + "." + protoClass.name, protoClass);
            }
        }
        return classes;
    }

    public List<MetaClass> getClasses() throws IOException {
        return new ArrayList<>(getClassesMap().values());
    }

    public boolean isMuliplyFiles() {
//        List<OptionElement> options = protocol.options();
//        for (OptionElement option : options) {
//            if ("java_multiple_files".equals(option.name())) {
//                return Boolean.parseBoolean(String.valueOf(option.value()));
//            }
//        }
        return true;
    }

    public String getJavaPackage() {
//        List<OptionElement> options = protoFile.options();
//        for (OptionElement option : options) {
//            if ("java_package".equals(option.name())) {
//                return String.valueOf(option.value());
//            }
//        }
        return protocol.getNamespace();
    }

}
