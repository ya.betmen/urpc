package com.unalarabe.urpc.maven.plugin.proto2.writer;

import java.util.HashSet;
import java.util.Set;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.factory.CreationException;
import com.unalarabe.urpc.factory.DefaultObjectFactory;
import com.unalarabe.urpc.factory.ObjectFactory;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ConvertersWriter {

    private final String proto2Package;
    private final String converterPackage;

    private Set<ExportClass> processedConverters = new HashSet<>();
    private WriterFactory writerFactory;

    public ConvertersWriter(String proto2Package, String converterPackage) {
        this.proto2Package = proto2Package;
        this.converterPackage = converterPackage;
        this.writerFactory = new WriterFactory(processedConverters, proto2Package);
    }

    public String writeClassConverters(ExportClass ec) {
        ConverterWriter writer = writerFactory.createWriter(ec);
        System.out.println("CONVERT: " + ec.name + " => " + writer);
        return writer != null ? writer.write() : null;
    }

    public String writeServiceConverters(String className, Set<ExportClass> classes) {
        StringBuilder result = new StringBuilder();
        result.append("package ").append(converterPackage).append(";\n");
        result.append("\n");
        result.append("public class ").append(className).append(" {\n");
        result.append("\n");
        result.append("    private static java.util.List<java.lang.Integer> convertListOfCharsToIntegers(java.util.List<java.lang.Character> listOfChars) {\n");
        result.append("        if (listOfChars == null){\n");
        result.append("            return null;\n");
        result.append("        }\n");
        result.append("        java.util.List<java.lang.Integer> result = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Integer.class);\n");
        result.append("        for (java.lang.Character c : listOfChars) {\n");
        result.append("            result.add(convertCharToInteger(c));\n");
        result.append("        }\n");
        result.append("        return result;\n");
        result.append("    }\n");
        result.append("\n");
        result.append("    private static java.util.List<java.lang.Character> convertListOfIntegersToChars(java.util.List<java.lang.Integer> listOfIntegers) {\n");
        result.append("        if (listOfIntegers == null){\n");
        result.append("            return null;\n");
        result.append("        }\n");
        result.append("        java.util.List<java.lang.Character> result = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Character.class);\n");
        result.append("        for (java.lang.Integer i : listOfIntegers) {\n");
        result.append("            result.add(convertIntegerToChar(i));\n");
        result.append("        }\n");
        result.append("        return result;\n");
        result.append("    }\n");
        result.append("\n");
        result.append("    private static java.lang.Integer convertCharToInteger(java.lang.Character c) {\n");
        result.append("        return c != null ? (int) c : null;\n");
        result.append("    }\n");
        result.append("\n");
        result.append("    private static java.lang.Character convertIntegerToChar(java.lang.Integer i) {\n");
        result.append("        return i != null ? (char) i.intValue() : null;\n");
        result.append("    }\n");
        result.append("    private static java.util.List<java.lang.Integer> convertListOfBytesToIntegers(java.util.List<java.lang.Byte> listOfChars) {\n");
        result.append("        if (listOfChars == null){\n");
        result.append("            return null;\n");
        result.append("        }\n");
        result.append("        java.util.List<java.lang.Integer> result = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Integer.class);\n");
        result.append("        for (java.lang.Byte c : listOfChars) {\n");
        result.append("            result.add(c.intValue());\n");
        result.append("        }\n");
        result.append("        return result;\n");
        result.append("    }\n");
        result.append("\n");
        result.append("    private static java.util.List<java.lang.Byte> convertListOfIntegersToBytes(java.util.List<java.lang.Integer> listOfIntegers) {\n");
        result.append("        if (listOfIntegers == null){\n");
        result.append("            return null;\n");
        result.append("        }\n");
        result.append("        java.util.List<java.lang.Byte> result = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Byte.class);\n");
        result.append("        for (java.lang.Integer i : listOfIntegers) {\n");
        result.append("            result.add(i.byteValue());\n");
        result.append("        }\n");
        result.append("        return result;\n");
        result.append("    }\n");
        result.append("\n");
        result.append("    private static Long convertDateToTimestamp(java.util.Date date){\n");
        result.append("        return date == null ? null : date.getTime();\n");
        result.append("    }\n");
        result.append("\n");
        result.append("    private static java.util.Date convertTimestampToDate(java.lang.Long timestamp){\n");
        result.append("        return timestamp == null ? null : new java.util.Date(timestamp);\n");
        result.append("    }");
        result.append("\n");
        result.append("    private static java.util.List<java.util.Date> convertListOfDateToTimestamps(java.util.List<java.lang.Long> listOfTimestamp){\n");
        result.append("        if (listOfTimestamp == null){\n");
        result.append("            return null;\n");
        result.append("        }\n");
        result.append("        java.util.List<java.util.Date> result = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createCollection(java.util.List.class, java.util.Date.class);\n");
        result.append("        for (java.lang.Long l : listOfTimestamp) {\n");
        result.append("            result.add(convertTimestampToDate(l));\n");
        result.append("        }\n");
        result.append("        return result;\n");
        result.append("    }\n");
        result.append("\n");
        result.append("    private static java.util.List<java.lang.Long> convertListOfTimestampToDates(java.util.List<java.util.Date> listOfDates){\n");
        result.append("        if (listOfDates == null){\n");
        result.append("            return null;\n");
        result.append("        }\n");
        result.append("        java.util.List<java.lang.Long> result = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Long.class);\n");
        result.append("        for (java.util.Date d : listOfDates) {\n");
        result.append("            result.add(convertDateToTimestamp(d));\n");
        result.append("        }\n");
        result.append("        return result;\n");
        result.append("    }");
        result.append("\n");
        for (ExportClass clazz : classes) {
            String converter = this.writeClassConverters(clazz);
            if (converter != null) {
                result.append(converter);
            }
        }
        result.append("\n}\n");
        return result.toString();
    }

    public String writeObjectFactoryImpl() {
        return ""
                + "package " + converterPackage + ";\n"
                + "\n"
                + "import java.lang.reflect.InvocationTargetException;\n"
                + "import java.util.logging.Level;\n"
                + "import java.util.logging.Logger;\n"
                + "import " + CreationException.class.getCanonicalName() + ";\n"
                + "import " + ObjectFactory.class.getCanonicalName() + ";\n"
                + "public class ObjectFactoryImpl {\n"
                + "\n"
                + "    private static final Logger LOG = Logger.getLogger(ObjectFactoryImpl.class.getName());\n"
                + "\n"
                + "    private static ObjectFactory instance;\n"
                + "\n"
                + "    private static synchronized void init() {\n"
                + "        if (instance == null) {\n"
                + "            try {\n"
                + "                String factoryClassName = System.getProperty(\"com.unalarabe.urpc.factory.ObjectFactory.class\", " + DefaultObjectFactory.class.getCanonicalName() + ".class.getCanonicalName());\n"
                + "                Class<?> factoryClass = ObjectFactoryImpl.class.getClassLoader().loadClass(factoryClassName);\n"
                + "                instance = (ObjectFactory) factoryClass.getConstructor().newInstance();\n"
                + "            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {\n"
                + "                LOG.log(Level.SEVERE, null, ex);\n"
                + "            }\n"
                + "        }\n"
                + "    }\n"
                + "\n"
                + "    private static ObjectFactory getInstance() {\n"
                + "        if (instance == null) {\n"
                + "            init();\n"
                + "        }\n"
                + "        return instance;\n"
                + "    }\n"
                + "\n"
                + "    public static <T> T createObject(Class<T> clazz) {\n"
                + "        try {\n"
                + "            return getInstance().createObject(clazz);\n"
                + "        } catch (CreationException ex) {\n"
                + "            LOG.log(Level.SEVERE, ex.getMessage(), ex);\n"
                + "            throw new IllegalStateException(ex);\n"
                + "        }\n"
                + "    }\n"
                + "\n"
                + "    public static <T, L extends java.util.Collection<T>> L createCollection(Class<L> collectionClazz, Class<T> clazz) {\n"
                + "        try {\n"
                + "            return getInstance().createCollection(collectionClazz, clazz);\n"
                + "        } catch (CreationException ex) {\n"
                + "            LOG.log(Level.SEVERE, ex.getMessage(), ex);\n"
                + "            throw new IllegalStateException(ex);\n"
                + "        }\n"
                + "    }\n"
                + "\n"
                + "    public static <K, V, M extends java.util.Map<K, V>> M createMap(Class<M> mapClazz, Class<K> keyClazz, Class<V> valueClazz) {\n"
                + "        try {\n"
                + "            return getInstance().createMap(mapClazz, keyClazz, valueClazz);\n"
                + "        } catch (CreationException ex) {\n"
                + "            LOG.log(Level.SEVERE, ex.getMessage(), ex);\n"
                + "            throw new IllegalStateException(ex);\n"
                + "        }\n"
                + "    }\n"
                + "}\n"
                + "\n";
    }

}
