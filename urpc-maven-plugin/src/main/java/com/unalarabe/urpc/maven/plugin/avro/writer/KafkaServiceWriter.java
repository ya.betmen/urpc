package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.unalarabe.urpc.maven.plugin.proto2.writer.*;
import com.google.inject.Injector;
import com.google.protobuf.InvalidProtocolBufferException;
import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.config.ConnectionConfig;
import com.unalarabe.urpc.converter.structure.ExportMethod;
import com.unalarabe.urpc.exception.ConvertException;
import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.service.AbstractService;
import com.unalarabe.urpc.service.Connection;
import com.unalarabe.urpc.service.Message;
import com.unalarabe.urpc.service.RpcMessage;
import com.unalarabe.urpc.util.RpcParser;
import com.unalarabe.urpc.util.ServiceUtil;
import java.util.List;
import java.util.Set;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class KafkaServiceWriter {

    public static final String TEMPLATE
        = "package {javaNamespace};\n"
          + "\n"
          + "import {javaNamespace}.{serviceName}Proxy;\n"
          + "import " + AbstractService.class.getCanonicalName() + ";\n"
          + "import " + Injector.class.getCanonicalName() + ";\n"
          + "import " + ConnectionConfig.class.getCanonicalName() + ";\n"
          + "import " + Connection.class.getCanonicalName() + ";\n"
          + "import " + RpcMessage.class.getCanonicalName() + ";\n"
          + "import " + ConvertException.class.getCanonicalName() + ";\n"
          + "import " + RpcParser.class.getCanonicalName() + ";\n"
          + "import " + Message.class.getCanonicalName() + ";\n"
          + "import " + ServiceUtil.class.getCanonicalName() + ";\n"
          + "import " + ConsumerRecord.class.getCanonicalName() + ";\n"
          + "import " + ProducerRecord.class.getCanonicalName() + ";\n"
          + "import {serviceFullName};\n"
          + "\n"
          + "public class {jmsServiceName} extends " + AbstractService.class.getSimpleName() + "<{serviceName}, {serviceName}Proxy> {\n"
          + "    public static final " + RpcParser.class.getSimpleName() + " RPC_PARSER = new " + RpcParser.class.getSimpleName() + "() {\n"
          + "        @Override\n"
          + "        public " + RpcMessage.class.getSimpleName() + " parseRpcMessageRequest(Object data) throws " + ConvertException.class.getSimpleName() + " {\n"
          + "            " + ConsumerRecord.class.getSimpleName() + " cr = (" + ConsumerRecord.class.getSimpleName() + ")data;\n"
          + "            {serviceName}Request rpcRequest = ({serviceName}Request)cr.value();\n"
          + "            String correlationId = " + ServiceUtil.class.getSimpleName() + ".toStringOrNull(rpcRequest.getCorrelationId());\n"
          + "            String replyTo = " + ServiceUtil.class.getSimpleName() + ".toStringOrNull(rpcRequest.getReplyTo());\n"
          + "            String version = " + ServiceUtil.class.getSimpleName() + ".toStringOrNull(rpcRequest.getVersion());\n"
          + "            String method;\n"
          + "            Object request;\n"
          + "{ifelseCasesRequest} else {\n"
          + "                throw new " + ConvertException.class.getSimpleName() + "(\"Method not found\");\n"
          + "            }\n"
          + "            " + RpcMessage.class.getSimpleName() + " result = new " + RpcMessage.class.getSimpleName() + "();\n"
          + "            result.setCorrelationId(correlationId);\n"
          + "            result.setReplyTo(replyTo);\n"
          + "            result.setMethod(method);\n"
          + "            result.setRequest(request);\n"
          + "            result.setVersion(version);\n"
          + "            return result;\n"
          + "        }\n"
          + "\n"
          + "        @Override\n"
          + "        public " + Message.class.getSimpleName() + " parseRpcMessageResponse(Object data) throws " + ConvertException.class.getSimpleName() + " {\n"
          + "            {serviceName}Response rpcResponse = ({serviceName}Response)data;\n"
          + "            String correlationId = ServiceUtil.toStringOrNull(rpcResponse.getCorrelationId());\n"
          + "            String method;\n"
          + "            Object response;\n"
          + "{ifelseCasesResponse} else {\n"
          + "                throw new ConvertException(\"Method not found\");\n"
          + "            }\n"
          + "            " + Message.class.getSimpleName() + " result = new " + Message.class.getSimpleName() + "(data);\n"
          + "            result.setParsedBody(response);\n"
          + "            result.setCorrelationId(correlationId);\n"
          + "            result.setMethod(method);\n"
          + "            return result;\n"
          + "        }\n"
          + "\n"
          + "        @Override\n"
          + "        public Object createRpcMessageRequest(" + Message.class.getSimpleName() + " data) throws " + ConvertException.class.getSimpleName() + " {\n"
          + "            {serviceName}Request.Builder builder = {serviceName}Request.newBuilder();\n"
          + "            builder.setCorrelationId(data.getCorrelationId());\n"
          + "            builder.setReplyTo(data.getReplyToName());\n"
          + "            builder.setVersion(data.getVersion());\n"
          + "            String method = data.getMethod();\n"
          + "            switch (method) {\n"
          + "{casesRequest}"
          + "                default: {\n"
          + "                    throw new IllegalStateException(\"Unknown method: \" + method);\n"
          + "                }\n"
          + "            }\n"
          + "            return new ProducerRecord<>(data.getDestinationName(), builder.build());\n"
          + "        }\n"
          + "\n"
          + "        @Override\n"
          + "        public Object createRpcMessageResponse(" + Message.class.getSimpleName() + " data) throws " + ConvertException.class.getSimpleName() + " {\n"
          + "            {serviceName}Response.Builder builder = {serviceName}Response.newBuilder();\n"
          + "            builder.setCorrelationId(data.getCorrelationId());\n"
          + "            String method = data.getMethod();\n"
          + "            switch (method) {\n"
          + "{casesResponse}"
          + "                default: {\n"
          + "                    throw new IllegalStateException(\"Unknown method: \" + method);\n"
          + "                }\n"
          + "            }\n"
          + "            return new ProducerRecord<>(data.getDestinationName(), builder.build());\n"
          + "        }\n"
          + "    };\n"
          + "\n"
          + "    public {jmsServiceName}(Class<? extends " + Connection.class.getSimpleName() + "> connectionClass, " + Injector.class.getSimpleName() + " injector, " + ConnectionConfig.class.getSimpleName()
          + " connectionConfig) {\n"
          + "        super(connectionClass, \".\", {serviceName}.class, injector, connectionConfig);\n"
          + "    }\n"
          + "\n"
          + "    @Override\n"
          + "    protected {serviceName}Proxy wrapService({serviceName} service) {\n"
          + "        return new {serviceName}Proxy(service);\n"
          + "    }\n"
          + "\n"
          + "    @Override\n"
          + "    public RpcMessage parseRpcMessageRequest(Object data) throws ConvertException {\n"
          + "        return RPC_PARSER.parseRpcMessageRequest(data);\n"
          + "    }\n"
          + "\n"
          + "    @Override\n"
          + "    public Message parseRpcMessageResponse(Object data) throws ConvertException {\n"
          + "        return RPC_PARSER.parseRpcMessageResponse(data);\n"
          + "    }\n"
          + "\n"
          + "    @Override\n"
          + "    public Object createRpcMessageRequest(Message data) throws ConvertException {\n"
          + "        return RPC_PARSER.createRpcMessageRequest(data);\n"
          + "    }\n"
          + "\n"
          + "    @Override\n"
          + "    public Object createRpcMessageResponse(Message data) throws ConvertException {\n"
          + "        return RPC_PARSER.createRpcMessageResponse(data);\n"
          + "    }\n"
          + "\n"
          + "}\n";

    private final String javaNamespace;
    private final ExportService exportService;
    private final String jmsServiceName;

    public KafkaServiceWriter(String javaNamespace, ExportService exportService) {
        this.javaNamespace = javaNamespace;
        this.exportService = exportService;
        this.jmsServiceName = Util.getClassName(exportService.name) + "KafkaService";
    }

    public String getJmsServiceName() {
        return jmsServiceName;
    }

    public String write() {
        StringBuilder ifelseCasesRequest = new StringBuilder();
        StringBuilder ifelseCasesResponse = new StringBuilder();
        StringBuilder casesRequest = new StringBuilder();
        StringBuilder casesResponse = new StringBuilder();

        Set<String> methodNames = exportService.getMethodNames();
        for (String methodName : methodNames) {
            if (ifelseCasesRequest.length() > 0) {
                ifelseCasesRequest.append(" else ");
            } else {
                ifelseCasesRequest.append("                ");
            }
            if (ifelseCasesResponse.length() > 0) {
                ifelseCasesResponse.append(" else ");
            } else {
                ifelseCasesResponse.append("                ");
            }
            final String partMethodName = Util.firstLetterToUpperCase(methodName);
            ifelseCasesRequest.append("if (rpcRequest.get").append(partMethodName).append("() != null) {\n")
                .append("                    method = \"").append(methodName).append("\";\n")
                .append("                    request = rpcRequest.get").append(partMethodName).append("();\n            }");
            
            ifelseCasesResponse.append("if (rpcResponse.get").append(partMethodName).append("() != null) {\n")
                .append("                    method = \"").append(methodName).append("\";\n")
                .append("                    response = rpcResponse.get").append(partMethodName).append("();\n            }");

            casesRequest.append("                case \"").append(methodName).append("\": {\n")
                .append("                    builder.set").append(partMethodName).append("((Request").append(partMethodName).append(") data.getParsedBody());\n")
                .append("                    break;\n")
                .append("                }\n");

            casesResponse.append("                case \"").append(methodName).append("\": {\n")
                .append("                    builder.set").append(partMethodName).append("((Response").append(partMethodName).append(") data.getParsedBody());\n")
                .append("                    break;\n")
                .append("                }\n");
        }

        return Util.format(TEMPLATE,
                           "javaNamespace", javaNamespace + "." + Util.toNamespace(exportService.name),
                           "serviceName", Util.getClassName(exportService.name),
                           "jmsServiceName", jmsServiceName,
                           "serviceFullName", exportService.name,
                           "ifelseCasesRequest", ifelseCasesRequest,
                           "ifelseCasesResponse", ifelseCasesResponse,
                           "casesRequest", casesRequest,
                           "casesResponse", casesResponse
        );
    }

}
