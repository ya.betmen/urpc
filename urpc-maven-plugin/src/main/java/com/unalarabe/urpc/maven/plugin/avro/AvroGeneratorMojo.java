package com.unalarabe.urpc.maven.plugin.avro;

import com.unalarabe.urpc.exception.PluginException;
import com.unalarabe.urpc.meta.Generator;
import com.squareup.protoparser.ProtoFile;
import com.squareup.protoparser.ProtoParser;

import com.unalarabe.urpc.maven.plugin.avro.writer.ConvertersWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import com.unalarabe.urpc.converter.ServiceConverter;
import com.unalarabe.urpc.converter.ConverterException;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.maven.plugin.avro.writer.AvdlWriter;
import com.unalarabe.urpc.maven.plugin.avro.writer.ClientClassWriter;
import com.unalarabe.urpc.maven.plugin.avro.writer.ClientWriter;
import com.unalarabe.urpc.maven.plugin.avro.writer.JmsServiceWriter;
import com.unalarabe.urpc.maven.plugin.avro.writer.KafkaServiceWriter;
import com.unalarabe.urpc.maven.plugin.avro.writer.ProxyWriter;
import com.unalarabe.urpc.meta.MetaClass;
import com.unalarabe.urpc.meta.MetaService;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.avro.compiler.idl.ParseException;
import org.apache.avro.mojo.IDLProtocolMojo;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.reflect.FieldUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.VersionRange;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.StringUtils;

import static com.unalarabe.urpc.maven.plugin.Util.SEPARATOR;
import static com.unalarabe.urpc.maven.plugin.Util.path;

/**
 * @phase generate-sources
 * @goal compile-avro
 * @requiresDependencyResolution compile
 * @author Petr Zalyautdinov (Заляутдинов Пётр) <zalyautdinov.petr@gmail.com>
 */
public class AvroGeneratorMojo extends AbstractMojo {

    /**
     * @parameter default-value="${basedir}/src/main/java"
     */
    protected File basedir;
    /**
     * @parameter default-value="${basedir}/src/main/java"
     */
    protected File servicedir;
    /**
     * @parameter default-value="${basedir}/src/main"
     */
    protected File avroBaseDir;
    /**
     * @parameter default-value={"/*.avdl"}
     */
    protected String[] avroFiles = new String[]{"/*.avdl"};
    /**
     * @parameter default-value="avro"
     */
    protected String avroNamespace;
    /**
     * @parameter default-value={"**\/*"}
     */
    protected String[] includes;
    /**
     * @parameter default-value={}
     */
    protected String[] excludes;
    /**
     * @parameter default-value={}
     */
    protected String[] extraLangs;
    /**
     * @parameter default-value={}
     */
    protected String[] avroExtraPaths;
    /**
     * The output directory for bundles.
     *
     * @parameter default-value="${project.build.directory}/generated-sources"
     */
    protected File outputDirectory;
    /**
     * The Maven project.
     *
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    protected MavenProject project;
    /**
     * Used to look up Artifacts in the remote repository.
     *
     * @component
     */
    protected ArtifactFactory factory;
    /**
     * Used to look up Artifacts in the remote repository.
     *
     * @component
     */
    protected ArtifactResolver resolver;
    /**
     * Location of the local repository.
     *
     * @parameter expression="${localRepository}"
     * @readonly
     * @required
     */
    protected ArtifactRepository local;
    /**
     * List of Remote Repositories used by the resolver.
     *
     * @parameter expression="${project.remoteArtifactRepositories}"
     * @readonly
     * @required
     */
    protected List<?> remoteRepos;

    /**
     * The Maven project.
     *
     * @parameter expression="${project}"
     * @required
     * @readonly
     */
    private MavenProject mavenProject;

    /**
     * The Maven session.
     *
     * @parameter expression="${session}"
     * @required
     * @readonly
     */
    private MavenSession mavenSession;

    /**
     * The Maven pluginManager.
     *
     * @component
     * @readonly
     */
    private BuildPluginManager pluginManager;

    private ClassLoader classLoader = null;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            File generatedSourcesDir = outputDirectory;
            String generatedSourcesDirName = generatedSourcesDir.getAbsolutePath();
            if (generatedSourcesDir.exists()) {
                generatedSourcesDir.delete();
            }
            generatedSourcesDir.mkdirs();

            String[] files = Util.scanDirectory(servicedir, includes, excludes);
            ConvertersWriter sw = new ConvertersWriter(avroNamespace, "com.unalarabe.urpc.plugin.util.avro");
            Generator generator = new Generator(avroNamespace);
            List<ExportService> services = new ArrayList<>();
            Set<ExportClass> classesForConvert = new HashSet<>();
            File servicesDir = createDir(path(generatedSourcesDirName, "java-service"));
            File avroDir = createDir(path(generatedSourcesDirName, "avro"));
            for (String file : files) {
                String clazz = file.replaceAll(SEPARATOR, ".").replaceAll(".java", "");
                ServiceConverter cc = new ServiceConverter(getClassLoader(), servicedir, basedir);
                ExportService service = cc.export(clazz);
                if (service != null) {
                    classesForConvert.addAll(Util.extractClasses(service));
                    ExportService rpcService = generator.generateRpcService(service);
                    AvdlWriter avdlWriter = new AvdlWriter(avroNamespace, path(generatedSourcesDirName, "avro"));
                    File avroFile = avdlWriter.writeService(rpcService);
                    Set<File> messagesAvroFiles = avdlWriter.getMessagesAvroFiles();
                    String result = "";
//                    try {
//                        List<String> cmd = Util.toList(avroPath,
//                                                       "-I", avroDir.getAbsolutePath(),
//                                                       "--java_out", servicesDir.getAbsolutePath(), avroFile.getAbsolutePath());
//                        messagesAvroFiles.forEach((messagesAvroFiles) -> cmd.add(messagesAvroFiles.getAbsolutePath()));
//                        getLog().info(String.join(" ", cmd));
//                        result = Util.executeCommand(avroFile.getParentFile(), cmd.toArray(new String[cmd.size()]));
//                    } catch (Exception ex) {
//                        getLog().error(ex.getMessage());
//                        throw new IllegalStateException(ex);
//                    }
                    getLog().info(result);

                    ProxyWriter proxyWriter = new ProxyWriter(avroNamespace, service);
                    writeFile(path(generatedSourcesDirName, "java-service", (avroNamespace + "." + Util.toNamespace(service.name)).replaceAll("\\.", SEPARATOR), proxyWriter.proxyClassName() + ".java"), proxyWriter.write());

                    JmsServiceWriter jmsServiceWriter = new JmsServiceWriter(avroNamespace, rpcService);
                    writeFile(path(generatedSourcesDirName, "java-service", (avroNamespace + "." + Util.toNamespace(service.name)).replaceAll("\\.", SEPARATOR), jmsServiceWriter.getJmsServiceName() + ".java"), jmsServiceWriter.write());

                    KafkaServiceWriter kafkaServiceWriter = new KafkaServiceWriter(avroNamespace, rpcService);
                    writeFile(path(generatedSourcesDirName, "java-service", (avroNamespace + "." + Util.toNamespace(service.name)).replaceAll("\\.", SEPARATOR), kafkaServiceWriter.getJmsServiceName() + ".java"), kafkaServiceWriter.write());

                    services.add(rpcService);
                }
            }
            File converterUtils = writeFile(path(generatedSourcesDirName, "java-service", "com", "unalarabe", "urpc", "plugin", "util", "avro", "AvroConverterUtils.java"), sw.writeServiceConverters("AvroConverterUtils", classesForConvert));
            File objectFactory = writeFile(path(generatedSourcesDirName, "java-service", "com", "unalarabe", "urpc", "plugin", "util", "avro", "ObjectFactoryImpl.java"), sw.writeObjectFactoryImpl());

            IDLProtocolMojo idlProtocolMojo = new IDLProtocolMojo();
            try {
                FieldUtils.writeField(idlProtocolMojo, "sourceDirectory", avroDir, true);
                FieldUtils.writeField(idlProtocolMojo, "outputDirectory", servicesDir, true);
                FieldUtils.writeField(idlProtocolMojo, "stringType", "String", true);
                FieldUtils.writeField(idlProtocolMojo, "project", new MavenProject() {
                                      @Override
                                      public void addCompileSourceRoot(String path) {
                                          System.out.println("SKIP addCompileSourceRoot: " + path);
                                      }

                                  }, true);

            } catch (IllegalAccessException ex) {
                getLog().error(ex);
                throw new MojoExecutionException(ex.getMessage(), ex);
            }
            idlProtocolMojo.execute();

            createDir(path(generatedSourcesDirName, "java-client"));
            String[] protoFiles = Util.scanDirectory(avroBaseDir, avroFiles, new String[0]);
            for (String avroFile : protoFiles) {
                System.out.println("avroFile = " + avroFile);
                if (avroFile.endsWith(".avdl")) {
                    String fullpath = "";
                    try {
                        File file = new File(avroBaseDir, avroFile);
                        AvdlFileParser parsed = new AvdlFileParser(file, Collections.EMPTY_LIST);
                        List<MetaService> metaServices = parsed.getServices();
                        List<MetaClass> metaClasses = parsed.getClasses();
                        List<String> metaClassesNames = metaClasses.stream().map(metaClass -> metaClass.getFullName()).collect(Collectors.toList());
                        System.out.println("metaServices=" + metaServices);
                        System.out.println("classes=" + metaClasses);
                        String javaPackage = parsed.getJavaPackage();

                        Map<String, ClientWriter> writesMap = new HashMap<>();
                        for (MetaService metaService : metaServices) {
                            ClientWriter cw = new ClientWriter(javaPackage, metaService, metaClasses, false);
                            writeFile(path(generatedSourcesDirName, "java-client", javaPackage.replaceAll("\\.", SEPARATOR), cw.getClassName() + ".java"), cw.writeService());
                            writeFile(path(generatedSourcesDirName, "java-client", javaPackage.replaceAll("\\.", SEPARATOR), cw.getProviderClassName() + ".java"), cw.writeServiceProvider());
                            for (String usedClass : cw.getUsedClasses()) {
                                System.out.println("used class=" + usedClass);
                                writesMap.put(usedClass, cw);
                            }
                            String requestName = javaPackage + "." +  metaService.name + "Request";
                            String responseName = javaPackage + "." + metaService.name + "Response";
                            if (metaClassesNames.contains(requestName)) {
                                writesMap.put(requestName, cw);
                            }
                            if (metaClassesNames.contains(responseName)) {
                                writesMap.put(responseName, cw);
                            }
                        }
                        for (MetaClass protoClass : metaClasses) {
                            ClientWriter clientWriter = writesMap.get(protoClass.getFullName());
                            System.out.println("metaClass=" + protoClass.getFullName());
                            ClientClassWriter ccw = new ClientClassWriter(protoClass, clientWriter);
                            writeFile(path(generatedSourcesDirName, "java-client", ccw.getPackage().replaceAll("\\.", SEPARATOR), ccw.getName() + ".java"), ccw.writeClass());
                        }

                    } catch (IOException | ParseException ex) {
                        getLog().error(ex.getMessage());
                        throw new MojoExecutionException(fullpath + ": " + ex.getMessage(), ex);
                    }
                }
            }
//                    createDir(path(generatedSourcesDirName, "java-client"));
//                    protoFile = avroBaseDir.getAbsolutePath() + SEPARATOR + protoFile;
//                    String result = "";
//                    try {
//                        File f = new File(protoFile);
//                        List<String> protoIncludes = loadIncludes(f);
//                        if (avroExtraPaths != null) {
//                            protoIncludes.addAll(Arrays.asList(avroExtraPaths));
//                        }
//                        List<File> protoFileIncludes = convertNamesToFiles(protoIncludes);
//                        ProtoFileParser parsed = new ProtoFileParser(f, protoFileIncludes);
//
//                        List<MetaService> protoServices = parsed.getServices();
//                        List<MetaClass> protoClasses = parsed.getClasses();
//                        String javaPackage = parsed.getJavaPackage();
//                        boolean muliplyFiles = parsed.isMuliplyFiles();
//                        List<String> cmd = Util.toList(avroPath, "--java_out", path(generatedSourcesDirName, "java-client"));
//
//                        String relativePath = parsed.getJavaPackage().replaceAll("\\.", SEPARATOR) + SEPARATOR + f.getName();
//                        String absolutePath = f.getAbsolutePath();
//                        if (absolutePath.endsWith(relativePath)) {
//                            cmd.add("-I");
//                            cmd.add(absolutePath.substring(0, absolutePath.length() - relativePath.length()));
//                        } else {
//                            cmd.add("-I");
//                            cmd.add(f.getParentFile().getAbsolutePath());
//                        }
//
//                        for (File protoFileInclude : protoFileIncludes) {
//                            cmd.add("-I");
//                            cmd.add(protoFileInclude.getAbsolutePath());
//                        }
//
//                        cmd.add(f.getAbsolutePath());
//                        getLog().info(String.join(" ", cmd));
//                        result = Util.executeCommand(f.getParentFile(), cmd.toArray(new String[cmd.size()]));
//                        Map<String, ClientWriter> writesMap = new HashMap<>();
//                        for (MetaService protoService : protoServices) {
//                            ClientWriter cw = new ClientWriter(javaPackage, protoService, protoClasses, !muliplyFiles);
//                            writeFile(path(generatedSourcesDirName, "java-client", javaPackage.replaceAll("\\.", SEPARATOR), cw.getClassName() + ".java"), cw.writeService());
//                            writeFile(path(generatedSourcesDirName, "java-client", javaPackage.replaceAll("\\.", SEPARATOR), cw.getProviderClassName() + ".java"), cw.writeServiceProvider());
//                            for (String usedClass : cw.getUsedClasses()) {
//                                writesMap.put(usedClass, cw);
//                            }
//                        }
//                        for (MetaClass protoClass : protoClasses) {
//                            ClientWriter clientWriter = writesMap.get(protoClass.getFullName());
//                            ClientClassWriter ccw = new ClientClassWriter(protoClass, clientWriter);
//                            writeFile(path(generatedSourcesDirName, "java-client", ccw.getPackage().replaceAll("\\.", SEPARATOR), ccw.getName() + ".java"), ccw.writeClass());
//                        }
//                    } catch (Exception ex) {
//                        getLog().error(ex.getMessage());
//                        throw new IllegalStateException(ex);
//                    }
//                    getLog().info(result);
//                }
//            }
        } catch (PluginException | ConverterException ex) {
            getLog().error(ex.getMessage(), ex);
        }
    }

    private List<File> convertNamesToFiles(List<String> fileNames) {
        List<File> result = new ArrayList<>();
        for (String fileName : fileNames) {
            result.add(new File(fileName));
        }
        return result;
    }

    private List<String> loadIncludes(File f) throws PluginException {
        List<String> result = new ArrayList<>();
        File propertiesFile = findPropertiesFile(f);
        Properties properties = getProperties(propertiesFile);
        if (properties != null) {
            String extrapaths = properties.getProperty("extrapaths");
            if (extrapaths != null) {
                String[] parts = extrapaths.split(" ");
                for (String path : parts) {
                    File extraProtoFile = resolvePath(f.getParentFile(), path);
                    if (extraProtoFile == null) {
                        throw new PluginException("Can't find " + path + " listed in " + propertiesFile.getAbsolutePath());
                    }
                    result.add(extraProtoFile.getAbsolutePath());
                }
            }
        }
        return result;
    }

    private Set<String> findProtoDependencies(File f) {
        Set<String> allDependencies = new HashSet<>();
        try {
            ProtoFile parsed = ProtoParser.parseUtf8(f);
            String absolutePath = f.getAbsolutePath();
            String protoPath = parsed.packageName().replaceAll("\\.", SEPARATOR) + SEPARATOR + f.getName();
            if (!absolutePath.endsWith(protoPath)) {
                return allDependencies;
            }
            String basePath = absolutePath.substring(0, absolutePath.length() - protoPath.length());

            Set<String> dependencies = new HashSet<>();
            dependencies.addAll(parsed.dependencies());
            dependencies.addAll(parsed.publicDependencies());
            dependencies.removeAll(dependencies);

        } catch (IOException ex) {
            getLog().error(ex);
        }
        return null;
    }

    private File resolvePath(File baseDir, String path) throws PluginException {
        try {
            return new File(baseDir, path).getCanonicalFile();
        } catch (IOException ex) {
            throw new PluginException(ex);
        }
    }

    private File findPropertiesFile(File protoFile) throws PluginException {
        try {
            String baseName = FilenameUtils.getBaseName(protoFile.getName());
            File propertiesFile = new File(path(protoFile.getParentFile().getAbsolutePath(), baseName + ".properties"));
            if (propertiesFile.exists()) {
                return propertiesFile;
            }
            return null;
        } catch (Exception ex) {
            throw new PluginException(ex);
        }
    }

    private Properties getProperties(File propertiesFile) throws PluginException {
        try {
            Properties properties = new Properties();
            if (propertiesFile != null) {
                properties.load(new FileInputStream(propertiesFile));
            }
            return properties;
        } catch (Exception ex) {
            throw new PluginException(ex);
        }
    }

    private File writeFile(String path, String content) {
        try {
            File file = new File(path);
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.append(content);
            fileWriter.flush();
            return file;
        } catch (IOException ex) {
            getLog().error(ex);
        }
        return null;
    }

    private File createDir(String path) {
        File file = new File(path);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
        return file;
    }

    protected ClassLoader getClassLoader() {
        if (classLoader == null) {
            try {
                List<Dependency> dependencies = project.getDependencies();
                List<URL> url = new ArrayList<>();
                url.add(outputDirectory.toURI().toURL());
                for (Dependency d : dependencies) {
                    String scope = d.getScope();
                    if (!Artifact.SCOPE_TEST.equals(scope)) {
                        Artifact artifact = getArtifact(d.getGroupId(), d.getArtifactId(), d.getVersion(), d.getType(), d.getClassifier());
                        url.add(artifact.getFile().toURI().toURL());
                    }
                }

                URL[] classpath = url.toArray(new URL[url.size()]);

                URLClassLoader uc = new URLClassLoader(classpath, this.getClass().getClassLoader()) {
                    @Override
                    public Class<?> loadClass(String name) throws ClassNotFoundException {
                        getLog().debug("Loading Class for compile [" + name + "]");
                        Class<?> c = super.loadClass(name);
                        getLog().debug("Loading Class for compile [" + name + "] found [" + c + "]");
                        return c;
                    }

                    @Override
                    protected Class<?> findClass(String name) throws ClassNotFoundException {
                        getLog().debug("Finding Class for compile [" + name + "]");
                        Class<?> c = super.findClass(name);
                        getLog().debug("Finding Class for compile [" + name + "] found [" + c + "]");
                        return c;
                    }
                };

                return uc;

            } catch (MalformedURLException | MojoExecutionException ex) {
                getLog().error(ex);
            }
        }
        return classLoader;
    }

    protected Artifact getArtifact(String groupId, String artifactId, String version, String type, String classifier) throws MojoExecutionException {
        Artifact artifact;
        VersionRange vr;

        try {
            vr = VersionRange.createFromVersionSpec(version);
        } catch (InvalidVersionSpecificationException e) {
            vr = VersionRange.createFromVersion(version);
        }

        if (StringUtils.isEmpty(classifier)) {
            artifact = factory.createDependencyArtifact(groupId, artifactId, vr, type, null,
                                                        Artifact.SCOPE_COMPILE);
        } else {
            artifact = factory.createDependencyArtifact(groupId, artifactId, vr, type,
                                                        classifier, Artifact.SCOPE_COMPILE);
        }
        try {
            resolver.resolve(artifact, remoteRepos, local);
        } catch (ArtifactResolutionException e) {
            throw new MojoExecutionException("Unable to resolve artifact.", e);
        } catch (ArtifactNotFoundException e) {
            throw new MojoExecutionException("Unable to find artifact.", e);
        }
        return artifact;
    }
}
