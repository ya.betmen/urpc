package com.unalarabe.urpc.maven.plugin.proto2.writer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.unalarabe.urpc.converter.structure.ExportArray;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportEnum;
import com.unalarabe.urpc.converter.structure.ExportField;
import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.converter.structure.ExportMethod;
import com.unalarabe.urpc.converter.structure.ExportParameter;

import static com.unalarabe.urpc.maven.plugin.Util.format;

import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.converter.structure.ExportSet;
import com.unalarabe.urpc.exception.PluginException;
import com.unalarabe.urpc.maven.plugin.Util;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class Proto2Writer {

    public static String TEMPLATE
        = "syntax = \"proto2\";\n"
          + "\n"
          + "package {javaNamespace};\n"
          + "\n"
          + "//option optimize_for = LITE_RUNTIME;\n"
          + "option cc_generic_services = true;\n"
          + "option java_generic_services = true;\n"
          + "\n"
          + "option java_package = \"{javaNamespace}\";\n"
          //            + "option java_multiple_files = true;\n"
          + "\n"
          + "{imports}"
          + "{messages}"
          + "\n"
          + "service {serviceName} {\n"
          + "\n"
          + "{methods}"
          + "}\n"
          + "\n";
    public static String TEMPLATE_PARAMETERS
        = "{parameterType}";
    public static String TEMPLATE_METHOD
        = "rpc {methodName}({parameters}) returns ({resultType}) {} {throws}";
    public static String TEMPLATE_STRUCT
        = "message {className} {\n"
          + "{fields}"
          + "}\n";
    public static String TEMPLATE_ENUM
        = "enum {className} {\n"
          + "{entries}"
          + "}\n"
          + "\n";
    public static String TEMPLATE_FILED
        = "{modifier}{proto2Type} {fieldName} = {number};";

    private final String baseDir;
    private final String proto2Namespace;
    private final List<File> messageFiles = new ArrayList<>();
    private final Set<String> processed = new HashSet<>();
    private final Map<String, String> typesMap = new HashMap<>();

    public Proto2Writer(String proto2Namespace, String baseDir) {
        this.baseDir = baseDir;
        this.proto2Namespace = proto2Namespace;
    }

    private String toPath(String namespace) {
        return namespace.replaceAll("\\.", File.separator);
    }

    public String writeStructBody(ExportClass ec) {
        String result;
        if (Util.isExistsInProto2(ec)) {
            result = null;
        } else if (Util.isCollection(ec)) {
            processed.remove(ec.name);
            ExportClass simpleClass = new ExportClass(ec.name);
            simpleClass.fields.addAll(ec.fields);
            result = writeStructBody(simpleClass);
        } else if (ec instanceof ExportEnum) {
            result = format(TEMPLATE_ENUM, "className", toClassName(ec), "entries", writeEntries((ExportEnum) ec));
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("\n").append(format(TEMPLATE_STRUCT, "className", toClassName(ec), "fields", writeFields(ec)));
            result = sb.toString();
        }
        return result;
    }

    public String writeImport(ExportClass ec) {
        String namespace = proto2Namespace + "." + toNamespace(ec.name);
        return "import \"" + Util.path(toPath(namespace), toClassName(ec) + ".proto") + "\";\n";
    }

    public String writeStruct(ExportClass ec) throws PluginException {
        try {
            System.out.println("writeStruct: " + ec.name + " => " + ec.fields);
            String namespace = proto2Namespace + "." + toNamespace(ec.name);
            String toClassName = toClassName(ec);
            String messageFilePath = Util.path(toPath(namespace), toClassName + ".proto");

            String result;
            if ("void".equals(ec.name) || Object.class.getCanonicalName().equals(ec.name) || Util.isExistsInProto2(ec)) {
                result = "";
            } else if (processed.contains(ec.name) && !Util.isExistsInProto2(ec)) {
                result = writeImport(ec);
            } else {
                result = writeImport(ec);
                processed.add(ec.name);
                typesMap.put(ec.name, toClassName);
                String body = writeStructBody(ec);
                if (body != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("syntax = \"proto2\";\n");
                    sb.append("\n");
                    sb.append("package ").append(namespace).append(";\n");
                    sb.append("\n");
                    sb.append("option java_package = \"").append(namespace).append("\";\n");
                    sb.append("option java_multiple_files = true;\n");
                    sb.append("\n");
                    for (ExportField field : ec.fields) {
                        if (!field.isIgnored("proto2")) {
                            sb.append(writeStruct(field.type));
                        }
                    }
                    sb.append(body);
                    File messageProtoFile = new File(baseDir, messageFilePath);
                    FileUtils.writeStringToFile(messageProtoFile, sb.toString(), "UTF-8");
                    messageFiles.add(messageProtoFile);
                }
            }
            return result;
        } catch (Exception ex) {
            throw new PluginException(ex);
        }
    }

    public String writeEntries(ExportEnum ee) {
        StringBuilder sb = new StringBuilder();

        List<String> entries = new ArrayList<>(ee.entries);

        for (int i = 0; i < entries.size(); i++) {
            String entry = entries.get(i);
            entry = Util.getClassName(ee.name).toUpperCase() + "_" + entry;
            sb.append("    ").append(entry).append(" = ").append(i).append(";\n");
        }
        return sb.toString();
    }

    public String writeFields(ExportClass ec) {
        StringBuilder sb = new StringBuilder();
        int number = 0;
        for (ExportField field : ec.fields) {
            if (!field.isIgnored("proto2")) {
                number++;
                String modifier;
                if (field.required || Util.isSimple(field.type)) {
                    modifier = "required ";
                } else {
                    modifier = "optional ";
                }
                if (Util.isCollection(field.type)) {
                    modifier = "";
                }
                if (field.type instanceof ExportMap) {
                    modifier = "";
                }
                boolean full = false;
                if (!Util.isExistsInProto2(field.type) && !toNamespace(ec.name).equals(toNamespace(field.type.name))) {
                    System.out.println(field.type.name);
                    full = true;
                }
                String protobuf2Type = toProtobuf2Type(field.type, full);
                sb.append("    ").append(format(TEMPLATE_FILED,
                                                "number", number,
                                                "modifier", modifier,
                                                "proto2Type", protobuf2Type,
                                                "fieldName", field.name)).append("\n");
            }
        }
        return sb.toString();
    }

    public File writeService(ExportService service) throws PluginException {
        try {
            StringBuilder methods = new StringBuilder();

            Set<String> imports = new HashSet<>();
            StringBuilder messages = new StringBuilder();

            Set<String> methodNames = service.getMethodNames();

            Set<ExportClass> allClasses = new HashSet<>();
            for (String methodName : methodNames) {
                List<ExportMethod> exportMethods = service.getMethods(methodName);
                if (exportMethods.size() != 1) {
                    throw new IllegalStateException("Overloaded methods: " + exportMethods);
                }
                for (ExportMethod exportMethod : exportMethods) {
                    allClasses.add(exportMethod.type);
                    for (ExportParameter parameter : exportMethod.parameters) {
                        allClasses.add(parameter.type);
                    }
                }
            }
            allClasses.addAll(service.getExtraClasses());

            Set<ExportClass> newClasses = new HashSet<>();
            do {
                newClasses.clear();
                for (ExportClass clazz : allClasses) {
                    for (ExportField field : clazz.fields) {
                        newClasses.add(field.type);
                    }
                }
                newClasses.removeAll(allClasses);
                allClasses.addAll(newClasses);
            } while (!newClasses.isEmpty());

            for (ExportClass clazz : allClasses) {
                if (clazz.rpcRequest || clazz.rpcResponse || clazz.rpcError) {
                    String body = writeStructBody(clazz);
                    if (body != null) {
                        messages.append(body);
                    }
                } else {
                    imports.add(writeStruct(clazz));
                }
            }

            for (String methodName : methodNames) {
                List<ExportMethod> exportMethods = service.getMethods(methodName);
                if (exportMethods.size() == 1) {
                    ExportMethod method = exportMethods.get(0);
                    StringBuilder parameters = new StringBuilder();
                    for (int i = 0; i < method.parameters.size(); i++) {
                        ExportParameter parameter = method.parameters.get(i);
                        boolean full = !toNamespace(parameter.type.name).equals(service.name);
                        if (i > 0) {
                            parameters.append(", ");
                        }
                        parameters.append(format(TEMPLATE_PARAMETERS,
                                                 "number", (i + 1),
                                                 "parameterType", toClassName(parameter.type, false),
                                                 "parameterName", parameter.name));
                    }
                    boolean full = !toNamespace(method.type.name).equals(service.name);
                    methods.append("    ").append(format(TEMPLATE_METHOD,
                                                         "resultType", toClassName(method.type, false),
                                                         "methodName", method.name,
                                                         "parameters", parameters,
                                                         "throws", "")).append("\n\n");
                } else {
                    System.out.println("MULTI METHOD: " + methodName);
                }
            }
            String namespace = proto2Namespace + "." + toNamespace(service.name);
            String content = format(TEMPLATE,
                                    "javaNamespace", namespace,
                                    "serviceName", toServiceName(service),
                                    "imports", String.join("", imports),
                                    "messages", messages,
                                    "methods", methods);
            File protoFile = new File(Util.path(baseDir, toPath(namespace)), toServiceName(service) + ".proto");
            FileUtils.writeStringToFile(protoFile, content, "UTF-8");
            return protoFile;
        } catch (IOException ex) {
            throw new PluginException(ex);
        }
    }

    public Set<File> getMessagesProtoFiles() {
        return new HashSet<>(messageFiles);
    }

    public static String toProtobuf2TypeBasic(String type) {
        switch (type) {
            case "boolean": {
                return "bool";
            }
            case "java.lang.Boolean": {
                return "bool";
            }
            case "byte": {
                return "int32";
            }
            case "java.lang.Byte": {
                return "int32";
            }
            case "char": {
                return "int32";
            }
            case "java.lang.Character": {
                return "int32";
            }
            case "int": {
                return "int32";
            }
            case "java.lang.Integer": {
                return "int32";
            }
            case "long": {
                return "int64";
            }
            case "java.lang.Long": {
                return "int64";
            }
            case "float": {
                return "float";
            }
            case "java.lang.Float": {
                return "float";
            }
            case "double": {
                return "double";
            }
            case "java.lang.Double": {
                return "double";
            }
            case "java.lang.String": {
                return "string";
            }
            case "java.util.Date": {
                return "int64";
            }
            default: {
                return type;
//                    throw new IllegalStateException("");
            }
        }
    }

    public String toProtobuf2TypeBasic(ExportClass ec, boolean full) {
        if (Util.isExistsInProto2(ec)) {
            return toProtobuf2TypeBasic(ec.name);
        } else {
            return toClassName(ec, full);
        }
    }

    public String toProtobuf2Type(ExportClass ec) {
        return toProtobuf2Type(ec, false);
    }

    public String toProtobuf2Type(ExportClass ec, boolean full) {
        String protobuf2TypeBasic = toProtobuf2TypeBasic(ec, full);
        if (Util.isCollection(ec)) {
            if (ec instanceof ExportArray) {
                return format("repeated {of}", "of", protobuf2TypeBasic);
            } else if (ec instanceof ExportList) {
                return format("repeated {of}", "of", protobuf2TypeBasic);
            } else if (ec instanceof ExportSet) {
                return format("repeated {of}", "of", protobuf2TypeBasic);
            } else if (ec instanceof ExportMap) {
                ExportMap em = (ExportMap) ec;
                String keyType;
                if (Util.isExistsInProto2(em.keyType)) {
                    keyType = toProtobuf2TypeBasic(em.keyType);
                } else {
                    keyType = toClassName(em.keyType, full);
                }
                return format("map<{key}, {value}>", "key", keyType, "value", protobuf2TypeBasic);
            } else {
                throw new IllegalStateException();
            }
        } else {
            return protobuf2TypeBasic;
        }
    }

    public static String toServiceName(ExportService es) {
        int lastIndexOfDot = es.name.lastIndexOf(".");
        return es.name.substring(lastIndexOfDot + 1);
    }

    public String toClassName(String type, boolean full) {
//        full = false;
        String result;
        if (Util.isExistsInProto2(type)) {
            result = toProtobuf2TypeBasic(type);
        } else if (full) {
            result = proto2Namespace + "." + type;
        } else {
            int lastIndexOfDot = type.lastIndexOf(".");
            result = type.substring(lastIndexOfDot + 1);
        }
        return result;
    }

    public static String toNamespace(String type) {
        int lastIndexOfDot = type.lastIndexOf(".");
        return lastIndexOfDot >= 0 ? type.substring(0, lastIndexOfDot) : type;
    }

    public String toClassName(ExportClass ec) {
        return toClassName(ec, false);
    }

    public String toClassName(ExportClass ec, boolean full) {
        return toClassName(ec.name, full);
    }

}
