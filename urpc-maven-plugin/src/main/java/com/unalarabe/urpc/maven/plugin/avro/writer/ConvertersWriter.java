package com.unalarabe.urpc.maven.plugin.avro.writer;

import java.util.HashSet;
import java.util.Set;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.exception.ConvertException;
import com.unalarabe.urpc.factory.CreationException;
import com.unalarabe.urpc.factory.DefaultObjectFactory;
import com.unalarabe.urpc.factory.ObjectFactory;
import com.unalarabe.urpc.service.RpcMessage;
import com.unalarabe.urpc.util.RpcParser;
import com.unalarabe.urpc.util.ServiceUtil;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ConvertersWriter {

    private final String proto2Package;
    private final String converterPackage;

    private Set<ExportClass> processedConverters = new HashSet<>();
    private WriterFactory writerFactory;

    public ConvertersWriter(String proto2Package, String converterPackage) {
        this.proto2Package = proto2Package;
        this.converterPackage = converterPackage;
        this.writerFactory = new WriterFactory(processedConverters, proto2Package);
    }

    public String writeClassConverters(ExportClass ec) {
        ConverterWriter writer = writerFactory.createWriter(ec);
        System.out.println("CONVERT: " + ec.name + " => " + writer);
        return writer != null ? writer.write() : null;
    }

    public String writeServiceConverters(String className, Set<ExportClass> classes) {
        StringBuilder result = new StringBuilder();
        result.append("package " + converterPackage + ";\n\npublic class " + className + " {\n"
                      + "\n"
                      + "    private static java.util.List<java.lang.Integer> convertListOfCharsToIntegers(java.util.List<java.lang.Character> listOfChars) {\n"
                      + "        if (listOfChars == null){\n"
                      + "            return null;\n"
                      + "        }\n"
                      + "        java.util.List<java.lang.Integer> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Integer.class);\n"
                      + "        for (java.lang.Character c : listOfChars) {\n"
                      + "            result.add(convertCharToInteger(c));\n"
                      + "        }\n"
                      + "        return result;\n"
                      + "    }\n"
                      + "\n"
                      + "    private static java.util.List<java.lang.Character> convertListOfIntegersToChars(java.util.List<java.lang.Integer> listOfIntegers) {\n"
                      + "        if (listOfIntegers == null){\n"
                      + "            return null;\n"
                      + "        }\n"
                      + "        java.util.List<java.lang.Character> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Character.class);\n"
                      + "        for (java.lang.Integer i : listOfIntegers) {\n"
                      + "            result.add(convertIntegerToChar(i));\n"
                      + "        }\n"
                      + "        return result;\n"
                      + "    }\n"
                      + "\n"
                      + "    private static java.lang.Integer convertCharToInteger(java.lang.Character c) {\n"
                      + "        return c != null ? (int) c : null;\n"
                      + "    }\n"
                      + "\n"
                      + "    private static java.lang.Character convertIntegerToChar(java.lang.Integer i) {\n"
                      + "        return i != null ? (char) i.intValue() : null;\n"
                      + "    }\n"
                      + "    private static java.util.List<java.lang.Integer> convertListOfBytesToIntegers(java.util.List<java.lang.Byte> listOfChars) {\n"
                      + "        if (listOfChars == null){\n"
                      + "            return null;\n"
                      + "        }\n"
                      + "        java.util.List<java.lang.Integer> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Integer.class);\n"
                      + "        for (java.lang.Byte c : listOfChars) {\n"
                      + "            result.add(c.intValue());\n"
                      + "        }\n"
                      + "        return result;\n"
                      + "    }\n"
                      + "\n"
                      + "    private static java.util.List<java.lang.Byte> convertListOfIntegersToBytes(java.util.List<java.lang.Integer> listOfIntegers) {\n"
                      + "        if (listOfIntegers == null){\n"
                      + "            return null;\n"
                      + "        }\n"
                      + "        java.util.List<java.lang.Byte> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Byte.class);\n"
                      + "        for (java.lang.Integer i : listOfIntegers) {\n"
                      + "            result.add(i.byteValue());\n"
                      + "        }\n"
                      + "        return result;\n"
                      + "    }\n"
                      + "\n"
                      + "    private static Long convertDateToTimestamp(java.util.Date date){\n"
                      + "        return date == null ? null : date.getTime();\n"
                      + "    }\n"
                      + "\n"
                      + "    private static java.util.Date convertTimestampToDate(java.lang.Long timestamp){\n"
                      + "        return timestamp == null ? null : new java.util.Date(timestamp);\n"
                      + "    }"
                      + "\n"
                      + "    private static java.util.List<java.util.Date> convertListOfDateToTimestamps(java.util.List<java.lang.Long> listOfTimestamp){\n"
                      + "        if (listOfTimestamp == null){\n"
                      + "            return null;\n"
                      + "        }\n"
                      + "        java.util.List<java.util.Date> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.List.class, java.util.Date.class);\n"
                      + "        for (java.lang.Long l : listOfTimestamp) {\n"
                      + "            result.add(convertTimestampToDate(l));\n"
                      + "        }\n"
                      + "        return result;\n"
                      + "    }\n"
                      + "\n"
                      + "    private static java.util.List<java.lang.Long> convertListOfTimestampToDates(java.util.List<java.util.Date> listOfDates){\n"
                      + "        if (listOfDates == null){\n"
                      + "            return null;\n"
                      + "        }\n"
                      + "        java.util.List<java.lang.Long> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.Long.class);\n"
                      + "        for (java.util.Date d : listOfDates) {\n"
                      + "            result.add(convertDateToTimestamp(d));\n"
                      + "        }\n"
                      + "        return result;\n"
                      + "    }\n"
                      + "\n"
//                      + "    public static java.lang.String convertCharSequenceToString(java.lang.CharSequence sequence) {\n"
//                      + "        return sequence != null ? sequence.toString() : null;\n"
//                      + "    }\n"
//                      + "\n"
//                      + "    public static java.util.List<CharSequence> convertCollectionOfStringsToListOfCharSequence(java.util.Collection<java.lang.String> collectionOfStrings) {\n"
//                      + "        if (collectionOfStrings == null) {\n"
//                      + "            return null;\n"
//                      + "        }\n"
//                      + "        java.util.List<CharSequence> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.CharSequence.class);\n"
//                      + "        result.addAll(collectionOfStrings);\n"
//                      + "        return result;\n"
//                      + "    }\n"
//                      + "\n"
//                      + "    public static java.util.Set<CharSequence> convertCollectionOfStringsToSetOfCharSequence(java.util.Collection<java.lang.String> collectionOfStrings) {\n"
//                      + "        if (collectionOfStrings == null) {\n"
//                      + "            return null;\n"
//                      + "        }\n"
//                      + "        java.util.Set<CharSequence> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.Set.class, java.lang.CharSequence.class);\n"
//                      + "        result.addAll(collectionOfStrings);\n"
//                      + "        return result;\n"
//                      + "    }\n"
//                      + "\n"
//                      + "    public static java.util.List<String> convertCollectionOfCharSequenceToListOfStrings(java.util.Collection<java.lang.CharSequence> collectionOfCharSequences) {\n"
//                      + "        if (collectionOfCharSequences == null) {\n"
//                      + "            return null;\n"
//                      + "        }\n"
//                      + "        java.util.List<String> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.List.class, java.lang.String.class);\n"
//                      + "        for (CharSequence charSequence : collectionOfCharSequences) {\n"
//                      + "            result.add(charSequence != null ? charSequence.toString() : null);\n"
//                      + "        }\n"
//                      + "        return result;\n"
//                      + "    }\n"
//                      + "\n"
//                      + "    public static java.util.Set<String> convertCollectionOfCharSequenceToSetOfStrings(java.util.Collection<java.lang.CharSequence> collectionOfCharSequences) {\n"
//                      + "        if (collectionOfCharSequences == null) {\n"
//                      + "            return null;\n"
//                      + "        }\n"
//                      + "        java.util.Set<String> result = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createCollection(java.util.Set.class, java.lang.String.class);\n"
//                      + "        for (CharSequence charSequence : collectionOfCharSequences) {\n"
//                      + "            result.add(charSequence != null ? charSequence.toString() : null);\n"
//                      + "        }\n"
//                      + "        return result;\n"
//                      + "    }\n"
//                      + "\n"
        );
        result.append("\n");
        for (ExportClass clazz : classes) {
            String converter = this.writeClassConverters(clazz);
            if (converter != null) {
                result.append(converter);
            }
        }
        result.append("\n}\n");
        return result.toString();
    }

    public String writeObjectFactoryImpl() {
        return ""
               + "package " + converterPackage + ";\n"
               + "\n"
               + "import java.lang.reflect.InvocationTargetException;\n"
               + "import java.util.logging.Level;\n"
               + "import java.util.logging.Logger;\n"
               + "import " + CreationException.class.getCanonicalName() + ";\n"
               + "import " + ObjectFactory.class.getCanonicalName() + ";\n"
               + "public class ObjectFactoryImpl {\n"
               + "\n"
               + "    private static final Logger LOG = Logger.getLogger(ObjectFactoryImpl.class.getName());\n"
               + "\n"
               + "    private static ObjectFactory instance;\n"
               + "\n"
               + "    private static synchronized void init() {\n"
               + "        if (instance == null) {\n"
               + "            try {\n"
               + "                String factoryClassName = System.getProperty(\"com.unalarabe.urpc.factory.ObjectFactory.class\", " + DefaultObjectFactory.class.getCanonicalName() + ".class.getCanonicalName());\n"
               + "                Class<?> factoryClass = ObjectFactoryImpl.class.getClassLoader().loadClass(factoryClassName);\n"
               + "                instance = (ObjectFactory) factoryClass.getConstructor().newInstance();\n"
               + "            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {\n"
               + "                LOG.log(Level.SEVERE, null, ex);\n"
               + "            }\n"
               + "        }\n"
               + "    }\n"
               + "\n"
               + "    private static ObjectFactory getInstance() {\n"
               + "        if (instance == null) {\n"
               + "            init();\n"
               + "        }\n"
               + "        return instance;\n"
               + "    }\n"
               + "\n"
               + "    public static <T> T createObject(Class<T> clazz) {\n"
               + "        try {\n"
               + "            return getInstance().createObject(clazz);\n"
               + "        } catch (CreationException ex) {\n"
               + "            LOG.log(Level.SEVERE, ex.getMessage(), ex);\n"
               + "            throw new IllegalStateException(ex);\n"
               + "        }\n"
               + "    }\n"
               + "\n"
               + "    public static <T, L extends java.util.Collection<T>> L createCollection(Class<L> collectionClazz, Class<T> clazz) {\n"
               + "        try {\n"
               + "            return getInstance().createCollection(collectionClazz, clazz);\n"
               + "        } catch (CreationException ex) {\n"
               + "            LOG.log(Level.SEVERE, ex.getMessage(), ex);\n"
               + "            throw new IllegalStateException(ex);\n"
               + "        }\n"
               + "    }\n"
               + "\n"
               + "    public static <K, V, M extends java.util.Map<K, V>> M createMap(Class<M> mapClazz, Class<K> keyClazz, Class<V> valueClazz) {\n"
               + "        try {\n"
               + "            return getInstance().createMap(mapClazz, keyClazz, valueClazz);\n"
               + "        } catch (CreationException ex) {\n"
               + "            LOG.log(Level.SEVERE, ex.getMessage(), ex);\n"
               + "            throw new IllegalStateException(ex);\n"
               + "        }\n"
               + "    }\n"
               + "}\n"
               + "\n";
    }

}
