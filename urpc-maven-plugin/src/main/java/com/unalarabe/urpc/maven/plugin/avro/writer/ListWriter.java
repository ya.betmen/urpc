package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.maven.plugin.Util;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ListWriter extends CollectionWriter {

    public static final String TEMPLATE_COPY_LIST_TO
            = "        for ({originalItemClass} o : original){\n"
            + "            th.add({converterToName}(o));\n"
            + "        }\n";
    public static final String TEMPLATE_COPY_LIST_FROM
            = "        for ({proto2ItemClass} t : th) {\n"
            + "            original.add({converterFromName}(t));\n"
            + "        }\n"
            + "";
    public static final String TEMPLATE_COPY_LIST_SIMPLE_TO
            = "        for ({originalItemClass} o : original){\n"
            + "            th.add(o);\n"
            + "        }\n";
    public static final String TEMPLATE_COPY_LIST_SIMPLE_FROM
            = "        original.addAll(th);\n";

    public ListWriter(ExportList exportList, WriterFactory processed) {
        super(exportList, processed);
    }

    @Override
    protected String getTemplateCopyTo() {
        if (Util.isExistsInAvro(exportCollection.name)) {
            return TEMPLATE_COPY_LIST_SIMPLE_TO;
        } else {
            return TEMPLATE_COPY_LIST_TO;
        }
    }

    @Override
    protected String getTemplateCopyFrom() {
        if (Util.isExistsInAvro(exportCollection.name)) {
            return TEMPLATE_COPY_LIST_SIMPLE_FROM;
        } else {
            return TEMPLATE_COPY_LIST_FROM;
        }
    }

}
