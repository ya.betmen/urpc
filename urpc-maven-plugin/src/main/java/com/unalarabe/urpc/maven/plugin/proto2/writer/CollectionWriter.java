package com.unalarabe.urpc.maven.plugin.proto2.writer;

import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportCollection;
import com.unalarabe.urpc.maven.plugin.Util;

import static com.unalarabe.urpc.maven.plugin.Util.format;
import static com.unalarabe.urpc.maven.plugin.proto2.writer.ConverterWriter.toName;
import static com.unalarabe.urpc.maven.plugin.proto2.writer.ConverterWriter.writeConverterFromName;
import static com.unalarabe.urpc.maven.plugin.proto2.writer.ConverterWriter.writeConverterToName;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public abstract class CollectionWriter extends ConverterWriter {

    public static final String TEMPLATE_COLLECTION_TO_PROTO
        = "    public static {proto2Class} {converterToName}({originalClass} original) {\n"
        + "        if (original == null) {\n"
        + "            return null;\n"
        + "        }\n"
        + "        {proto2Class} th = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createCollection({collectionClass}.class, {proto2ItemClass}.class);\n"
        + "{bodyTo}"
        + "        return th;\n"
        + "    }\n";
    public static final String TEMPLATE_COLLECTION_FROM_PROTO
        = "    public static {originalClass} {converterFromName}({proto2Class} th) {\n"
        + "        if (th == null) {\n"
        + "            return null;\n"
        + "        }\n"
        + "        {originalClass} original = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createCollection({collectionClass}.class, {originalItemClass}.class);\n"
        + "{bodyFrom}"
        + "        return original;\n"
        + "    }\n";

    protected final ExportCollection exportCollection;

    public CollectionWriter(ExportCollection exportCollection, WriterFactory processed) {
        super(processed);
        this.exportCollection = exportCollection;
    }

    @Override
    public String write() {
        String templateCopyTo = getTemplateCopyTo();
        String templateCopyFrom = getTemplateCopyFrom();
        String proto2ItemClass;
        String originalItemClass = exportCollection.name;
        String proto2Class = toProto2Class(exportCollection);
        if (!Util.isExistsInProto2(exportCollection.name)) {
            proto2ItemClass = toProto2Class(exportCollection.name);
        } else {
            proto2ItemClass = exportCollection.name;
        }
        String converterItemToName = format(TEMPLATE_TO_PROTO2_NAME, "className", toName(exportCollection.name));
        String converterItemFromName = format(TEMPLATE_FROM_PROTO2_NAME, "className", toName(exportCollection.name));
        String bodyTo = format(templateCopyTo, "converterToName", converterItemToName, "originalItemClass", originalItemClass);
        String bodyFrom = format(templateCopyFrom, "converterFromName", converterItemFromName, "proto2ItemClass", proto2ItemClass);

        StringBuilder sb = new StringBuilder();
        ExportClass simpleClass = new ExportClass(exportCollection.name);
        simpleClass.fields.addAll(exportCollection.fields);
        simpleClass.imports.addAll(exportCollection.imports);

        ConverterWriter simpleWriter = this.createWriter(simpleClass);
        if (simpleWriter != null && !Util.isExistsInProto2(simpleClass)) {
            sb.append(simpleWriter.write());
        }

        boolean addSeparator = false;
        String converterToName = writeConverterToName(exportCollection);
        String converterFromName = writeConverterFromName(exportCollection);
        if (!isConverterMethoExists(converterToName)) {
            addConverterMethod(converterToName);
            String converterTo = Util.format(TEMPLATE_COLLECTION_TO_PROTO,
                "className", toName(exportCollection.name),
                "proto2Class", proto2Class,
                "originalClass", toOriginalClass(exportCollection),
                "proto2ItemClass", proto2ItemClass,
                "originalItemClass", originalItemClass,
                "converterToName", converterToName,
                "converterFromName", converterFromName,
                "collectionClass", getCollectionClass(),
                "bodyTo", bodyTo,
                "bodyFrom", bodyFrom);
            sb.append(converterTo);
            addSeparator = true;
        }

        if (!isConverterMethoExists(converterFromName)) {
            addConverterMethod(converterFromName);
            if (addSeparator) {
                sb.append("\n");
            }
            String converterFrom = Util.format(TEMPLATE_COLLECTION_FROM_PROTO,
                "className", toName(exportCollection.name),
                "proto2Class", proto2Class,
                "originalClass", toOriginalClass(exportCollection),
                "proto2ItemClass", proto2ItemClass,
                "originalItemClass", originalItemClass,
                "converterToName", converterToName,
                "converterFromName", converterFromName,
                "collectionClass", getCollectionClass(),
                "bodyTo", bodyTo,
                "bodyFrom", bodyFrom);
            sb.append(converterFrom);
        }
        return sb.toString();
    }

    protected String getCollectionClass() {
        return exportCollection.collectionClass;
    }

    abstract protected String getTemplateCopyTo();

    abstract protected String getTemplateCopyFrom();

}
