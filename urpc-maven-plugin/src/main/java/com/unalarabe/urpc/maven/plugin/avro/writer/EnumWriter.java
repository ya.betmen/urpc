package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.unalarabe.urpc.converter.structure.ExportEnum;
import com.unalarabe.urpc.maven.plugin.Util;

import static com.unalarabe.urpc.maven.plugin.Util.format;
import static com.unalarabe.urpc.maven.plugin.avro.writer.SimpleWriter.toName;
import static com.unalarabe.urpc.maven.plugin.avro.writer.SimpleWriter.writeConverterFromName;
import static com.unalarabe.urpc.maven.plugin.avro.writer.SimpleWriter.writeConverterToName;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class EnumWriter extends ConverterWriter {

    private static final String TEMPLATE_CASE_ENTRY_FROM = "case {entryPrefix}{entry}: return {originalClass}.{entry};";
    private static final String TEMPLATE_CASE_ENTRY_TO = "case {entry}: return {proto2Class}.{entryPrefix}{entry};";

    private static final String TEMPLATE_ENUM_TO_TRIFT
        = "    public static {proto2Class} {converterToName}({originalClass} original) {\n"
        + "        if (original == null) {\n"
        + "            return null;\n"
        + "        }\n"
        + "        switch (original) {\n"
        + "{bodyTo}"
        + "            default: throw new IllegalArgumentException(original + \" doesn't exists\");\n"
        + "        }\n"
        + "    }\n";
    private static final String TEMPLATE_ENUM_FROM_TRIFT
        = "    public static {originalClass} {converterFromName}({proto2Class} th) {\n"
        + "        if (th == null) {\n"
        + "            return null;\n"
        + "        }\n"
        + "        switch (th) {\n"
        + "{bodyFrom}"
        + "            default: throw new IllegalArgumentException(th + \" doesn't exists\");\n"
        + "        }\n"
        + "    }\n";
    private final ExportEnum exportEnum;

    public EnumWriter(ExportEnum exportEnum, WriterFactory processed) {
        super(processed);
        this.exportEnum = exportEnum;
    }

    @Override
    public String write() {
        StringBuilder bodyTo = new StringBuilder();
        StringBuilder bodyFrom = new StringBuilder();
        StringBuilder result = new StringBuilder();
        String enumPrefix = Util.getClassName(exportEnum.name).toUpperCase() + "_";
        for (String entry : exportEnum.entries) {
            bodyTo.append("            ").append(format(TEMPLATE_CASE_ENTRY_TO, "entryPrefix", enumPrefix, "entry", entry, "proto2Class", toAvroClass(exportEnum)))
                .append("\n");
            bodyFrom.append("            ").append(format(TEMPLATE_CASE_ENTRY_FROM, "entryPrefix", enumPrefix, "entry", entry, "originalClass", toOriginalProto2Class(exportEnum)))
                .append("\n");
        }
        String converterToName = writeConverterToName(exportEnum);
        String converterFromName = writeConverterFromName(exportEnum);
        boolean addSeparator = false;
        if (!isConverterMethoExists(converterToName)) {
            addConverterMethod(converterToName);
            result.append(Util.format(TEMPLATE_ENUM_TO_TRIFT,
                "className", toName(exportEnum.name),
                "proto2Class", toAvroClass(exportEnum),
                "originalClass", toOriginalProto2Class(exportEnum),
                "converterToName", converterToName,
                "converterFromName", converterFromName,
                "bodyTo", bodyTo,
                "bodyFrom", bodyFrom));
        }
        if (!isConverterMethoExists(converterFromName)) {
            addConverterMethod(converterToName);
            if (addSeparator) {
                result.append("\n");
            }
            result.append(Util.format(TEMPLATE_ENUM_FROM_TRIFT,
                "className", toName(exportEnum.name),
                "proto2Class", toAvroClass(exportEnum),
                "originalClass", toOriginalProto2Class(exportEnum),
                "converterToName", converterToName,
                "converterFromName", converterFromName,
                "bodyTo", bodyTo,
                "bodyFrom", bodyFrom));
        }
        return result.toString();
    }

}
