package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.unalarabe.urpc.maven.plugin.avro.writer.MapWriter;
import java.util.HashSet;
import java.util.Set;
import com.unalarabe.urpc.converter.structure.ExportArray;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportCollection;
import com.unalarabe.urpc.converter.structure.ExportEnum;
import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.converter.structure.ExportSet;
import com.unalarabe.urpc.maven.plugin.Util;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class WriterFactory {

    private final Set<ExportClass> processed;
    private final String proto2Package;
    private final Set<String> converterMethods = new HashSet<>();

    public WriterFactory(Set<ExportClass> processed, String proto2Package) {
        this.processed = processed;
        this.proto2Package = proto2Package;
    }

    public ConverterWriter createWriter(ExportClass exportClass) {
        if (processed.contains(exportClass)) {
            return null;
        } else {
            processed.add(exportClass);
            if (Util.isExistsInAvro(exportClass) && !Util.isCollection(exportClass) && !String.class.getCanonicalName().equals(exportClass.name)) {
                return null;
            } else if (exportClass instanceof ExportArray) {
                return new ArrayWriter((ExportArray) exportClass, this);
            } else if (exportClass instanceof ExportEnum) {
                return new EnumWriter((ExportEnum) exportClass, this);
            } else if (exportClass instanceof ExportList) {
                return new ListWriter((ExportList) exportClass, this);
            } else if (exportClass instanceof ExportMap) {
                return new MapWriter((ExportMap) exportClass, this);
            } else if (exportClass instanceof ExportSet) {
                return new SetWriter((ExportSet) exportClass, this);
            } else if (exportClass instanceof ExportCollection) {
                return null;
            } else if ("void".equals(exportClass.name)) {
                return null;
            } else {
                return new SimpleWriter(exportClass, this);
            }
        }
    }

    public void addConverterMethod(String converterMethod) {
        converterMethods.add(converterMethod);
    }

    public boolean isConverterMethoExists(String converterMethod) {
        return converterMethods.contains(converterMethod);
    }

    public String getAvroPackage() {
        return proto2Package;
    }

}
