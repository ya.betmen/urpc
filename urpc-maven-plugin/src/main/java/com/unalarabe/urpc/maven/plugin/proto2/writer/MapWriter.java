package com.unalarabe.urpc.maven.plugin.proto2.writer;

import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.maven.plugin.Util;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MapWriter extends ConverterWriter {

    private static final String TEMPLATE_MAP_TO_PROTO
        = "    public static {proto2Class} {converterToName}({originalClass} original) {\n"
          + "        if (original == null) {\n"
          + "            return null;\n"
          + "        }\n"
          + "        {proto2Class} th = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createMap({proto2MapClass}.class, {proto2KeyClass}.class, {proto2ItemClass}.class);\n"
          + "{bodyTo}"
          + "        return th;\n"
          + "    }\n";
    private static final String TEMPLATE_MAP_FROM_PROTO
        = "    public static {originalClass} {converterFromName}({proto2Class} th) {\n"
          + "        if (th == null) {\n"
          + "            return null;\n"
          + "        }\n"
          + "        {originalClass} original = com.unalarabe.urpc.plugin.util.proto2.ObjectFactoryImpl.createMap({originalMapClass}.class, {originalKeyClass}.class, {originalItemClass}.class);\n"
          + "{bodyFrom}"
          + "        return original;\n"
          + "    }\n";

    private static final String TEMPLATE_COPY_MAP_TO
        = "        for (java.util.Map.Entry<{originalKeyClassName}, {originalValueClassName}> o : original.entrySet()){\n"
          + "            th.put({converterKeyToName}(o.getKey()), {converterValueToName}(o.getValue()));\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_FROM
        = "        for (java.util.Map.Entry<{proto2KeyClassName}, {proto2ValueClassName}> t : th.entrySet()){\n"
          + "            original.put({converterKeyFromName}(t.getKey()), {converterValueFromName}(t.getValue()));\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_KEY_TO
        = "        for (java.util.Map.Entry<{originalKeyClassName}, {originalValueClassName}> o : original.entrySet()){\n"
          + "            th.put(o.getKey(), {converterValueToName}(o.getValue()));\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_KEY_FROM
        = "        for (java.util.Map.Entry<{proto2KeyClassName}, {proto2ValueClassName}> t : th.entrySet()){\n"
          + "            original.put(t.getKey(), {converterValueFromName}(t.getValue()));\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_KEY_SIMPLE_VALUE_TO
        = "        th.putAll(original);\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_KEY_SIMPLE_VALUE_FROM
        = "        original.putAll(th);\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_VALUE_TO
        = "        for (java.util.Map.Entry<{originalKeyClassName}, {originalValueClassName}> o : original.entrySet()){\n"
          + "            th.put({converterKeyToName}(o.getKey()), o.getValue());\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_VALUE_FROM
        = "        for (java.util.Map.Entry<{proto2KeyClassName}, {proto2ValueClassName}> t : th.entrySet()){\n"
          + "            original.put({converterKeyFromName}(t.getKey()), t.getValue());\n"
          + "        }\n";

    private final ExportMap exportMap;

    public MapWriter(ExportMap exportMap, WriterFactory processed) {
        super(processed);
        this.exportMap = exportMap;
    }

    public String toProto2MapClass(String className) {
        if (!Util.isExistsInProto2(className)) {
            return super.toProto2Class(className);
        } else {
            return className;
        }
    }

    @Override
    public String write() {
        String proto2KeyClass = toProto2MapClass(exportMap.keyType);
        String proto2ValueClass = toProto2MapClass(exportMap.name);

        String proto2MapClass = "java.util.Map";
        String templateCopyTo = "// copy to //\n";
        String templateCopyFrom = "// copy from //\n";

        String originalMapClass = exportMap.collectionClass;
        String originalKeyClass = exportMap.keyType;
        String originalValueClass = exportMap.name;

        StringBuilder sb = new StringBuilder();

        if (!Util.isExistsInProto2(proto2ValueClass)) {
            ExportClass valueClass = new ExportClass(exportMap.name);
            valueClass.fields.addAll(exportMap.fields);
            ConverterWriter valueWriter = createWriter(valueClass);

            if (valueWriter != null) {
                sb.append(valueWriter.write()).append("\n");
            }
        }

        if (Util.isExistsInProto2(proto2KeyClass) && Util.isExistsInProto2(proto2ValueClass)) {
            templateCopyTo = TEMPLATE_COPY_MAP_SIMPLE_KEY_SIMPLE_VALUE_TO;
            templateCopyFrom = TEMPLATE_COPY_MAP_SIMPLE_KEY_SIMPLE_VALUE_FROM;
        } else if (Util.isExistsInProto2(proto2KeyClass) && !Util.isExistsInProto2(proto2ValueClass)) {
            templateCopyTo = TEMPLATE_COPY_MAP_SIMPLE_KEY_TO;
            templateCopyFrom = TEMPLATE_COPY_MAP_SIMPLE_KEY_FROM;
        } else if (!Util.isExistsInProto2(proto2KeyClass) && Util.isExistsInProto2(proto2ValueClass)) {
            templateCopyTo = TEMPLATE_COPY_MAP_SIMPLE_VALUE_TO;
            templateCopyFrom = TEMPLATE_COPY_MAP_SIMPLE_VALUE_FROM;
        } else {
            templateCopyTo = TEMPLATE_COPY_MAP_TO;
            templateCopyFrom = TEMPLATE_COPY_MAP_FROM;
        }
        String proto2Class = proto2MapClass + "<" + proto2KeyClass + ", " + proto2ValueClass + ">";
        String bodyTo = Util.format(templateCopyTo,
                                    "originalKeyClassName", originalKeyClass,
                                    "originalValueClassName", originalValueClass,
                                    "converterKeyToName", Util.format(TEMPLATE_TO_PROTO2_NAME, "className", toName(originalKeyClass)),
                                    "converterValueToName", Util.format(TEMPLATE_TO_PROTO2_NAME, "className", toName(originalValueClass)));
        String bodyFrom = Util.format(templateCopyFrom,
                                      "proto2KeyClassName", proto2KeyClass,
                                      "proto2ValueClassName", proto2ValueClass,
                                      "converterKeyFromName", Util.format(TEMPLATE_FROM_PROTO2_NAME, "className", toName(originalKeyClass)),
                                      "converterValueFromName", Util.format(TEMPLATE_FROM_PROTO2_NAME, "className", toName(originalValueClass)));

        String converterToName = writeConverterToName(exportMap);
        String converterFromName = writeConverterFromName(exportMap);
        boolean addSeparator = false;

        if (!isConverterMethoExists(converterToName)) {
            addConverterMethod(converterToName);
            String mapTo = Util.format(TEMPLATE_MAP_TO_PROTO,
                                       "className", toName(exportMap.name),
                                       "originalBaseClass", exportMap.name,
                                       "originalClass", toOriginalClass(exportMap),
                                       "converterToName", converterToName,
                                       "converterFromName", converterFromName,
                                       "proto2Class", proto2Class,
                                       "proto2MapClass", proto2MapClass,
                                       "proto2KeyClass", proto2KeyClass,
                                       "proto2ItemClass", proto2ValueClass,
                                       "originalMapClass", originalMapClass,
                                       "originalKeyClass", originalKeyClass,
                                       "originalItemClass", originalValueClass,
                                       "bodyTo", bodyTo,
                                       "bodyFrom", bodyFrom);
            sb.append(mapTo);
        }

        if (!isConverterMethoExists(converterFromName)) {
            addConverterMethod(converterFromName);
            if (addSeparator) {
                sb.append("\n");
            }
            String mapFrom = Util.format(TEMPLATE_MAP_FROM_PROTO,
                                         "className", toName(exportMap.name),
                                         "originalBaseClass", exportMap.name,
                                         "originalClass", toOriginalClass(exportMap),
                                         "converterToName", converterToName,
                                         "converterFromName", converterFromName,
                                         "proto2Class", proto2Class,
                                         "proto2MapClass", proto2MapClass,
                                         "proto2KeyClass", proto2KeyClass,
                                         "proto2ItemClass", proto2ValueClass,
                                         "originalMapClass", originalMapClass,
                                         "originalKeyClass", originalKeyClass,
                                         "originalItemClass", originalValueClass,
                                         "bodyTo", bodyTo,
                                         "bodyFrom", bodyFrom);
            sb.append(mapFrom);
        }

        return sb.toString();
    }

}
