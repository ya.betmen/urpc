package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.converter.structure.ExportSet;
import com.unalarabe.urpc.maven.plugin.Util;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class SetWriter extends CollectionWriter {

    private static final String TEMPLATE_COPY_SET_TO
            = "        for ({originalItemClass} o : original){\n"
            + "            th.add({converterToName}(o));\n"
            + "        }\n";
    public static final String TEMPLATE_COPY_SET_FROM
            = "        for ({proto2ItemClass} t : th) {\n"
            + "            original.add({converterFromName}(t));\n"
            + "        }\n"
            + "";
    private static final String TEMPLATE_COPY_SET_SIMPLE_TO
            = "        for ({originalItemClass} o : original){\n"
            + "            th.add(o);\n"
            + "        }\n";
    public static final String TEMPLATE_COPY_SET_SIMPLE_FROM
            = "        original.addAll(th);\n";

    public SetWriter(ExportSet exportSet, WriterFactory processed) {
        super(exportSet, processed);
    }

    @Override
    protected String getTemplateCopyTo() {
        if (Util.isExistsInAvro(exportCollection.name)) {
            return TEMPLATE_COPY_SET_SIMPLE_TO;
        } else {
            return TEMPLATE_COPY_SET_TO;
        }
    }

    @Override
    protected String getTemplateCopyFrom() {
        if (Util.isExistsInAvro(exportCollection.name)) {
            return TEMPLATE_COPY_SET_SIMPLE_FROM;
        } else {
            return TEMPLATE_COPY_SET_FROM;
        }
    }

}
