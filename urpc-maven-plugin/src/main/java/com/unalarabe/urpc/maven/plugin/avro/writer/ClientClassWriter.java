package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.meta.MetaClass;
import com.unalarabe.urpc.meta.MetaProperty;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ClientClassWriter {

    private static final String TEMPLATE_CLASS
        = "package {package};\n"
          + "\n"
          + "public class {className} {\n"
          + "\n"
          + "{fields}"
          + "\n"
          + "    public {className}(){\n"
          + "    }\n"
          + "\n"
          + "{allFieldsConstructor}"
          + "{requiredFieldsConstructor}"
          + "\n"
          + "    public static {avroClassName} toAvro({className} request) {\n"
          + "        {avroClassName}.Builder builder = {avroClassName}.newBuilder();\n"
          + "{copyFieldsToAvro}"
          + "        return builder.build();\n"
          + "    }\n"
          + "\n"
          + "    public static {className} fromAvro({avroClassName} response) {\n"
          + "        {className} result = new {className}();\n"
          + "{copyFieldsFromAvro}"
          + "        return result;\n"
          + "    }\n"
          + "\n"
          + "}\n";
    private static final String TEMPLATE_ENUM
        = "package {package};\n"
          + "\n"
          + "public enum {className} {\n"
          + "\n"
          + "{entries}"
          + "    public static {avroClassName} toAvro({className} value) {\n"
          + "        switch (value) {\n"
          + "{casesTo}"
          + "            default:\n"
          + "                throw new IllegalStateException(\"Shouldn't be here\");\n"
          + "        }\n"
          + "    }\n"
          + "\n"
          + "    public static {className} fromAvro({avroClassName} value) {\n"
          + "        switch (value){\n"
          + "{casesFrom}"
          + "            default:\n"
          + "                throw new IllegalStateException(\"Shouldn't be here\");\n"
          + "        }\n"
          + "    }\n"
          + "}\n";
    private static final String TEMPLATE_FILED
        = "    public {class} {name};";

    private final MetaClass avroClass;
    private final com.unalarabe.urpc.maven.plugin.avro.writer.ClientWriter clientWriter;

    public ClientClassWriter(MetaClass avroClass, com.unalarabe.urpc.maven.plugin.avro.writer.ClientWriter clientWriter) {
        this.avroClass = avroClass;
        this.clientWriter = clientWriter;
    }

    public String getPackage() {
        return avroClass.packet;
    }

    public String getName() {
        return toProxyName(avroClass.name, false);
    }

    public static String toProxyName(String name, boolean javaType) {
        return (javaType || name.endsWith(">")) ? name : name + "Proxy";
    }

    public String writeClass() {
        return avroClass.entries.isEmpty() ? generateClass(avroClass) : generateEnum(avroClass);
    }

    protected String generateEnum(MetaClass avroClass) {
        StringBuilder entries = new StringBuilder();
        StringBuilder casesTo = new StringBuilder();
        StringBuilder casesFrom = new StringBuilder();

        for (String entry : avroClass.entries) {
            if (entries.length() > 0) {
                entries.append(",\n");
            }
            entries.append("    ").append(entry);
            casesTo.append("            case ").append(entry).append(":\n")
                .append("                return ").append(avroClass.name).append(".").append(entry).append(";\n");
            casesFrom.append("            case ").append(entry).append(":\n")
                .append("                return ").append(entry).append(";\n");
        }
        entries.append(";\n");

        return Util.format(TEMPLATE_ENUM,
                           "package", avroClass.packet,
                           "avroClassName", avroClass.name,
                           "className", getName(),
                           "entries", entries,
                           "casesTo", casesTo,
                           "casesFrom", casesFrom);
    }

    protected String generateClass(MetaClass avroClass) {
        StringBuilder fields = new StringBuilder();
        String allFieldsConstructor = "";
        String requiredFieldsConstructor = "";
        StringBuilder copyFieldsFromAvro = new StringBuilder();
        StringBuilder copyFieldsToAvro = new StringBuilder();

        List<MetaProperty> requiredProperties = new ArrayList<>();
        List<MetaProperty> allProperties = new ArrayList<>();
        for (MetaProperty property : avroClass.properties) {
            System.out.println("avroClass=" + avroClass);

            if (avroClass.rpcResponse
                && ("error".equals(property.name) || "errorText".equals(property.name))) {
                continue;
            }
            fields.append(Util.format(TEMPLATE_FILED,
                                      "class", toProxyName(property.type, property.javaType),
                                      "name", property.name
            )).append("\n");
            if (property.required) {
                requiredProperties.add(property);
            }
            allProperties.add(property);
        }
        if (!allProperties.isEmpty()) {
            allFieldsConstructor = generateConstructor(getName(), allProperties);
            for (MetaProperty property : allProperties) {
                boolean simpleType = Util.isSimple(property.type);
                if (!simpleType) {
                    copyFieldsToAvro.append("        if (request.").append(property.name).append(" != null) {\n");
                    copyFieldsToAvro.append("    ");
                }
                String setterPrefix = "set";
                String getterSuffix = "";
                boolean collection = false;
                if (property.type.startsWith("java.util.List<") || property.type.startsWith("java.util.Set<")) {
//                    setterPrefix = "addAll";
//                    getterSuffix = "List";
                    collection = true;
                }
                if (property.type.startsWith("java.util.Map<")) {
//                    setterPrefix = "putAll";
//                    getterSuffix = "Map";
                    collection = true;
                }

                if (property.javaType) {
                    if (!collection) {
                        copyFieldsFromAvro.append("        if (response.get").append(Util.firstLetterToUpperCase(property.name)).append("() != null) {\n");
                    }
                    copyFieldsFromAvro.append("            result.").append(property.name)
                        .append(" = response.get").append(Util.firstLetterToUpperCase(property.name)).append(getterSuffix)
                        .append("();\n");
                    if (!collection) {
                        copyFieldsFromAvro.append("        }\n");
                    }
                    copyFieldsToAvro.append("        builder.").append(setterPrefix).append(Util.firstLetterToUpperCase(property.name))
                        .append("(request.").append(property.name).append(");\n");
                } else {
                    if (!collection) {
                        copyFieldsFromAvro.append("        if (response.get").append(Util.firstLetterToUpperCase(property.name)).append("() != null) {\n");
                    }
                    copyFieldsFromAvro.append("            result.").append(property.name)
                        .append(" = ");
                    if (!collection) {
                        copyFieldsFromAvro.append(property.type).append("Proxy.fromAvro(");
                    }
                    copyFieldsFromAvro.append("response.get").append(Util.firstLetterToUpperCase(property.name)).append(getterSuffix).append("()");
                    if (!collection) {
                        copyFieldsFromAvro.append(");\n").append("        }\n");
                    } else {
                        copyFieldsFromAvro.append(";\n");
                    }

                    copyFieldsToAvro.append("        builder.").append(setterPrefix).append(Util.firstLetterToUpperCase(property.name)).append("(");
                    if (!collection) {
                        copyFieldsToAvro.append(property.type).append("Proxy.toAvro(");
                    }
                    copyFieldsToAvro.append("request.").append(property.name).append(")");
                    if (!collection) {
                        copyFieldsToAvro.append(");\n");
                    } else {
                        copyFieldsToAvro.append(";\n");
                    }
                }
                if (!simpleType) {
                    copyFieldsToAvro.append("        }\n");
                }
            }
        }
        if (!requiredProperties.isEmpty() && requiredProperties.size() != allProperties.size()) {
            requiredFieldsConstructor = generateConstructor(getName(), requiredProperties);
        }

        return Util.format(TEMPLATE_CLASS,
                           "package", avroClass.packet,
                           "className", getName(),
                           "allFieldsConstructor", allFieldsConstructor,
                           "requiredFieldsConstructor", requiredFieldsConstructor,
                           "avroClassName", avroClass.getFullName(),
                           "copyFieldsToAvro", copyFieldsToAvro,
                           "copyFieldsFromAvro", copyFieldsFromAvro,
                           "fields", fields
        );
    }

    protected String generateConstructor(String className, Collection<MetaProperty> properties) {
        StringBuilder result = new StringBuilder();
        for (MetaProperty property : properties) {
            if (result.length() > 0) {
                result.append(", ");
            }
            result.append(toProxyName(property.type, property.javaType)).append(" ").append(property.name);
        }
        result.insert(0, "    public " + className + "(");
        result.append("){\n");
        for (MetaProperty property : properties) {
            result.append("        this.").append(property.name).append(" = ").append(property.name).append(";\n");
        }
        result.append("    }\n\n");
        return result.toString();
    }
}
