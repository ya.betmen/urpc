package com.unalarabe.urpc.maven.plugin.proto2;

import com.unalarabe.urpc.meta.MetaClass;
import com.unalarabe.urpc.meta.MetaMethod;
import com.unalarabe.urpc.meta.MetaService;
import com.unalarabe.urpc.meta.MetaProperty;
import com.squareup.protoparser.OptionElement;
import com.squareup.protoparser.ProtoFile;
import com.squareup.protoparser.ProtoParser;
import com.squareup.protoparser.RpcElement;
import com.squareup.protoparser.ServiceElement;
import com.squareup.protoparser.TypeElement;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static com.unalarabe.urpc.maven.plugin.Util.SEPARATOR;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ProtoFileParser {

    private static final Logger LOG = Logger.getLogger(ProtoFileParser.class.getName());

    private final ProtoFile protoFile;
    private final Map<String, ProtoFile> includes = new HashMap<>();
    private Map<String, MetaClass> classes;
    private ArrayList<MetaService> services;

    public ProtoFileParser(File file, List<File> includes) throws IOException {
        this.protoFile = ProtoParser.parseUtf8(file);
//        List<String> dependencies = protoFile.dependencies();

        Set<String> dependencies = new HashSet<>();
        dependencies.addAll(protoFile.dependencies());

        int oldDependenciesSize = 0;
        do {
            oldDependenciesSize = dependencies.size();
            Set<String> newDependencies = new HashSet<>();
            for (String dependency : dependencies) {
                String absolutePath = file.getAbsolutePath();
                String protoPath = protoFile.packageName().replaceAll("\\.", SEPARATOR) + SEPARATOR + file.getName();
                File basePath;
                if (absolutePath.endsWith(protoPath)) {
                    basePath = new File(absolutePath.substring(0, absolutePath.length() - protoPath.length()));
                } else {
                    basePath = file.getParentFile();
                }
                ProtoFile protoDependency = findDependency(basePath, dependency, includes);
                if (protoDependency == null) {
                    throw new IllegalStateException("Can't resolve " + dependency + " in " + file.getCanonicalPath());
                }
                newDependencies.addAll(protoDependency.dependencies());
                this.includes.put(dependency, protoDependency);
            }
            dependencies.addAll(newDependencies);
        } while (dependencies.size() != oldDependenciesSize);
    }

    private ProtoFile loadProtoFile(File file) {
        try {
            if (file.exists() && file.isFile()) {
                ProtoFile protoFileDependency;
                protoFileDependency = ProtoParser.parseUtf8(file);
                return protoFileDependency;
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return null;
    }

    private ProtoFile findDependency(File baseDir, String dependency, List<File> includes) {
        for (File includeFile : includes) {
            if (includeFile.isDirectory()) {
                ProtoFile protoFileDependency = loadProtoFile(new File(includeFile, dependency));
                if (protoFileDependency != null) {
                    return protoFileDependency;
                }
            }
        }
        for (File includeFile : includes) {
            try {
                if (includeFile.isFile() && includeFile.getCanonicalPath().endsWith(dependency)) {
                    ProtoFile protoFileDependency = loadProtoFile(includeFile);
                    if (protoFileDependency != null) {
                        return protoFileDependency;
                    }
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        return loadProtoFile(new File(baseDir, dependency));
    }

    public List<MetaService> getServices() throws IOException {
        if (services == null) {
            services = new ArrayList<>();
            String packageName = protoFile.packageName();
            for (ServiceElement service : protoFile.services()) {
                MetaService protoService = new MetaService();
                protoService.name = service.name();
                for (RpcElement rpcElement : service.rpcs()) {
                    MetaMethod protoMethod = new MetaMethod();
                    protoMethod.name = rpcElement.name();
                    protoMethod.resultType = rpcElement.responseType().name();
                    protoMethod.parameterType = rpcElement.requestType().name();
                    protoService.methods.add(protoMethod);
                }
                services.add(protoService);
            }
            Map<String, MetaClass> classesMap = getClassesMap();
            for (MetaService service : services) {
                for (MetaMethod protoMethod : service.methods) {
                    MetaClass protoClass = classesMap.get(packageName + "." + protoMethod.resultType);
                    if (protoClass.rpcResponse) {
                        for (MetaProperty property : protoClass.properties) {
                            if ("errorValue".equals(property.name)) {
                                MetaClass errorEnum = classesMap.get(packageName + "." + property.type);
                                protoMethod.errors.addAll(errorEnum.entries);
                                break;
                            }
                        }
                    }
                }
            }
        }
        return services;
    }

    public Map<String, MetaClass> getClassesMap() throws IOException {
        if (classes == null) {
            Set<MetaClass> allClasses = new HashSet<>();
            List<MetaService> services = getServices();
            Set<String> allResponses = new HashSet<>();
            Set<String> allRequests = new HashSet<>();
            Map<String, Set<String>> serviceToClassMap = new HashMap<>();

            for (MetaService service : services) {
                allRequests.add(service.name + "Request");
                allResponses.add(service.name + "Response");
            }

            for (MetaService service : services) {
                Set<String> serviceResponses = new HashSet<>();
                for (MetaMethod method : service.methods) {
                    serviceResponses.add(method.resultType);
                    allResponses.add(method.resultType);
                    allRequests.add(method.parameterType);
                }
                serviceToClassMap.put(service.name, serviceResponses);
            }

            Map<String, TypeElement> types = new HashMap<>();
            List<TypeElement> typeElements = protoFile.typeElements();
            Set<String> subRequests = new HashSet<>();
            Set<String> subResponses = new HashSet<>();
            for (TypeElement typeElement : typeElements) {
                for (String request : allRequests) {
                    if (typeElement.name().matches(request + "[0-9]+")) {
                        subRequests.add(typeElement.name());
                        break;
                    }
                }
                for (String response : allResponses) {
                    if (typeElement.name().matches(response + "[0-9]+")) {
                        subResponses.add(typeElement.name());
                        break;
                    }
                }
                types.put(typeElement.qualifiedName(), typeElement);
            }
            allRequests.addAll(subRequests);
            allResponses.addAll(subResponses);

            for (ProtoFile include : includes.values()) {
                for (TypeElement typeElement : include.typeElements()) {
                    types.put(typeElement.qualifiedName(), typeElement);
                }
            }

            for (String name : types.keySet()) {
                TypeElement typeElement = types.get(name);
                MetaClass protoClass = new MetaClass(typeElement, types, allResponses.contains(typeElement.name()), allRequests.contains(typeElement.name()));
                allClasses.add(protoClass);
            }

            classes = new HashMap<>();

//            int allClassesSize = 0;
//            do {
//                allClassesSize = allClasses.size();
//                for (MetaClass protoClass : allClasses) {
//                    for (MetaProperty property : protoClass.properties) {
//                        
//                        allClasses.add();
//                    }
//                }
//            } while (allClasses.size() != allClassesSize);
            for (MetaClass protoClass : allClasses) {
                classes.put(protoClass.packet + "." + protoClass.name, protoClass);
            }
        }
        return classes;
    }

    public List<MetaClass> getClasses() throws IOException {
        return new ArrayList<>(getClassesMap().values());
    }

    public boolean isMuliplyFiles() {
        List<OptionElement> options = protoFile.options();
        for (OptionElement option : options) {
            if ("java_multiple_files".equals(option.name())) {
                return Boolean.parseBoolean(String.valueOf(option.value()));
            }
        }
        return false;
    }

    public String getJavaPackage() {
        List<OptionElement> options = protoFile.options();
        for (OptionElement option : options) {
            if ("java_package".equals(option.name())) {
                return String.valueOf(option.value());
            }
        }
        return protoFile.packageName();
    }

}
