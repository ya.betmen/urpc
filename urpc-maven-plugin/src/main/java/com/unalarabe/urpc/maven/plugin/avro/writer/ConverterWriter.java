package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.unalarabe.urpc.converter.structure.ExportArray;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportCollection;
import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.converter.structure.ExportSet;
import com.unalarabe.urpc.maven.plugin.Util;

import static com.unalarabe.urpc.maven.plugin.Util.format;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public abstract class ConverterWriter {

    public static final String TEMPLATE_FROM_AVRO_NAME = "convert{className}FromAvro";
    public static final String TEMPLATE_LIST_TO_AVRO_NAME = "convertListOf{className}ToAvro";
    public static final String TEMPLATE_LIST_FROM_AVRO_NAME = "convertListOf{className}FromAvro";
    public static final String TEMPLATE_SET_TO_AVRO_NAME = "convertSetOf{className}ToAvro";
    public static final String TEMPLATE_SET_FROM_AVRO_NAME = "convertSetOf{className}FromAvro";
    public static final String TEMPLATE_ARRAY_TO_AVRO_NAME = "convertArrayOf{className}ToAvro";
    public static final String TEMPLATE_ARRAY_FROM_AVRO_NAME = "convertArrayOf{className}FromAvro";
    public static final String TEMPLATE_MAP_TO_AVRO_NAME = "convertMapOf{keyClassName}To{valueClassName}ToAvro";
    public static final String TEMPLATE_MAP_FROM_AVRO_NAME = "convertMapOf{keyClassName}To{valueClassName}FromAvro";
    public static final String TEMPLATE_TO_AVRO_NAME = "convert{className}ToAvro";

    private final WriterFactory factory;

    public ConverterWriter(WriterFactory processed) {
        this.factory = processed;
    }

    protected ConverterWriter createWriter(ExportClass exportClass) {
        return this.factory.createWriter(exportClass);
    }

    public static String toName(String className) {
        StringBuilder sb = new StringBuilder();
        String[] parts = className.replace("[]", "").split("\\.");
        for (String part : parts) {
            sb.append(part.substring(0, 1).toUpperCase());
            if (part.length() > 1) {
                sb.append(part.substring(1));
            }
        }
        return sb.toString();
    }

    public static String writeConverterToName(ExportClass ec) {
        String converterToName;
        if (ec instanceof ExportList) {
            converterToName = format(TEMPLATE_LIST_TO_AVRO_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportSet) {
            converterToName = format(TEMPLATE_SET_TO_AVRO_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportArray) {
            converterToName = format(TEMPLATE_ARRAY_TO_AVRO_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportMap) {
            ExportMap em = (ExportMap) ec;
            converterToName = format(TEMPLATE_MAP_TO_AVRO_NAME, "keyClassName", toName(em.keyType), "valueClassName", toName(em.name));
        } else {
            converterToName = format(TEMPLATE_TO_AVRO_NAME, "className", toName(ec.name));
        }
        return converterToName;
    }

    public static String writeConverterFromName(ExportClass ec) {
        String converterFromName;
        if (ec instanceof ExportList) {
            converterFromName = format(TEMPLATE_LIST_FROM_AVRO_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportSet) {
            converterFromName = format(TEMPLATE_SET_FROM_AVRO_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportArray) {
            converterFromName = format(TEMPLATE_ARRAY_FROM_AVRO_NAME, "className", toName(ec.name));
        } else if (ec instanceof ExportMap) {
            ExportMap em = (ExportMap) ec;
//            if (Util.isExistsInAvro(em.keyType)) {
//                converterFromName = format(TEMPLATE_MAP_FROM_AVRO_NAME, "keyClassName", toName(em.keyType), "valueClassName", toName(em.name));
//            } else {
            converterFromName = format(TEMPLATE_MAP_FROM_AVRO_NAME, "keyClassName", toName(em.keyType), "valueClassName", toName(em.name));
//            }
        } else {
            converterFromName = format(TEMPLATE_FROM_AVRO_NAME, "className", toName(ec.name));
        }
        return converterFromName;
    }

    public String getClassName(String className) {
        int lastIndexOfDot = className.lastIndexOf('.');
        return className.substring(lastIndexOfDot + 1);
    }

    public String toAvroClass(String className) {
        return getAvroPackage() + "." + className;
    }

    public String toAvroClass(ExportClass ec) {
        if (Util.isCollection(ec)) {
            String proto2Class = ec.name;
            if (!Util.isExistsInAvro(ec)) {
                proto2Class = toAvroClass(ec.name);
            }
            if (ec instanceof ExportArray) {
                return "java.util.List<" + proto2Class + ">";
            } else if (ec instanceof ExportList) {
                return "java.util.List<" + proto2Class + ">";
            } else if (ec instanceof ExportSet) {
                return "java.util.List   <" + proto2Class + ">";
            } else if (ec instanceof ExportMap) {
                ExportMap em = (ExportMap) ec;
                proto2Class = em.name;
                if (!Util.isExistsInAvro(proto2Class)) {
                    proto2Class = toAvroClass(proto2Class);
                }
                return "java.util.Map<" + String.class.getCanonicalName() + ", " + proto2Class + ">";
            } else if (ec instanceof ExportCollection) {
                return "java.util.Collection<" + proto2Class + ">";
            } else {
                throw new IllegalStateException(ec.toString());
            }
        } else if (Util.isExistsInAvro(ec)) {
            return ec.name;
        } else {
            return toAvroClass(ec.name);
        }
    }

    public static String toOriginalProto2Class(ExportClass ec) {
        if (ec instanceof ExportArray) {
            return ec.name + "[]";
        } else if (ec instanceof ExportList) {
            return "java.util.List<" + ec.name + ">";
        } else if (ec instanceof ExportSet) {
            return "java.util.Set<" + ec.name + ">";
        } else if (ec instanceof ExportMap) {
            return "java.util.Map<" + ((ExportMap) ec).keyType + ", " + ec.name + ">";
        } else if (ec instanceof ExportCollection) {
            return "java.util.Collection<" + ec.name + ">";
        } else {
            return ec.name;
        }
    }

    public static String toOriginalAvroClass(ExportClass ec) {
        boolean isString = Util.isString(ec.name);
        String clazz = ec.name;
        if (isString) {
            clazz = String.class.getCanonicalName();
        }
        if (ec instanceof ExportArray) {
            return clazz + "[]";
        } else if (ec instanceof ExportList) {
            return "java.util.List<" + clazz + ">";
        } else if (ec instanceof ExportSet) {
            return "java.util.Set<" + clazz + ">";
        } else if (ec instanceof ExportMap) {
            return "java.util.Map<" + ((ExportMap) ec).keyType + ", " + clazz + ">";
        } else if (ec instanceof ExportCollection) {
            return "java.util.Collection<" + clazz + ">";
        } else {
            return ec.name;
        }
    }

    public String getAvroPackage() {
        return factory.getAvroPackage();
    }

    public void addConverterMethod(String converterMethod) {
        factory.addConverterMethod(converterMethod);
    }

    public boolean isConverterMethoExists(String converterMethod) {
        return factory.isConverterMethoExists(converterMethod);
    }

    abstract public String write();

}
