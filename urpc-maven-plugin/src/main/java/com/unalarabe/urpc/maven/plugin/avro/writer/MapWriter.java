package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.maven.plugin.avro.writer.WriterFactory;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MapWriter extends ConverterWriter {

    private static final String TEMPLATE_MAP_TO_AVRO
        = "    public static {avroClass} {converterToName}({originalClass} original) {\n"
          + "        if (original == null) {\n"
          + "            return null;\n"
          + "        }\n"
          + "        {avroClass} th = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createMap({avroMapClass}.class, {avroKeyClass}.class, {avroItemClass}.class);\n"
          + "{bodyTo}"
          + "        return th;\n"
          + "    }\n";
    private static final String TEMPLATE_MAP_FROM_AVRO
        = "    public static {originalClass} {converterFromName}({avroClass} th) {\n"
          + "        if (th == null) {\n"
          + "            return null;\n"
          + "        }\n"
          + "        {originalClass} original = com.unalarabe.urpc.plugin.util.avro.ObjectFactoryImpl.createMap({originalMapClass}.class, {originalKeyClass}.class, {originalItemClass}.class);\n"
          + "{bodyFrom}"
          + "        return original;\n"
          + "    }\n";

    private static final String TEMPLATE_COPY_MAP_TO
        = "        for (java.util.Map.Entry<{originalKeyClassName}, {originalValueClassName}> o : original.entrySet()){\n"
          + "            th.put({converterKeyToName}(o.getKey()), {converterValueToName}(o.getValue()));\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_FROM
        = "        for (java.util.Map.Entry<{avroClassName}, {avroValueClassName}> t : th.entrySet()){\n"
          + "            original.put({converterKeyFromName}(t.getKey()), {converterValueFromName}(t.getValue()));\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_KEY_TO
        = "        for (java.util.Map.Entry<{originalKeyClassName}, {originalValueClassName}> o : original.entrySet()){\n"
          + "            th.put(o.getKey(), {converterValueToName}(o.getValue()));\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_KEY_FROM
        = "        for (java.util.Map.Entry<{avroClassName}, {avroValueClassName}> t : th.entrySet()){\n"
          + "            original.put(t.getKey(), {converterValueFromName}(t.getValue()));\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_KEY_SIMPLE_VALUE_TO
        = "        th.putAll(original);\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_KEY_SIMPLE_VALUE_FROM
        = "        original.putAll(th);\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_VALUE_TO
        = "        for (java.util.Map.Entry<{originalKeyClassName}, {originalValueClassName}> o : original.entrySet()){\n"
          + "            th.put({converterKeyToName}(o.getKey()), o.getValue());\n"
          + "        }\n";
    private static final String TEMPLATE_COPY_MAP_SIMPLE_VALUE_FROM
        = "        for (java.util.Map.Entry<{avroClassName}, {avroValueClassName}> t : th.entrySet()){\n"
          + "            original.put({converterKeyFromName}(t.getKey()), t.getValue());\n"
          + "        }\n";

    private final ExportMap exportMap;

    public MapWriter(ExportMap exportMap, WriterFactory processed) {
        super(processed);
        this.exportMap = exportMap;
    }

    public String toAvroMapClass(String className) {
        if (!Util.isExistsInAvro(className)) {
            return super.toAvroClass(className);
        } else {
            return className;
        }
    }

    @Override
    public String write() {
        String avroKeyClass = toAvroMapClass(exportMap.keyType);
        String avroValueClass = toAvroMapClass(exportMap.name);

        String avroMapClass = "java.util.Map";
        String templateCopyTo = "// copy to //\n";
        String templateCopyFrom = "// copy from //\n";

        String originalMapClass = exportMap.collectionClass;
        String originalKeyClass = exportMap.keyType;
        String originalValueClass = exportMap.name;

        StringBuilder sb = new StringBuilder();

        if (!Util.isExistsInAvro(avroValueClass)) {
            ExportClass valueClass = new ExportClass(exportMap.name);
            valueClass.fields.addAll(exportMap.fields);
            ConverterWriter valueWriter = createWriter(valueClass);

            if (valueWriter != null) {
                sb.append(valueWriter.write()).append("\n");
            }
        }

        if (Util.isExistsInAvro(avroKeyClass) && Util.isExistsInAvro(avroValueClass)) {
            templateCopyTo = TEMPLATE_COPY_MAP_SIMPLE_KEY_SIMPLE_VALUE_TO;
            templateCopyFrom = TEMPLATE_COPY_MAP_SIMPLE_KEY_SIMPLE_VALUE_FROM;
        } else if (Util.isExistsInAvro(avroKeyClass) && !Util.isExistsInAvro(avroValueClass)) {
            templateCopyTo = TEMPLATE_COPY_MAP_SIMPLE_KEY_TO;
            templateCopyFrom = TEMPLATE_COPY_MAP_SIMPLE_KEY_FROM;
        } else if (!Util.isExistsInAvro(avroKeyClass) && Util.isExistsInAvro(avroValueClass)) {
            templateCopyTo = TEMPLATE_COPY_MAP_SIMPLE_VALUE_TO;
            templateCopyFrom = TEMPLATE_COPY_MAP_SIMPLE_VALUE_FROM;
        } else {
            templateCopyTo = TEMPLATE_COPY_MAP_TO;
            templateCopyFrom = TEMPLATE_COPY_MAP_FROM;
        }
        String converterKeyFromName = Util.format(TEMPLATE_FROM_AVRO_NAME, "className", toName(originalKeyClass));
//        if (Util.isString(avroKeyClass)) {
//            avroKeyClass = CharSequence.class.getCanonicalName();
//            converterKeyFromName = "String.valueOf";
//            if (Util.isExistsInAvro(avroValueClass)) {
//                templateCopyFrom = TEMPLATE_COPY_MAP_SIMPLE_VALUE_FROM;
//            } else {
//                templateCopyFrom = TEMPLATE_COPY_MAP_FROM;
//            }
//        }
        String avroClass = avroMapClass + "<" + avroKeyClass + ", " + avroValueClass + ">";
        String bodyTo = Util.format(templateCopyTo,
                                    "originalKeyClassName", originalKeyClass,
                                    "originalValueClassName", originalValueClass,
                                    "converterKeyToName", Util.format(TEMPLATE_TO_AVRO_NAME, "className", toName(originalKeyClass)),
                                    "converterValueToName", Util.format(TEMPLATE_TO_AVRO_NAME, "className", toName(originalValueClass)));
        String bodyFrom = Util.format(templateCopyFrom,
                                      "avroClassName", avroKeyClass,
                                      "avroValueClassName", avroValueClass,
                                      "converterKeyFromName", converterKeyFromName,
                                      "converterValueFromName", Util.format(TEMPLATE_FROM_AVRO_NAME, "className", toName(originalValueClass)));

        String converterToName = writeConverterToName(exportMap);
        String converterFromName = writeConverterFromName(exportMap);
        boolean addSeparator = false;

        if (!isConverterMethoExists(converterToName)) {
            addConverterMethod(converterToName);
            String mapTo = Util.format(TEMPLATE_MAP_TO_AVRO,
                                       "className", toName(exportMap.name),
                                       "originalBaseClass", exportMap.name,
                                       "originalClass", toOriginalProto2Class(exportMap),
                                       "converterToName", converterToName,
                                       "converterFromName", converterFromName,
                                       "avroClass", avroClass,
                                       "avroMapClass", avroMapClass,
                                       "avroKeyClass", avroKeyClass,
                                       "avroItemClass", avroValueClass,
                                       "originalMapClass", originalMapClass,
                                       "originalKeyClass", originalKeyClass,
                                       "originalItemClass", originalValueClass,
                                       "bodyTo", bodyTo,
                                       "bodyFrom", bodyFrom);
            sb.append(mapTo);
        }

        if (!isConverterMethoExists(converterFromName)) {
            addConverterMethod(converterFromName);
            if (addSeparator) {
                sb.append("\n");
            }
            String mapFrom = Util.format(TEMPLATE_MAP_FROM_AVRO,
                                         "className", toName(exportMap.name),
                                         "originalBaseClass", exportMap.name,
                                         "originalClass", toOriginalProto2Class(exportMap),
                                         "converterToName", converterToName,
                                         "converterFromName", converterFromName,
                                         "avroClass", avroClass,
                                         "avroMapClass", avroMapClass,
                                         "avroKeyClass", avroKeyClass,
                                         "avroItemClass", avroValueClass,
                                         "originalMapClass", originalMapClass,
                                         "originalKeyClass", originalKeyClass,
                                         "originalItemClass", originalValueClass,
                                         "bodyTo", bodyTo,
                                         "bodyFrom", bodyFrom);
            sb.append(mapFrom);
        }

        return sb.toString();
    }

}
