package com.unalarabe.urpc.maven.plugin.proto2.writer;

import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.meta.MetaClass;
import com.unalarabe.urpc.meta.MetaProperty;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ClientClassWriter {

    private static final String TEMPLATE_CLASS
        = "package {package};\n"
          + "\n"
          + "public class {className} {\n"
          + "\n"
          + "{fields}"
          + "\n"
          + "    public {className}(){\n"
          + "    }\n"
          + "\n"
          + "{allFieldsConstructor}"
          + "{requiredFieldsConstructor}"
          + "\n"
          + "    public static {protoClassName} toProto({className} request) {\n"
          + "        {protoClassName}.Builder builder = {protoClassName}.newBuilder();\n"
          + "{copyFieldsToProto}"
          + "        return builder.build();\n"
          + "    }\n"
          + "\n"
          + "    public static {className} fromProto({protoClassName} response) {\n"
          + "        {className} result = new {className}();\n"
          + "{copyFieldsFromProto}"
          + "        return result;\n"
          + "    }\n"
          + "\n"
          + "}\n";
    private static final String TEMPLATE_ENUM
        = "package {package};\n"
          + "\n"
          + "public enum {className} {\n"
          + "\n"
          + "{entries}"
          + "    public static {protoClassName} toProto({className} value) {\n"
          + "        switch (value) {\n"
          + "{casesTo}"
          + "            default:\n"
          + "                throw new IllegalStateException(\"Shouldn't be here\");\n"
          + "        }\n"
          + "    }\n"
          + "\n"
          + "    public static {className} fromProto({protoClassName} value) {\n"
          + "        switch (value){\n"
          + "{casesFrom}"
          + "            default:\n"
          + "                throw new IllegalStateException(\"Shouldn't be here\");\n"
          + "        }\n"
          + "    }\n"
          + "}\n";
    private static final String TEMPLATE_FILED
        = "    public {class} {name};";

    private final MetaClass protoClass;
    private final ClientWriter clientWriter;

    public ClientClassWriter(MetaClass protoClass, ClientWriter clientWriter) {
        this.protoClass = protoClass;
        this.clientWriter = clientWriter;
    }

    public String getPackage() {
        return protoClass.packet;
    }

    public String getName() {
        return toProxyName(protoClass.name, false);
    }

    public static String toProxyName(String name, boolean javaType) {
        return (javaType || name.endsWith(">")) ? name : name + "Proxy";
    }

    public String writeClass() {
        return protoClass.entries.isEmpty() ? generateClass(protoClass) : generateEnum(protoClass);
    }

    protected String generateEnum(MetaClass protoClass) {
        StringBuilder entries = new StringBuilder();
        StringBuilder casesTo = new StringBuilder();
        StringBuilder casesFrom = new StringBuilder();

        for (String entry : protoClass.entries) {
            if (entries.length() > 0) {
                entries.append(",\n");
            }
            entries.append("    ").append(entry);
            casesTo.append("            case ").append(entry).append(":\n")
                .append("                return ").append(clientWriter.getOuterClassName(protoClass.name)).append(".").append(entry).append(";\n");
            casesFrom.append("            case ").append(entry).append(":\n")
                .append("                return ").append(entry).append(";\n");
        }
        entries.append(";\n");

        return Util.format(TEMPLATE_ENUM,
                           "package", protoClass.packet,
                           "protoClassName", clientWriter.getOuterClassName(protoClass.name),
                           "className", getName(),
                           "entries", entries,
                           "casesTo", casesTo,
                           "casesFrom", casesFrom);
    }

    protected String generateClass(MetaClass protoClass) {
        StringBuilder fields = new StringBuilder();
        String allFieldsConstructor = "";
        String requiredFieldsConstructor = "";
        StringBuilder copyFieldsFromProto = new StringBuilder();
        StringBuilder copyFieldsToProto = new StringBuilder();

        List<MetaProperty> requiredProperties = new ArrayList<>();
        List<MetaProperty> allProperties = new ArrayList<>();
        for (MetaProperty property : protoClass.properties) {
            if (protoClass.rpcResponse
                && ("error".equals(property.name) || "errorText".equals(property.name))) {
                continue;
            }
            fields.append(Util.format(TEMPLATE_FILED,
                                      "class", toProxyName(property.type, property.javaType),
                                      "name", property.name
            )).append("\n");
            if (property.required) {
                requiredProperties.add(property);
            }
            allProperties.add(property);
        }
        if (!allProperties.isEmpty()) {
            allFieldsConstructor = generateConstructor(getName(), allProperties);
            for (MetaProperty property : allProperties) {
                boolean simpleType = Util.isSimple(property.type);
                if (!simpleType) {
                    copyFieldsToProto.append("        if (request.").append(property.name).append(" != null) {\n");
                    copyFieldsToProto.append("    ");
                }
                String setterPrefix = "set";
                String getterSuffix = "";
                boolean collection = false;
                if (property.type.startsWith("java.util.List<") || property.type.startsWith("java.util.Set<")) {
                    setterPrefix = "addAll";
                    getterSuffix = "List";
                    collection = true;
                }
                if (property.type.startsWith("java.util.Map<")) {
                    setterPrefix = "putAll";
                    getterSuffix = "Map";
                    collection = true;
                }

                if (property.javaType) {
                    if (!collection) {
                        copyFieldsFromProto.append("        if (response.has").append(Util.firstLetterToUpperCase(property.name)).append("()) {\n");
                    }
                    copyFieldsFromProto.append("            result.").append(property.name)
                        .append(" = response.get").append(Util.firstLetterToUpperCase(property.name)).append(getterSuffix)
                        .append("();\n");
                    if (!collection) {
                        copyFieldsFromProto.append("        }\n");
                    }
                    copyFieldsToProto.append("        builder.").append(setterPrefix).append(Util.firstLetterToUpperCase(property.name))
                        .append("(request.").append(property.name).append(");\n");
                } else {
                    if (!collection) {
                        copyFieldsFromProto.append("        if (response.has").append(Util.firstLetterToUpperCase(property.name)).append("()) {\n");
                    }
                    copyFieldsFromProto.append("            result.").append(property.name)
                        .append(" = ");
                    if (!collection) {
                        copyFieldsFromProto.append(property.type).append("Proxy.fromProto(");
                    }
                    copyFieldsFromProto.append("response.get").append(Util.firstLetterToUpperCase(property.name)).append(getterSuffix).append("()");
                    if (!collection) {
                        copyFieldsFromProto.append(");\n").append("        }\n");
                    } else {
                        copyFieldsFromProto.append(";\n");
                    }

                    copyFieldsToProto.append("        builder.").append(setterPrefix).append(Util.firstLetterToUpperCase(property.name)).append("(");
                    if (!collection) {
                        copyFieldsToProto.append(property.type).append("Proxy.toProto(");
                    }
                    copyFieldsToProto.append("request.").append(property.name).append(")");
                    if (!collection) {
                        copyFieldsToProto.append(");\n");
                    } else {
                        copyFieldsToProto.append(";\n");
                    }
                }
                if (!simpleType) {
                    copyFieldsToProto.append("        }\n");
                }
            }
        }
        if (!requiredProperties.isEmpty() && requiredProperties.size() != allProperties.size()) {
            requiredFieldsConstructor = generateConstructor(getName(), requiredProperties);
        }

        return Util.format(TEMPLATE_CLASS,
                           "package", protoClass.packet,
                           "className", getName(),
                           "allFieldsConstructor", allFieldsConstructor,
                           "requiredFieldsConstructor", requiredFieldsConstructor,
                           "protoClassName", protoClass.rpcResponse || protoClass.rpcRequest
                                             ? clientWriter.getOuterClassName(protoClass.name)
                                             : protoClass.getFullName(),
                           "copyFieldsToProto", copyFieldsToProto,
                           "copyFieldsFromProto", copyFieldsFromProto,
                           "fields", fields
        );
    }

    protected String generateConstructor(String className, Collection<MetaProperty> properties) {
        StringBuilder result = new StringBuilder();
        for (MetaProperty property : properties) {
            if (result.length() > 0) {
                result.append(", ");
            }
            result.append(toProxyName(property.type, property.javaType)).append(" ").append(property.name);
        }
        result.insert(0, "    public " + className + "(");
        result.append("){\n");
        for (MetaProperty property : properties) {
            result.append("        this.").append(property.name).append(" = ").append(property.name).append(";\n");
        }
        result.append("    }\n\n");
        return result.toString();
    }
}
