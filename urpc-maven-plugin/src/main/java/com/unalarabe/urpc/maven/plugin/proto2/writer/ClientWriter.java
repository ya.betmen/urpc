package com.unalarabe.urpc.maven.plugin.proto2.writer;

import com.google.inject.Inject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import java.util.concurrent.Future;
import com.unalarabe.urpc.client.AbstractServiceClient;
import com.unalarabe.urpc.client.AbstractServiceClientProvider;
import com.unalarabe.urpc.client.ChannelConfig;
import com.unalarabe.urpc.exception.ConvertException;
import com.unalarabe.urpc.exception.MethodCallException;
import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.util.ProcessManager;
import com.unalarabe.urpc.util.UrpcFuture;
import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.meta.MetaClass;
import com.unalarabe.urpc.meta.MetaMethod;
import com.unalarabe.urpc.meta.MetaProperty;
import com.unalarabe.urpc.meta.MetaService;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ClientWriter {

    public static final String TEMPLATE
        = "package {javaPackage};\n"
          + "\n"
          + "import " + AbstractServiceClient.class.getCanonicalName() + ";\n"
          + "import " + ConvertException.class.getCanonicalName() + ";\n"
          + "import " + InvalidProtocolBufferException.class.getCanonicalName() + ";\n"
          + "import " + Message.class.getCanonicalName() + ";\n"
          + "import " + MethodCallException.class.getCanonicalName() + ";\n"
          + "import " + UrpcFuture.class.getCanonicalName() + ";\n"
          + "import " + UrpcException.class.getCanonicalName() + ";\n"
          + "import " + Future.class.getCanonicalName() + ";\n"
          + "import " + ChannelConfig.class.getCanonicalName() + ";\n"
          + "import " + Level.class.getCanonicalName() + ";\n"
          + "import " + Logger.class.getCanonicalName() + ";\n"
          + ""
          + "\n"
          + "public class {className} extends " + AbstractServiceClient.class.getSimpleName() + "<" + Message.class.getSimpleName() + "> {\n"
          + "\n"
          + "    private static final " + Logger.class.getSimpleName() + " LOG = " + Logger.class.getSimpleName() + ".getLogger({className}.class.getName());"
          + "\n"
          + "    public {className}(" + ChannelConfig.class.getSimpleName() + " channelConfig) {\n"
          + "        super(channelConfig);\n"
          + "    }\n"
          + "\n"
          + "{methods}"
          + "{futures}"
          + "{exceptions}"
          + "\n"
          + "    @Override\n"
          + "    protected " + Message.class.getSimpleName() + " parseResponse(String method, byte[] data) throws " + ConvertException.class.getSimpleName() + " {\n"
          + "        try {\n"
          + "            switch (method) {\n"
          + "{cases}"
          + "                default: {\n"
          + "                    throw new IllegalStateException(\"Method not found: \" + method);\n"
          + "                }\n"
          + "            }\n"
          + "        } catch (" + InvalidProtocolBufferException.class.getSimpleName() + " ex) {\n"
          + "            LOG.log(Level.FINE, ex.getMessage(), ex);\n"
          + "            throw new " + ConvertException.class.getSimpleName() + "(ex);\n"
          + "        }\n"
          + "    }\n"
          + "\n"
          + "}\n"
          + "";
    public static final String TEMPLATE_METHOD
        = "    public {futureName} {methodName}({parameters}) throws " + MethodCallException.class.getSimpleName() + " {\n"
          + "        try {\n"
          + "            {outerClassRequest}.Builder builder = {outerClassRequest}.newBuilder();\n"
          + "{fieldsToProto}\n"
          + "            " + Future.class.getSimpleName() + "<{javaPackage}.{responseType}> value = callMethod(\"{methodName}\", builder.build().toByteArray());\n"
          + "{fieldsFromProto}\n"
          + "            return new {futureName}(value);\n"
          + "        } catch (" + MethodCallException.class.getSimpleName() + " ex) {\n"
          + "            throw ex;\n"
          + "        } catch (Throwable ex) {\n"
          + "            throw new " + MethodCallException.class.getSimpleName() + "(ex);\n"
          + "        }\n"
          + "    }\n";
    public static final String TEMPLATE_OVERLOADED_METHOD
        = "    public {futureName} {methodName}({parameters}) throws " + MethodCallException.class.getSimpleName() + " {\n"
          + "    }\n";
    public static final String TEMPLATE_CASE
        = "                case \"{methodName}\": {\n"
          + "                    return {javaPackage}.{responseType}.parseFrom(data);\n"
          + "                }\n";

    public static final String TEMPLATE_PROVIDER
        = "package {javaPackage};\n"
          + "\n"
          + "import " + Inject.class.getCanonicalName() + ";\n"
          + "import " + UrpcException.class.getCanonicalName() + ";\n"
          + "import " + AbstractServiceClientProvider.class.getCanonicalName() + ";\n"
          + "import " + ProcessManager.class.getCanonicalName() + ";\n"
          + "import " + ChannelConfig.class.getCanonicalName() + ";\n"
          + "\n"
          + "public class {providerClassName} extends AbstractServiceClientProvider<{clientClassName}> {\n"
          + "\n"
          + "    private " + ProcessManager.class.getSimpleName() + " processManager;\n"
          + "\n"
          + "    @Inject\n"
          + "    public {providerClassName}(" + ProcessManager.class.getSimpleName() + " processManager, " + ChannelConfig.class.getSimpleName() + " channelConfig) {\n"
          + "        super(channelConfig);\n"
          + "        this.processManager = processManager;\n"
          + "    }\n"
          + "\n"
          + "    @Override\n"
          + "    protected {clientClassName} createService(" + ChannelConfig.class.getSimpleName() + " channelConfig) throws " + UrpcException.class.getSimpleName() + " {\n"
          + "        {clientClassName} client = new {clientClassName}(channelConfig);\n"
          + "        processManager.addListener(client);\n"
          + "        return client;\n"
          + "    }\n"
          + "\n"
          + "    @Override\n"
          + "    protected String getServiceName() {\n"
          + "        return \"{serviceName}\";\n"
          + "    }\n"
          + "}\n"
          + "";

    public static final String TEMPLATE_CHECK_EXCEPTION
        = "        private void checkException({javaPackage}.{responseType} response){throwExceptions} {\n"
          + "            if (response.hasErrorValue()) {\n"
          + "                {enumClass} errorValue = response.getErrorValue();\n"
          + "                String errorText = response.getErrorText();\n"
          + "                switch (errorValue) {\n"
          + "{errorCases}"
          + "                    default:\n"
          + "                        throw new IllegalStateException(\"Can't be here\");\n"
          + "                }\n"
          + "            }\n"
          + "        }\n\n";

    public static final String TEMPLATE_FUTURE
        = "    public static class {futureName} extends " + UrpcFuture.class.getSimpleName() + "<{javaPackage}.{responseType}>{\n"
          + "\n"
          + "        public {futureName}(Future<{javaPackage}.{responseType}> future) {\n"
          + "            super(future);\n"
          + "        }\n"
          + "\n"
          + "        public {originalResponseType} val(){throwExceptions} {\n"
          + "            {javaPackage}.{responseType} response = super.get();\n"
          + "{checkExceptionCall}"
          + "{return}"
          + "        }\n"
          + "\n"
          + "        public {originalResponseType} val(long timeout, " + TimeUnit.class.getCanonicalName() + " unit){throwExceptions} {\n"
          + "            {javaPackage}.{responseType} response = super.get(timeout, unit);\n"
          + "{checkExceptionCall}"
          + "{return}"
          + "        }\n"
          + "\n"
          + "{checkException}"
          + "        @Deprecated\n"
          + "        @Override\n"
          + "        public {javaPackage}.{responseType} get() throws " + UrpcException.class.getSimpleName() + " {\n"
          + "            return super.get();\n"
          + "        }\n"
          + "\n"
          + "        @Deprecated\n"
          + "        @Override\n"
          + "        public {javaPackage}.{responseType} get(long timeout, " + TimeUnit.class.getCanonicalName() + " unit) throws " + UrpcException.class.getSimpleName() + " {\n"
          + "            return super.get(timeout, unit);\n"
          + "        }\n"
          + "\n"
          + "    }";

    private final String javaPackage;
    private final MetaService protoService;
    private final boolean innerClasses;
    private final List<MetaClass> protoClasses;
    private final Set<String> usedClasses = new HashSet<>();

    public ClientWriter(String javaPackage, MetaService protoService, List<MetaClass> protoClasses, boolean innerClasses) {
        this.javaPackage = javaPackage;
        this.protoService = protoService;
        this.protoClasses = protoClasses;
        this.innerClasses = innerClasses;
    }

    public String getClassName() {
        return protoService.name + "Client";
    }

    public String getProviderClassName() {
        return protoService.name + "ClientProvider";
    }

    public String writeService() {
        StringBuilder methods = new StringBuilder();
        StringBuilder cases = new StringBuilder();
        StringBuilder futures = new StringBuilder();
        StringBuilder exceptions = new StringBuilder();

        Set<MetaClass> usedProtoClasses = new HashSet<>();
        Set<String> exceptionSet = new HashSet<>();
        for (MetaMethod method : protoService.methods) {

            boolean overloaded = false;
            MetaClass resultClass = getClass(method.resultType);
            MetaClass parameterClass = getClass(method.parameterType);

            usedProtoClasses.add(resultClass);
            usedProtoClasses.add(parameterClass);

            int maxNumber = 0;
            String methodName = method.name;
            for (MetaProperty property : resultClass.properties) {
                if (isResponse(property) || getResponseNumber(property) != null) {
                    overloaded = true;
                    maxNumber = Math.max(maxNumber, getResponseNumber(property));
                } else if (isRequest(property) || getRequestNumber(property) != null) {
                    overloaded = true;
                    maxNumber = Math.max(maxNumber, getRequestNumber(property));
                }
            }

            if (!overloaded) {
                StringBuilder fieldsToProto = new StringBuilder();
                StringBuilder fieldsFromProto = new StringBuilder();

                for (MetaProperty property : parameterClass.properties) {
                    boolean simpleType = Util.isSimple(property.type);
                    if (!simpleType) {
                        fieldsToProto.append("            if (").append(property.name).append(" != null) {\n");
                        fieldsToProto.append("    ");
                    }
                    String setterName = "set" + Util.firstLetterToUpperCase(property.name);
                    if (property.type.startsWith("java.util.List<")
                        || property.type.startsWith("java.util.Set<")) {
                        setterName = "addAll" + Util.firstLetterToUpperCase(property.name);
                    }
                    if (property.javaType) {
                        fieldsToProto.append("            builder.").append(setterName)
                            .append("(").append(property.name).append(");\n");
                    } else {
                        fieldsToProto.append("            builder.").append(setterName)
                            .append("(").append(property.type).append("Proxy.toProto(").append(property.name).append("));\n");
                    }
                    if (!simpleType) {
                        fieldsToProto.append("            }\n");
                    }
                }

                StringBuilder errorCases = new StringBuilder();
                StringBuilder throwExceptions = new StringBuilder();
                String checkException;
                String checkExceptionCall;
                if (method.errors.isEmpty()) {
                    checkExceptionCall = "";
                    checkException = "";
                } else {
                    for (String error : method.errors) {
                        if (throwExceptions.length() > 0) {
                            throwExceptions.append(", ");
                        } else {
                            throwExceptions.append(" throws ");
                        }
                        int lastIndexOfUnderline = error.lastIndexOf("_");
                        String errorName = error.substring(lastIndexOfUnderline + 1);
                        throwExceptions.append(errorName);

                        errorCases.append("                    case ").append(error)
                            .append(":\n                        throw new ")
                            .append(errorName).append("(errorText);\n");

                        exceptionSet.add(errorName);
                    }
                    checkExceptionCall = "            checkException(response);\n";
                    String errorEnum = protoService.name + Util.firstLetterToUpperCase(methodName) + "ErrorEnum";
                    usedClasses.add(javaPackage + "." + errorEnum);
                    checkException = Util.format(TEMPLATE_CHECK_EXCEPTION,
                                                 "javaPackage", javaPackage,
                                                 "responseType", getOuterClassName(method.resultType),
                                                 "errorCases", errorCases,
                                                 "enumClass", getOuterClassName(errorEnum),
                                                 "throwExceptions", throwExceptions
                    );
                }

                String returnStr = "";
                String originalResponseType = "void";
                String responseProxyType = method.resultType;
                if (!responseProxyType.startsWith("java.util.Map<")) {
                    responseProxyType += "Proxy";
                }
                for (MetaProperty property : resultClass.properties) {
                    if ("result".equals(property.name)) {
                        originalResponseType = property.type;
                        if (!property.javaType && !originalResponseType.startsWith("java.util.Map<")) {
                            originalResponseType += "Proxy";
                        }
                        returnStr = Util.format("            return {responseProxyType}.fromProto(response).result;\n",
                                                "responseProxyType", responseProxyType);
                    }
                }

                StringBuilder parameters = new StringBuilder();

                for (MetaProperty property : parameterClass.properties) {
                    if (parameters.length() > 0) {
                        parameters.append(", ");
                    }
                    parameters.append(property.type);
                    if (!property.javaType) {
                        parameters.append("Proxy");
                    }
                    parameters.append(" ").append(property.name);
                }

                futures.append(Util.format(TEMPLATE_FUTURE,
                                           "futureName", Util.firstLetterToUpperCase(methodName) + "Future",
                                           "javaPackage", javaPackage,
                                           "responseType", getOuterClassName(method.resultType),
                                           "originalResponseType", originalResponseType,
                                           "responseProxyType", responseProxyType,
                                           "checkException", checkException,
                                           "checkExceptionCall", checkExceptionCall,
                                           "throwExceptions", throwExceptions,
                                           "return", returnStr
                )).append("\n\n");
                methods.append(Util.format(TEMPLATE_METHOD,
                                           "javaPackage", javaPackage,
                                           "parameters", parameters,
                                           "futureName", Util.firstLetterToUpperCase(methodName) + "Future",
                                           "responseType", getOuterClassName(method.resultType),
                                           "requestType", method.parameterType + "Proxy",
                                           "outerClassRequest", getOuterClassName(method.parameterType),
                                           "fieldsToProto", fieldsToProto,
                                           "fieldsFromProto", fieldsFromProto,
                                           "methodName", methodName)
                );
                cases.append(Util.format(TEMPLATE_CASE,
                                         "javaPackage", javaPackage,
                                         "responseType", getOuterClassName(method.resultType),
                                         "methodName", methodName));
            } else {
                for (int number = 0; number <= maxNumber; number++) {
                    String originalResponseType = "void";
                    String returnStr = "";

                    MetaClass requestClass = getRequest(parameterClass.properties, methodName, number);
                    MetaClass responseClass = getResponse(resultClass.properties, methodName, number);
                    StringBuilder parameters = new StringBuilder();

                    for (MetaProperty property : requestClass.properties) {
                        if (parameters.length() > 0) {
                            parameters.append(", ");
                        }
                        parameters.append(property.type);
                        if (!property.javaType) {
                            parameters.append("Proxy");
                        }
                        parameters.append(" ").append(property.name);
                    }
                    for (MetaProperty property : responseClass.properties) {
                        if ("result".equals(property.name)) {
                            originalResponseType = property.type;
                            returnStr = "            return response" + number + ".getResult();\n";
                        }
                    }

                    StringBuilder fieldsToProto = new StringBuilder();
                    StringBuilder fieldsFromProto = new StringBuilder();

                    fieldsToProto.append("            ")
                        .append(getOuterClassName(method.parameterType)).append(number).append(".Builder builder").append(number)
                        .append(" = ").append(getOuterClassName(method.parameterType)).append(number).append(".newBuilder();\n");

                    for (MetaProperty property : requestClass.properties) {
                        boolean simpleType = Util.isSimple(property.type);
                        if (!simpleType) {
                            fieldsToProto.append("            if (").append(property.name).append(" != null) {\n");
                            fieldsToProto.append("    ");
                        }
                        if (property.javaType) {
                            fieldsToProto.append("            builder").append(number).append(".set").append(Util.firstLetterToUpperCase(property.name))
                                .append("(").append(property.name).append(");\n");
                        } else {
                            fieldsToProto.append("            builder").append(number).append(".set").append(Util.firstLetterToUpperCase(property.name))
                                .append("(").append(property.type).append("Proxy.toProto(").append(property.name).append("));\n");
                        }
                        if (!simpleType) {
                            fieldsToProto.append("            }\n");
                        }
                    }
                    fieldsToProto.append("            builder.setRequest").append(number).append("(builder").append(number).append(");\n");

                    String responseProxyType = responseClass.getFullName();
                    if (!responseProxyType.startsWith("java.util.Map<")) {
                        responseProxyType += "Proxy";
                    }

                    methods.append(Util.format(TEMPLATE_METHOD,
                                               "javaPackage", javaPackage,
                                               "parameters", parameters,
                                               "futureName", Util.firstLetterToUpperCase(methodName) + "Future" + number,
                                               "responseType", getOuterClassName(method.resultType),
                                               "requestType", requestClass.getFullName() + "Proxy",
                                               "outerClassRequest", getOuterClassName(method.parameterType),
                                               "fieldsToProto", fieldsToProto,
                                               "fieldsFromProto", fieldsFromProto,
                                               "methodName", methodName)
                    );

                    StringBuilder errorCases = new StringBuilder();
                    StringBuilder throwExceptions = new StringBuilder();
                    String checkException = "";
                    String checkExceptionCall = "";

                    MetaClass errorClass = null;
                    for (MetaProperty property : responseClass.properties) {
                        if ("errorValue".equals(property.name)) {
                            errorClass = getClass(property.type);
                            break;
                        }
                    }

                    if (errorClass != null) {
                        for (String error : errorClass.entries) {
                            if (throwExceptions.length() > 0) {
                                throwExceptions.append(", ");
                            } else {
                                throwExceptions.append(" throws ");
                            }
                            int lastIndexOfUnderline = error.lastIndexOf("_");
                            String errorName = error.substring(lastIndexOfUnderline + 1);
                            throwExceptions.append(errorName);

                            errorCases.append("                    case ").append(error)
                                .append(":\n                        throw new ")
                                .append(errorName).append("(errorText);\n");

                            exceptionSet.add(errorName);
                        }
                        checkExceptionCall = "            checkException(response" + number + ");\n";
                        String errorEnum = protoService.name + Util.firstLetterToUpperCase(methodName) + number + "ErrorEnum";
                        usedClasses.add(javaPackage + "." + errorEnum);
                        checkException = Util.format(TEMPLATE_CHECK_EXCEPTION,
                                                     "javaPackage", javaPackage,
                                                     "responseType", getOuterClassName(responseClass.name),
                                                     "errorCases", errorCases,
                                                     "enumClass", getOuterClassName(errorEnum),
                                                     "throwExceptions", throwExceptions
                        );
                    }

                    checkExceptionCall = "            " + getOuterClassName(responseClass.name) + " response" + number + " = response.getResponse" + number + "();\n"
                                         + checkExceptionCall;
                    futures.append(Util.format(TEMPLATE_FUTURE,
                                               "futureName", Util.firstLetterToUpperCase(methodName) + "Future" + number,
                                               "javaPackage", javaPackage,
                                               "responseType", getOuterClassName(method.resultType),
                                               "originalResponseType", originalResponseType,
                                               "responseProxyType", responseProxyType,
                                               "checkException", checkException,
                                               "checkExceptionCall", checkExceptionCall,
                                               "throwExceptions", throwExceptions,
                                               "return", returnStr
                    )).append("\n\n");
                }

                cases.append(Util.format(TEMPLATE_CASE,
                                         "javaPackage", javaPackage,
                                         "responseType", getOuterClassName(method.resultType),
                                         "methodName", methodName));
            }
        }
        for (String exception : exceptionSet) {
            exceptions
                .append("    public static class ").append(exception).append(" extends Exception {\n")
                .append("        public ").append(exception).append(" () { }\n")
                .append("        public ").append(exception).append(" (String message) {\n")
                .append("            super(message);\n")
                .append("        }\n")
                .append("    }\n");
        }

        HashSet<MetaClass> childClasses = new HashSet<>();
        do {
            usedProtoClasses.addAll(childClasses);
            childClasses.clear();
            for (MetaClass usedProtoClass : usedProtoClasses) {
                for (MetaProperty property : usedProtoClass.properties) {
                    MetaClass protoClass = getClass(property.type);
                    if (protoClass != null) {
                        childClasses.add(protoClass);
                    } else {
                        usedClasses.add(property.type);
                    }
                }
            }
            childClasses.removeAll(usedProtoClasses);
        } while (!childClasses.isEmpty());
        for (MetaClass usedProtoClass : usedProtoClasses) {
            usedClasses.add(usedProtoClass.getFullName());
        }

        return Util.format(TEMPLATE,
                           "javaPackage", javaPackage,
                           "className", getClassName(),
                           "serviceName", protoService.name,
                           "methods", methods,
                           "futures", futures,
                           "exceptions", exceptions,
                           "cases", cases
        );
    }

    private MetaClass getRequest(Set<MetaProperty> properties, String methodName, int index) {
        String requestName = "Request" + Util.firstLetterToUpperCase(methodName) + index;
        for (MetaProperty property : properties) {
            if (requestName.equals(property.type)) {
                return getClass(property.type);
            }
        }
        return null;
    }

    private MetaClass getResponse(Set<MetaProperty> properties, String methodName, int index) {
        String responseName = "Response" + Util.firstLetterToUpperCase(methodName) + index;
        for (MetaProperty property : properties) {
            if (responseName.equals(property.type)) {
                return getClass(property.type);
            }
        }
        return null;
    }

    private Integer getResponseNumber(MetaProperty property) {
        if (property.name.startsWith("response")) {
            try {
                return Integer.parseInt(property.name.substring(8));
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return null;
    }

    private Integer getRequestNumber(MetaProperty property) {
        if (property.name.startsWith("request")) {
            try {
                return Integer.parseInt(property.name.substring(7));
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return null;
    }

    private boolean isResponse(MetaProperty property) {
        if (property.name.startsWith("response")) {
            try {
                Integer number = Integer.parseInt(property.name.substring(8));
                return true;
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return false;
    }

    private boolean isRequest(MetaProperty property) {
        if (property.name.startsWith("request")) {
            try {
                Integer number = Integer.parseInt(property.name.substring(8));
                return true;
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return false;
    }

    public Set<String> getUsedClasses() {
        return usedClasses;
    }

    protected MetaClass getClass(String name) {
        for (MetaClass protoClass : protoClasses) {
            if (protoClass.name.equals(name)) {
                return protoClass;
            }
        }
        for (MetaClass protoClass : protoClasses) {
            if (protoClass.getFullName().equals(name)) {
                return protoClass;
            }
        }
        return null;
    }

    protected String getOuterClassName(String typeName) {
        return (innerClasses ? protoService.name + "OuterClass." : "") + typeName;
    }

    public String writeServiceProvider() {
        return Util.format(TEMPLATE_PROVIDER,
                           "javaPackage", javaPackage,
                           "clientClassName", getClassName(),
                           "serviceName", protoService.name,
                           "providerClassName", getProviderClassName());
    }

}
