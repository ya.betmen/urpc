package com.unalarabe.urpc.maven.plugin.avro.writer;

import com.google.inject.Injector;
import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.config.ConnectionConfig;
import com.unalarabe.urpc.exception.ConvertException;
import com.unalarabe.urpc.maven.plugin.Util;
import com.unalarabe.urpc.service.AbstractService;
import com.unalarabe.urpc.service.Connection;
import com.unalarabe.urpc.service.RpcMessage;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ServiceWriter {

    public static final String TEMPLATE
        = "package {javaNamespace};\n"
          + "\n"
          + "import {javaNamespace}.{serviceName}Proxy;\n"
          + "import " + AbstractService.class.getCanonicalName() + ";\n"
          + "import " + Injector.class.getCanonicalName() + ";\n"
          + "import " + ConnectionConfig.class.getCanonicalName() + ";\n"
          + "import " + Connection.class.getCanonicalName() + ";\n"
          + "import " + RpcMessage.class.getCanonicalName() + ";\n"
          + "import " + ConvertException.class.getCanonicalName() + ";\n"
          + "import {serviceFullName};\n"
          + "\n"
          + "public class {jmsServiceName} extends " + AbstractService.class.getSimpleName() + "<{serviceName}, {serviceName}Proxy> {\n"
          + "\n"
          + "    public {jmsServiceName}(Class<? extends " + Connection.class.getSimpleName() + "> connectionClass, " + Injector.class.getSimpleName() + " injector, " + ConnectionConfig.class.getSimpleName() + " connectionConfig) {\n"
          + "        super(connectionClass, {serviceName}.class, injector, connectionConfig);\n"
          + "    }\n"
          + "\n"
          + "    @Override\n"
          + "    protected {serviceName}Proxy wrapService({serviceName} service) {\n"
          + "        return new {serviceName}Proxy(service);\n"
          + "    }\n"
          + "    @Override\n"
          + "    public " + RpcMessage.class.getSimpleName() + " parseRpcMessageRequest(byte[] data) throws " + ConvertException.class.getSimpleName() + " {\n"
          + "        return {serviceName}Proxy.RPC_PARSER.parseRpcMessageRequest(data);\n"
          + "    }\n"
          + "\n"
          + "}\n"
          + "";

    private final String javaNamespace;
    private final ExportService exportService;
    private final String jmsServiceName;

    public ServiceWriter(String javaNamespace, ExportService exportService) {
        this.javaNamespace = javaNamespace;
        this.exportService = exportService;
        this.jmsServiceName = Util.getClassName(exportService.name) + "JmsService";
    }

    public String getJmsServiceName() {
        return jmsServiceName;
    }

    public String write() {
        return Util.format(TEMPLATE,
                           "javaNamespace", javaNamespace + "." + Util.toNamespace(exportService.name),
                           "serviceName", Util.getClassName(exportService.name),
                           "jmsServiceName", jmsServiceName,
                           "serviceFullName", exportService.name
        );
    }

}
