package com.unalarabe.urpc.jms.util;

import com.unalarabe.urpc.util.ServiceUtil;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class JmsUtilTest {

    public JmsUtilTest() {
    }

    /**
     * Test of unwrapTo method, of class JmsUtil.
     */
    @Test
    public void testUnwrapTo() {
        System.out.println("unwrapTo");

        NumberFormatException ex0 = new NumberFormatException("test0");
        IllegalStateException ex1 = new IllegalStateException("test1", ex0);
        IllegalArgumentException ex2 = new IllegalArgumentException("test2", ex1);
        UnsupportedOperationException ex3 = new UnsupportedOperationException("test3", ex2);

        IllegalStateException ise = ServiceUtil.unwrapTo(ex3, IllegalStateException.class);
        assertEquals("test1", ise.getMessage());
        NumberFormatException nfe = ServiceUtil.unwrapTo(ex3, NumberFormatException.class);
        assertEquals("test0", nfe.getMessage());
    }

}
