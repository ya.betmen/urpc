package com.unalarabe.urpc.service;

import com.google.common.base.Preconditions;
import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.util.ErrorReason;
import com.unalarabe.urpc.util.ServiceUtil;
import com.unalarabe.urpc.util.WaitThread;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <REQUEST>
 * @param <RESPONSE>
 * @param <CONVERTER>
 * @param <SERVICE>
 * @param <PROXY>
 */
public class ServiceCallable<REQUEST, RESPONSE, CONVERTER extends RequestResponseConverter<REQUEST, RESPONSE>, SERVICE, PROXY extends ServiceProxy<? extends SERVICE>> implements Callable<Message> {

    private static final Logger LOG = Logger.getLogger(ServiceCallable.class.getName());

    private REQUEST request;
    private CONVERTER converter;
    private WaitThread.Slot slot;

    public ServiceCallable(REQUEST request, CONVERTER converter, WaitThread.Slot slot) {
        this.request = request;
        this.converter = converter;
        this.slot = Preconditions.checkNotNull(slot);
    }

    @Override
    public Message call() throws Exception {
        Message result = null;
        Message requestMessage = null;
        try {
            requestMessage = converter.convertFrom(request);
            ServiceThread<PROXY> thread = (ServiceThread<PROXY>) Thread.currentThread();
            PROXY serviceProxy = thread.getServiceProxy();
            result = serviceProxy.processMessage(requestMessage);
        } catch (UrpcException ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex);
            Throwable th = ServiceUtil.unwrap(ex);
            result = new Message();
            result.setErrorReason(ErrorReason.RPC_FAILED.getReason());
            result.setErrorText(th.getMessage());
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            Throwable th = ServiceUtil.unwrap(ex);
            result = new Message();
            result.setErrorReason(ErrorReason.RPC_ERROR.getReason());
            result.setErrorText(th.getMessage());
        } finally {
            if (result == null) {
                slot.setError("Unknown error");
            } else {
                if (requestMessage != null) {
                    result.setCorrelationId(requestMessage.getCorrelationId());
                    result.setDestination(requestMessage.getReplyTo());
                    result.setDestinationName(requestMessage.getReplyToName());
                    result.setMethod(requestMessage.getMethod());
                }
            }
            slot.setResult(result);
            clear();
        }
        return result;
    }

    protected void clear() {
        this.request = null;
        this.converter = null;
        this.slot = null;
    }

}
