package com.unalarabe.urpc.service.kafka;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Injector;
import com.unalarabe.urpc.config.ConnectionConfig;
import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.service.Connection;
import com.unalarabe.urpc.service.Message;
import com.unalarabe.urpc.service.RpcMessage;
import com.unalarabe.urpc.util.RpcParser;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDecoder;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.json.JSONObject;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.ConsumerTimeoutException;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;
import kafka.serializer.StringDecoder;
import kafka.utils.VerifiableProperties;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class KafkaConnection extends Connection<ConsumerRecord, ProducerRecord> {

    private static final Logger LOG = Logger.getLogger(KafkaConnection.class.getName());

    protected KafkaProducer producer;
    protected KafkaConsumer consumer;

    public KafkaConnection(Injector injector, RpcParser rpcParser, ConnectionConfig connectionConfig, String serviceQueue, ThreadFactory threadFactory, UncaughtExceptionHandler uncaughtExceptionHandler) {
        super(injector, rpcParser, connectionConfig, serviceQueue, threadFactory, uncaughtExceptionHandler);
        if (!connectionConfig.getProperties().containsKey("schema.registry.url")) {
            throw new IllegalArgumentException("Missing parameter 'schema.registry.url'");
        }
    }

    @Override
    public Connection init() {
        if (!stoped) {
            producer = new KafkaProducer(getProducerProperties());

            Properties props = new Properties();
            props.put("bootstrap.servers", "192.168.57.101:9092");
            props.put("group.id", "test");
            props.put("enable.auto.commit", "true");
            props.put("auto.commit.interval.ms", "1000");
//            props.put("autooffset.reset", "largest");
            props.put("session.timeout.ms", "30000");
            props.put("key.deserializer", StringDeserializer.class.getCanonicalName());
            props.put("value.deserializer", KafkaAvroDeserializer.class.getCanonicalName());
            props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
            props.put(AbstractKafkaAvroSerDeConfig.AUTO_REGISTER_SCHEMAS, true);
            props.put("schema.registry.url", "http://192.168.57.101:8081");
            consumer = new KafkaConsumer(props);
        }
        return this;
    }

    protected Map<Object, Object> getConumerProperties() {
        Map<Object, Object> props = new HashMap<>();
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
//        props.put("autooffset.reset", "largest");
        props.put("session.timeout.ms", "30000");
        props.put("group.id", "test");

        props.putAll(connectionConfig.getProperties());

        props.put("bootstrap.servers", connectionConfig.getBrokerUri());
        props.put("key.deserializer", StringDeserializer.class.getCanonicalName());
        props.put("value.deserializer", KafkaAvroDeserializer.class.getCanonicalName());
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
        props.put(AbstractKafkaAvroSerDeConfig.AUTO_REGISTER_SCHEMAS, true);
        return props;
    }

    protected Map<Object, Object> getProducerProperties() {
        Map<Object, Object> props = new HashMap<>();
        props.put("acks", "all");
//        props.put("retries", 0);
//        props.put("batch.size", 16384);
//        props.put("linger.ms", 1);
//        props.put("buffer.memory", 33554432);

        props.putAll(connectionConfig.getProperties());

        props.put("bootstrap.servers", connectionConfig.getBrokerUri());
        props.put("key.serializer", StringSerializer.class.getCanonicalName());
        props.put("value.serializer", KafkaAvroSerializer.class.getCanonicalName());
        props.put(AbstractKafkaAvroSerDeConfig.AUTO_REGISTER_SCHEMAS, true);
        return props;
    }

    protected List<KafkaStream<String, Object>> kafkaStream = null;

    @Override
    public void connect() {
        Properties properties = new Properties();
        properties.putAll(getConumerProperties());
//        ConsumerConfig cc = new ConsumerConfig(properties);
//        ConsumerConnector consumerConnector = Consumer.createJavaConsumerConnector(cc);
//        Map<String, List<KafkaStream<String, Object>>> messageStreams = consumerConnector.createMessageStreams(ImmutableMap.of(serviceQueue, 1), new StringDecoder(new VerifiableProperties(properties)), new KafkaAvroDecoder(new VerifiableProperties(properties)));
//        kafkaStream = messageStreams.get(serviceQueue);
        consumer.subscribe(Arrays.asList(serviceQueue));
    }

    private static void runMethod(Object object, String methodName) {
        try {
            Method method = object.getClass().getDeclaredMethod(methodName);
            method.setAccessible(true);
            Object r = method.invoke(object);
        } catch (Throwable ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void beforeShutdown() {
        if (consumer != null) {
            runMethod(consumer, "release");
            consumer.commitSync();
            consumer.close();
        }
        if (producer != null) {
            producer.flush();
            producer.close();
        }
    }

    @Override
    public void mainLoop() throws UrpcException {
        ConsumerRecords consumerRecords = consumer.poll(100);
        Iterator<ConsumerRecord> iterator = consumerRecords.iterator();
        while (iterator.hasNext()) {
            ConsumerRecord consumerRecord = iterator.next();
            LOG.log(Level.INFO, "RECIVE {0}", getCorrelationId(consumerRecord));
            onRequest(consumerRecord);
        }
//        if (kafkaStream != null) {
//            try {
//                Iterator<KafkaStream<String, Object>> it = kafkaStream.iterator();
//                while (it.hasNext()) {
//                    ConsumerIterator<String, Object> it1 = it.next().iterator();
//                    while (it1.hasNext()) {
//                        MessageAndMetadata<String, Object> msgAndMetadata = it1.next();
//                        String key = msgAndMetadata.key();
//                        Object value = msgAndMetadata.message();
//                        System.out.println("MESSAGE: " + value);
//                        ConsumerRecord cr = new ConsumerRecord(serviceQueue, 0, ttlMs, key, value);
//                        onRequest(cr);
//                    }
//                }
//            } catch (ConsumerTimeoutException ex) {
//                LOG.info("consumer timeout");
//            }
//        }
    }

    @Override
    public boolean isRequestValid(Object request) {
        return true;
    }

    @Override
    public String getCorrelationId(ConsumerRecord request) {
        return new JSONObject(request.value().toString()).getString("correlationId");
    }

    @Override
    public Object getReplyTo(ConsumerRecord request) {
        return new JSONObject(request.value().toString()).getString("replyTo");
    }

    @Override
    public boolean sendMessage(Object destination, final Message message, long ttlMs) {
        ProducerRecord producerRecord = convertTo(message);
        LOG.log(Level.INFO, "KafkaConnection: sendMessage {0} to {1}", new Object[]{message.getCorrelationId(), producerRecord.topic()});
        producer.send(producerRecord, new Callback() {
                      @Override
                      public void onCompletion(RecordMetadata metadata, Exception exception) {
                          LOG.log(Level.INFO, "KafkaConnection: message {0} sent", new Object[]{message.getCorrelationId()});
                      }
                  });
        return true;
    }

    @Override
    public Message convertFrom(ConsumerRecord request) throws UrpcException {
        RpcMessage rpcMessage = rpcParser.parseRpcMessageRequest(request);
        Message message = new Message(request.value());
        message.setParsedBody(rpcMessage.getRequest());
        message.setCorrelationId(rpcMessage.getCorrelationId());
        message.setReplyTo(rpcMessage.getReplyTo());
        message.setReplyToName(rpcMessage.getReplyTo());
        message.setVersion(rpcMessage.getVersion());
        message.setMethod(rpcMessage.getMethod());
        return message;
    }

    @Override
    public ProducerRecord convertTo(Message message) throws UrpcException {
        return (ProducerRecord) rpcParser.createRpcMessageResponse(message);
    }

}
