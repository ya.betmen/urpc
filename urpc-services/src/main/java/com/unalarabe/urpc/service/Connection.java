package com.unalarabe.urpc.service;

import com.google.inject.Injector;
import com.unalarabe.urpc.config.ConnectionConfig;
import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.util.ErrorReason;
import com.unalarabe.urpc.util.MessageListener;
import com.unalarabe.urpc.util.MessageSender;
import com.unalarabe.urpc.util.RpcParser;
import com.unalarabe.urpc.util.ServiceUtil;
import com.unalarabe.urpc.util.UrpcThreadPool;
import com.unalarabe.urpc.util.UrpcTimer;
import com.unalarabe.urpc.util.WaitThread;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <REQUEST>
 * @param <RESPONSE>
 */
public abstract class Connection<REQUEST, RESPONSE> extends Thread implements MessageSender, RequestResponseConverter<REQUEST, RESPONSE> {

    private static final Logger LOG = Logger.getLogger(Connection.class.getName());

    protected final Injector injector;
    protected final String serviceQueue;
    protected final ConnectionConfig connectionConfig;
    protected final ThreadFactory threadFactory;
    protected final UncaughtExceptionHandler uncaughtExceptionHandler;
    protected final int ttlMs;
    protected final int awaitTimeMs;
    protected final RpcParser rpcParser;
    protected final Lock runLock = new ReentrantLock();

    protected volatile boolean inited = false;
    protected volatile boolean stoped = false;
    protected volatile boolean inProcess = false;

    protected ThreadPoolExecutor poolExecutor;
    protected WaitThread waitThread;

    public Connection(Injector injector, RpcParser rpcParser, ConnectionConfig connectionConfig, String serviceQueue, ThreadFactory threadFactory, UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.injector = injector;
        this.rpcParser = rpcParser;
        this.serviceQueue = serviceQueue;
        this.connectionConfig = connectionConfig;
        this.threadFactory = threadFactory;
        this.uncaughtExceptionHandler = uncaughtExceptionHandler;
        this.ttlMs = connectionConfig.getTtlMs();
        this.awaitTimeMs = connectionConfig.getAwaitTimeMs();
    }

    @Override
    public void run() {
        try {
            runLock.lock();
            try {
                if (stoped) {
                    return;
                }
                if (!inited) {
                    initInternal();
                    init();
                }
                LOG.log(Level.INFO, "Run service pool for {0}", serviceQueue);
                connect();
            } finally {
                runLock.unlock();
            }
            while (!stoped) {
                inProcess = true;
                try {
                    mainLoop();
                } catch (Throwable th) {
                    throw th;
                } finally {
                    inProcess = false;
                }
            }
            LOG.log(Level.INFO, "Stop service pool for {0}", serviceQueue);
        } catch (Throwable ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    protected void initInternal() {
        UrpcTimer timer = this.injector.getInstance(UrpcTimer.class);
        this.waitThread = new WaitThread(awaitTimeMs, ttlMs, this, this, timer);
        this.waitThread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
        this.poolExecutor = new UrpcThreadPool(connectionConfig, new LinkedBlockingQueue<>(connectionConfig.getMaxPoolSize()), threadFactory);
        this.poolExecutor.prestartCoreThread();
    }

    public void onRequest(Object requestObject) {
        try {
            if (isRequestValid(requestObject)) {
                REQUEST request = (REQUEST) requestObject;
                WaitThread.Slot slot = waitThread.getSlot(request, ttlMs);
                ServiceCallable callable = new ServiceCallable(request, this, slot);
                try {
                    this.poolExecutor.submit(callable);
                } catch (RejectedExecutionException ex) {
                    LOG.log(Level.INFO, ex.getMessage(), ex);
                    Message response = new Message();
                    response.setCorrelationId(getCorrelationId(request));
                    response.setErrorReason(ErrorReason.SERVER_TIMEOUT.getReason());
                    response.setErrorText("Server is busy");
                    sendMessage(getReplyTo(request), response, 0);
                } catch (Exception ex) {
                    LOG.log(Level.INFO, ex.getMessage(), ex);
                    Message response = new Message();
                    response.setCorrelationId(getCorrelationId(request));
                    response.setErrorReason(ErrorReason.RPC_FAILED.getReason());
                    response.setErrorText(ServiceUtil.unwrap(ex).getMessage());
                    sendMessage(getReplyTo(request), response, 0);
                }
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public final synchronized void start() {
        super.start();
    }

    abstract public void connect();

    abstract public boolean isRequestValid(Object request);

    abstract public Connection init();

    abstract public void mainLoop() throws UrpcException;

    protected void beforeShutdown() {

    }

    protected void afterShutdown() {

    }

    public void shutdown() {
        runLock.lock();
        try {
            stoped = true;
            while (inProcess) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    LOG.log(Level.WARNING, ex.getMessage());
                }
            }
            beforeShutdown();
            if (waitThread != null) {
                waitThread.shutdown();
            }
            if (poolExecutor != null) {
                poolExecutor.shutdown();
            }
            int tries = 0;
            while (waitThread != null && waitThread.isAlive() || poolExecutor != null && poolExecutor.getActiveCount() > 0 && tries < 100) {
                tries++;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
            afterShutdown();
        } finally {
            runLock.unlock();
        }
    }
}
