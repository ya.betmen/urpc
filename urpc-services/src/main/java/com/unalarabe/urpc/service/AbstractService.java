package com.unalarabe.urpc.service;

import com.unalarabe.urpc.config.ConnectionConfig;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.inject.Injector;
import com.unalarabe.urpc.exception.UrpcServiceException;
import java.lang.Thread.UncaughtExceptionHandler;
import com.unalarabe.urpc.util.RpcParser;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <SERVICE>
 * @param <PROXY>
 */
public abstract class AbstractService<SERVICE, PROXY extends ServiceProxy<? extends SERVICE>> implements RpcParser, ThreadFactory, UncaughtExceptionHandler {

    private static final Logger LOG = Logger.getLogger(AbstractService.class.getName());

    protected final Class<SERVICE> serviceClass;
    protected final ConnectionConfig connectionConfig;
    protected final String serviceName;
    protected final String serviceQueue;
    protected final Injector injector;
    protected final Class<? extends Connection> connectionClass;

    protected Connection connection;

    protected boolean inited = false;
    protected volatile boolean stoped = false;

    public AbstractService(Class<? extends Connection> connectionClass, String nameSeparator, Class<SERVICE> serviceClass, Injector injector, ConnectionConfig connectionConfig) {
        this.connectionClass = connectionClass;
        this.connectionConfig = connectionConfig;
        this.serviceClass = serviceClass;
        this.serviceName = this.serviceClass.getSimpleName();
        this.serviceQueue = serviceName + nameSeparator + connectionConfig.getEndpoint();
        this.injector = injector;
    }

    public Connection init() throws UrpcServiceException {
        try {
            this.connection = connectionClass
                .getConstructor(Injector.class, RpcParser.class, ConnectionConfig.class, String.class, ThreadFactory.class, UncaughtExceptionHandler.class)
                .newInstance(injector, this, connectionConfig, serviceQueue, this, this);
            inited = true;
            return connection;
        } catch (Exception ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex);
            throw new UrpcServiceException(ex);
        }
    }

    @Override
    public Thread newThread(Runnable r) {
        SERVICE service = createService();
        PROXY proxy = wrapService(service);
        ServiceThread<PROXY> thread = new ServiceThread<>(proxy, r);
        thread.setUncaughtExceptionHandler(this);
        return thread;
    }

    public void shutdown() {
        stoped = true;
        connection.shutdown();
    }

    public void start() {
        if (!inited) {
            init();
        }
        connection.start();
    }

    protected SERVICE createService() {
        return injector.getInstance(serviceClass);
    }

    public String getServiceName() {
        return serviceName;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        LOG.log(Level.SEVERE, e.getMessage(), e);
        if (t instanceof ServiceThread) {
            ServiceThread jst = (ServiceThread) t;
            Runnable target = jst.getTarget();
            LOG.log(Level.SEVERE, "target: {0}", new Object[]{target});
        }
        //TODO process exception
    }

    abstract protected PROXY wrapService(SERVICE service);

}
