package com.unalarabe.urpc.service;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <PROXY>
 */
public class ServiceThread<PROXY extends ServiceProxy> extends Thread {

    private static final Logger LOG = Logger.getLogger(ServiceThread.class.getName());

    private final PROXY serviceProxy;

    public ServiceThread(PROXY service, Runnable target) {
        super(target);
        this.serviceProxy = service;
    }

    public PROXY getServiceProxy() {
        return serviceProxy;
    }

    public Runnable getTarget() {
        try {
            Field targetField = this.getClass().getDeclaredField("target");
            targetField.setAccessible(true);
            Runnable target = (Runnable) targetField.get(this);
            return target;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
