package com.unalarabe.urpc.service.jms;

import com.google.inject.Injector;
import com.unalarabe.urpc.config.ConnectionConfig;
import com.unalarabe.urpc.exception.ConvertException;
import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.service.Connection;
import com.unalarabe.urpc.service.Message;
import com.unalarabe.urpc.service.RpcMessage;
import com.unalarabe.urpc.util.JmsConnectionFactory;
import com.unalarabe.urpc.util.JmsUtil;
import com.unalarabe.urpc.util.RpcParser;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.BytesMessage;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class JmsConnection extends Connection<BytesMessage, BytesMessage> {

    private static final Logger LOG = Logger.getLogger(JmsConnection.class.getName());

    protected javax.jms.Connection jmsConnection;
    protected MessageProducer producer;
    protected MessageConsumer consumer;
    protected Session producerSession;
    protected Session consumerSession;
    protected Map<String, Destination> destinations = new HashMap<>();

    public JmsConnection(Injector injector, RpcParser rpcParser, ConnectionConfig connectionConfig, String serviceQueue, ThreadFactory threadFactory, UncaughtExceptionHandler uncaughtExceptionHandler) {
        super(injector, rpcParser, connectionConfig, serviceQueue, threadFactory, uncaughtExceptionHandler);
    }

    @Override
    public Connection init() {
        try {
            JmsConnectionFactory connectionFactory = this.injector.getInstance(JmsConnectionFactory.class);
            jmsConnection = connectionFactory.connect();

            this.consumerSession = jmsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            this.producerSession = jmsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            producer = producerSession.createProducer(null);

            Queue systemqueue = consumerSession.createQueue(serviceQueue);
            consumer = consumerSession.createConsumer(systemqueue, "JMSCorrelationID IS NOT NULL");
        } catch (JMSException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return this;
    }

    @Override
    public void connect() {
        try {
            jmsConnection.start();
        } catch (JMSException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    protected void afterShutdown() {
        JmsUtil.stop(consumer);
        JmsUtil.stop(producer);
        JmsUtil.stop(consumerSession);
        JmsUtil.stop(producerSession);
        JmsUtil.stop(jmsConnection);
    }

    @Override
    public void mainLoop() throws UrpcException {
        try {
            javax.jms.Message message = consumer.receive(100);
            if (message != null) {
                onRequest(message);
            }
        } catch (JMSException ex) {
            throw new UrpcException(ex);
        }
    }

    @Override
    public com.unalarabe.urpc.service.Message convertFrom(BytesMessage request) throws UrpcException {
        try {
            String jmsCorrelationID = request.getJMSCorrelationID();
            byte[] data = new byte[(int) request.getBodyLength()];
            request.readBytes(data);
            if (jmsCorrelationID == null) {
                RpcMessage rpcMessage = rpcParser.parseRpcMessageRequest(data);
                Message message = new Message(request);
                String replyToName = rpcMessage.getReplyTo();
                Queue replyTo = producerSession.createQueue(replyToName);
                message.setReplyTo(replyTo);
                message.setReplyToName(replyToName);
                message.setCorrelationId(rpcMessage.getCorrelationId());
                message.setMethod(rpcMessage.getMethod());
                message.setParsedBody(rpcMessage.getRequest());
                destinations.put(replyToName, replyTo);
                return message;
            } else {
                Message msg = new Message(request);
                msg.setBody(data);
                msg.setMethod(request.getStringProperty("Method"));
                msg.setCorrelationId(jmsCorrelationID);
                Destination replyTo = request.getJMSReplyTo();
                String replyToName = replyTo.toString();
                destinations.put(replyToName, replyTo);
                msg.setReplyTo(replyTo);
                msg.setReplyToName(replyToName);
                return msg;
            }
        } catch (Throwable ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            throw new ConvertException(ex);
        }
    }

    @Override
    public BytesMessage convertTo(Message message) throws UrpcException {
        try {
            BytesMessage responseMessage = producerSession.createBytesMessage();
            responseMessage.setJMSCorrelationID(message.getCorrelationId());
//            responseMessage.setJMSDestination((Destination) message.getReplyTo());
            if (message.isError()) {
                responseMessage.setIntProperty("ErrorReason", message.getErrorReason());
                responseMessage.setStringProperty("ErrorText", message.getErrorText());
            } else {
                responseMessage.writeBytes(message.getBody());
            }
            return responseMessage;
        } catch (Throwable ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new ConvertException(ex);
        }
    }

    @Override
    public boolean sendMessage(Object destination, Message message, long ttlMs) {
        BytesMessage response = convertTo(message);
        try {
            producer.send((Destination) destination, response, DeliveryMode.NON_PERSISTENT, 0, 0);
            return true;
        } catch (JMSException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String getCorrelationId(BytesMessage request) {
        try {
            return request.getJMSCorrelationID();
        } catch (JMSException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public Object getReplyTo(BytesMessage request) {
        try {
            return request.getJMSReplyTo();
        } catch (JMSException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public boolean isRequestValid(Object request) {
        try {
            return (request instanceof BytesMessage) && ((BytesMessage) request).getJMSReplyTo() != null;
        } catch (JMSException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return false;
    }

}
