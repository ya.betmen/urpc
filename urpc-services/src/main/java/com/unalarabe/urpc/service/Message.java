package com.unalarabe.urpc.service;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class Message {

    private String correlationId;
    private String version;
    private Object replyTo;
    private String replyToName;
    private Object destination;
    private String destinationName;
    private String method;
    private byte[] body;
    private Object parsedBody;
    private int errorReason = -1;
    private String errorText;
    private final Object original;

    public Message() {
        this(null);
    }

    public Message(Object original) {
        this.original = original;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Object getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(Object replyTo) {
        this.replyTo = replyTo;
    }

    public String getReplyToName() {
        return replyToName;
    }

    public void setReplyToName(String replyToName) {
        this.replyToName = replyToName;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public Object getParsedBody() {
        return parsedBody;
    }

    public void setParsedBody(Object parsedBody) {
        this.parsedBody = parsedBody;
    }

    public int getErrorReason() {
        return errorReason;
    }

    public void setErrorReason(int errorReason) {
        this.errorReason = errorReason;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public <T> T getOriginal() {
        return (T) original;
    }

    public boolean isError() {
        return this.errorReason >= 0;
    }

    public Object getDestination() {
        return destination;
    }

    public void setDestination(Object destination) {
        this.destination = destination;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    @Override
    public String toString() {
        return "Message{" + "body=" + body + ", method=" + method + ", errorReason=" + errorReason + ", errorTest=" + errorText + '}';
    }

}
