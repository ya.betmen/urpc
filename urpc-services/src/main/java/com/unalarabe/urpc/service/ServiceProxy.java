package com.unalarabe.urpc.service;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.unalarabe.urpc.annotations.Request;
import com.unalarabe.urpc.exception.ConvertException;
import com.unalarabe.urpc.exception.MethodCallException;
import com.unalarabe.urpc.exception.MethodNotFoundException;
import com.unalarabe.urpc.util.ErrorReason;
import com.unalarabe.urpc.util.ServiceUtil;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <SERVICE>
 */
public abstract class ServiceProxy<SERVICE> {

    private final SERVICE service;
    private Logger logger;
    private Field requestField = null;

    public ServiceProxy(SERVICE service, Class<SERVICE> clazz) {
        this.service = service;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.getAnnotation(Request.class) != null) {
                field.setAccessible(true);
                requestField = field;
                break;
            }
        }
    }

    protected void setRequestField(Message message) {
        try {
            if (requestField != null) {
                requestField.set(service, message);
            }
        } catch (IllegalArgumentException | IllegalAccessException ex) {
            getLogger().log(Level.SEVERE, null, ex);
        }
    }

    public SERVICE getService() {
        return service;
    }

    public Message processMessage(Message requestMessage) {
        Message responseMessage = createMessage();
        responseMessage.setCorrelationId(requestMessage.getCorrelationId());
        responseMessage.setDestination(requestMessage.getReplyTo());
        responseMessage.setDestinationName(requestMessage.getReplyToName());
        try {
            setRequestField(requestMessage);
            byte[] responseBody = null;
            Object responseBodyParsed = null;
            
            byte[] requestBody = requestMessage.getBody();
            if (requestBody != null) {
                responseBody = callMethod(requestMessage.getMethod(), requestBody);
            } else {
                responseBodyParsed = callMethod(requestMessage.getMethod(), requestMessage.getParsedBody());
            }
            setRequestField(null);
            responseMessage.setBody(responseBody);
            responseMessage.setParsedBody(responseBodyParsed);
        } catch (MethodCallException th) {
            getLogger().log(Level.FINE, th.getMessage(), th);
            responseMessage.setErrorReason(ErrorReason.RPC_ERROR.getReason());
            responseMessage.setErrorText(ServiceUtil.unwrap(th).getMessage());
        } catch (MethodNotFoundException th) {
            getLogger().log(Level.FINE, th.getMessage(), th);
            responseMessage.setErrorReason(ErrorReason.METHOD_NOT_FOUND.getReason());
            responseMessage.setErrorText(th.getMessage());
        } catch (Throwable th) {
            Throwable ex = ServiceUtil.unwrap(th);
            String text = ex.getMessage();
            getLogger().log(Level.SEVERE, text, th);
            responseMessage.setErrorReason(ErrorReason.RPC_FAILED.getReason());
            responseMessage.setErrorText(text != null ? text : ex.getClass().getCanonicalName());
        }

        return responseMessage;
    }

    private Logger getLogger() {
        if (logger == null) {
            logger = Logger.getLogger(getService().getClass().getName());
        }
        return logger;
    }

    protected Message createMessage() {
        return new com.unalarabe.urpc.service.Message();
    }

//    abstract protected RpcMessage parseRpcMessage(byte[] data) throws ConvertException;
    abstract protected Object callMethod(String method, Object data) throws MethodCallException;

    abstract protected byte[] callMethod(String method, byte[] data) throws ConvertException, MethodCallException;

}
