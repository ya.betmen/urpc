package com.unalarabe.urpc.service;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public interface MessageProcessor {

    public Message processMessage(Message requestMessage);
}
