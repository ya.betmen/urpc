package com.unalarabe.urpc.service;

import com.unalarabe.urpc.exception.UrpcException;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <REQUEST>
 * @param <RESPONSE>
 */
public interface RequestResponseConverter<REQUEST, RESPONSE> {

    Message convertFrom(REQUEST request) throws UrpcException;

    RESPONSE convertTo(Message message) throws UrpcException;

    String getCorrelationId(REQUEST request);
    
    Object getReplyTo(REQUEST request);
}
