package com.unalarabe.urpc.client;

import com.unalarabe.urpc.exception.ConvertException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.BytesMessage;
import com.unalarabe.urpc.exception.MethodCallException;
import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.service.Message;
import com.unalarabe.urpc.util.ProcessListener;
import com.unalarabe.urpc.util.MessageListener;
import com.unalarabe.urpc.util.RpcParser;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <MESSAGE>
 */
public abstract class AbstractServiceClient<MESSAGE> implements MessageListener<Message>, ProcessListener {

    private static final Logger LOG = Logger.getLogger(AbstractServiceClient.class.getName());

    protected final ReadWriteLock responsesMapLock = new ReentrantReadWriteLock();
    protected Map<String, Message> responsesMap = new HashMap<>();

    protected final ChannelConfig channelConfig;
    protected final String endpoint;
    protected final String serviceName;
    private AbstractChannel<MESSAGE, MESSAGE> channel;

    private ExecutorService executor = Executors.newCachedThreadPool();

    private boolean inited = false;

    public AbstractServiceClient(ChannelConfig channelConfig) throws UrpcException {
        this.endpoint = channelConfig.getEndpoint();
        this.serviceName = channelConfig.getServiceName() != null ? channelConfig.getServiceName() : getClass().getSimpleName();
        this.channelConfig = channelConfig;
    }

    private void checkInit() throws UrpcException {
        if (!inited) {
            try {
                channel = channelConfig.getChannelClass().getConstructor(MessageListener.class, ChannelConfig.class, RpcParser.class)
                    .newInstance(this, channelConfig, channelConfig.getRpcParser());
                init();
            } catch (Exception ex) {
                throw new UrpcException(ex);
            }
        }
    }

    private synchronized void init() throws UrpcException {
        channel.init();
        inited = true;
    }

    @Override
    public void onMessage(Message message) {
        try {
            String correlationID = message.getCorrelationId();
            if (correlationID != null) {
                responsesMapLock.writeLock().lock();
                try {
                    if (responsesMap.containsKey(correlationID) && responsesMap.get(correlationID) == null) {
                        responsesMap.put(correlationID, message);
                    }
                } finally {
                    responsesMapLock.writeLock().unlock();
                }
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    protected <T> Future<T> callMethod(String method, Object data) throws UrpcException {
        final String correlationId = UUID.randomUUID().toString();
        LOG.log(Level.INFO, "Create meassage {0}", correlationId);
        try {
            checkInit();
            responsesMapLock.writeLock().lock();
            try {
                responsesMap.put(correlationId, null);
            } finally {
                responsesMapLock.writeLock().unlock();
            }

            Message message = channel.createMethodMessage(method, correlationId, data);
            channel.sendMessage(null, message, 0);

            FutureTask futureTask = new FutureTask(new AbstractServiceClient.ResponseCallable(responsesMap, responsesMapLock, method, correlationId));
            executor.execute(futureTask);
            return futureTask;

        } catch (Exception ex) {
            responsesMapLock.writeLock().lock();
            try {
                responsesMap.remove(correlationId);
            } finally {
                responsesMapLock.writeLock().unlock();
            }
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            throw new UrpcException(ex);
        }

    }

    public void shutdown() {
        executor.shutdown();
        channel.shutdown();
    }

    abstract protected MESSAGE parseResponse(String method, byte[] data) throws ConvertException;

    @Override
    public void onShutdown() {
        shutdown();
    }

    private class ResponseCallable implements Callable<MESSAGE> {

        private String method;
        private String correlationId;
        private ReadWriteLock responsesMapLock;
        private Map<String, Message> responsesMap;

        public ResponseCallable(Map<String, Message> responsesMap, ReadWriteLock responsesMapLock, String method, String correlationId) {
            this.method = method;
            this.correlationId = correlationId;
            this.responsesMapLock = responsesMapLock;
            this.responsesMap = responsesMap;
        }

        @Override
        public MESSAGE call() throws Exception {
            try {
                Message responseMessage;
                do {
                    Thread.sleep(1);
                    responsesMapLock.readLock().lock();
                    try {
                        responseMessage = responsesMap.get(correlationId);
                    } finally {
                        responsesMapLock.readLock().unlock();
                    }
                } while (!Thread.interrupted() && responseMessage == null);
                MESSAGE result = null;
                if (responseMessage != null) {
                    String error = responseMessage.getErrorText();
                    if (error != null) {
                        throw new MethodCallException(error);
                    }
                    result = (MESSAGE) responseMessage.getParsedBody();
                    if (result == null) {
                        byte[] data = responseMessage.getBody();
                        if (data == null) {
                            throw new NullPointerException();
                        }
                        result = parseResponse(method, data);
                    }
                }
                responsesMapLock.writeLock().lock();
                try {
                    responsesMap.remove(correlationId);
                } finally {
                    responsesMapLock.writeLock().unlock();
                }
                return result;
            } finally {
                this.method = null;
                this.correlationId = null;
                this.responsesMapLock = null;
                this.responsesMap = null;
            }
        }

    }

}
