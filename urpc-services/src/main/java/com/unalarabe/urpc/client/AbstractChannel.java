package com.unalarabe.urpc.client;

import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.service.Message;
import com.unalarabe.urpc.service.RequestResponseConverter;
import com.unalarabe.urpc.util.MessageListener;
import com.unalarabe.urpc.util.MessageSender;
import com.unalarabe.urpc.util.RpcParser;
import java.util.concurrent.Future;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <REQUEST>
 * @param <RESPONSE>
 */
public abstract class AbstractChannel<REQUEST, RESPONSE> implements RequestResponseConverter<REQUEST, RESPONSE>, MessageSender {

    protected final String endpoint;
    protected final String serviceName;
    protected final MessageListener messageListener;
    protected final ChannelConfig channelConfig;
    protected final RpcParser rpcParser;

    public AbstractChannel(MessageListener messageListener, ChannelConfig channelConfig, RpcParser rpcParser) {
        this.endpoint = channelConfig.endpoint;
        this.serviceName = channelConfig.serviceName;
        this.messageListener = messageListener;
        this.channelConfig = channelConfig;
        this.rpcParser = rpcParser;
    }

    protected abstract void init() throws UrpcException;

    protected abstract void shutdown();

    public abstract Message createMethodMessage(String method, String correlationId, Object data) throws UrpcException;
}
