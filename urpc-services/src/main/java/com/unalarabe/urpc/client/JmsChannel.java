package com.unalarabe.urpc.client;

import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.service.Message;
import com.unalarabe.urpc.util.JmsConnectionFactory;
import com.unalarabe.urpc.util.JmsUtil;
import com.unalarabe.urpc.util.MessageListener;
import com.unalarabe.urpc.util.RpcParser;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class JmsChannel extends AbstractChannel<BytesMessage, BytesMessage> implements javax.jms.MessageListener {

    private static final Logger LOG = Logger.getLogger(JmsChannel.class.getName());

    private final JmsConnectionFactory connectionFactory;
    private Connection connection;
    private MessageProducer producer;
    private MessageConsumer consumer;
    private Queue serviceQueue;
    private Queue responseQueue;
    private Session sessionConsumer;
    private Session sessionProducer;

    public JmsChannel(MessageListener messageListener, ChannelConfig channelConfig, RpcParser rpcParser) {
        super(messageListener, channelConfig, rpcParser);
        this.connectionFactory = (JmsConnectionFactory) channelConfig.getParameters().get("jmsConnectionFactory");
    }

    @Override
    protected void init() throws UrpcException {
        try {
            connection = connectionFactory.connect();
            sessionConsumer = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            responseQueue = sessionConsumer.createTemporaryQueue();
            consumer = sessionConsumer.createConsumer(responseQueue);
            consumer.setMessageListener(this);
            sessionProducer = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            serviceQueue = sessionProducer.createQueue(serviceName + "@" + endpoint);
            producer = sessionProducer.createProducer(serviceQueue);
            connection.start();
        } catch (JMSException ex) {
            throw new UrpcException(ex);
        }
    }

    @Override
    public Message convertFrom(BytesMessage request) throws UrpcException {
        try {
            Message message = new Message(request);

            long bodyLength = request.getBodyLength();
            if (bodyLength > 0) {
                byte[] data = new byte[(int) bodyLength];
                request.readBytes(data);
                message.setBody(data);
            } else {
                message.setErrorReason(request.getIntProperty("ErrorReason"));
                message.setErrorText(request.getStringProperty("ErrorText"));
            }
            message.setCorrelationId(request.getJMSCorrelationID());
            return message;
        } catch (JMSException ex) {
            throw new UrpcException(ex);
        }
    }

    @Override
    public BytesMessage convertTo(Message message) throws UrpcException {
        try {
            BytesMessage result = sessionConsumer.createBytesMessage();
            result.setStringProperty("Method", message.getMethod());
            result.setJMSReplyTo(responseQueue);
            result.setJMSCorrelationID(message.getCorrelationId());
            result.writeBytes(message.getBody());
            return result;
        } catch (JMSException ex) {
            throw new UrpcException(ex);
        }
    }

    @Override
    public boolean sendMessage(Object destination, Message message, long ttlMs) {
        try {
            BytesMessage response = convertTo(message);
            producer.send(response);
            return true;
        } catch (JMSException ex) {
            LOG.log(Level.WARNING, ex.getMessage(), ex);
            return false;
        }
    }

    @Override
    public void onMessage(javax.jms.Message request) {
        if (request instanceof BytesMessage) {
            Message message = convertFrom((BytesMessage) request);
            messageListener.onMessage(message);
        }
    }

    @Override
    protected void shutdown() {
        JmsUtil.stop(consumer);
        JmsUtil.stop(producer);
        JmsUtil.stop(connection);
        JmsUtil.stop(sessionConsumer);
    }

    @Override
    public String getCorrelationId(BytesMessage request) {
        try {
            return request.getJMSCorrelationID();
        } catch (JMSException ex) {
            LOG.warning(ex.getMessage());
            LOG.log(Level.FINE, ex.getMessage(), ex);
            return null;
        }
    }

    @Override
    public Object getReplyTo(BytesMessage request) {
        try {
            return request.getJMSReplyTo();
        } catch (JMSException ex) {
            LOG.warning(ex.getMessage());
            LOG.log(Level.FINE, ex.getMessage(), ex);
            return null;
        }
    }

    @Override
    public Message createMethodMessage(String method, String correlationId, Object data) throws UrpcException {
        Message message = new Message();
        message.setCorrelationId(correlationId);
        message.setBody((byte[])data);
        message.setMethod(method);
        return message;
    }

}
