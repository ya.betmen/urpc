package com.unalarabe.urpc.client;

import com.unalarabe.urpc.util.RpcParser;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ChannelConfig {

    protected final Class<? extends AbstractChannel> channelClass;
    protected final String endpoint;
    protected final String serviceName;
    protected final RpcParser rpcParser;
    protected final Map<String, Object> parameters = new HashMap<>();

    public ChannelConfig(Class<? extends AbstractChannel> channelClass, String endpoint, String serviceName, RpcParser rpcParser) {
        this.channelClass = channelClass;
        this.endpoint = endpoint;
        this.serviceName = serviceName;
        this.rpcParser = rpcParser;
    }

    public ChannelConfig(Class<? extends AbstractChannel> channelClass, String endpoint, RpcParser rpcParser) {
        this(channelClass, endpoint, null, rpcParser);
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getServiceName() {
        return serviceName;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public Class<? extends AbstractChannel> getChannelClass() {
        return channelClass;
    }

    public RpcParser getRpcParser() {
        return rpcParser;
    }

}
