package com.unalarabe.urpc.client;

import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.service.Message;
import com.unalarabe.urpc.service.RpcMessage;
import com.unalarabe.urpc.util.MessageListener;
import com.unalarabe.urpc.util.RpcParser;
import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import kafka.utils.ZKStringSerializer;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.I0Itec.zkclient.exception.ZkMarshallingError;
import org.I0Itec.zkclient.serialize.ZkSerializer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.errors.TopicExistsException;
import org.json.JSONObject;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class KafkaChannel extends AbstractChannel<ConsumerRecord, ProducerRecord> {

    private static final SecureRandom RND = new SecureRandom();
    private static final Logger LOG = Logger.getLogger(KafkaChannel.class.getName());

    private KafkaProducer producer;
    private KafkaConsumer consumer;
    private String serviceQueue;
    private final String responseQueue = "resp" + RND.nextInt(Integer.MAX_VALUE);

    private volatile boolean stop;
    private volatile boolean inProgress;

    public KafkaChannel(MessageListener messageListener, ChannelConfig channelConfig, RpcParser rpcParser) {
        super(messageListener, channelConfig, rpcParser);
    }

    @Override
    protected void init() throws UrpcException {
        System.out.println("init KafkaChannel for " + responseQueue);
        ZkClient zkClient = new ZkClient("192.168.57.101:2181", 15000, 15000);
        zkClient.setZkSerializer(new ZkSerializer() {
            @Override
            public byte[] serialize(Object o) throws ZkMarshallingError {
                return ZKStringSerializer.serialize(o);
            }

            @Override
            public Object deserialize(byte[] bytes) throws ZkMarshallingError {
                return ZKStringSerializer.deserialize(bytes);
            }
        });
        ZkUtils zkUtils = new ZkUtils(zkClient, new ZkConnection("192.168.57.101:2181"), false);
        try {
            AdminUtils.createTopic(zkUtils, responseQueue, 1, 1, new Properties(), RackAwareMode.Enforced$.MODULE$);
        } catch (TopicExistsException ex) {
            LOG.log(Level.INFO, "Topic {0} already exists", responseQueue);
        }
        Map<String, Object> parameters = channelConfig.getParameters();
        System.out.println("--------------------------------------");
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            System.out.println(entry.getKey() + " => " + entry.getValue());
        }
        System.out.println("--------------------------------------");
        consumer = new KafkaConsumer(parameters);
        consumer.subscribe(Arrays.asList(responseQueue));
        serviceQueue = serviceName + "." + endpoint;
        try {
            AdminUtils.createTopic(zkUtils, serviceQueue, 1, 1, new Properties(), RackAwareMode.Enforced$.MODULE$);
        } catch (TopicExistsException ex) {
            LOG.log(Level.INFO, "Topic {0} already exists", responseQueue);
        }
        producer = new KafkaProducer(channelConfig.getParameters());
        stop = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (!stop) {
                    inProgress = true;
                    try {
//                        System.out.println("Poll in KafkaChannel for " + responseQueue);
                        ConsumerRecords consumerRecords = consumer.poll(100);
                        Iterator<ConsumerRecord> iterator = consumerRecords.iterator();
                        while (iterator.hasNext()) {
                            ConsumerRecord consumerRecord = iterator.next();
                            onMessage(consumerRecord);
                        }
                    } finally {
                        inProgress = false;
                    }
                }
            }
        }).start();
    }

    @Override
    public Message convertFrom(ConsumerRecord request) throws UrpcException {
        Message message = new Message(request);

        RpcMessage rpcMessage = rpcParser.parseRpcMessageRequest(request);
        message.setParsedBody(rpcMessage.getRequest());
        message.setCorrelationId(rpcMessage.getCorrelationId());
        message.setReplyTo(rpcMessage.getReplyTo());
        message.setReplyToName(rpcMessage.getReplyTo());
        message.setMethod(rpcMessage.getMethod());
        message.setVersion(rpcMessage.getVersion());

        return message;
    }

    @Override
    public ProducerRecord convertTo(Message message) throws UrpcException {
        return (ProducerRecord) rpcParser.createRpcMessageRequest(message);
    }

    @Override
    public boolean sendMessage(Object destination, Message message, long ttlMs) {
        message.setDestination(serviceQueue);
        message.setDestinationName(serviceQueue);
        ProducerRecord response = convertTo(message);
        LOG.log(Level.INFO, "KafkaChannel: sendMessage {0} to {1}", new Object[]{message.getCorrelationId(), serviceQueue});
        producer.send(response);
        return true;
    }

    public void onMessage(ConsumerRecord request) {
        LOG.log(Level.INFO, "KafkaChannel: MESSAGE {0}", request.value());
        Message message = rpcParser.parseRpcMessageResponse(request.value());
        messageListener.onMessage(message);
    }

    private static void runMethod(Object object, String methodName) {
        try {
            Method method = object.getClass().getDeclaredMethod(methodName);
            method.setAccessible(true);
            Object r = method.invoke(object);
        } catch (Throwable ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    protected void shutdown() {
        stop = true;
        while (inProgress) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                LOG.log(Level.WARNING, ex.getMessage());
            }
        }
        runMethod(consumer, "release");
        consumer.commitSync();
        consumer.close();
        producer.flush();
        producer.close();
    }

    @Override
    public String getCorrelationId(ConsumerRecord request) {
        return new JSONObject(request.value().toString()).getString("correlationId");
    }

    @Override
    public Object getReplyTo(ConsumerRecord request) {
        return new JSONObject(request.value().toString()).getString("replyTo");
    }

    @Override
    public Message createMethodMessage(String method, String correlationId, Object data) throws UrpcException {
        Message message = new Message();
        message.setParsedBody(data);
        message.setCorrelationId(correlationId);
        message.setMethod(method);
        message.setReplyTo(responseQueue);
        message.setReplyToName(responseQueue);
        return message;
    }

}
