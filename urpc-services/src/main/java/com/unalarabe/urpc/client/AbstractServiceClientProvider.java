package com.unalarabe.urpc.client;

import java.util.HashMap;
import java.util.Map;
import javax.jms.JMSException;
import com.unalarabe.urpc.exception.UrpcException;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <T>
 */
public abstract class AbstractServiceClientProvider<T extends AbstractServiceClient> {

    protected final ChannelConfig channelConfig;
    private final Map<String, T> servicesMap = new HashMap<>();

    public AbstractServiceClientProvider(ChannelConfig channelConfig) {
        this.channelConfig = channelConfig;
    }

    public synchronized T get() throws UrpcException {
        return get(channelConfig.getServiceName(), channelConfig.getEndpoint());
    }

    public synchronized T get(String endpoint) throws UrpcException {
        return get(getServiceName(), endpoint);
    }

    public synchronized T get(String serviceName, String endpoint) throws UrpcException {
        try {
            String key = serviceName + "@" + endpoint;
            T client = servicesMap.get(key);
            if (client == null) {
                ChannelConfig channelConfigNew = new ChannelConfig(channelConfig.getChannelClass(), endpoint, serviceName, channelConfig.getRpcParser());
                channelConfigNew.getParameters().putAll(channelConfig.getParameters());
                client = createService(channelConfigNew);
                servicesMap.put(serviceName, client);
            }
            return client;
        } catch (Throwable ex) {
            throw new UrpcException(ex);
        }
    }
    
    protected abstract T createService(ChannelConfig channelConfig) throws JMSException;

    protected abstract String getServiceName();
}
