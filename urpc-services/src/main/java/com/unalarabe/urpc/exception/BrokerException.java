package com.unalarabe.urpc.exception;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class BrokerException extends Exception {

    public BrokerException() {
    }

    public BrokerException(String message) {
        super(message);
    }

    public BrokerException(String message, Throwable cause) {
        super(message, cause);
    }

    public BrokerException(Throwable cause) {
        super(cause);
    }

    public BrokerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
