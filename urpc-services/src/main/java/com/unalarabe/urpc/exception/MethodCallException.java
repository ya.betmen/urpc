package com.unalarabe.urpc.exception;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MethodCallException extends UrpcException {

    public MethodCallException() {
    }

    public MethodCallException(String message) {
        super(message);
    }

    public MethodCallException(String message, Throwable cause) {
        super(message, cause);
    }

    public MethodCallException(Throwable cause) {
        super(cause);
    }

    public MethodCallException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
