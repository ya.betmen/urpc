package com.unalarabe.urpc.exception;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class UrpcException extends RuntimeException {

    public UrpcException() {
    }

    public UrpcException(String message) {
        super(message);
    }

    public UrpcException(String message, Throwable cause) {
        super(message, cause);
    }

    public UrpcException(Throwable cause) {
        super(cause);
    }

    public UrpcException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
