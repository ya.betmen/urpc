package com.unalarabe.urpc.exception;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class UrpcServiceException extends UrpcException {

    public UrpcServiceException() {
    }

    public UrpcServiceException(String message) {
        super(message);
    }

    public UrpcServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UrpcServiceException(Throwable cause) {
        super(cause);
    }

    public UrpcServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
