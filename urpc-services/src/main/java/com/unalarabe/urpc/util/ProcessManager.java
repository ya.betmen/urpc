package com.unalarabe.urpc.util;

import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ProcessManager {

    private final List<ProcessListener> listeners = new ArrayList<>();

    public boolean addListener(ProcessListener listener) {
        synchronized (listeners) {
            return listeners.add(Preconditions.checkNotNull(listener));
        }
    }

    public boolean removeListener(ProcessListener listener) {
        synchronized (listeners) {
            return listeners.remove(listener);
        }
    }

    public void shutdown() {
        synchronized (listeners) {
            for (ProcessListener listener : listeners) {
                listener.onShutdown();
            }
        }
    }
}
