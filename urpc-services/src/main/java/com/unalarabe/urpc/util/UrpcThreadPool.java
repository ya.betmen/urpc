package com.unalarabe.urpc.util;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import com.unalarabe.urpc.config.PoolConfig;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class UrpcThreadPool extends ThreadPoolExecutor {

    public UrpcThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    public UrpcThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
    }

    public UrpcThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
    }

    public UrpcThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
    }

    public UrpcThreadPool(PoolConfig poolConfig, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
        this(poolConfig.getCorePoolSize(), poolConfig.getMaxPoolSize(), poolConfig.getKeepAliveTimeInMs(), TimeUnit.MILLISECONDS, workQueue, threadFactory);
    }

    public UrpcThreadPool(PoolConfig poolConfig, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
        this(poolConfig.getCorePoolSize(), poolConfig.getMaxPoolSize(), poolConfig.getKeepAliveTimeInMs(), TimeUnit.MILLISECONDS, workQueue, handler);
    }

    public UrpcThreadPool(PoolConfig poolConfig, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        this(poolConfig.getCorePoolSize(), poolConfig.getMaxPoolSize(), poolConfig.getKeepAliveTimeInMs(), TimeUnit.MILLISECONDS, workQueue, threadFactory, handler);
    }

}
