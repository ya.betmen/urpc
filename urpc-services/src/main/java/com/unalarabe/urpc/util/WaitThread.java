package com.unalarabe.urpc.util;

import com.google.common.base.Preconditions;
import com.unalarabe.urpc.service.Message;
import com.unalarabe.urpc.service.RequestResponseConverter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class WaitThread extends Thread implements UpdatesListener {

    private static final Logger LOG = Logger.getLogger(WaitThread.class.getName());

    private final Lock updatesLock = new ReentrantLock();
    private final Condition hasUpdatesCondition = updatesLock.newCondition();
    private final List<Slot> updates = new LinkedList<>();
    private final MessageSender sender;
    private final RequestResponseConverter converter;
    private final int ttlMs;
    private final UrpcTimer timer;

    private volatile boolean hasUpdates = false;
    private volatile boolean started = false;

    public WaitThread(long awaitTimeInMs, int ttlMs, MessageSender sender, RequestResponseConverter converter, UrpcTimer timer) {
        this.sender = Preconditions.checkNotNull(sender);
        this.ttlMs = ttlMs;
        this.converter = converter;
        this.timer = timer;
    }

    public Slot getSlot(Object originalRequest, long timeoutinMs) {
        final Slot slot = new Slot(originalRequest, ttlMs, this);
        updatesLock.lock();
        try {
            updates.add(slot);
        } finally {
            updatesLock.unlock();
        }
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!slot.isDone()) {
                    slot.cancel();
                }
                notifyUpdates();
            }
        }, timeoutinMs);
        if (this.getState() == Thread.State.NEW) {
            this.start();
        }
        if (!this.isAlive()) {
            throw new IllegalStateException("Thread is not alive!");
        }
        return slot;
    }

//    public void push(final Future<Message> future, long timeoutinMs, Object destination, String correlationId) {
//        updatesLock.lock();
//        try {
//            updates.add(new FutureMessage<>(future, correlationId, destination));
//        } finally {
//            updatesLock.unlock();
//        }
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                future.cancel(true);
//                notifyUpdates();
//            }
//        }, timeoutinMs);
//        notifyUpdates();
//        if (this.getState() == Thread.State.NEW) {
//            this.start();
//        }
//        if (!this.isAlive()) {
//            throw new IllegalStateException("Thread is not alive!");
//        }
//    }
    @Override
    public void run() {
        started = true;
        while (started) {
            updatesLock.lock();//TODO: refactor it. Block queue is too slow
            try {
                if (!hasUpdates) {
                    try {
                        hasUpdatesCondition.await();
                        hasUpdates = false;
                    } catch (InterruptedException ex) {
                        LOG.log(Level.SEVERE, null, ex);
                    }
                }
                hasUpdates = false;
                if (!updates.isEmpty()) {
                    processUpdatesUnsafe(updates);
                }
            } finally {
                updatesLock.unlock();
            }
        }
    }

    protected void processUpdatesUnsafe(List<Slot> updates) {
        for (Iterator<Slot> iterator = updates.iterator(); this.isAlive() && iterator.hasNext();) {
            Slot slot = iterator.next();
            try {
                Message responseMessage = null;
                if (slot.isError()) {
                    iterator.remove();
                    responseMessage = new Message();
                    responseMessage.setErrorReason(ErrorReason.RPC_ERROR.getReason());
                    responseMessage.setErrorText(slot.getError());
                } else if (slot.isCancelled()) {
                    iterator.remove();
                    responseMessage = new Message();
                    responseMessage.setErrorReason(ErrorReason.SERVER_TIMEOUT.getReason());
                    responseMessage.setErrorText("Timeout expired");
                } else if (slot.isDone()) {
                    iterator.remove();
                    responseMessage = slot.getResult();
                }
                if (responseMessage != null) {
                    String correlationId = slot.correlationId;
                    Object destination = slot.getDestination();
                    if (correlationId == null) {
                        correlationId = converter.getCorrelationId(slot.getOriginalRequest());
                    }
                    if (destination == null) {
                        destination = converter.getReplyTo(slot.getOriginalRequest());
                    }
                    responseMessage.setCorrelationId(correlationId);
                    sender.sendMessage(destination, responseMessage, ttlMs);
                }
            } catch (Exception ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void shutdown() {
        started = false;
        notifyUpdates();
    }

    @Override
    public void notifyUpdates() {
        updatesLock.lock();
        try {
            hasUpdates = true;
            hasUpdatesCondition.signal();
        } finally {
            updatesLock.unlock();
        }
    }

    public static class Slot {

        public final long timeout;
        public final UpdatesListener updatesListener;

        private final Object originalRequest;

        private String correlationId;
        private Object destination;
        private boolean cancelled;
        private Message result;
        private String error;

        public Slot(Object originalRequest, long timeout, UpdatesListener updatesListener) {
            this.originalRequest = originalRequest;
            this.timeout = timeout;
            this.updatesListener = updatesListener;
        }

        public Object getOriginalRequest() {
            return originalRequest;
        }

        public boolean isCancelled() {
            return cancelled;
        }

        public void cancel() {
            if (!cancelled) {
                this.cancelled = true;
                updatesListener.notifyUpdates();
            }
        }

        public Message getResult() {
            return result;
        }

        public boolean isError() {
            return error != null;
        }

        public String getError() {
            return error;
        }

        public void setError(String error) {
            if (this.error == null) {
                this.error = error;
                updatesListener.notifyUpdates();
            }
        }

        public void setResult(Message result) {
            if (this.result == null) {
                this.result = result;
                this.correlationId = Preconditions.checkNotNull(result.getCorrelationId());
                this.destination = Preconditions.checkNotNull(result.getDestination());
                updatesListener.notifyUpdates();
            }
        }

        public boolean isDone() {
            return this.result != null;
        }

        public String getCorrelationId() {
            return correlationId;
        }

        public Object getDestination() {
            return destination;
        }

    }
}
