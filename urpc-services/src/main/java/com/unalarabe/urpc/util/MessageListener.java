package com.unalarabe.urpc.util;

import com.unalarabe.urpc.service.Message;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public interface MessageListener<MESSAGE> {

    public void onMessage(MESSAGE message);

}
