package com.unalarabe.urpc.util;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class JmsUtil {

    private static final Logger LOG = Logger.getLogger(JmsUtil.class.getName());

    public static void stop(MessageConsumer consumer) {
        if (consumer != null) {
            try {
                consumer.close();
            } catch (JMSException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void stop(MessageProducer producer) {
        if (producer != null) {
            try {
                producer.close();
            } catch (JMSException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void stop(Session session) {
        if (session != null) {
            try {
                session.close();
            } catch (JMSException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void stop(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
    }

}
