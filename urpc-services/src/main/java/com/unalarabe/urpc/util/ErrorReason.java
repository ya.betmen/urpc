package com.unalarabe.urpc.util;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public enum ErrorReason {
    BAD_REQUEST_DATA(0), // Server received bad request data
    BAD_REQUEST_PROTO(1), // Server received bad request proto
    SERVICE_NOT_FOUND(2), // Service not found on server (akin to HTTP 501)
    SERVICE_FIELD_NOT_FOUND(3),
    METHOD_NOT_FOUND(4), // Method not found on server (akin to HTTP 501)
    METHOD_FIELD_NOT_FOUND(5),
    RPC_ERROR(6), // RPC threw exception on server (akin to HTTP 500)
    RPC_FAILED(7), // RPC encountered an unexpected condition (akin to HTTP 500)
    RPC_FAILED_BAD_REQUEST(8), // RPC failed due to an incorrect message (akin to HTTP 400)
    RPC_FAILED_NOT_FOUND(9), // RPC failed due to un-met prerequisites (akin to HTTP 404)
    RPC_FAILED_CONFLICT(10), // RPC failed due to a conflict (akin to HTTP 409)
    RPC_FAILED_USER_DEFINED_1(11), // RPC failed due to user defined reason
    RPC_FAILED_USER_DEFINED_2(12), // RPC failed due to user defined reason
    RPC_FAILED_USER_DEFINED_3(13), // RPC failed due to user defined reason

    // Client-side errors
    SERVER_TIMEOUT(14);

    private final int reason;

    private ErrorReason(int reason) {
        this.reason = reason;
    }

    public int getReason() {
        return reason;
    }

}
