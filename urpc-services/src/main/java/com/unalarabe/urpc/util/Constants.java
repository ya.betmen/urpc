package com.unalarabe.urpc.util;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class Constants {

    public static final String JMSX_USER_ID = "JMSXUserID";
    public static final String TOPIC_PUBLIC_BUS = "topic://publicbus";
    public static final String TOPIC_CONNECTION = "topic://ActiveMQ.Advisory.Connection";
    public static final String TOPIC_QUEUE = "topic://ActiveMQ.Advisory.Queue";
    public static final String TOPIC_TOPIC = "topic://ActiveMQ.Advisory.Topic";
    public static final String TOPIC_SYSTEMQUEUE = "topic://systemqueue";
    public static final String QUEUE_SYSTEMQUEUE = "queue://systemqueue";
    public static final String TOPIC_TEMPORARY = "ActiveMQ.Advisory.TempQueue,ActiveMQ.Advisory.TempTopic";

}
