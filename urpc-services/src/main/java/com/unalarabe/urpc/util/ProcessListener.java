package com.unalarabe.urpc.util;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public interface ProcessListener {

    public void onShutdown();

}
