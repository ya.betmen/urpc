package com.unalarabe.urpc.util;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import com.unalarabe.urpc.exception.UrpcException;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class UrpcFuture<T> implements Future<T> {

    private final Future<T> future;

    public UrpcFuture(Future<T> future) {
        this.future = future;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return future.cancel(mayInterruptIfRunning);
    }

    @Override
    public boolean isCancelled() {
        return future.isCancelled();
    }

    @Override
    public boolean isDone() {
        return future.isDone();
    }

    @Override
    public T get() throws UrpcException {
        try {
            return future.get();
        } catch (Throwable ex) {
            Throwable original = ServiceUtil.unwrap(ex);
            if (original instanceof UrpcException) {
                throw (UrpcException) original;
            } else {
                throw new UrpcException(original);
            }
        }
    }

    @Override
    public T get(long timeout, TimeUnit unit) throws UrpcException {
        try {
            return future.get(timeout, unit);
        } catch (Throwable ex) {
            Throwable original = ServiceUtil.unwrap(ex);
            if (original instanceof UrpcException) {
                throw (UrpcException) original;
            } else {
                throw new UrpcException();
            }
        }
    }

}
