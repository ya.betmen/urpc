package com.unalarabe.urpc.util;

import com.google.inject.Inject;
import javax.jms.JMSException;
import com.unalarabe.urpc.config.JmsConfig;
import javax.jms.Connection;
import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class JmsConnectionFactory {

    private final JmsConfig jmsConfig;
    private final ActiveMQConnectionFactory connectionFactory;

    @Inject
    public JmsConnectionFactory(JmsConfig jmsConfig) {
        this.jmsConfig = jmsConfig;
        this.connectionFactory = new ActiveMQConnectionFactory(jmsConfig.getBrokerUri(), jmsConfig.getUser(), jmsConfig.getPassword());
    }

    public Connection connect() throws JMSException {
//        connectionFactory.setBrokerURL(jmsConfig.getBrokerUri());
//        connectionFactory.setMaxThreadPoolSize(500);
//        connectionFactory.setPrefetchPolicy(prefetchPolicy);
        Connection connection = connectionFactory.createConnection(jmsConfig.getUser(), jmsConfig.getPassword());
//        connection.setMessagePrioritySupported(true);
        return connection;
    }

}
