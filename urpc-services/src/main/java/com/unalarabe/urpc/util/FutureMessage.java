package com.unalarabe.urpc.util;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.jms.Destination;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class FutureMessage<T> implements Future<T> {

    private final Future<T> future;
    private final String correlationId;
    private final Object destination;

    public FutureMessage(Future<T> future, String correlationId, Object destination) {
        this.future = future;
        this.correlationId = correlationId;
        this.destination = destination;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return future.cancel(mayInterruptIfRunning);
    }

    @Override
    public boolean isCancelled() {
        return future.isCancelled();
    }

    @Override
    public boolean isDone() {
        return future.isDone();
    }

    @Override
    public T get() throws InterruptedException, ExecutionException {
        return future.get();
    }

    @Override
    public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return future.get(timeout, unit);
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public Object getDestination() {
        return destination;
    }

}
