package com.unalarabe.urpc.util;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public interface UpdatesListener {

    public void notifyUpdates();

}
