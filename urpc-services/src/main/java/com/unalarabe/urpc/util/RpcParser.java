package com.unalarabe.urpc.util;

import com.unalarabe.urpc.exception.ConvertException;
import com.unalarabe.urpc.service.Message;
import com.unalarabe.urpc.service.RpcMessage;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public interface RpcParser {

    RpcMessage parseRpcMessageRequest(Object data) throws ConvertException;
    
    Message parseRpcMessageResponse(Object data) throws ConvertException;

    Object createRpcMessageRequest(Message data) throws ConvertException;
    
    Object createRpcMessageResponse(Message data) throws ConvertException;
    
}
