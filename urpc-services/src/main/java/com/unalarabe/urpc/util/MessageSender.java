package com.unalarabe.urpc.util;

import com.unalarabe.urpc.exception.UrpcException;
import com.unalarabe.urpc.service.Message;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public interface MessageSender {

    public boolean sendMessage(Object destination, Message message, long ttlMs);

}
