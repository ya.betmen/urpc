package com.unalarabe.urpc.util;

import java.util.Collection;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ServiceUtil {

    public static <T> T first(Collection<T> list) {
        for (T item : list) {
            return item;
        }
        return null;
    }

    public static Throwable unwrap(Throwable th) {
        while (th.getCause() != null) {
            th = th.getCause();
        }
        return th;
    }

    public static <T extends Throwable> T unwrapTo(Throwable th, Class<T> exClass) {
        while (th != null) {
            if (exClass.isInstance(th)) {
                return (T) th;
            }
            th = th.getCause();
        }
        return null;
    }
    
    public static String toStringOrNull(Object obj) {
        return obj != null ? String.valueOf(obj) : null;
    }
}
