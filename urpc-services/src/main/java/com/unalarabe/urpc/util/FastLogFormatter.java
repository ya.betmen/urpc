package com.unalarabe.urpc.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class FastLogFormatter extends SimpleFormatter {

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SSS");
    private static final int METHOD_NAME_LEN = 20;
    private static final String EMPTY_STRING;

    static {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < METHOD_NAME_LEN; i++) {
            sb.append(' ');
        }
        EMPTY_STRING = sb.toString();
    }

    @Override
    public synchronized String format(LogRecord record) {
        StringBuilder sb = new StringBuilder(SDF.format(new Date(record.getMillis())));
        sb.append("  ");
        int level = record.getLevel().intValue();
        switch (level) {
            case 1000: {//SEVERE
                sb.append("SEVERE:  ");
                break;
            }
            case 900: {//WARNING
                sb.append("WARNING: ");
                break;
            }
            case 800: {//INFO
                sb.append("INFO:    ");
                break;
            }
            case 700: {//CONFIG
                sb.append("CONFIG:  ");
                break;
            }
            case 500: {//FINE
                sb.append("FINE:    ");
                break;
            }
            case 400: {//FINER
                sb.append("FINER:   ");
                break;
            }
            case 300: {//FINEST
                sb.append("FINEST:  ");
                break;
            }
            case Integer.MIN_VALUE: {//ALL
                sb.append("ALL:     ");
                break;
            }
        }
        String classname = record.getSourceClassName();
        int classNameStart = classname.lastIndexOf('.') + 1;
        int classNameLen = classname.length() - classNameStart;
        if (classNameLen > METHOD_NAME_LEN) {
            sb.append(classname.substring(classNameStart, classNameStart + METHOD_NAME_LEN));
        } else if (classNameLen == METHOD_NAME_LEN) {
            sb.append(classname.substring(classNameStart));
        } else {
            sb.append(classname.substring(classNameStart));
            sb.append(EMPTY_STRING.substring(0, METHOD_NAME_LEN - classNameLen));
        }
        sb.append("  [");
        int threadID = record.getThreadID();
        if (threadID <= 9) {
            sb.append("    ");
        } else if (threadID <= 99) {
            sb.append("   ");
        } else if (threadID <= 999) {
            sb.append("  ");
        } else if (threadID <= 9999) {
            sb.append(" ");
        }
        sb.append(threadID);
        sb.append("]  ");
        sb.append(record.getMessage());
        if (sb.indexOf("{0") > -1) {
            Object[] parameters = record.getParameters();
            for (int i = 0; i < parameters.length; i++) {
                String pattern = "{" + i + "}";
                int position = sb.indexOf(pattern);
                sb.replace(position, position + pattern.length(), String.valueOf(parameters[i]));
            }
        }
        sb.append("\n");
        Throwable thrown = record.getThrown();
        while (thrown != null) {
            if (thrown.getMessage() != null) {
                sb.append(thrown.getMessage());
                sb.append("\n");
            }
            for (StackTraceElement element : thrown.getStackTrace()) {
                sb.append("\t");
                sb.append(element);
                sb.append("\n");
            }
            thrown = thrown.getCause();
        }
        return sb.toString();
    }

}
