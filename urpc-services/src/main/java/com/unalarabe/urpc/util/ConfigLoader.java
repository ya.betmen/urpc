package com.unalarabe.urpc.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 * @param <T>
 */
public abstract class ConfigLoader<T> {

    private static final Logger LOG = Logger.getLogger(ConfigLoader.class.getName());

    public List<T> loadList(Properties properties, String key, String prefix) {
        LOG.log(Level.INFO, "Load list with prefix {0} for {1} from {2}", new Object[]{prefix, key, properties});
        List<T> result = new ArrayList<>();
        List<String> names = extractListValue(properties, key);
        for (String name : names) {
            Properties subtable = extractSubtable(properties, prefix + "." + name);
            T item = createObject();
            item = mapProperties(subtable, item);
            result.add(item);
        }
        return result;
    }

    public abstract T createObject();

    public static Properties loadProperties(Properties defaultProperties, String... tryPath) throws IOException {
        Properties result = null;
        for (String path : tryPath) {
            File file = new File(path);
            if (file.exists() && file.isFile()) {
                try {
                    result = new Properties();
                    result.load(new FileInputStream(file));
                } catch (IOException ex) {
                    result = null;
                    LOG.log(Level.WARNING, ex.getMessage());
                    LOG.log(Level.FINE, ex.getMessage(), ex);
                }
            }
        }
        if (result == null) {
            result = defaultProperties;
        }
        return result;
    }

    public static Properties loadProperties(InputStream defaultSource, String... tryPath) throws IOException {
        Properties defaultProperties = new Properties();
        defaultProperties.load(defaultSource);
        return loadProperties(defaultProperties, tryPath);
    }

    public static List<String> extractListValue(Properties properties, String key) {
        List<String> result = new ArrayList<>();
        String value = getValue(properties, key, "");
        LOG.log(Level.INFO, "Extract value with key{0}: {1}", new Object[]{key, value});
        String[] split = value.split("[ ,;]+");
        for (String name : split) {
            name = name.trim();
            if (!name.isEmpty()) {
                result.add(name);
            }
        }
        LOG.log(Level.INFO, "Extract list with key {0}: {2} from {1}", new Object[]{key, properties, result});
        return result;
    }

    public static <T> T mapProperties(Properties properties, T target, boolean ignoreInherit) {
        try {
            LOG.log(Level.INFO, "Map {0} to {1}", new Object[]{properties, target});
            JSONObject json = new JSONObject();
            for (String key : properties.stringPropertyNames()) {
                if (key.contains(".")) {
                    if (!ignoreInherit) {
                        throw new IllegalArgumentException("Key '" + key + "' is not valid");
                    }
                } else {
                    String value = getValue(properties, key);
                    json.put(key, value);
                }
            }
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.readerForUpdating(target).readValue(json.toString());
            return target;
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static <T> T mapProperties(Properties properties, T target) {
        return mapProperties(properties, target, false);
    }

    public static Properties extractSubtable(Properties properties, String prefix) {
        Properties result = new Properties();
        String start = prefix + ".";
        for (String key : properties.stringPropertyNames()) {
            if (key.startsWith(start)) {
                String value = getValue(properties, key);
                result.setProperty(key.substring(prefix.length() + 1), value);
            }
        }

        LOG.log(Level.INFO, "Extract subtable with prefix {0}: {1}", new Object[]{prefix, result});
        return result;
    }

    private static String getValue(Properties properties, String key) {
        String value = properties.getProperty(key);
        return replaceVariables(properties, value);
    }

    private static String getValue(Properties properties, String key, String defaultValue) {
        String value = properties.getProperty(key, defaultValue);
        return replaceVariables(properties, value);
    }

    private static String replaceVariables(Properties properties, String value) {
        if (value.contains("${")) {
            Set<String> keys = properties.stringPropertyNames();
            do {
                String variable = value.substring(value.indexOf("${") + 2, value.indexOf("}"));
                if (keys.contains(variable)) {
                    String varValue = getValue(properties, variable);
                    LOG.log(Level.INFO, "Replace properity {0} with value {1} by key {2}", new String[]{value, varValue, variable});
                    value = value.replaceAll("\\$\\{" + variable + "\\}", varValue);
                } else {
                    LOG.log(Level.WARNING, "Property not found with name {0}", variable);
                    value = value.replaceAll("\\$\\{" + variable + "\\}", "");
                }
            } while (value.contains("${"));
        }
        return value;
    }

}
