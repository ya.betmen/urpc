package com.unalarabe.urpc.config;

import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class PoolConfig {

    private static final Logger LOG = Logger.getLogger(PoolConfig.class.getName());

    protected int corePoolSize;
    protected int maxPoolSize;
    protected int keepAliveTimeInMs;
    protected int awaitTimeMs;
    protected int ttlMs;

    public PoolConfig() {
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public int getKeepAliveTimeInMs() {
        return keepAliveTimeInMs;
    }

    public void setKeepAliveTimeInMs(int keepAliveTimeInMs) {
        this.keepAliveTimeInMs = keepAliveTimeInMs;
    }

    public int getAwaitTimeMs() {
        return awaitTimeMs;
    }

    public void setAwaitTimeMs(int awaitTimeMs) {
        this.awaitTimeMs = awaitTimeMs;
    }

    public int getTtlMs() {
        return ttlMs;
    }

    public void setTtlMs(int ttlMs) {
        this.ttlMs = ttlMs;
    }

}
