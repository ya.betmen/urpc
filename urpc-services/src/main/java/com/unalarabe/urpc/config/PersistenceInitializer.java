package com.unalarabe.urpc.config;

import com.google.inject.Inject;
import com.google.inject.persist.PersistService;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class PersistenceInitializer {

    @Inject
    PersistenceInitializer(PersistService service) {
        service.start();
    }
}
