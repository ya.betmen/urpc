package com.unalarabe.urpc.config;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class PluginConfig {

    private String pluginName;
    private String pluginClass;

    public String getPluginName() {
        return pluginName;
    }

    public void setPluginName(String pluginName) {
        this.pluginName = pluginName;
    }

    public String getPluginClass() {
        return pluginClass;
    }

    public void setPluginClass(String pluginClass) {
        this.pluginClass = pluginClass;
    }

}
