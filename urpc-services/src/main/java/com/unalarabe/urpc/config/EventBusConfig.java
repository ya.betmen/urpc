package com.unalarabe.urpc.config;

import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class EventBusConfig extends PoolConfig {

    private static final Logger LOG = Logger.getLogger(EventBusConfig.class.getName());

    private String topic;

    public EventBusConfig() {
    }

    private EventBusConfig(String topic) {
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

}
