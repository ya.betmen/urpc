package com.unalarabe.urpc.config;

import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class JmsConfig {

    private static final Logger LOG = Logger.getLogger(JmsConfig.class.getName());

    private String brokerUri;
    private String user;
    private String password;

    public JmsConfig() {
    }

    public String getBrokerUri() {
        return brokerUri;
    }

    public void setBrokerUri(String brokerUri) {
        this.brokerUri = brokerUri;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
