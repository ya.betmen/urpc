package com.unalarabe.urpc.config;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ConnectionConfig extends PoolConfig {

    private static final Logger LOG = Logger.getLogger(ConnectionConfig.class.getName());

    private final Map<?, ?> properties = new HashMap<>();

    private String connectionClass;
    private String serviceClass;
    private String brokerUri;
    private String endpoint;

    public ConnectionConfig() {
    }

    public String getConnectionClass() {
        return connectionClass;
    }

    public void setConnectionClass(String connectionClass) {
        this.connectionClass = connectionClass;
    }

    public String getBrokerUri() {
        return brokerUri;
    }

    public void setBrokerUri(String brokerUri) {
        this.brokerUri = brokerUri;
    }

    public String getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(String serviceClass) {
        this.serviceClass = serviceClass;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public Map<?, ?> getProperties() {
        return properties;
    }

}
