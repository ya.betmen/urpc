package com.unalarabe.urpc.config;

import java.util.Hashtable;
import java.util.logging.Logger;

/**
 *
 * @author liliya
 */
public class LdapConfig {

    private static final Logger LOG = Logger.getLogger(LdapConfig.class.getName());

    private Hashtable environment;

    public LdapConfig() {
    }

    public LdapConfig(Hashtable environment) {
        this.environment = environment;
    }

    public Hashtable getEnvironment() {
        return environment;
    }

    public void setEnvironment(Hashtable environment) {
        this.environment = environment;
    }

}
