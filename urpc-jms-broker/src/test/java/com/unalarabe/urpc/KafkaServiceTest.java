package com.unalarabe.urpc;

import avro.com.unalarabe.urpc.TestServiceClient;
import avro.com.unalarabe.urpc.TestServiceClientProvider;
import avro.com.unalarabe.urpc.TestServiceKafkaService;
import avro.com.unalarabe.urpc.subdir.TsRequestWrapperProxy;
import avro.com.unalarabe.urpc.subdir.TsResponseWrapperProxy;
import avro.com.unalarabe.urpc.subdir1.TsRequestWrapperWrapperProxy;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.unalarabe.urpc.client.ChannelConfig;
import com.unalarabe.urpc.client.KafkaChannel;
import com.unalarabe.urpc.config.ConnectionConfig;
import com.unalarabe.urpc.exception.MethodCallException;
import com.unalarabe.urpc.jms.util.TcpUtils;
import com.unalarabe.urpc.service.AbstractService;
import com.unalarabe.urpc.service.Connection;
import com.unalarabe.urpc.service.kafka.KafkaConnection;
import com.unalarabe.urpc.util.ProcessManager;
import com.unalarabe.urpc.util.UrpcTimer;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import kafka.utils.ZKStringSerializer;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.I0Itec.zkclient.exception.ZkMarshallingError;
import org.I0Itec.zkclient.serialize.ZkSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class KafkaServiceTest {

    private static final SecureRandom RND = new SecureRandom();
    private static int serverId = RND.nextInt(Integer.MAX_VALUE);

    private AbstractService kafkaService;

    public KafkaServiceTest() {
    }

//    @Before
    public void setUp() throws IOException, ClassNotFoundException, Exception {
        System.out.println("setUp");

        if (TcpUtils.isPortAvailable("192.168.57.101", 9092)) {
            throw new IllegalStateException("Kafka is not available");
        }
        if (TcpUtils.isPortAvailable("192.168.57.101", 8081)) {
            throw new IllegalStateException("Avro schema repository is not available");
        }
        if (TcpUtils.isPortAvailable("192.168.57.101", 2181)) {
            throw new IllegalStateException("Zookeeper is not available");
        }

        final UrpcTimer urpcTimer = new UrpcTimer();
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(UrpcTimer.class).toInstance(urpcTimer);
            }
        });

        ConnectionConfig kafkaConnectionConfig = new ConnectionConfig();
        kafkaConnectionConfig.setAwaitTimeMs(30000);
        kafkaConnectionConfig.setCorePoolSize(10);
        kafkaConnectionConfig.setEndpoint("server" + serverId);
        kafkaConnectionConfig.setBrokerUri("192.168.57.101:9092");
        kafkaConnectionConfig.setConnectionClass(KafkaConnection.class.getCanonicalName());
        kafkaConnectionConfig.setKeepAliveTimeInMs(10000);
        kafkaConnectionConfig.setMaxPoolSize(500);
        kafkaConnectionConfig.setServiceClass(TestServiceKafkaService.class.getCanonicalName());
        kafkaConnectionConfig.setTtlMs(10000);
        Map props = kafkaConnectionConfig.getProperties();
        props.put("schema.registry.url", "http://192.168.57.101:8081");
//        props.put("zookeeper.connect", "192.168.57.101:2181");
        props.put("consumer.timeout.ms", "10000");
        props.put("group.id", "test");

        Class<AbstractService> kafkaServiceClass = (Class<AbstractService>) getClass().getClassLoader().loadClass(kafkaConnectionConfig.getServiceClass());
        Class<Connection> connectionClass = (Class<Connection>) getClass().getClassLoader().loadClass(kafkaConnectionConfig.getConnectionClass());
        kafkaService = kafkaServiceClass
            .getConstructor(Class.class, Injector.class, ConnectionConfig.class)
            .newInstance(connectionClass, injector, kafkaConnectionConfig);

        kafkaService.start();

        ZkClient zkClient = new ZkClient("192.168.57.101:2181", 15000, 15000);
        zkClient.setZkSerializer(new ZkSerializer() {
            @Override
            public byte[] serialize(Object o) throws ZkMarshallingError {
                return ZKStringSerializer.serialize(o);
            }

            @Override
            public Object deserialize(byte[] bytes) throws ZkMarshallingError {
                return ZKStringSerializer.deserialize(bytes);
            }
        });
        ZkUtils zkUtils = new ZkUtils(zkClient, new ZkConnection("192.168.57.101:2181"), false);
        AdminUtils.createTopic(zkUtils, "TestService.server" + serverId, 1, 1, new Properties(), RackAwareMode.Enforced$.MODULE$);

        System.out.println("finish setUp");
    }

//    @After
    public void tearDown() throws InterruptedException {
        System.out.println("tearDown");
        kafkaService.shutdown();
    }

//    @Test
    public void testSum() throws InterruptedException {
        for (int i = 0; i < 2; i++) {
//            try {
//                Thread.sleep(5000);
//                tearDown();
//                setUp();
//                System.out.println("reconnect");
//            } catch (Exception ex) {
//                Logger.getLogger(KafkaServiceTest.class.getName()).log(Level.SEVERE, null, ex);
//            }

            System.out.println("testSum");
            assertNotNull(kafkaService);

            ProcessManager processManager = new ProcessManager();

            ChannelConfig channelConfig = createChannelConfig("TestService");
            TestServiceClientProvider testServiceClientProvider = new TestServiceClientProvider(processManager, channelConfig);
            TestServiceClient serviceClient = testServiceClientProvider.get("server" + serverId);
            try {
//            TestServiceClient.SumFuture0 sumF0 = serviceClient.sum(1, 2);
//            TestServiceClient.SumFuture0 sumF1 = serviceClient.sum(1, 2);
                int sum1 = serviceClient.sum(1, 2).val(10, TimeUnit.SECONDS);
                System.out.println("SUM = " + sum1);
//                assertEquals(3, sum1);
            } catch (Exception ex) {
                System.out.println(ex);
//            throw new IllegalStateException("Shouldn't be here");
            }

            processManager.shutdown();
        }
        assertEquals(3, 3);
    }

//    @Test
    public void testTimeout() throws Exception {
        System.out.println("testTimeout");
        ProcessManager processManager = new ProcessManager();

        final int threads = 1;
        final int count = 50;

        final long[] totalTimes = new long[threads * count];

        final AtomicInteger errors = new AtomicInteger(0);

        CountDownLatch latch = new CountDownLatch(threads);
        for (int i = 0; i < threads; i++) {
            final int l = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ChannelConfig channelConfig = createChannelConfig("TestService");
                    TestServiceClientProvider testServiceClientProvider = new TestServiceClientProvider(processManager, channelConfig);
                    TestServiceClient serviceClient = testServiceClientProvider.get("server" + serverId);
                    try {
                        Thread.yield();
                        for (int i = 0; i < count; i++) {
                            TsResponseWrapperProxy trwp = serviceClient.echoTs(new TsRequestWrapperProxy(new TsRequestWrapperWrapperProxy(System.currentTimeMillis()), null)).val();
                            totalTimes[i + l * count] = System.currentTimeMillis() - trwp.ts.ts;
//                            System.out.println("time[" + l + "x" + i + "]: " + (t1 - t0) + "ms");
                        }
                    } catch (MethodCallException ex) {
                        errors.incrementAndGet();
                        System.out.println("ERROR: " + ex.getMessage());
                    } finally {
                        latch.countDown();
                    }
                }
            }).start();
        }
        latch.await();
        long min = Long.MAX_VALUE;
        long max = Long.MIN_VALUE;
        long sum = 0;
        for (long time : totalTimes) {
            min = Math.min(min, time);
            max = Math.max(max, time);
            sum += time;
        }
        long avg = sum / totalTimes.length;
        Arrays.sort(totalTimes);
        long median;
        if (totalTimes.length % 2 == 0) {
            median = (totalTimes[totalTimes.length / 2] + totalTimes[totalTimes.length / 2 - 1]) / 2;
        } else {
            median = totalTimes[totalTimes.length / 2];
        }
        int errorsCount = errors.get();

        System.out.println("threads=" + threads + ", iterations=" + count);
        System.out.println("----------------------------------------------------------------------");
        System.out.println("min=" + min + "ms,\tmax=" + max + "ms,\tavg=" + avg + "ms,\tmedian=" + median + "ms,\terrors=" + errorsCount);
        System.out.println("----------------------------------------------------------------------");

        assertEquals(0, errorsCount);
        processManager.shutdown();
    }

    protected ChannelConfig createChannelConfig(String serviceName) {
        ChannelConfig channelConfig = new ChannelConfig(KafkaChannel.class, "server" + serverId, TestServiceKafkaService.RPC_PARSER);
        Map<String, Object> props = channelConfig.getParameters();
        props.put("bootstrap.servers", "192.168.57.101:9092");
        props.put("group.id", "test");
        props.put("group.max.session.timeout.ms", "40000");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
//        props.put("autooffset.reset", "largest");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer", StringDeserializer.class.getCanonicalName());
        props.put("value.deserializer", KafkaAvroDeserializer.class.getCanonicalName());
        props.put("key.serializer", StringSerializer.class.getCanonicalName());
        props.put("value.serializer", KafkaAvroSerializer.class.getCanonicalName());
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
        props.put(AbstractKafkaAvroSerDeConfig.AUTO_REGISTER_SCHEMAS, true);
        props.put("schema.registry.url", "http://192.168.57.101:8081");
        props.put("acks", "all");
//        props.put("retries", 10);
        props.put("batch.size", 16384);
//        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        return channelConfig;
    }

}
