package com.unalarabe.urpc;

import com.unalarabe.urpc.artemis.server.ConnectorConfiguration;
import com.unalarabe.urpc.artemis.server.Protocol;
import com.unalarabe.urpc.artemis.server.ServerConfiguration;
import com.unalarabe.urpc.artemis.server.security.Role;
import com.unalarabe.urpc.artemis.server.security.SecurityManager;
import com.unalarabe.urpc.artemis.server.security.User;
import com.unalarabe.urpc.client.ChannelConfig;
import com.unalarabe.urpc.client.JmsChannel;
import com.unalarabe.urpc.config.JmsConfig;
import com.unalarabe.urpc.config.ConnectionConfig;
import com.unalarabe.urpc.exception.MethodCallException;
import com.unalarabe.urpc.jms.JmsBroker;
import com.unalarabe.urpc.jms.MessagePreprocessorImpl;
import com.unalarabe.urpc.util.JmsConnectionFactory;
import com.unalarabe.urpc.util.ProcessManager;
import com.unalarabe.urpc.jms.util.TcpUtils;
import com.unalarabe.urpc.service.jms.JmsConnection;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.activemq.artemis.core.remoting.impl.netty.NettyAcceptorFactory;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import proto.com.unalarabe.urpc.TestServiceClient;
import proto.com.unalarabe.urpc.TestServiceClientProvider;
import proto.com.unalarabe.urpc.TestServiceJmsService;
import proto.com.unalarabe.urpc.subdir.MapValue;
import proto.com.unalarabe.urpc.subdir.TsRequestWrapperProxy;
import proto.com.unalarabe.urpc.subdir.TsResponseWrapperProxy;
import proto.com.unalarabe.urpc.subdir1.TsRequestWrapperWrapperProxy;

import static org.junit.Assert.*;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class JmsBrokerTest {

    private static final SecureRandom RND = new SecureRandom();
    private JmsBroker jmsBroker;
    private int port;
    private File brokerDir;

    public JmsBrokerTest() {
    }

    private static int findPort() {
        int startport = 1024;
        int maxport = 10000;
        int port = 0;
        int maxTries = 100;
        int tryNumber = 0;
        do {
            tryNumber++;
            port = startport + RND.nextInt(maxport - startport);
        } while (port < maxport && !TcpUtils.isPortAvailable(port) && tryNumber < maxTries);
        if (tryNumber == maxTries) {
            throw new IllegalStateException("Can't fins free port");
        }
        return port;
    }

    @Before
    public void setUp() throws IOException, ClassNotFoundException, Exception {
        System.out.println("setUp");
        port = findPort();
        System.out.println("Free port is " + port);
        jmsBroker = new JmsBroker();
        ServerConfiguration serverConfiguration = new ServerConfiguration();
        SecurityManager securityManager = new SecurityManager();
        serverConfiguration.setSecurityManager(securityManager);
        brokerDir = new File("/tmp/artemis-test-" + port);

        serverConfiguration.setBrokerDir(brokerDir);
        ConnectorConfiguration coreConnectorConfiguration
            = new ConnectorConfiguration("core", NettyAcceptorFactory.class, new InetSocketAddress("localhost", port), Protocol.CORE);
        serverConfiguration.getConnectors().add(coreConnectorConfiguration);

        ConnectionConfig jmsServiceConfig = new ConnectionConfig();
        jmsServiceConfig.setAwaitTimeMs(30000);
        jmsServiceConfig.setBrokerUri("localhost:" + port);
        jmsServiceConfig.setCorePoolSize(10);
        jmsServiceConfig.setEndpoint("server");
        JmsConfig jmsConfig = new JmsConfig();
        jmsConfig.setBrokerUri("tcp://localhost:" + port);
        jmsConfig.setPassword("password");
        jmsConfig.setUser("server");
        jmsServiceConfig.setConnectionClass(JmsConnection.class.getCanonicalName());
        jmsServiceConfig.setKeepAliveTimeInMs(10000);
        jmsServiceConfig.setMaxPoolSize(500);
        jmsServiceConfig.setServiceClass(TestServiceJmsService.class.getCanonicalName());
        jmsServiceConfig.setTtlMs(10000);

        jmsBroker.setJmsConfig(jmsConfig);
        jmsBroker.addService(jmsServiceConfig);

        Role serverRole = securityManager.createRole("server");
        User serverUser = securityManager.createUser("server", "password");
        serverRole.addUser(serverUser);

        Role clientRole = securityManager.createRole("client");
        User clinet1User = securityManager.createUser("client1", "password");
        clinet1User.addRole(clientRole);

        securityManager.addRole(serverRole);
        securityManager.addUser(serverUser);
        securityManager.addRole(clientRole);
        securityManager.addUser(clinet1User);

        jmsBroker.setMessagePreprocessor(new MessagePreprocessorImpl());
        jmsBroker.init(serverConfiguration);
        jmsBroker.startAndWait(30, TimeUnit.SECONDS);
        System.out.println("finish setUp");
    }

    @After
    public void tearDown() {
        System.out.println("tearDown");
        jmsBroker.shutdown();
        try {
            FileUtils.deleteDirectory(brokerDir);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testSumBase() throws TestException {
        System.out.println("testSumBase");
        TestService testService = new TestService();
        int sum = testService.sum(1, 2);
        assertEquals(3, sum);
    }

    @Test
    public void testSum() {
        System.out.println("testSum");
        JmsConfig jmsConfig = new JmsConfig();
        jmsConfig.setBrokerUri("tcp://localhost:" + port);
        jmsConfig.setPassword("password");
        jmsConfig.setUser("client1");
        ProcessManager processManager = new ProcessManager();

        ChannelConfig channelConfig = new ChannelConfig(JmsChannel.class, "server", TestServiceJmsService.RPC_PARSER);
        channelConfig.getParameters().put("jmsConnectionFactory", new JmsConnectionFactory(jmsConfig));
        TestServiceClientProvider testServiceClientProvider = new TestServiceClientProvider(processManager, channelConfig);
        TestServiceClient serviceClient = testServiceClientProvider.get("server");
        try {
            int sum = serviceClient.sum(1, 2).val();
            assertEquals(3, sum);
        } catch (TestServiceClient.TestException ex) {
            ex.printStackTrace();
            throw new IllegalStateException("Shouldn't be here");
        }

        try {
            long sum = serviceClient.sum(1000L, 2000L).val();
            assertEquals(3000, sum);
        } catch (TestServiceClient.TestException | TestServiceClient.OtherException ex) {
            ex.printStackTrace();
            throw new IllegalStateException("Shouldn't be here");
        }
        processManager.shutdown();
    }

    @Test
    public void testTimeout() throws Exception {
        System.out.println("testTimeout");
        JmsConfig jmsConfig = new JmsConfig();
        jmsConfig.setBrokerUri("tcp://localhost:" + port);
        jmsConfig.setPassword("password");
        jmsConfig.setUser("client1");
        ProcessManager processManager = new ProcessManager();

        final int threads = 10;
        final int count = 200;

        final long[] totalTimes = new long[threads * count];

        final AtomicInteger errors = new AtomicInteger(0);

        CountDownLatch latch = new CountDownLatch(threads);
        for (int i = 0; i < threads; i++) {
            final int l = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ChannelConfig channelConfig = new ChannelConfig(JmsChannel.class, "server", TestServiceJmsService.RPC_PARSER);
                    channelConfig.getParameters().put("jmsConnectionFactory", new JmsConnectionFactory(jmsConfig));
                    TestServiceClientProvider testServiceClientProvider = new TestServiceClientProvider(processManager, channelConfig);
                    TestServiceClient serviceClient = testServiceClientProvider.get("server");
                    try {
                        Thread.yield();
                        for (int i = 0; i < count; i++) {
                            TsResponseWrapperProxy trwp = serviceClient.echoTs(new TsRequestWrapperProxy(new TsRequestWrapperWrapperProxy(System.currentTimeMillis()), null)).val();
                            totalTimes[i + l * count] = System.currentTimeMillis() - trwp.ts.ts;
//                            System.out.println("time[" + l + "x" + i + "]: " + (t1 - t0) + "ms");
                        }
                    } catch (TestServiceClient.TestException | MethodCallException ex) {
                        errors.incrementAndGet();
                        System.out.println("ERROR: " + ex.getMessage());
                    } finally {
                        latch.countDown();
                    }
                }
            }).start();
        }
        latch.await();
        long min = Long.MAX_VALUE;
        long max = Long.MIN_VALUE;
        long sum = 0;
        for (long time : totalTimes) {
            min = Math.min(min, time);
            max = Math.max(max, time);
            sum += time;
        }
        long avg = sum / totalTimes.length;
        Arrays.sort(totalTimes);
        long median;
        if (totalTimes.length % 2 == 0) {
            median = (totalTimes[totalTimes.length / 2] + totalTimes[totalTimes.length / 2 - 1]) / 2;
        } else {
            median = totalTimes[totalTimes.length / 2];
        }
        int errorsCount = errors.get();

        System.out.println("threads=" + threads + ", iterations=" + count);
        System.out.println("----------------------------------------------------------------------");
        System.out.println("min=" + min + "ms,\tmax=" + max + "ms,\tavg=" + avg + "ms,\tmedian=" + median + "ms,\terrors=" + errorsCount);
        System.out.println("----------------------------------------------------------------------");

        assertEquals(0, errorsCount);
        processManager.shutdown();
    }

    @Test
    public void testError() throws Exception {
        System.out.println("testError");
        JmsConfig jmsConfig = new JmsConfig();
        jmsConfig.setBrokerUri("tcp://localhost:" + port);
        jmsConfig.setPassword("password");
        jmsConfig.setUser("client1");
        ProcessManager processManager = new ProcessManager();
        ChannelConfig channelConfig = new ChannelConfig(JmsChannel.class, "server", TestServiceJmsService.RPC_PARSER);
        channelConfig.getParameters().put("jmsConnectionFactory", new JmsConnectionFactory(jmsConfig));
        TestServiceClientProvider testServiceClientProvider = new TestServiceClientProvider(processManager, channelConfig);
        TestServiceClient client = testServiceClientProvider.get("server");
        try {
            client.error().val();
            throw new IllegalStateException("Shouldn't be here");
        } catch (MethodCallException ex) {
            String message = ex.getMessage();
            assertEquals("ERROR", message);
        }
    }

    @Test
    public void testCollections() throws Exception {
        System.out.println("testCollections");
        JmsConfig jmsConfig = new JmsConfig();
        jmsConfig.setBrokerUri("tcp://localhost:" + port);
        jmsConfig.setPassword("password");
        jmsConfig.setUser("client1");
        ProcessManager processManager = new ProcessManager();
        ChannelConfig channelConfig = new ChannelConfig(JmsChannel.class, "server", TestServiceJmsService.RPC_PARSER);
        channelConfig.getParameters().put("jmsConnectionFactory", new JmsConnectionFactory(jmsConfig));
        TestServiceClientProvider testServiceClientProvider = new TestServiceClientProvider(processManager, channelConfig);
        TestServiceClient client = testServiceClientProvider.get("server");
        List<String> resultList = client.testList(Arrays.asList(0, 1, 2)).val();
        assertEquals(3, resultList.size());

        Map<String, MapValue> resultMap = client.testMap(Arrays.asList("1", "2", "2")).val();
        assertEquals(2, resultMap.size());

        Map<String, Integer> resultMap2 = client.testMap2(Arrays.asList("1", "2", "2")).val();
        assertEquals(2, resultMap2.size());

        List<String> resultList1 = client.testSet(Arrays.asList(0, 1, 2, 2)).val();
        assertEquals(3, resultList1.size());
    }

}
