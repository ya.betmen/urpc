package com.unalarabe.urpc.subdir;

import com.unalarabe.urpc.subdir.subsubdir.TsResponseWrapperWrapper;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class TsResponseWrapper {

    private TsResponseWrapperWrapper ts;

    public TsResponseWrapperWrapper getTs() {
        return ts;
    }

    public void setTs(TsResponseWrapperWrapper ts) {
        this.ts = ts;
    }

}
