package com.unalarabe.urpc.subdir.subsubdir;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class TsResponseWrapperWrapper {

    private long ts;

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

}
