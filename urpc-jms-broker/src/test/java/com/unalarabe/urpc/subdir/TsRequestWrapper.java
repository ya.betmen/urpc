package com.unalarabe.urpc.subdir;

import com.unalarabe.urpc.subdir1.TsRequestWrapperWrapper;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class TsRequestWrapper {

    private TsRequestWrapperWrapper ts;
    private String appendix;

    public TsRequestWrapperWrapper getTs() {
        return ts;
    }

    public void setTs(TsRequestWrapperWrapper ts) {
        this.ts = ts;
    }

    public String getAppendix() {
        return appendix;
    }

    public void setAppendix(String appendix) {
        this.appendix = appendix;
    }

}
