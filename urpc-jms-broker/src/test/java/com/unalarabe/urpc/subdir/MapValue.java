package com.unalarabe.urpc.subdir;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MapValue {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
