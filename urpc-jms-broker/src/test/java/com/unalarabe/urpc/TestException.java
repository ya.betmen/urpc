package com.unalarabe.urpc;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class TestException extends Exception {

    private static final long serialVersionUID = 3156319732136643489L;

    public TestException() {
    }

    public TestException(String message) {
        super(message);
    }

    public TestException(String message, Throwable cause) {
        super(message, cause);
    }

    public TestException(Throwable cause) {
        super(cause);
    }

    public TestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
