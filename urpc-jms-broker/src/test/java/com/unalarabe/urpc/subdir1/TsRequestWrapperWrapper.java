package com.unalarabe.urpc.subdir1;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class TsRequestWrapperWrapper {

    private long ts;

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

}
