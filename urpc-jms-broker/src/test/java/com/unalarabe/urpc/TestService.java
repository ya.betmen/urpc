package com.unalarabe.urpc;

import com.unalarabe.urpc.annotations.Ignore;
import com.unalarabe.urpc.subdir.TsResponseWrapper;
import com.unalarabe.urpc.annotations.Method;
import com.unalarabe.urpc.annotations.Service;
import com.unalarabe.urpc.subdir.MapValue;
import com.unalarabe.urpc.subdir.TsRequestWrapper;
import com.unalarabe.urpc.subdir.subsubdir.TsResponseWrapperWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
@Service
public class TestService {

    @Method
    public int sum(int a, int b) throws TestException {
        System.out.println("sum: " + a + ", " + b);
        return a + b;
    }

    @Method
    public long sum(long a, long b) throws TestException, OtherException {
        System.out.println("sum: " + a + ", " + b);
        return a + b;
    }

    @Method
    public TsResponseWrapper echoTs(TsRequestWrapper trw) throws TestException, com.unalarabe.urpc.subdir.TestException {
        System.out.println("echoTs: " + trw);
        TsResponseWrapper tsWrpper = new TsResponseWrapper();
        TsResponseWrapperWrapper tsResponseWrapperWrapper = new TsResponseWrapperWrapper();
        tsResponseWrapperWrapper.setTs(trw.getTs().getTs());
        tsWrpper.setTs(tsResponseWrapperWrapper);
        return tsWrpper;
    }

    @Method
    @Ignore(generators = "avro")
    public void error() throws TestException, OtherException {
        System.out.println("error: " + this.getClass().getSimpleName());
        throw new TestException("ERROR");
    }

    @Method
    public Map<String, MapValue> testMap(Set<String> set) {
        System.out.println("testMap: " + set);
        HashMap<String, MapValue> result = new HashMap<>();
        for (String str : set) {
            MapValue v = new MapValue();
            v.setValue(str);
            result.put(str, v);
        }
        return result;
    }

    @Method
    public Map<String, Integer> testMap2(Set<String> set) {
        System.out.println("testMap2: " + set);
        HashMap<String, Integer> result = new HashMap<>();
        for (String str : set) {
            result.put(str, 0);
        }
        return result;
    }

    @Method
    public List<String> testList(List<Integer> list) {
        System.out.println("testList: " + list);
        ArrayList<String> result = new ArrayList<>();
        for (Integer integer : list) {
            result.add(String.valueOf(integer));
        }
        return result;
    }

    @Method
    public Set<String> testSet(Set<Integer> list) {
        System.out.println("testSet: " + list);
        Set<String> result = new HashSet<>();
        for (Integer integer : list) {
            result.add(String.valueOf(integer));
        }
        return result;
    }

}
