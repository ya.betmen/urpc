package com.unalarabe.urpc;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class OtherException extends Exception {

    private static final long serialVersionUID = 3503251851768713811L;

    public OtherException() {
    }

    public OtherException(String message) {
        super(message);
    }

    public OtherException(String message, Throwable cause) {
        super(message, cause);
    }

    public OtherException(Throwable cause) {
        super(cause);
    }

    public OtherException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
