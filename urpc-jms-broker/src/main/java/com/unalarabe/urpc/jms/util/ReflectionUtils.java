package com.unalarabe.urpc.jms.util;

import io.netty.util.concurrent.GlobalEventExecutor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.activemq.artemis.api.core.client.ActiveMQClient;
import org.apache.activemq.artemis.core.remoting.impl.netty.SharedEventLoopGroup;

import static com.unalarabe.urpc.jms.util.ExecutorUtil.createGlobalEventExecutor;
import static com.unalarabe.urpc.jms.util.ExecutorUtil.shutdown;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ReflectionUtils {

    private static final Logger LOG = Logger.getLogger(ReflectionUtils.class.getName());

    public static <T> T getStaticProperty(Class clazz, String property) {
        Object result = null;
        while (result == null && clazz != null) {
            try {
                Field field = clazz.getDeclaredField(property);
                field.setAccessible(true);
                result = field.get(null);
            } catch (NoSuchFieldException ex) {
                LOG.log(Level.FINEST, ex.getMessage(), ex);
                LOG.log(Level.INFO, ex.getMessage());
                clazz = clazz.getSuperclass();
            } catch (SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
                break;
            }
        }
        return (T) result;
    }

    public static <T> T getObjectProperty(Object obj, String property) {
        Object result = null;
        Class<?> clazz = obj.getClass();
        while (result == null && clazz != null) {
            try {
                Field field = clazz.getDeclaredField(property);
                field.setAccessible(true);
                result = field.get(obj);
            } catch (NoSuchFieldException ex) {
                LOG.log(Level.FINEST, ex.getMessage(), ex);
                LOG.log(Level.INFO, ex.getMessage());
                clazz = clazz.getSuperclass();
            } catch (SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
                break;
            }
        }
        return (T) result;
    }

    public static <T> boolean setObjectProperty(Object obj, String property, T value) {
        try {
            Field field = null;
            Class<?> clazz = obj.getClass();
            while (field == null && clazz != null) {
                try {
                    field = clazz.getDeclaredField(property);
                } catch (NoSuchFieldException ex) {
                    LOG.log(Level.FINEST, ex.getMessage(), ex);
                    LOG.log(Level.INFO, ex.getMessage());
                    clazz = clazz.getSuperclass();
                } catch (SecurityException | IllegalArgumentException ex) {
                    LOG.log(Level.SEVERE, ex.getMessage(), ex);
                    break;
                }
            }
            if (field != null) {
                field.setAccessible(true);
                Field modifiersField = Field.class.getDeclaredField("modifiers");
                modifiersField.setAccessible(true);
                modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
                field.set(obj, value);
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return false;
    }

    public static <T> boolean setStaticProperty(Class clazz, String property, T value) {
        try {
            Field field = null;
            while (field == null && clazz != null) {
                try {
                    field = clazz.getDeclaredField(property);
                } catch (NoSuchFieldException ex) {
                    LOG.log(Level.FINEST, ex.getMessage(), ex);
                    LOG.log(Level.INFO, ex.getMessage());
                    clazz = clazz.getSuperclass();
                } catch (SecurityException | IllegalArgumentException ex) {
                    LOG.log(Level.SEVERE, ex.getMessage(), ex);
                    break;
                }
            }
            if (field != null) {
                field.setAccessible(true);
                Field modifiersField = Field.class.getDeclaredField("modifiers");
                modifiersField.setAccessible(true);
                modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
                field.set(null, value);
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return false;
    }

    public static LinkedBlockingQueueHacked hackGlobalEventExecutor() {
        GlobalEventExecutor globalEventExecutor = createGlobalEventExecutor();
        LinkedBlockingQueueHacked linkedBlockingQueueHacked = new LinkedBlockingQueueHacked();
        setObjectProperty(globalEventExecutor, "taskQueue", linkedBlockingQueueHacked);
        setStaticProperty(GlobalEventExecutor.class, "INSTANCE", globalEventExecutor);
        return linkedBlockingQueueHacked;
    }

    public static void stopGlobalQueues() throws InterruptedException {
        SharedEventLoopGroup eventLoopGroup = getStaticProperty(SharedEventLoopGroup.class, "instance");
        BlockingQueue<Runnable> taskQueue = getObjectProperty(GlobalEventExecutor.INSTANCE, "taskQueue");
        ScheduledExecutorService globalScheduledThreadPool = ActiveMQClient.getGlobalScheduledThreadPool();
        ExecutorService globalThreadPool = ActiveMQClient.getGlobalThreadPool();

        shutdown(eventLoopGroup, 10, TimeUnit.SECONDS);
        LOG.info("shutdown eventLoopGroup");
        shutdown(globalScheduledThreadPool, 10, TimeUnit.SECONDS);
        LOG.info("shutdown globalScheduledThreadPool");
        shutdown(globalThreadPool, 10, TimeUnit.SECONDS);
        LOG.info("shutdown globalThreadPool");

        setObjectProperty(GlobalEventExecutor.INSTANCE, "scheduledTaskQueue", null);
        taskQueue.clear();
    }
}
