package com.unalarabe.urpc.jms.event;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import com.unalarabe.urpc.config.EventBusConfig;
import com.unalarabe.urpc.util.JmsConnectionFactory;
import com.unalarabe.urpc.util.UrpcThreadPool;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
@Singleton
public class EventBus implements ThreadFactory, Thread.UncaughtExceptionHandler {

    private static final Logger LOG = Logger.getLogger(EventBus.class.getName());

    private final SecureRandom random = new SecureRandom();
    private final EventBusConfig eventBusConfig;
    private final Map<String, Long> processedMessages = new ConcurrentHashMap<>();
    private final ObjectMapper mapper;

    private Map<Class<? extends Event>, Set<Object>> listeners = new HashMap<>();

    @Inject
    private JmsConnectionFactory connectionFactory;

    private Connection connection;
    private MessageProducer producer;
    private MessageConsumer consumer;
    private Thread consumerThread;
    private ThreadPoolExecutor eventThreadPool;
    private Session producerSession;

    private volatile boolean stop = false;

    public EventBus(EventBusConfig eventBusConfig) {
        this.eventBusConfig = eventBusConfig;
        this.mapper = new ObjectMapper();
        this.mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.OBJECT_AND_NON_CONCRETE);
        this.mapper.enableDefaultTypingAsProperty(ObjectMapper.DefaultTyping.NON_FINAL, "@class");
        eventThreadPool = new UrpcThreadPool(eventBusConfig, new LinkedBlockingQueue<>(eventBusConfig.getMaxPoolSize()), this);
    }

    public void start() {
        start(connectionFactory);
    }

    public void start(JmsConnectionFactory connectionFactory) {
        try {
            connection = connectionFactory.connect();

            producerSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Topic producerTopic = producerSession.createTopic(eventBusConfig.getTopic());
            producer = producerSession.createProducer(producerTopic);

            Session consumerSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Topic consumerTopic = producerSession.createTopic(eventBusConfig.getTopic());
            consumer = consumerSession.createConsumer(consumerTopic);

            connection.start();

            consumerThread = new Thread() {
                @Override
                public void run() {
                    while (!stop) {
                        try {
                            Message message = consumer.receive(1000);
                            if (message != null) {
                                String correlationID = message.getJMSCorrelationID();
                                if (!processedMessages.containsKey(correlationID)) {
                                    TextMessage textMessage = (TextMessage) message;
                                    String json = textMessage.getText();
                                    Event event = parseEvent(json);
                                    processedMessages.put(correlationID, System.currentTimeMillis() + 10000);
                                    if (event != null) {
                                        fireEventInternal(event);
                                    }
                                }
                            }
                            Thread.yield();
                        } catch (JMSException ex) {
                            LOG.log(Level.SEVERE, null, ex);
                        }
                    }
                }
            };
            consumerThread.start();
        } catch (JMSException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    public void shutdown() {
        try {
            stop = true;
            producer.close();
            consumer.close();
            connection.stop();
            connection.close();
            eventThreadPool.shutdown();
        } catch (JMSException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public synchronized void addListener(Class<? extends Event> eventClass, Object listener) {
        if (!listeners.containsKey(eventClass)) {
            listeners.put(eventClass, new HashSet<>());
        }
        listeners.get(eventClass).add(listener);
    }

    public synchronized void removeListener(Class<? extends Event> eventClass, Object listener) {
        if (listeners.containsKey(eventClass)) {
            listeners.get(eventClass).remove(listener);
        }
    }

    private Event parseEvent(String json) {
        try {
            Object e = mapper.readValue(json, Object.class);
            return (Event) e;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public <T extends Event> void fireEvent(T event) {
        if (producer == null) {
            start();
        }
        String correlationID = new UUID(random.nextLong(), random.nextLong()).toString();
        processedMessages.put(correlationID, System.currentTimeMillis() + 10000);
        SendEventRunnable sendEventRunnable = new SendEventRunnable(producerSession, producer, correlationID, event, mapper);
        eventThreadPool.submit(sendEventRunnable);
        fireEventInternal(event);
    }

    private <T extends Event> void fireEventInternal(T event) {
        if (listeners.containsKey(event.getClass())) {
            Set<Object> eventListeners = new HashSet<>(listeners.get(event.getClass()));
            FireEventRunnable runnable = new FireEventRunnable(event, eventListeners);
            eventThreadPool.submit(runnable);
        }
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.setUncaughtExceptionHandler(this);
        return thread;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        LOG.log(Level.SEVERE, e.getMessage(), e);
    }

}
