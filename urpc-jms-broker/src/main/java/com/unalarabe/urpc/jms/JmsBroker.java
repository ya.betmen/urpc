package com.unalarabe.urpc.jms;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.unalarabe.urpc.artemis.server.ArtemisMQServerExt;
import com.unalarabe.urpc.artemis.server.ConnectorConfiguration;
import com.unalarabe.urpc.artemis.server.MessagePreprocessor;
import com.unalarabe.urpc.artemis.server.ServerConfiguration;
import com.unalarabe.urpc.artemis.server.security.SecurityManager;
import com.unalarabe.urpc.config.JmsConfig;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.unalarabe.urpc.config.ConnectionConfig;
import com.unalarabe.urpc.exception.BrokerException;
import com.unalarabe.urpc.service.AbstractService;
import com.unalarabe.urpc.service.Connection;
import com.unalarabe.urpc.util.UrpcTimer;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.activemq.artemis.api.core.SimpleString;
import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.core.config.Configuration;
import org.apache.activemq.artemis.core.config.impl.ConfigurationImpl;
import org.apache.activemq.artemis.core.config.impl.SecurityConfiguration;
import org.apache.activemq.artemis.core.security.CheckType;
import org.apache.activemq.artemis.core.security.Role;
import org.apache.activemq.artemis.core.server.JournalType;
import org.apache.activemq.artemis.core.server.ServerSession;
import org.apache.activemq.artemis.core.settings.impl.AddressSettings;
import org.apache.activemq.artemis.spi.core.protocol.RemotingConnection;
import org.apache.activemq.artemis.spi.core.security.ActiveMQJAASSecurityManager;
import org.apache.activemq.artemis.spi.core.security.jaas.InVMLoginModule;
import org.apache.commons.io.FileUtils;
//import org.apache.activemq.broker.BrokerPlugin;
//import org.apache.activemq.broker.BrokerService;
//import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class JmsBroker implements Thread.UncaughtExceptionHandler {

    private static final Logger LOG = Logger.getLogger(JmsBroker.class.getName());

    protected final Lock brokerLock = new ReentrantLock();
    protected final Condition brokerLockCondition = brokerLock.newCondition();

    protected final List<AbstractService> services = new ArrayList<>();
    protected final Map<Class<? extends AbstractService>, ConnectionConfig> servicesConfigMap = new HashMap<>();

    protected ServerConfiguration serverConfiguration;
    protected SecurityManager securityManager;
    protected MessagePreprocessor messagePreprocessor;
    protected ArtemisMQServerExt artemisMQServerExt;
    protected Injector injector;
    protected JmsConfig jmsConfig;
    protected UrpcTimer urpcTimer;

    protected HashSet<Role> roles;

    protected volatile boolean started = false;
    protected volatile boolean shutdown = false;
    protected volatile boolean inited = false;

    public SecurityManager getSecurityManager() {
        return securityManager;
    }

    public JmsBroker() {
    }

    public void start() throws InterruptedException, IOException, Exception {
        if (!inited) {
            throw new BrokerException("Server not inited");
        }
        if (shutdown) {
            throw new IllegalStateException("Server is shutdown");
        }
        brokerLock.lock();
        try {
            LOG.info("Start server");
            artemisMQServerExt.start();
            roles.add(new Role("server", true, true, true, true, true, true, true, true, true, true));
            roles.add(new Role("client", true, true, true, true, true, true, true, true, true, true));
            for (AbstractService service : services) {
                service.start();
            }
            started = true;
            brokerLockCondition.await();
        } finally {
            brokerLock.unlock();
        }
    }

    public JmsConfig getJmsConfig() {
        return jmsConfig;
    }

    public void setJmsConfig(JmsConfig jmsConfig) {
        this.jmsConfig = jmsConfig;
    }

    public void init(ServerConfiguration configuration) throws IOException {
        if (shutdown) {
            throw new IllegalStateException("Server is shutdown");
        }
        this.serverConfiguration = configuration;
        this.urpcTimer = new UrpcTimer();
        this.securityManager = configuration.getSecurityManager();
        List<Module> modules = initModules();
        this.injector = createIngector(modules.toArray(new Module[modules.size()]));
        for (ConnectionConfig config : servicesConfigMap.values()) {
            try {
                Class<AbstractService> jmsServiceClass = (Class<AbstractService>) getClass().getClassLoader().loadClass(config.getServiceClass());
                Class<Connection> connectionClass = (Class<Connection>) getClass().getClassLoader().loadClass(config.getConnectionClass());
                AbstractService jmsService = jmsServiceClass
                    .getConstructor(Class.class, Injector.class, ConnectionConfig.class)
                    .newInstance(connectionClass, injector, config);
                services.add(jmsService);
            } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
        artemisMQServerExt = createServer(serverConfiguration);
        artemisMQServerExt.setMessagePreprocessor(messagePreprocessor);
        initJAASRoles(artemisMQServerExt);
        inited = true;
    }

    protected ArtemisMQServerExt createServer(ServerConfiguration configuration) throws IOException {
        Configuration serverConfig = new ConfigurationImpl();
        List<ConnectorConfiguration> connectors = configuration.getConnectors();
        for (ConnectorConfiguration connector : connectors) {
            String transportConfigurationName = connector.getTransportConfigurationName();
            TransportConfiguration transportConfiguration = connector.createTransportConfiguration();
            serverConfig.addAcceptorConfiguration(transportConfiguration);
            serverConfig.addConnectorConfiguration("connector-" + transportConfigurationName, transportConfiguration);
        }
        AddressSettings addressSettings = new AddressSettings();
        addressSettings.setDeadLetterAddress(new SimpleString("dead-messages"));
        addressSettings.setExpiryAddress(new SimpleString("expired-messages"));
        serverConfig.getAddressesSettings().put("#", addressSettings);

        initBrokerDir(configuration.getBrokerDir(), configuration.isCleanDirOnStart());
        String brokerDirPath = configuration.getBrokerDir().getAbsolutePath();
        serverConfig.setPagingDirectory(brokerDirPath + "/paging");
        serverConfig.setBindingsDirectory(brokerDirPath + "/bindings");
        serverConfig.setJournalDirectory(brokerDirPath + "/journal");
        serverConfig.setJournalMinFiles(2);
        serverConfig.setJournalType(JournalType.ASYNCIO);

        updateConfig(serverConfig);

        SecurityConfiguration securityConfiguration = configuration.getSecurityManager().getSecurityConfiguration();
//
//        securityConfiguration.addUser("server", "password");
//        securityConfiguration.addRole("server", "server");

        ActiveMQJAASSecurityManager activeMQJAASSecurityManager = new ActiveMQJAASSecurityManager(InVMLoginModule.class.getName(), securityConfiguration) {
            @Override
            public String validateUserAndRole(String user, String password, Set<Role> roles, CheckType checkType, String address, RemotingConnection remotingConnection) {
                return super.validateUserAndRole(user, password, roles, checkType, address, remotingConnection);
            }

            @Override
            public boolean validateUserAndRole(String user, String password, Set<Role> roles, CheckType checkType) {
                return super.validateUserAndRole(user, password, roles, checkType);
            }

            @Override
            public String validateUser(String user, String password, RemotingConnection remotingConnection) {
                return super.validateUser(user, password, remotingConnection);
            }

            @Override
            public boolean validateUser(String user, String password) {
                return super.validateUser(user, password);
            }

        };
        return new ArtemisMQServerExt(serverConfig, activeMQJAASSecurityManager);
    }

    protected Injector createIngector(Module... modules) {
        return Guice.createInjector(modules);
    }

    protected void initBrokerDir(File brokerDir, boolean cleanDirOnStart) throws IOException {
        if (!brokerDir.exists()) {
            brokerDir.mkdirs();
        }
        if (!brokerDir.isDirectory()) {
            throw new IllegalStateException(brokerDir.getAbsolutePath() + " is not directory");
        }
        File[] listFiles = brokerDir.listFiles();
        for (File innerFile : listFiles) {
            String innerFileName = innerFile.getName();
            if (innerFileName.equals("paging") && cleanDirOnStart) {
                FileUtils.deleteDirectory(innerFile);
            }
            if (innerFileName.equals("bindings") && cleanDirOnStart) {
                FileUtils.deleteDirectory(innerFile);
            }
            if (innerFileName.equals("journal") && cleanDirOnStart) {
                FileUtils.deleteDirectory(innerFile);
            }
        }
    }

    protected void updateConfig(Configuration serverConfig) {
    }

    public boolean isStarted() {
        return started;
    }

    protected void checkNotStarted() {
        if (started) {
            throw new IllegalStateException("Broker is started already");
        }
    }

    public boolean addService(ConnectionConfig config) throws ClassNotFoundException {
        checkNotStarted();
        Class<? extends AbstractService> serviceClass = (Class<? extends AbstractService>) getClass().getClassLoader().loadClass(config.getServiceClass());
        return servicesConfigMap.put(serviceClass, config) == null;
    }

    public boolean removeService(Class<? extends AbstractService> serviceClass) {
        checkNotStarted();
        return servicesConfigMap.remove(serviceClass) == null;
    }

    public MessagePreprocessor getMessagePreprocessor() {
        return messagePreprocessor;
    }

    public void setMessagePreprocessor(MessagePreprocessor messagePreprocessor) {
        this.messagePreprocessor = messagePreprocessor;
    }

    public void shutdown() {
        try {
            LOG.info("Shutdown server");
//            eventBus.shutdown();
//            processManager.shutdown();
            for (AbstractService service : services) {
                service.shutdown();
            }
            if (artemisMQServerExt != null) {
                Set<ServerSession> sessions = artemisMQServerExt.getSessions();
                for (ServerSession session : sessions) {
                    session.stop();
                }
                artemisMQServerExt.stop();
            }
        } catch (Exception ex1) {
            LOG.log(Level.SEVERE, ex1.getMessage(), ex1);
        }
        urpcTimer.cancel();
        started = false;
        brokerLock.lock();
        try {
            brokerLockCondition.signalAll();
        } finally {
            brokerLock.unlock();
        }
        shutdown = true;
    }

    public void startAndWait(long time, TimeUnit timeUnit) throws InterruptedException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    start();
                } catch (Exception ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
        }).start();
        long awaitTime = timeUnit.toMillis(time);
        long waitTo = System.currentTimeMillis() + awaitTime;
        while (!isStarted() && System.currentTimeMillis() < waitTo) {
            Thread.sleep(100);
        }
        if (!isStarted()) {
            if (!isShutdown()) {
                shutdown();
            }
            throw new InterruptedException("Server didn't start");
        }
    }

    public boolean isShutdown() {
        return shutdown;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        LOG.log(Level.SEVERE, "Error in " + t, e);
    }

    protected List<Module> initModules() {
        List<Module> result = new ArrayList<>();
        AbstractModule module = new AbstractModule() {
            @Override
            protected void configure() {
                bind(ServerConfiguration.class).toInstance(serverConfiguration);
                bind(SecurityManager.class).toInstance(securityManager);
                bind(MessagePreprocessor.class).toInstance(messagePreprocessor);
                bind(JmsConfig.class).toInstance(jmsConfig);
                bind(UrpcTimer.class).toInstance(urpcTimer);
            }
        };
        result.add(module);
        return result;
    }

    protected void initJAASRoles(ArtemisMQServerExt artemisMQServerExt) {
        roles = new HashSet<>();
        artemisMQServerExt.getSecurityRepository().addMatch("*", roles);
    }
}
