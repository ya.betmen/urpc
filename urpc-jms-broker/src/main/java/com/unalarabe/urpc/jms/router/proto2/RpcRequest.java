package com.unalarabe.urpc.jms.router.proto2;

/**
 * Protobuf type {@code RpcRequest}
 */
public  final class RpcRequest extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:RpcRequest)
    RpcRequestOrBuilder {
  // Use RpcRequest.newBuilder() to construct.
  private RpcRequest(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private RpcRequest() {
    method_ = "";
    correlationId_ = "";
    replyTo_ = "";
    version_ = 0;
    request_ = com.google.protobuf.ByteString.EMPTY;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private RpcRequest(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!parseUnknownField(input, unknownFields,
                                   extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
          case 18: {
            com.google.protobuf.ByteString bs = input.readBytes();
            bitField0_ |= 0x00000001;
            method_ = bs;
            break;
          }
          case 26: {
            com.google.protobuf.ByteString bs = input.readBytes();
            bitField0_ |= 0x00000002;
            correlationId_ = bs;
            break;
          }
          case 34: {
            com.google.protobuf.ByteString bs = input.readBytes();
            bitField0_ |= 0x00000004;
            replyTo_ = bs;
            break;
          }
          case 40: {
            bitField0_ |= 0x00000008;
            version_ = input.readInt32();
            break;
          }
          case 50: {
            bitField0_ |= 0x00000010;
            request_ = input.readBytes();
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcRequest_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcRequest_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.unalarabe.urpc.jms.router.proto2.RpcRequest.class, com.unalarabe.urpc.jms.router.proto2.RpcRequest.Builder.class);
  }

  private int bitField0_;
  public static final int METHOD_FIELD_NUMBER = 2;
  private volatile java.lang.Object method_;
  /**
   * <code>required string Method = 2;</code>
   */
  public boolean hasMethod() {
    return ((bitField0_ & 0x00000001) == 0x00000001);
  }
  /**
   * <code>required string Method = 2;</code>
   */
  public java.lang.String getMethod() {
    java.lang.Object ref = method_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (bs.isValidUtf8()) {
        method_ = s;
      }
      return s;
    }
  }
  /**
   * <code>required string Method = 2;</code>
   */
  public com.google.protobuf.ByteString
      getMethodBytes() {
    java.lang.Object ref = method_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      method_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int CORRELATIONID_FIELD_NUMBER = 3;
  private volatile java.lang.Object correlationId_;
  /**
   * <code>required string CorrelationId = 3;</code>
   */
  public boolean hasCorrelationId() {
    return ((bitField0_ & 0x00000002) == 0x00000002);
  }
  /**
   * <code>required string CorrelationId = 3;</code>
   */
  public java.lang.String getCorrelationId() {
    java.lang.Object ref = correlationId_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (bs.isValidUtf8()) {
        correlationId_ = s;
      }
      return s;
    }
  }
  /**
   * <code>required string CorrelationId = 3;</code>
   */
  public com.google.protobuf.ByteString
      getCorrelationIdBytes() {
    java.lang.Object ref = correlationId_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      correlationId_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int REPLYTO_FIELD_NUMBER = 4;
  private volatile java.lang.Object replyTo_;
  /**
   * <code>required string ReplyTo = 4;</code>
   */
  public boolean hasReplyTo() {
    return ((bitField0_ & 0x00000004) == 0x00000004);
  }
  /**
   * <code>required string ReplyTo = 4;</code>
   */
  public java.lang.String getReplyTo() {
    java.lang.Object ref = replyTo_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (bs.isValidUtf8()) {
        replyTo_ = s;
      }
      return s;
    }
  }
  /**
   * <code>required string ReplyTo = 4;</code>
   */
  public com.google.protobuf.ByteString
      getReplyToBytes() {
    java.lang.Object ref = replyTo_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      replyTo_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int VERSION_FIELD_NUMBER = 5;
  private int version_;
  /**
   * <code>optional int32 Version = 5;</code>
   */
  public boolean hasVersion() {
    return ((bitField0_ & 0x00000008) == 0x00000008);
  }
  /**
   * <code>optional int32 Version = 5;</code>
   */
  public int getVersion() {
    return version_;
  }

  public static final int REQUEST_FIELD_NUMBER = 6;
  private com.google.protobuf.ByteString request_;
  /**
   * <code>required bytes request = 6;</code>
   */
  public boolean hasRequest() {
    return ((bitField0_ & 0x00000010) == 0x00000010);
  }
  /**
   * <code>required bytes request = 6;</code>
   */
  public com.google.protobuf.ByteString getRequest() {
    return request_;
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    if (!hasMethod()) {
      memoizedIsInitialized = 0;
      return false;
    }
    if (!hasCorrelationId()) {
      memoizedIsInitialized = 0;
      return false;
    }
    if (!hasReplyTo()) {
      memoizedIsInitialized = 0;
      return false;
    }
    if (!hasRequest()) {
      memoizedIsInitialized = 0;
      return false;
    }
    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (((bitField0_ & 0x00000001) == 0x00000001)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 2, method_);
    }
    if (((bitField0_ & 0x00000002) == 0x00000002)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 3, correlationId_);
    }
    if (((bitField0_ & 0x00000004) == 0x00000004)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 4, replyTo_);
    }
    if (((bitField0_ & 0x00000008) == 0x00000008)) {
      output.writeInt32(5, version_);
    }
    if (((bitField0_ & 0x00000010) == 0x00000010)) {
      output.writeBytes(6, request_);
    }
    unknownFields.writeTo(output);
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (((bitField0_ & 0x00000001) == 0x00000001)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, method_);
    }
    if (((bitField0_ & 0x00000002) == 0x00000002)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(3, correlationId_);
    }
    if (((bitField0_ & 0x00000004) == 0x00000004)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(4, replyTo_);
    }
    if (((bitField0_ & 0x00000008) == 0x00000008)) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(5, version_);
    }
    if (((bitField0_ & 0x00000010) == 0x00000010)) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(6, request_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.unalarabe.urpc.jms.router.proto2.RpcRequest)) {
      return super.equals(obj);
    }
    com.unalarabe.urpc.jms.router.proto2.RpcRequest other = (com.unalarabe.urpc.jms.router.proto2.RpcRequest) obj;

    boolean result = true;
    result = result && (hasMethod() == other.hasMethod());
    if (hasMethod()) {
      result = result && getMethod()
          .equals(other.getMethod());
    }
    result = result && (hasCorrelationId() == other.hasCorrelationId());
    if (hasCorrelationId()) {
      result = result && getCorrelationId()
          .equals(other.getCorrelationId());
    }
    result = result && (hasReplyTo() == other.hasReplyTo());
    if (hasReplyTo()) {
      result = result && getReplyTo()
          .equals(other.getReplyTo());
    }
    result = result && (hasVersion() == other.hasVersion());
    if (hasVersion()) {
      result = result && (getVersion()
          == other.getVersion());
    }
    result = result && (hasRequest() == other.hasRequest());
    if (hasRequest()) {
      result = result && getRequest()
          .equals(other.getRequest());
    }
    result = result && unknownFields.equals(other.unknownFields);
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasMethod()) {
      hash = (37 * hash) + METHOD_FIELD_NUMBER;
      hash = (53 * hash) + getMethod().hashCode();
    }
    if (hasCorrelationId()) {
      hash = (37 * hash) + CORRELATIONID_FIELD_NUMBER;
      hash = (53 * hash) + getCorrelationId().hashCode();
    }
    if (hasReplyTo()) {
      hash = (37 * hash) + REPLYTO_FIELD_NUMBER;
      hash = (53 * hash) + getReplyTo().hashCode();
    }
    if (hasVersion()) {
      hash = (37 * hash) + VERSION_FIELD_NUMBER;
      hash = (53 * hash) + getVersion();
    }
    if (hasRequest()) {
      hash = (37 * hash) + REQUEST_FIELD_NUMBER;
      hash = (53 * hash) + getRequest().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.unalarabe.urpc.jms.router.proto2.RpcRequest prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code RpcRequest}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:RpcRequest)
      com.unalarabe.urpc.jms.router.proto2.RpcRequestOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcRequest_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcRequest_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.unalarabe.urpc.jms.router.proto2.RpcRequest.class, com.unalarabe.urpc.jms.router.proto2.RpcRequest.Builder.class);
    }

    // Construct using com.unalarabe.urpc.jms.router.proto2.RpcRequest.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      method_ = "";
      bitField0_ = (bitField0_ & ~0x00000001);
      correlationId_ = "";
      bitField0_ = (bitField0_ & ~0x00000002);
      replyTo_ = "";
      bitField0_ = (bitField0_ & ~0x00000004);
      version_ = 0;
      bitField0_ = (bitField0_ & ~0x00000008);
      request_ = com.google.protobuf.ByteString.EMPTY;
      bitField0_ = (bitField0_ & ~0x00000010);
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcRequest_descriptor;
    }

    public com.unalarabe.urpc.jms.router.proto2.RpcRequest getDefaultInstanceForType() {
      return com.unalarabe.urpc.jms.router.proto2.RpcRequest.getDefaultInstance();
    }

    public com.unalarabe.urpc.jms.router.proto2.RpcRequest build() {
      com.unalarabe.urpc.jms.router.proto2.RpcRequest result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.unalarabe.urpc.jms.router.proto2.RpcRequest buildPartial() {
      com.unalarabe.urpc.jms.router.proto2.RpcRequest result = new com.unalarabe.urpc.jms.router.proto2.RpcRequest(this);
      int from_bitField0_ = bitField0_;
      int to_bitField0_ = 0;
      if (((from_bitField0_ & 0x00000001) == 0x00000001)) {
        to_bitField0_ |= 0x00000001;
      }
      result.method_ = method_;
      if (((from_bitField0_ & 0x00000002) == 0x00000002)) {
        to_bitField0_ |= 0x00000002;
      }
      result.correlationId_ = correlationId_;
      if (((from_bitField0_ & 0x00000004) == 0x00000004)) {
        to_bitField0_ |= 0x00000004;
      }
      result.replyTo_ = replyTo_;
      if (((from_bitField0_ & 0x00000008) == 0x00000008)) {
        to_bitField0_ |= 0x00000008;
      }
      result.version_ = version_;
      if (((from_bitField0_ & 0x00000010) == 0x00000010)) {
        to_bitField0_ |= 0x00000010;
      }
      result.request_ = request_;
      result.bitField0_ = to_bitField0_;
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.unalarabe.urpc.jms.router.proto2.RpcRequest) {
        return mergeFrom((com.unalarabe.urpc.jms.router.proto2.RpcRequest)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.unalarabe.urpc.jms.router.proto2.RpcRequest other) {
      if (other == com.unalarabe.urpc.jms.router.proto2.RpcRequest.getDefaultInstance()) return this;
      if (other.hasMethod()) {
        bitField0_ |= 0x00000001;
        method_ = other.method_;
        onChanged();
      }
      if (other.hasCorrelationId()) {
        bitField0_ |= 0x00000002;
        correlationId_ = other.correlationId_;
        onChanged();
      }
      if (other.hasReplyTo()) {
        bitField0_ |= 0x00000004;
        replyTo_ = other.replyTo_;
        onChanged();
      }
      if (other.hasVersion()) {
        setVersion(other.getVersion());
      }
      if (other.hasRequest()) {
        setRequest(other.getRequest());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      if (!hasMethod()) {
        return false;
      }
      if (!hasCorrelationId()) {
        return false;
      }
      if (!hasReplyTo()) {
        return false;
      }
      if (!hasRequest()) {
        return false;
      }
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.unalarabe.urpc.jms.router.proto2.RpcRequest parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.unalarabe.urpc.jms.router.proto2.RpcRequest) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private java.lang.Object method_ = "";
    /**
     * <code>required string Method = 2;</code>
     */
    public boolean hasMethod() {
      return ((bitField0_ & 0x00000001) == 0x00000001);
    }
    /**
     * <code>required string Method = 2;</code>
     */
    public java.lang.String getMethod() {
      java.lang.Object ref = method_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          method_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>required string Method = 2;</code>
     */
    public com.google.protobuf.ByteString
        getMethodBytes() {
      java.lang.Object ref = method_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        method_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>required string Method = 2;</code>
     */
    public Builder setMethod(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000001;
      method_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>required string Method = 2;</code>
     */
    public Builder clearMethod() {
      bitField0_ = (bitField0_ & ~0x00000001);
      method_ = getDefaultInstance().getMethod();
      onChanged();
      return this;
    }
    /**
     * <code>required string Method = 2;</code>
     */
    public Builder setMethodBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000001;
      method_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object correlationId_ = "";
    /**
     * <code>required string CorrelationId = 3;</code>
     */
    public boolean hasCorrelationId() {
      return ((bitField0_ & 0x00000002) == 0x00000002);
    }
    /**
     * <code>required string CorrelationId = 3;</code>
     */
    public java.lang.String getCorrelationId() {
      java.lang.Object ref = correlationId_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          correlationId_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>required string CorrelationId = 3;</code>
     */
    public com.google.protobuf.ByteString
        getCorrelationIdBytes() {
      java.lang.Object ref = correlationId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        correlationId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>required string CorrelationId = 3;</code>
     */
    public Builder setCorrelationId(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000002;
      correlationId_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>required string CorrelationId = 3;</code>
     */
    public Builder clearCorrelationId() {
      bitField0_ = (bitField0_ & ~0x00000002);
      correlationId_ = getDefaultInstance().getCorrelationId();
      onChanged();
      return this;
    }
    /**
     * <code>required string CorrelationId = 3;</code>
     */
    public Builder setCorrelationIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000002;
      correlationId_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object replyTo_ = "";
    /**
     * <code>required string ReplyTo = 4;</code>
     */
    public boolean hasReplyTo() {
      return ((bitField0_ & 0x00000004) == 0x00000004);
    }
    /**
     * <code>required string ReplyTo = 4;</code>
     */
    public java.lang.String getReplyTo() {
      java.lang.Object ref = replyTo_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          replyTo_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>required string ReplyTo = 4;</code>
     */
    public com.google.protobuf.ByteString
        getReplyToBytes() {
      java.lang.Object ref = replyTo_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        replyTo_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>required string ReplyTo = 4;</code>
     */
    public Builder setReplyTo(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000004;
      replyTo_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>required string ReplyTo = 4;</code>
     */
    public Builder clearReplyTo() {
      bitField0_ = (bitField0_ & ~0x00000004);
      replyTo_ = getDefaultInstance().getReplyTo();
      onChanged();
      return this;
    }
    /**
     * <code>required string ReplyTo = 4;</code>
     */
    public Builder setReplyToBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000004;
      replyTo_ = value;
      onChanged();
      return this;
    }

    private int version_ ;
    /**
     * <code>optional int32 Version = 5;</code>
     */
    public boolean hasVersion() {
      return ((bitField0_ & 0x00000008) == 0x00000008);
    }
    /**
     * <code>optional int32 Version = 5;</code>
     */
    public int getVersion() {
      return version_;
    }
    /**
     * <code>optional int32 Version = 5;</code>
     */
    public Builder setVersion(int value) {
      bitField0_ |= 0x00000008;
      version_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional int32 Version = 5;</code>
     */
    public Builder clearVersion() {
      bitField0_ = (bitField0_ & ~0x00000008);
      version_ = 0;
      onChanged();
      return this;
    }

    private com.google.protobuf.ByteString request_ = com.google.protobuf.ByteString.EMPTY;
    /**
     * <code>required bytes request = 6;</code>
     */
    public boolean hasRequest() {
      return ((bitField0_ & 0x00000010) == 0x00000010);
    }
    /**
     * <code>required bytes request = 6;</code>
     */
    public com.google.protobuf.ByteString getRequest() {
      return request_;
    }
    /**
     * <code>required bytes request = 6;</code>
     */
    public Builder setRequest(com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000010;
      request_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>required bytes request = 6;</code>
     */
    public Builder clearRequest() {
      bitField0_ = (bitField0_ & ~0x00000010);
      request_ = getDefaultInstance().getRequest();
      onChanged();
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:RpcRequest)
  }

  // @@protoc_insertion_point(class_scope:RpcRequest)
  private static final com.unalarabe.urpc.jms.router.proto2.RpcRequest DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.unalarabe.urpc.jms.router.proto2.RpcRequest();
  }

  public static com.unalarabe.urpc.jms.router.proto2.RpcRequest getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  @java.lang.Deprecated public static final com.google.protobuf.Parser<RpcRequest>
      PARSER = new com.google.protobuf.AbstractParser<RpcRequest>() {
    public RpcRequest parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new RpcRequest(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<RpcRequest> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<RpcRequest> getParserForType() {
    return PARSER;
  }

  public com.unalarabe.urpc.jms.router.proto2.RpcRequest getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

