package com.unalarabe.urpc.jms.util;

import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class TcpUtils {

    public static boolean isPortAvailable(int port) {
        return isPortAvailable("localhost", port);
    }

    public static boolean isPortAvailable(String host, int port) {
        try (Socket ignored = new Socket(host, port)) {
            return false;
        } catch (IOException ignored) {
            return true;
        }
    }
}
