package com.unalarabe.urpc.jms;

import com.google.protobuf.ByteString;
import com.unalarabe.urpc.artemis.server.ArtemisMQServerExt;
import com.unalarabe.urpc.artemis.server.MessagePreprocessor;
import com.unalarabe.urpc.jms.router.proto2.RpcRequest;
import com.unalarabe.urpc.jms.router.proto2.RpcResponse;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;
import org.apache.activemq.artemis.api.core.ActiveMQBuffer;
import org.apache.activemq.artemis.api.core.Message;
import org.apache.activemq.artemis.api.core.SimpleString;
import org.apache.activemq.artemis.core.message.impl.CoreMessage;
import org.apache.activemq.artemis.core.postoffice.RoutingStatus;
import org.apache.activemq.artemis.core.transaction.Transaction;
import org.apache.activemq.artemis.reader.MessageUtil;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MessagePreprocessorImpl implements MessagePreprocessor {

    public static final SimpleString METHOD = SimpleString.toSimpleString("Method");
    public static final SimpleString VERSION = SimpleString.toSimpleString("Version");

    private Set<SimpleString> stompQueues = new HashSet<>();

    @Override
    public Message preprocess(Transaction tx, Message originalMsg, boolean direct, boolean noAutoCreateQueue, ArtemisMQServerExt.MyServerSessionImpl session) throws Exception {
        String protocolName = session.getProtocolName();
        if ("MQTT".equals(protocolName)) {
            CoreMessage m = (CoreMessage) originalMsg.copy();
            ActiveMQBuffer bodyBufferCopy = m.getBodyBuffer().copy();
            byte[] bytes = new byte[bodyBufferCopy.readableBytes()];
            bodyBufferCopy.readBytes(bytes);
            RpcRequest rpcRequest = RpcRequest.parseFrom(bytes);
            updateFromRpcRequest(m, rpcRequest);
            return m;
        }
        if ("STOMP".equals(protocolName)) {
            CoreMessage m = (CoreMessage) originalMsg.copy();
            ActiveMQBuffer bodyBufferCopy = m.getBodyBuffer().copy();
            byte[] base64bytes = new byte[bodyBufferCopy.readableBytes()];
            bodyBufferCopy.readBytes(base64bytes);
            byte[] bytes = Base64.getDecoder().decode(base64bytes);
            RpcRequest rpcRequest = RpcRequest.parseFrom(bytes);
            updateFromRpcRequest(m, rpcRequest);
            stompQueues.add(MessageUtil.getJMSReplyTo(m));
            return m;
        }
        if ("CORE".equals(protocolName)) {
            SimpleString address = originalMsg.getAddressSimpleString();
            if (stompQueues.contains(address)) {
                CoreMessage m = (CoreMessage) originalMsg.copy();
                ActiveMQBuffer bodyBufferCopy = m.getBodyBuffer().copy();
                RpcResponse response = RpcResponse.newBuilder()
                    .setCorrelationId(MessageUtil.getJMSCorrelationID(originalMsg))
                    .setResponse(ByteString.copyFrom(bodyBufferCopy.toByteBuffer()))
                    .build();
                byte[] bytes = response.toByteArray();
                byte[] base64bytes = Base64.getEncoder().encode(bytes);
                ActiveMQBuffer bodyBuffer = m.getBodyBuffer();
                bodyBuffer.clear();
                bodyBuffer.writeBytes(base64bytes);
                m.setType(Message.TEXT_TYPE);
                return m;
            }
        }
        return originalMsg;
    }

    protected void updateFromRpcRequest(CoreMessage m, RpcRequest rpcRequest) {
        String correlationId = rpcRequest.getCorrelationId();
        int version = rpcRequest.getVersion();
        String method = rpcRequest.getMethod();
        String replyTo = rpcRequest.getReplyTo();
        byte[] request = rpcRequest.getRequest().toByteArray();
        ActiveMQBuffer bodyBuffer = m.getBodyBuffer();
        bodyBuffer.clear();
        bodyBuffer.writeBytes(request);
        m.setType(Message.BYTES_TYPE);
        MessageUtil.setJMSReplyTo(m, new SimpleString(replyTo));
        MessageUtil.setJMSCorrelationID(m, correlationId);
        m.putStringProperty(METHOD, SimpleString.toSimpleString(method));
        m.putIntProperty(VERSION, version);
    }

    @Override
    public RoutingStatus send(Transaction tx, Message msg, boolean direct, boolean noAutoCreateQueue, ArtemisMQServerExt.MyServerSessionImpl session) throws Exception {
        return null;
    }

}
