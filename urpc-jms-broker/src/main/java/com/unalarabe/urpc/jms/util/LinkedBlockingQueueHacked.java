/*
 * Copyright (C) 2018 Petr Zalyautdinov<ya.betmen@gmail.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.unalarabe.urpc.jms.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class LinkedBlockingQueueHacked extends LinkedBlockingQueue<Runnable> {

    private static final Logger LOG = Logger.getLogger(LinkedBlockingQueueHacked.class.getName());

    private static final long serialVersionUID = 1906792464737835291L;

    private volatile boolean stopped = false;

    @Override
    public Runnable take() throws InterruptedException {
        if (stopped) {
            LOG.log(Level.INFO, "ignore take in taskQueue");
            return null;
        }
        return super.take();
    }

    @Override
    public boolean add(Runnable e) {
        if (stopped) {
            LOG.log(Level.INFO, "ignore add task in taskQueue: {0}", e);
            return true;
        }
        return super.add(e);
    }

    public void stop() {
        stopped = true;
    }

    public boolean isStopped() {
        return stopped;
    }

    public void start() {
        stopped = false;
    }
}
