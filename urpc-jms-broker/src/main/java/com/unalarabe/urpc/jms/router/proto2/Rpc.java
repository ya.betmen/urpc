package com.unalarabe.urpc.jms.router.proto2;

public final class Rpc {
  private Rpc() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_RpcRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_RpcRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_RpcResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_RpcResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\trpc.proto\"f\n\nRpcRequest\022\016\n\006Method\030\002 \002(" +
      "\t\022\025\n\rCorrelationId\030\003 \002(\t\022\017\n\007ReplyTo\030\004 \002(" +
      "\t\022\017\n\007Version\030\005 \001(\005\022\017\n\007request\030\006 \002(\014\"^\n\013R" +
      "pcResponse\022\023\n\013ErrorReason\030\001 \001(\005\022\021\n\tError" +
      "Text\030\002 \001(\t\022\025\n\rCorrelationId\030\003 \001(\t\022\020\n\010res" +
      "ponse\030\004 \001(\014B(\n$com.unalarabe.urpc.jms.ro" +
      "uter.proto2P\001"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_RpcRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_RpcRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_RpcRequest_descriptor,
        new java.lang.String[] { "Method", "CorrelationId", "ReplyTo", "Version", "Request", });
    internal_static_RpcResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_RpcResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_RpcResponse_descriptor,
        new java.lang.String[] { "ErrorReason", "ErrorText", "CorrelationId", "Response", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
