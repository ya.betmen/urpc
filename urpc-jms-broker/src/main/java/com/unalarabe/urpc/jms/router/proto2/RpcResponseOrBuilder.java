package com.unalarabe.urpc.jms.router.proto2;

public interface RpcResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:RpcResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>optional int32 ErrorReason = 1;</code>
   */
  boolean hasErrorReason();
  /**
   * <code>optional int32 ErrorReason = 1;</code>
   */
  int getErrorReason();

  /**
   * <code>optional string ErrorText = 2;</code>
   */
  boolean hasErrorText();
  /**
   * <code>optional string ErrorText = 2;</code>
   */
  java.lang.String getErrorText();
  /**
   * <code>optional string ErrorText = 2;</code>
   */
  com.google.protobuf.ByteString
      getErrorTextBytes();

  /**
   * <code>optional string CorrelationId = 3;</code>
   */
  boolean hasCorrelationId();
  /**
   * <code>optional string CorrelationId = 3;</code>
   */
  java.lang.String getCorrelationId();
  /**
   * <code>optional string CorrelationId = 3;</code>
   */
  com.google.protobuf.ByteString
      getCorrelationIdBytes();

  /**
   * <code>optional bytes response = 4;</code>
   */
  boolean hasResponse();
  /**
   * <code>optional bytes response = 4;</code>
   */
  com.google.protobuf.ByteString getResponse();
}
