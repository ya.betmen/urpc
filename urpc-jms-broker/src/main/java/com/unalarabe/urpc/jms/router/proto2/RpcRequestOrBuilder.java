package com.unalarabe.urpc.jms.router.proto2;

public interface RpcRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:RpcRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>required string Method = 2;</code>
   */
  boolean hasMethod();
  /**
   * <code>required string Method = 2;</code>
   */
  java.lang.String getMethod();
  /**
   * <code>required string Method = 2;</code>
   */
  com.google.protobuf.ByteString
      getMethodBytes();

  /**
   * <code>required string CorrelationId = 3;</code>
   */
  boolean hasCorrelationId();
  /**
   * <code>required string CorrelationId = 3;</code>
   */
  java.lang.String getCorrelationId();
  /**
   * <code>required string CorrelationId = 3;</code>
   */
  com.google.protobuf.ByteString
      getCorrelationIdBytes();

  /**
   * <code>required string ReplyTo = 4;</code>
   */
  boolean hasReplyTo();
  /**
   * <code>required string ReplyTo = 4;</code>
   */
  java.lang.String getReplyTo();
  /**
   * <code>required string ReplyTo = 4;</code>
   */
  com.google.protobuf.ByteString
      getReplyToBytes();

  /**
   * <code>optional int32 Version = 5;</code>
   */
  boolean hasVersion();
  /**
   * <code>optional int32 Version = 5;</code>
   */
  int getVersion();

  /**
   * <code>required bytes request = 6;</code>
   */
  boolean hasRequest();
  /**
   * <code>required bytes request = 6;</code>
   */
  com.google.protobuf.ByteString getRequest();
}
