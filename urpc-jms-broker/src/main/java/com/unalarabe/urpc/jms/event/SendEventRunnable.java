package com.unalarabe.urpc.jms.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class SendEventRunnable implements Runnable {

    private static final Logger LOG = Logger.getLogger(SendEventRunnable.class.getName());

    private Session session;
    private MessageProducer producer;
    private String correlationID;
    private Event event;
    private ObjectMapper mapper;

    public SendEventRunnable(Session session, MessageProducer producer, String correlationID, Event event, ObjectMapper mapper) {
        this.session = session;
        this.producer = producer;
        this.correlationID = correlationID;
        this.event = event;
        this.mapper = mapper;
    }

    @Override
    public void run() {
        try {
            String json = eventToJson(event);
            TextMessage message = session.createTextMessage();
            message.setText(json);
            message.setJMSCorrelationID(correlationID);
            producer.send(message);
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private String eventToJson(Event event) {
        try {
            return mapper.writeValueAsString(event);
        } catch (JsonProcessingException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
