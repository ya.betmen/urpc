package com.unalarabe.urpc.jms.router.proto2;

/**
 * Protobuf type {@code RpcResponse}
 */
public  final class RpcResponse extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:RpcResponse)
    RpcResponseOrBuilder {
  // Use RpcResponse.newBuilder() to construct.
  private RpcResponse(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private RpcResponse() {
    errorReason_ = 0;
    errorText_ = "";
    correlationId_ = "";
    response_ = com.google.protobuf.ByteString.EMPTY;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private RpcResponse(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!parseUnknownField(input, unknownFields,
                                   extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
          case 8: {
            bitField0_ |= 0x00000001;
            errorReason_ = input.readInt32();
            break;
          }
          case 18: {
            com.google.protobuf.ByteString bs = input.readBytes();
            bitField0_ |= 0x00000002;
            errorText_ = bs;
            break;
          }
          case 26: {
            com.google.protobuf.ByteString bs = input.readBytes();
            bitField0_ |= 0x00000004;
            correlationId_ = bs;
            break;
          }
          case 34: {
            bitField0_ |= 0x00000008;
            response_ = input.readBytes();
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcResponse_descriptor;
  }

  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcResponse_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.unalarabe.urpc.jms.router.proto2.RpcResponse.class, com.unalarabe.urpc.jms.router.proto2.RpcResponse.Builder.class);
  }

  private int bitField0_;
  public static final int ERRORREASON_FIELD_NUMBER = 1;
  private int errorReason_;
  /**
   * <code>optional int32 ErrorReason = 1;</code>
   */
  public boolean hasErrorReason() {
    return ((bitField0_ & 0x00000001) == 0x00000001);
  }
  /**
   * <code>optional int32 ErrorReason = 1;</code>
   */
  public int getErrorReason() {
    return errorReason_;
  }

  public static final int ERRORTEXT_FIELD_NUMBER = 2;
  private volatile java.lang.Object errorText_;
  /**
   * <code>optional string ErrorText = 2;</code>
   */
  public boolean hasErrorText() {
    return ((bitField0_ & 0x00000002) == 0x00000002);
  }
  /**
   * <code>optional string ErrorText = 2;</code>
   */
  public java.lang.String getErrorText() {
    java.lang.Object ref = errorText_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (bs.isValidUtf8()) {
        errorText_ = s;
      }
      return s;
    }
  }
  /**
   * <code>optional string ErrorText = 2;</code>
   */
  public com.google.protobuf.ByteString
      getErrorTextBytes() {
    java.lang.Object ref = errorText_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      errorText_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int CORRELATIONID_FIELD_NUMBER = 3;
  private volatile java.lang.Object correlationId_;
  /**
   * <code>optional string CorrelationId = 3;</code>
   */
  public boolean hasCorrelationId() {
    return ((bitField0_ & 0x00000004) == 0x00000004);
  }
  /**
   * <code>optional string CorrelationId = 3;</code>
   */
  public java.lang.String getCorrelationId() {
    java.lang.Object ref = correlationId_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (bs.isValidUtf8()) {
        correlationId_ = s;
      }
      return s;
    }
  }
  /**
   * <code>optional string CorrelationId = 3;</code>
   */
  public com.google.protobuf.ByteString
      getCorrelationIdBytes() {
    java.lang.Object ref = correlationId_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      correlationId_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int RESPONSE_FIELD_NUMBER = 4;
  private com.google.protobuf.ByteString response_;
  /**
   * <code>optional bytes response = 4;</code>
   */
  public boolean hasResponse() {
    return ((bitField0_ & 0x00000008) == 0x00000008);
  }
  /**
   * <code>optional bytes response = 4;</code>
   */
  public com.google.protobuf.ByteString getResponse() {
    return response_;
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (((bitField0_ & 0x00000001) == 0x00000001)) {
      output.writeInt32(1, errorReason_);
    }
    if (((bitField0_ & 0x00000002) == 0x00000002)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 2, errorText_);
    }
    if (((bitField0_ & 0x00000004) == 0x00000004)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 3, correlationId_);
    }
    if (((bitField0_ & 0x00000008) == 0x00000008)) {
      output.writeBytes(4, response_);
    }
    unknownFields.writeTo(output);
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (((bitField0_ & 0x00000001) == 0x00000001)) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(1, errorReason_);
    }
    if (((bitField0_ & 0x00000002) == 0x00000002)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, errorText_);
    }
    if (((bitField0_ & 0x00000004) == 0x00000004)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(3, correlationId_);
    }
    if (((bitField0_ & 0x00000008) == 0x00000008)) {
      size += com.google.protobuf.CodedOutputStream
        .computeBytesSize(4, response_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof com.unalarabe.urpc.jms.router.proto2.RpcResponse)) {
      return super.equals(obj);
    }
    com.unalarabe.urpc.jms.router.proto2.RpcResponse other = (com.unalarabe.urpc.jms.router.proto2.RpcResponse) obj;

    boolean result = true;
    result = result && (hasErrorReason() == other.hasErrorReason());
    if (hasErrorReason()) {
      result = result && (getErrorReason()
          == other.getErrorReason());
    }
    result = result && (hasErrorText() == other.hasErrorText());
    if (hasErrorText()) {
      result = result && getErrorText()
          .equals(other.getErrorText());
    }
    result = result && (hasCorrelationId() == other.hasCorrelationId());
    if (hasCorrelationId()) {
      result = result && getCorrelationId()
          .equals(other.getCorrelationId());
    }
    result = result && (hasResponse() == other.hasResponse());
    if (hasResponse()) {
      result = result && getResponse()
          .equals(other.getResponse());
    }
    result = result && unknownFields.equals(other.unknownFields);
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasErrorReason()) {
      hash = (37 * hash) + ERRORREASON_FIELD_NUMBER;
      hash = (53 * hash) + getErrorReason();
    }
    if (hasErrorText()) {
      hash = (37 * hash) + ERRORTEXT_FIELD_NUMBER;
      hash = (53 * hash) + getErrorText().hashCode();
    }
    if (hasCorrelationId()) {
      hash = (37 * hash) + CORRELATIONID_FIELD_NUMBER;
      hash = (53 * hash) + getCorrelationId().hashCode();
    }
    if (hasResponse()) {
      hash = (37 * hash) + RESPONSE_FIELD_NUMBER;
      hash = (53 * hash) + getResponse().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.unalarabe.urpc.jms.router.proto2.RpcResponse prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code RpcResponse}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:RpcResponse)
      com.unalarabe.urpc.jms.router.proto2.RpcResponseOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcResponse_descriptor;
    }

    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcResponse_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.unalarabe.urpc.jms.router.proto2.RpcResponse.class, com.unalarabe.urpc.jms.router.proto2.RpcResponse.Builder.class);
    }

    // Construct using com.unalarabe.urpc.jms.router.proto2.RpcResponse.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      errorReason_ = 0;
      bitField0_ = (bitField0_ & ~0x00000001);
      errorText_ = "";
      bitField0_ = (bitField0_ & ~0x00000002);
      correlationId_ = "";
      bitField0_ = (bitField0_ & ~0x00000004);
      response_ = com.google.protobuf.ByteString.EMPTY;
      bitField0_ = (bitField0_ & ~0x00000008);
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.unalarabe.urpc.jms.router.proto2.Rpc.internal_static_RpcResponse_descriptor;
    }

    public com.unalarabe.urpc.jms.router.proto2.RpcResponse getDefaultInstanceForType() {
      return com.unalarabe.urpc.jms.router.proto2.RpcResponse.getDefaultInstance();
    }

    public com.unalarabe.urpc.jms.router.proto2.RpcResponse build() {
      com.unalarabe.urpc.jms.router.proto2.RpcResponse result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.unalarabe.urpc.jms.router.proto2.RpcResponse buildPartial() {
      com.unalarabe.urpc.jms.router.proto2.RpcResponse result = new com.unalarabe.urpc.jms.router.proto2.RpcResponse(this);
      int from_bitField0_ = bitField0_;
      int to_bitField0_ = 0;
      if (((from_bitField0_ & 0x00000001) == 0x00000001)) {
        to_bitField0_ |= 0x00000001;
      }
      result.errorReason_ = errorReason_;
      if (((from_bitField0_ & 0x00000002) == 0x00000002)) {
        to_bitField0_ |= 0x00000002;
      }
      result.errorText_ = errorText_;
      if (((from_bitField0_ & 0x00000004) == 0x00000004)) {
        to_bitField0_ |= 0x00000004;
      }
      result.correlationId_ = correlationId_;
      if (((from_bitField0_ & 0x00000008) == 0x00000008)) {
        to_bitField0_ |= 0x00000008;
      }
      result.response_ = response_;
      result.bitField0_ = to_bitField0_;
      onBuilt();
      return result;
    }

    public Builder clone() {
      return (Builder) super.clone();
    }
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.setField(field, value);
    }
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.unalarabe.urpc.jms.router.proto2.RpcResponse) {
        return mergeFrom((com.unalarabe.urpc.jms.router.proto2.RpcResponse)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.unalarabe.urpc.jms.router.proto2.RpcResponse other) {
      if (other == com.unalarabe.urpc.jms.router.proto2.RpcResponse.getDefaultInstance()) return this;
      if (other.hasErrorReason()) {
        setErrorReason(other.getErrorReason());
      }
      if (other.hasErrorText()) {
        bitField0_ |= 0x00000002;
        errorText_ = other.errorText_;
        onChanged();
      }
      if (other.hasCorrelationId()) {
        bitField0_ |= 0x00000004;
        correlationId_ = other.correlationId_;
        onChanged();
      }
      if (other.hasResponse()) {
        setResponse(other.getResponse());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.unalarabe.urpc.jms.router.proto2.RpcResponse parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.unalarabe.urpc.jms.router.proto2.RpcResponse) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private int errorReason_ ;
    /**
     * <code>optional int32 ErrorReason = 1;</code>
     */
    public boolean hasErrorReason() {
      return ((bitField0_ & 0x00000001) == 0x00000001);
    }
    /**
     * <code>optional int32 ErrorReason = 1;</code>
     */
    public int getErrorReason() {
      return errorReason_;
    }
    /**
     * <code>optional int32 ErrorReason = 1;</code>
     */
    public Builder setErrorReason(int value) {
      bitField0_ |= 0x00000001;
      errorReason_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional int32 ErrorReason = 1;</code>
     */
    public Builder clearErrorReason() {
      bitField0_ = (bitField0_ & ~0x00000001);
      errorReason_ = 0;
      onChanged();
      return this;
    }

    private java.lang.Object errorText_ = "";
    /**
     * <code>optional string ErrorText = 2;</code>
     */
    public boolean hasErrorText() {
      return ((bitField0_ & 0x00000002) == 0x00000002);
    }
    /**
     * <code>optional string ErrorText = 2;</code>
     */
    public java.lang.String getErrorText() {
      java.lang.Object ref = errorText_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          errorText_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string ErrorText = 2;</code>
     */
    public com.google.protobuf.ByteString
        getErrorTextBytes() {
      java.lang.Object ref = errorText_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        errorText_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string ErrorText = 2;</code>
     */
    public Builder setErrorText(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000002;
      errorText_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string ErrorText = 2;</code>
     */
    public Builder clearErrorText() {
      bitField0_ = (bitField0_ & ~0x00000002);
      errorText_ = getDefaultInstance().getErrorText();
      onChanged();
      return this;
    }
    /**
     * <code>optional string ErrorText = 2;</code>
     */
    public Builder setErrorTextBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000002;
      errorText_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object correlationId_ = "";
    /**
     * <code>optional string CorrelationId = 3;</code>
     */
    public boolean hasCorrelationId() {
      return ((bitField0_ & 0x00000004) == 0x00000004);
    }
    /**
     * <code>optional string CorrelationId = 3;</code>
     */
    public java.lang.String getCorrelationId() {
      java.lang.Object ref = correlationId_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (bs.isValidUtf8()) {
          correlationId_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string CorrelationId = 3;</code>
     */
    public com.google.protobuf.ByteString
        getCorrelationIdBytes() {
      java.lang.Object ref = correlationId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        correlationId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string CorrelationId = 3;</code>
     */
    public Builder setCorrelationId(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000004;
      correlationId_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string CorrelationId = 3;</code>
     */
    public Builder clearCorrelationId() {
      bitField0_ = (bitField0_ & ~0x00000004);
      correlationId_ = getDefaultInstance().getCorrelationId();
      onChanged();
      return this;
    }
    /**
     * <code>optional string CorrelationId = 3;</code>
     */
    public Builder setCorrelationIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000004;
      correlationId_ = value;
      onChanged();
      return this;
    }

    private com.google.protobuf.ByteString response_ = com.google.protobuf.ByteString.EMPTY;
    /**
     * <code>optional bytes response = 4;</code>
     */
    public boolean hasResponse() {
      return ((bitField0_ & 0x00000008) == 0x00000008);
    }
    /**
     * <code>optional bytes response = 4;</code>
     */
    public com.google.protobuf.ByteString getResponse() {
      return response_;
    }
    /**
     * <code>optional bytes response = 4;</code>
     */
    public Builder setResponse(com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000008;
      response_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional bytes response = 4;</code>
     */
    public Builder clearResponse() {
      bitField0_ = (bitField0_ & ~0x00000008);
      response_ = getDefaultInstance().getResponse();
      onChanged();
      return this;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:RpcResponse)
  }

  // @@protoc_insertion_point(class_scope:RpcResponse)
  private static final com.unalarabe.urpc.jms.router.proto2.RpcResponse DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.unalarabe.urpc.jms.router.proto2.RpcResponse();
  }

  public static com.unalarabe.urpc.jms.router.proto2.RpcResponse getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  @java.lang.Deprecated public static final com.google.protobuf.Parser<RpcResponse>
      PARSER = new com.google.protobuf.AbstractParser<RpcResponse>() {
    public RpcResponse parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new RpcResponse(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<RpcResponse> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<RpcResponse> getParserForType() {
    return PARSER;
  }

  public com.unalarabe.urpc.jms.router.proto2.RpcResponse getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

