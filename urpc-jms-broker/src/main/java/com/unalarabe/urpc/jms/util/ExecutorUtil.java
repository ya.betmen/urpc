package com.unalarabe.urpc.jms.util;

import io.netty.util.concurrent.EventExecutorGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ExecutorUtil {

    private static final Logger LOG = Logger.getLogger(ExecutorUtil.class.getName());

    public static GlobalEventExecutor createGlobalEventExecutor() {
        try {
            Constructor<?>[] constructors = GlobalEventExecutor.class.getDeclaredConstructors();
            Constructor constructor = constructors[0];
            constructor.setAccessible(true);
            return (GlobalEventExecutor) constructor.newInstance();
        } catch (SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return null;
    }

    public static void shutdown(EventExecutorGroup executor, long time, TimeUnit unit) throws InterruptedException {
        if (!executor.isShutdown()) {
            executor.shutdownGracefully().await(time, unit);
        }
        if (!executor.isShutdown()) {
            executor.terminationFuture().await();
        }
    }

    public static void shutdown(ExecutorService executor, long time, TimeUnit unit) {
        executor.shutdown();
        try {
            boolean terminated = executor.awaitTermination(time, unit);
            if (!terminated) {
                executor.shutdownNow();
            }
        } catch (InterruptedException ex) {
            LOG.log(Level.SEVERE, null, ex);
            try {
                Thread.sleep(unit.toMillis(time));
            } catch (InterruptedException ex1) {
                LOG.log(Level.SEVERE, null, ex1);
            }
            if (!executor.isShutdown()) {
                executor.shutdownNow();
            }
        }
    }
}
