package com.unalarabe.urpc.jms.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class FireEventRunnable implements Runnable {

    private static final Logger LOG = Logger.getLogger(FireEventRunnable.class.getName());

    private final Event event;
    private final Set<Object> eventListeners;

    public FireEventRunnable(Event event, Set<Object> eventListeners) {
        this.event = event;
        this.eventListeners = eventListeners;
    }

    @Override
    public void run() {
        if (eventListeners != null) {
            for (Object eventListener : eventListeners) {
                callListener(event, eventListener);
            }
        }
    }

    protected <T extends Event> void callListener(T event, Object listener) {
        try {//TODO: add cache of methods
            Method method = listener.getClass().getDeclaredMethod("onEvent", event.getClass());
            method.setAccessible(true);
            method.invoke(listener, event);
        } catch (NoSuchMethodException | SecurityException ex) {
            LOG.log(Level.FINEST, ex.getMessage(), ex);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOG.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

}
