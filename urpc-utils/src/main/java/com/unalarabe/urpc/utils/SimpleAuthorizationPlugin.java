package com.unalarabe.urpc.utils;

import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerPlugin;
import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.broker.ProducerBrokerExchange;
import org.apache.activemq.broker.region.Destination;
import org.apache.activemq.broker.region.Subscription;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ConnectionInfo;
import org.apache.activemq.command.ConsumerInfo;
import org.apache.activemq.command.DestinationInfo;
import org.apache.activemq.command.Message;
import org.apache.activemq.command.ProducerInfo;
import org.apache.activemq.command.SessionInfo;
import org.apache.activemq.security.AbstractAuthenticationBroker;
import org.apache.activemq.security.SecurityContext;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class SimpleAuthorizationPlugin implements BrokerPlugin {

    private static final Logger LOG = Logger.getLogger(SimpleAuthorizationPlugin.class.getName());
    private final Map<String, String> userPasswords = new HashMap<>();

    @Override
    public Broker installPlugin(Broker broker) throws Exception {
        return new SimpleSecurityBroker(broker);
    }

    public void addUser(String userName, String password) {
        userPasswords.put(userName, password);
    }

    public class SimpleSecurityBroker extends AbstractAuthenticationBroker {

        public SimpleSecurityBroker(Broker next) {
            super(next);
        }

        @Override
        public void addDestinationInfo(ConnectionContext context, DestinationInfo info) throws Exception {
            super.addDestinationInfo(context, info);
        }

        @Override
        public void addConnection(ConnectionContext context, ConnectionInfo info) throws Exception {
            String userName = info.getUserName();
            if ("ActiveMQBroker".equals(userName)) {
                throw new SecurityException("invalid user");
            }
            String password = info.getPassword();
            SecurityContext securityContext = authenticate(userName, password, null);
            context.setSecurityContext(securityContext);
            super.addConnection(context, info);
        }

        @Override
        public Subscription addConsumer(ConnectionContext context, ConsumerInfo info) throws Exception {
            return super.addConsumer(context, info);
        }

        @Override
        public Destination addDestination(ConnectionContext context, ActiveMQDestination destination, boolean createIfTemporary) throws Exception {
            return super.addDestination(context, destination, createIfTemporary);
        }

        @Override
        public void addProducer(ConnectionContext context, ProducerInfo info) throws Exception {
            super.addProducer(context, info);
        }

        @Override
        public void addSession(ConnectionContext context, SessionInfo info) throws Exception {
            super.addSession(context, info);
        }

        @Override
        public void send(ProducerBrokerExchange producerExchange, Message messageSend) throws Exception {
            super.send(producerExchange, messageSend);
        }

        @Override
        public void removeConnection(ConnectionContext context, ConnectionInfo info, Throwable error) throws Exception {
            super.removeConnection(context, info, error);
        }

        @Override
        public void removeDestination(ConnectionContext context, ActiveMQDestination destination, long timeout) throws Exception {
            super.removeDestination(context, destination, timeout);
        }

        @Override
        public void removeConsumer(ConnectionContext context, ConsumerInfo info) throws Exception {
            super.removeConsumer(context, info);
        }

        @Override
        public SecurityContext authenticate(String username, String password, X509Certificate[] peerCertificates) throws SecurityException {
            String pw = userPasswords.get(username);
            if (password != null && password.equals(pw)) {
                return new SecurityContext(username) {
                    @Override
                    public Set<Principal> getPrincipals() {
                        return Collections.EMPTY_SET;
                    }
                };
            }
            throw new SecurityException("Invalid credentials");
        }
    }
}
