package com.unalarabe.urpc.utils;

import java.util.Base64;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerFilter;
import org.apache.activemq.broker.BrokerPlugin;
import org.apache.activemq.broker.ConnectionContext;
import org.apache.activemq.broker.ProducerBrokerExchange;
import org.apache.activemq.broker.region.Destination;
import org.apache.activemq.command.ActiveMQBytesMessage;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ConnectionId;
import org.apache.activemq.command.ConnectionInfo;
import org.apache.activemq.command.Message;
import org.apache.activemq.util.ByteSequence;
import org.eclipse.jetty.util.ConcurrentHashSet;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class MessageStompPlugin implements BrokerPlugin {

    public MessageStompPlugin(Map map) {
    }

    public MessageStompPlugin() {
    }

    @Override
    public Broker installPlugin(Broker broker) throws Exception {
        return new MessageStompFilter(broker);
    }

    public static class MessageStompFilter extends BrokerFilter {

        private static final Logger LOG = Logger.getLogger(MessageStompFilter.class.getName());

        private Set<String> stompEnpoints = new ConcurrentHashSet<>();
        private Set<String> stompConnections = new ConcurrentHashSet<>();

        public MessageStompFilter(Broker next) {
            super(next);
        }

        @Override
        public void addConnection(ConnectionContext context, ConnectionInfo info) throws Exception {
            if (info.getClientIp().startsWith("ws://") || info.getClientIp().startsWith("wss://")) {
                String connectionId = info.getConnectionId().toString();
                System.out.println("STOMP: addConnection = " + connectionId);
                stompConnections.add(connectionId);
            }
            super.addConnection(context, info);
        }

        @Override
        public Destination addDestination(ConnectionContext context, ActiveMQDestination destination, boolean createIfTemporary) throws Exception {
            Destination result = super.addDestination(context, destination, createIfTemporary);
            String connectionId = context.getConnectionId().toString();
            if (stompConnections.contains(connectionId)) {
                System.out.println("STOMP: addDestination = " + destination.getQualifiedName() + " connection = " + connectionId);
                stompEnpoints.add(destination.getQualifiedName());
            }
            return result;
        }

        @Override
        public void removeDestination(ConnectionContext context, ActiveMQDestination destination, long timeout) throws Exception {
            super.removeDestination(context, destination, timeout);
        }

        @Override
        public void send(ProducerBrokerExchange producerExchange, Message messageSend) throws Exception {
            LOG.log(Level.INFO, "{0}", messageSend);
            try {
                boolean readOnlyBody = messageSend.isReadOnlyBody();
                boolean readOnlyProperties = messageSend.isReadOnlyProperties();
                if (messageSend instanceof ActiveMQBytesMessage) {
                    Object base64obj = messageSend.getProperty("base64");
                    if (base64obj != null) {
                        ActiveMQBytesMessage abm = (ActiveMQBytesMessage) messageSend;//.copy();
                        byte[] decode = Base64.getDecoder().decode(String.valueOf(base64obj));
                        abm.setReadOnlyBody(false);
                        abm.setReadOnlyProperties(false);
//                        abm.clearBody();
                        abm.setContent(new ByteSequence(decode));
//                        abm.writeBytes(decode);
                        abm.removeProperty("base64");
//                        abm.setStringProperty("test", "!!!");
                        abm.setReadOnlyProperties(true);
                        abm.setReadOnlyBody(true);
//                        long bodyLength = abm.getBodyLength();
//                        LOG.log(Level.INFO, "decode.length={1}, bodyLength={0}", new Object[]{bodyLength, decode.length});
                        stompEnpoints.add(abm.getReplyTo().getQualifiedName());
//                        abm.storeContent();
                        super.send(producerExchange, abm);
                        return;
                    } else if (stompEnpoints.contains(messageSend.getDestination().getQualifiedName())) {
                        ActiveMQBytesMessage abm = (ActiveMQBytesMessage) messageSend;//.copy();
                        abm.setReadOnlyBody(true);
                        byte[] data = new byte[(int) abm.getBodyLength()];
                        abm.readBytes(data);
                        abm.setReadOnlyBody(false);
                        abm.clearBody();
//                        abm.setReadOnlyProperties(false);
                        abm.setProperty("base64", Base64.getEncoder().encodeToString(data));
//                        abm.setReadOnlyProperties(readOnlyProperties);
                        super.send(producerExchange, abm);
                        return;
                    }
                }
                super.send(producerExchange, messageSend);
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }
}
