package com.unalarabe.urpc.artemis.server.security;

import java.util.Set;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
interface RoleUsersChangeListener {

    void onUserAdded(String role, String username);

    void onUserRemoved(String role, String username);
}
