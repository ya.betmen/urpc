package com.unalarabe.urpc.artemis.server.security;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class User {

    private final String username;
    private final String password;
    private final Map<String, Role> roles = new HashMap<>();
    private final UserRolesChangeListener listener;

    User(String username, String password, UserRolesChangeListener listener) {
        this.username = username;
        this.password = password;
        this.listener = listener;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Set<String> getRoleNames() {
        return roles.keySet();
    }

    public void addRole(Role role) {
        String roleName = role.getRoleName();
        roles.put(roleName, role);
        listener.onRoleAdded(username, roleName);
    }

    public void removeRole(String rolename) {
        roles.remove(rolename);
        listener.onRoleRemoved(username, rolename);
    }

    public boolean hasRole(String role) {
        return roles.containsKey(role);
    }

    public boolean hasPermission(String permission) {
        for (Role role : roles.values()) {
            Set<String> permissions = role.getPermissions();
            if (permissions.contains(permission)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.username);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        return true;
    }

}
