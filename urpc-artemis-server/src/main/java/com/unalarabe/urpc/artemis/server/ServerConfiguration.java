package com.unalarabe.urpc.artemis.server;

import com.unalarabe.urpc.artemis.server.security.SecurityManager;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ServerConfiguration {

    private final List<ConnectorConfiguration> connectors = new ArrayList<>();
    private SecurityManager securityManager;
    private File brokerDir;
    private boolean cleanDirOnStart;

    public SecurityManager getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManager securityManager) {
        this.securityManager = securityManager;
    }

    public List<ConnectorConfiguration> getConnectors() {
        return connectors;
    }

    public File getBrokerDir() {
        return brokerDir;
    }

    public void setBrokerDir(File brokerDir) {
        if (brokerDir.exists() && !brokerDir.isDirectory()) {
            throw new IllegalStateException(brokerDir.getAbsolutePath() + " is not directory");
        }
        this.brokerDir = brokerDir;
    }

    public boolean isCleanDirOnStart() {
        return cleanDirOnStart;
    }

    public void setCleanDirOnStart(boolean cleanDirOnStart) {
        this.cleanDirOnStart = cleanDirOnStart;
    }

}
