package com.unalarabe.urpc.artemis.server;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public enum Protocol {
    CORE,
    MQTT,
    STOMP
}
