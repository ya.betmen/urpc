package com.unalarabe.urpc.artemis.server;

import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import javax.management.MBeanServer;
import org.apache.activemq.artemis.api.core.Message;
import org.apache.activemq.artemis.api.core.RoutingType;
import org.apache.activemq.artemis.api.core.SimpleString;
import org.apache.activemq.artemis.core.config.Configuration;
import org.apache.activemq.artemis.core.paging.PagingManager;
import org.apache.activemq.artemis.core.persistence.OperationContext;
import org.apache.activemq.artemis.core.persistence.StorageManager;
import org.apache.activemq.artemis.core.postoffice.PostOffice;
import org.apache.activemq.artemis.core.postoffice.RoutingStatus;
import org.apache.activemq.artemis.core.security.SecurityStore;
import org.apache.activemq.artemis.core.server.ActiveMQServer;
import org.apache.activemq.artemis.core.server.ServiceRegistry;
import org.apache.activemq.artemis.core.server.impl.ActiveMQServerImpl;
import org.apache.activemq.artemis.core.server.impl.ServerSessionImpl;
import org.apache.activemq.artemis.core.server.management.ManagementService;
import org.apache.activemq.artemis.core.transaction.ResourceManager;
import org.apache.activemq.artemis.core.transaction.Transaction;
import org.apache.activemq.artemis.spi.core.protocol.RemotingConnection;
import org.apache.activemq.artemis.spi.core.protocol.SessionCallback;
import org.apache.activemq.artemis.spi.core.security.ActiveMQSecurityManager;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ArtemisMQServerExt extends ActiveMQServerImpl {

    private MessagePreprocessor messagePreprocessor;

    public MessagePreprocessor getMessagePreprocessor() {
        return messagePreprocessor;
    }

    public void setMessagePreprocessor(MessagePreprocessor messagePreprocessor) {
        getBrokerPlugins().remove(this.messagePreprocessor);
        this.messagePreprocessor = messagePreprocessor;
        if (this.messagePreprocessor != null) {
            getBrokerPlugins().add(messagePreprocessor);
        }
    }
    
    public ArtemisMQServerExt() {
    }

    public ArtemisMQServerExt(Configuration configuration) {
        super(configuration);
    }

    public ArtemisMQServerExt(Configuration configuration, ActiveMQServer parentServer) {
        super(configuration, parentServer);
    }

    public ArtemisMQServerExt(Configuration configuration, MBeanServer mbeanServer) {
        super(configuration, mbeanServer);
    }

    public ArtemisMQServerExt(Configuration configuration, ActiveMQSecurityManager securityManager) {
        super(configuration, securityManager);
    }

    public ArtemisMQServerExt(Configuration configuration, MBeanServer mbeanServer, ActiveMQSecurityManager securityManager) {
        super(configuration, mbeanServer, securityManager);
    }

    public ArtemisMQServerExt(Configuration configuration, MBeanServer mbeanServer, ActiveMQSecurityManager securityManager, ActiveMQServer parentServer) {
        super(configuration, mbeanServer, securityManager, parentServer);
    }

    public ArtemisMQServerExt(Configuration configuration, MBeanServer mbeanServer, ActiveMQSecurityManager securityManager, ActiveMQServer parentServer, ServiceRegistry serviceRegistry) {
        super(configuration, mbeanServer, securityManager, parentServer, serviceRegistry);
    }

    @Override
    protected ServerSessionImpl internalCreateSession(String name, String username, String password, String validatedUser, int minLargeMessageSize, RemotingConnection connection, boolean autoCommitSends, boolean autoCommitAcks, boolean preAcknowledge, boolean xa, String defaultAddress, SessionCallback callback, OperationContext context, boolean autoCreateJMSQueues, Map<SimpleString, RoutingType> prefixes) throws Exception {
        Configuration configuration = getConfiguration();
        return new MyServerSessionImpl(name, username, password, validatedUser, minLargeMessageSize, autoCommitSends, autoCommitAcks, preAcknowledge, configuration.isPersistDeliveryCountBeforeDelivery(), xa, connection, getStorageManager(), getPostOffice(), getResourceManager(), getSecurityStore(), getManagementService(), this, configuration.getManagementAddress(), defaultAddress == null ? null : new SimpleString(defaultAddress), callback, context, getPagingManager(), prefixes);
    }

    public class MyServerSessionImpl extends ServerSessionImpl {

        private final ReentrantLock lock = new ReentrantLock();

        public MyServerSessionImpl(String name, String username, String password, String validatedUser, int minLargeMessageSize, boolean autoCommitSends, boolean autoCommitAcks, boolean preAcknowledge, boolean strictUpdateDeliveryCount, boolean xa, RemotingConnection remotingConnection, StorageManager storageManager, PostOffice postOffice, ResourceManager resourceManager, SecurityStore securityStore, ManagementService managementService, ActiveMQServer server, SimpleString managementAddress, SimpleString defaultAddress, SessionCallback callback, OperationContext context, PagingManager pagingManager, Map<SimpleString, RoutingType> prefixes) throws Exception {
            super(name, username, password, validatedUser, minLargeMessageSize, autoCommitSends, autoCommitAcks, preAcknowledge, strictUpdateDeliveryCount, xa, remotingConnection, storageManager, postOffice, resourceManager, securityStore, managementService, server, managementAddress, defaultAddress, callback, context, pagingManager, prefixes);
        }

        @Override
        public RoutingStatus send(Transaction tx, org.apache.activemq.artemis.api.core.Message msg, boolean direct, boolean noAutoCreateQueue) throws Exception {
            try {
                lock.lock();
                RoutingStatus result = null;
                if (messagePreprocessor != null) {
                    Message newMsg = messagePreprocessor.preprocess(tx, msg, direct, noAutoCreateQueue, this);
                    if (newMsg != null) {
                        msg = newMsg;
                    }
                    result = messagePreprocessor.send(tx, msg, direct, noAutoCreateQueue, this);
                }
                if (result == null) {
                    result = super.send(tx, msg, direct, noAutoCreateQueue);
                }
                return result;
            } finally {
                lock.unlock();
            }
        }

        public String getProtocolName() {
            return super.getRemotingConnection().getProtocolName();
        }

    }
}
