package com.unalarabe.urpc.artemis.server.security;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class Role {

    private final String roleName;
    private final Set<String> permissions = new CopyOnWriteArraySet<>();
    private final Map<String, User> users = new HashMap<>();
    private final RoleUsersChangeListener listener;

    Role(String roleName, RoleUsersChangeListener listener) {
        this.roleName = roleName;
        this.listener = listener;
    }

    public String getRoleName() {
        return roleName;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public Set<String> getUserNames() {
        return users.keySet();
    }

    public User getUser(String username) {
        return users.get(username);
    }

    public void addUser(User user) {
        String username = user.getUsername();
        users.put(username, user);
        listener.onUserAdded(roleName, username);
    }

    public boolean containsUser(String username) {
        return users.containsKey(username);
    }

    public void removeUser(String username) {
        users.remove(username);
        listener.onUserRemoved(roleName, username);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.roleName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Role other = (Role) obj;
        if (!Objects.equals(this.roleName, other.roleName)) {
            return false;
        }
        return true;
    }

}
