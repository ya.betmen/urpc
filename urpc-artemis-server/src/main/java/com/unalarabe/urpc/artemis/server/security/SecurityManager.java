package com.unalarabe.urpc.artemis.server.security;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.activemq.artemis.core.config.impl.SecurityConfiguration;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class SecurityManager implements RoleUsersChangeListener, UserRolesChangeListener {

    private final SecurityConfiguration securityConfiguration = new SecurityConfiguration();

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Map<String, User> users = new HashMap<>();
    private final Map<String, Role> roles = new HashMap<>();

    public Set<String> getUserNames() {
        return users.keySet();
    }

    public User createUser(String useraname, String password) {
        User user = users.get(useraname);
        if (user == null) {
            user = new User(useraname, password, this);
            users.put(user.getUsername(), user);
        }
        if (user.getPassword() == null ? password != null : !user.getPassword().equals(password)) {
            throw new IllegalArgumentException("User exists with different password");
        }
        return user;
    }

    public Role createRole(String roleName) {
        Role role = roles.get(roleName);
        if (role == null) {
            role = new Role(roleName, this);
            roles.put(role.getRoleName(), role);
        }
        return role;
    }

    public boolean addUser(User user) {
        if (user != null) {
            users.put(user.getUsername(), user);
            securityConfiguration.addUser(user.getUsername(), user.getPassword());
            return true;
        }
        return false;
    }

    public void removeUser(String user) {
        User removedUser = users.remove(user);
        if (removedUser != null) {
            securityConfiguration.removeUser(user);
        }
    }

    public boolean addRole(Role role) {
        if (role != null) {
            roles.put(role.getRoleName(), role);
            return true;
        }
        return false;
    }

    public void removeRole(String user) {
        User removedUser = users.remove(user);
        if (removedUser != null) {
            securityConfiguration.removeUser(user);
        }
    }

    public SecurityConfiguration getSecurityConfiguration() {
        return securityConfiguration;
    }

    @Override
    public void onUserAdded(String roleName, String username) {
        Role role = roles.get(roleName);
        User user = users.get(username);
        if (!user.hasRole(roleName)) {
            user.addRole(role);
            securityConfiguration.addRole(username, roleName);
        }
    }

    @Override
    public void onUserRemoved(String roleName, String username) {
        User user = users.get(username);
        if (user.hasRole(roleName)) {
            user.removeRole(roleName);
            securityConfiguration.removeRole(username, roleName);
        }
    }

    @Override
    public void onRoleAdded(String username, String roleName) {
        Role role = roles.get(roleName);
        User user = users.get(username);
        if (!role.containsUser(username)) {
            role.addUser(user);
            securityConfiguration.addRole(username, roleName);
        }
    }

    @Override
    public void onRoleRemoved(String username, String roleName) {
        Role role = roles.get(roleName);
        if (role.containsUser(username)) {
            role.removeUser(username);
            securityConfiguration.removeRole(username, roleName);
        }
    }

}
