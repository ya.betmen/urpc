package com.unalarabe.urpc.artemis.server.security;

import java.util.Set;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
interface UserRolesChangeListener {

    void onRoleAdded(String user, String role);

    void onRoleRemoved(String user, String role);
}
