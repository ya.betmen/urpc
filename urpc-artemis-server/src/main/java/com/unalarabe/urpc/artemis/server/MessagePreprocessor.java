package com.unalarabe.urpc.artemis.server;

import org.apache.activemq.artemis.api.core.Message;
import org.apache.activemq.artemis.core.postoffice.RoutingStatus;
import org.apache.activemq.artemis.core.server.ServerSession;
import org.apache.activemq.artemis.core.server.plugin.ActiveMQServerPlugin;
import org.apache.activemq.artemis.core.transaction.Transaction;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public interface MessagePreprocessor extends ActiveMQServerPlugin {

    default public RoutingStatus send(Transaction tx, Message msg, boolean direct, boolean noAutoCreateQueue, ServerSession session) throws Exception {
        return send(tx, msg, direct, noAutoCreateQueue, (ArtemisMQServerExt.MyServerSessionImpl) session);
    }

    public Message preprocess(Transaction tx, Message msg, boolean direct, boolean noAutoCreateQueue, ArtemisMQServerExt.MyServerSessionImpl session) throws Exception;
    
    public RoutingStatus send(Transaction tx, Message msg, boolean direct, boolean noAutoCreateQueue, ArtemisMQServerExt.MyServerSessionImpl session) throws Exception;

}
