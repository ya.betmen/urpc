package com.unalarabe.urpc.artemis.server;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.core.remoting.impl.netty.TransportConstants;
import org.apache.activemq.artemis.spi.core.remoting.AcceptorFactory;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ConnectorConfiguration {

    private String transportConfigurationName;
    private Class<? extends AcceptorFactory> acceptorFactoryClass;
    private Protocol protocol;
    private InetSocketAddress address;

    public ConnectorConfiguration(String transportConfigurationName, Class<? extends AcceptorFactory> acceptorFactoryClass, InetSocketAddress address, Protocol protocol) {
        this.transportConfigurationName = transportConfigurationName;
        this.acceptorFactoryClass = acceptorFactoryClass;
        this.protocol = protocol;
        this.address = address;
    }

    public String getTransportConfigurationName() {
        return transportConfigurationName;
    }

    public void setTransportConfigurationName(String transportConfigurationName) {
        this.transportConfigurationName = transportConfigurationName;
    }

    public Class<? extends AcceptorFactory> getAcceptorFactoryClass() {
        return acceptorFactoryClass;
    }

    public void setAcceptorFactoryClass(Class<? extends AcceptorFactory> acceptorFactoryClass) {
        this.acceptorFactoryClass = acceptorFactoryClass;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public void setAddress(InetSocketAddress address) {
        this.address = address;
    }

    public TransportConfiguration createTransportConfiguration() {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(TransportConstants.HOST_PROP_NAME, address.getHostName());
        parameters.put(TransportConstants.PORT_PROP_NAME, address.getPort());
        parameters.put(TransportConstants.PROTOCOLS_PROP_NAME, protocol.name());
        TransportConfiguration activeMTransport = new TransportConfiguration(acceptorFactoryClass.getCanonicalName(), parameters, transportConfigurationName);
        return activeMTransport;
    }
}
