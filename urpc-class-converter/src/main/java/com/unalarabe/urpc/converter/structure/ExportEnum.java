package com.unalarabe.urpc.converter.structure;

import java.util.TreeSet;

/**
 *
 * @author petr
 */
public class ExportEnum extends ExportClass {

    public TreeSet<String> entries = new TreeSet<>();

    public ExportEnum(String name, boolean rpcError) {
        super(name, rpcError);
    }

    @Override
    public String toString() {
        return "ExportEnum{" + "entries=" + entries + '}';
    }

}
