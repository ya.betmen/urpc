package com.unalarabe.urpc.converter.structure;

import java.util.Set;

/**
 *
 * @author petr
 */
public class ExportField {

    public ExportClass type;
    public String name;
    public boolean required;
    public Set<String> ignoredGenerators;

    @Override
    public String toString() {
        return "ExportField{" + "type=" + type.name + ", name=" + name + ", required=" + required + '}';
    }
    
    public boolean isIgnored(String generator){
        return ignoredGenerators != null && (ignoredGenerators.contains("all") || ignoredGenerators.contains(generator));
    }
    
}
