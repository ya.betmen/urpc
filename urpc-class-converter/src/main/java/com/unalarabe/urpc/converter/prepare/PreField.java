package com.unalarabe.urpc.converter.prepare;

import java.util.Objects;
import java.util.Set;

/**
 *
 * @author petr
 */
public class PreField {

    public PreClass type;
    public String name;
    public boolean required;
    public Set<String> ignoredGenerators;

    @Override
    public String toString() {
        return "\nPreField{" + "\n\ttype=" + (type != null ? type.getFullName() : null) + ",\n\tname=" + name + ",\n\trequired=" + required + "\n}";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.type);
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + (this.required ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PreField other = (PreField) obj;
        if (this.required != other.required) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }

}
