package com.unalarabe.urpc.converter.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author petr
 */
public class ExportService {

    public final String name;
    private final Map<String, List<ExportMethod>> methodsMap = new HashMap<>();
    private final Set<ExportClass> extraClasses = new HashSet<>();

    public ExportService(String name) {
        this.name = name;
    }

    public Set<String> getMethodNames() {
        return methodsMap.keySet();
    }

    public List<ExportMethod> getMethods(String name) {
        return methodsMap.get(name);
    }

    public void addMethod(String name, ExportMethod method) {
        if (!methodsMap.containsKey(name)) {
            methodsMap.put(name, new ArrayList<>());
        }
        methodsMap.get(name).add(method);
    }

    public Set<ExportClass> getExtraClasses() {
        return extraClasses;
    }
    
    @Override
    public String toString() {
        return "ExportService{" + "\n\tname=" + name + "\n\tmethodsMap=" + methodsMap + "\n}";
    }

}
