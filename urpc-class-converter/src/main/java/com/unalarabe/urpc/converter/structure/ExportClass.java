package com.unalarabe.urpc.converter.structure;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author petr
 */
public class ExportClass {

    public final String name;
    public final Set<String> imports = new HashSet<>();
    public final List<ExportField> fields = new ArrayList<>();
    public final boolean rpcRequest;
    public final boolean rpcResponse;
    public final boolean rpcError;

    public ExportClass(String name) {
        this(name, false, false);
    }

    public ExportClass(String name, boolean rpcError) {
        this(name, false, false, rpcError);
    }

    public ExportClass(String name, boolean rpcRequest, boolean rpcResponse) {
        this(name, rpcRequest, rpcResponse, false);
    }

    public ExportClass(String name, boolean rpcRequest, boolean rpcResponse, boolean rpcError) {
        this.name = name;
        this.rpcRequest = rpcRequest;
        this.rpcResponse = rpcResponse;
        this.rpcError = rpcError;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" + "name=" + name + ", fields=" + fields + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExportClass other = (ExportClass) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

}
