package com.unalarabe.urpc.converter.structure;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ExportException {

    public final String name;

    public ExportException(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ExportException{" + "name=" + name + '}';
    }

}
