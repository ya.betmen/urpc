package com.unalarabe.urpc.converter.util;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class Constants {

    public static final Set<String> JAVA_LANG_TYPES;

    static {
        Set<String> set = new HashSet<>();
        set.add(Boolean.class.getCanonicalName());
        set.add(Byte.class.getCanonicalName());
        set.add(Character.class.getCanonicalName());
        set.add(Class.class.getCanonicalName());
        set.add(ClassLoader.class.getCanonicalName());
        set.add(ClassValue.class.getCanonicalName());
        set.add(Compiler.class.getCanonicalName());
        set.add(Double.class.getCanonicalName());
        set.add(Enum.class.getCanonicalName());
        set.add(Float.class.getCanonicalName());
        set.add(InheritableThreadLocal.class.getCanonicalName());
        set.add(Integer.class.getCanonicalName());
        set.add(Long.class.getCanonicalName());
        set.add(Math.class.getCanonicalName());
        set.add(Number.class.getCanonicalName());
        set.add(Object.class.getCanonicalName());
        set.add(Package.class.getCanonicalName());
        set.add(Process.class.getCanonicalName());
        set.add(ProcessBuilder.class.getCanonicalName());
        set.add(Runtime.class.getCanonicalName());
        set.add(RuntimePermission.class.getCanonicalName());
        set.add(SecurityManager.class.getCanonicalName());
        set.add(Short.class.getCanonicalName());
        set.add(StackTraceElement.class.getCanonicalName());
        set.add(StrictMath.class.getCanonicalName());
        set.add(String.class.getCanonicalName());
        set.add(StringBuffer.class.getCanonicalName());
        set.add(StringBuilder.class.getCanonicalName());
        set.add(Throwable.class.getCanonicalName());
        set.add(Void.class.getCanonicalName());
//        set.add(Object.class.getCanonicalName());
        JAVA_LANG_TYPES = Collections.unmodifiableSet(set);
    }
}
