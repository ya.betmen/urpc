package com.unalarabe.urpc.converter.structure;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author petr
 */
public class ExportMethod {

    public ExportClass type;
    public String name;
    public final List<ExportParameter> parameters = new ArrayList<>();
    public final List<ExportException> exceptions = new ArrayList<>();
    public final List<String> ignoredGenerators = new ArrayList<>();

    @Override
    public String toString() {
        return "ExportMethod{" + "type=" + type + ", name=" + name + ", parameters=" + parameters + ", exceptions=" + exceptions + '}';
    }

}
