package com.unalarabe.urpc.converter.prepare;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author petr
 */
public class PreService {

    public final File javaFile;
    public String name;
    public String className;
    public final List<PreMethod> methods = new ArrayList<>();

    public PreService(File javaFile) {
        this.javaFile = javaFile;
    }

    @Override
    public String toString() {
        return "PreService{" + "\n\tname=" + name + ",\n\tmethods=" + methods + "\n}";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.javaFile);
        hash = 89 * hash + Objects.hashCode(this.name);
        hash = 89 * hash + Objects.hashCode(this.methods);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PreService other = (PreService) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.javaFile, other.javaFile)) {
            return false;
        }
        if (!Objects.equals(this.methods, other.methods)) {
            return false;
        }
        return true;
    }

}
