package com.unalarabe.urpc.converter.structure;

/**
 *
 * @author petr
 */
public class ExportParameter {

    public ExportClass type;
    public String name;
    public boolean required;

    @Override
    public String toString() {
        return "ExportParameter{" + "type=" + type + ", name=" + name + ", required=" + required + '}';
    }
    
}
