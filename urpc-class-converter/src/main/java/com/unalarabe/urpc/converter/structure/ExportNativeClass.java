package com.unalarabe.urpc.converter.structure;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class ExportNativeClass extends ExportClass {

    public final Class nativeClass;

    public ExportNativeClass(Class nativeClass) {
        super(nativeClass.getCanonicalName());
        this.nativeClass = nativeClass;
    }

    @Override
    public String toString() {
        return "ExportNativeClass{" + "nativeClass=" + nativeClass + '}';
    }
    
}
