package com.unalarabe.urpc.converter.structure;

import java.util.Objects;

/**
 *
 * @author petr
 */
public class ExportMap extends ExportCollection {

    public String keyType;

    public ExportMap(String keyType, String valueType) {
        super(valueType);
        this.keyType = keyType;
    }

    @Override
    public String toString() {
        return "ExportMap{" + "keyType=" + keyType + ", valueType=" + name + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.keyType);
        hash = 59 * hash + Objects.hashCode(this.collectionClass);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExportMap other = (ExportMap) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.keyType, other.keyType)) {
            return false;
        }
        if (!Objects.equals(this.collectionClass, other.collectionClass)) {
            return false;
        }
        return true;
    }

}
