package com.unalarabe.urpc.converter.util;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.EnumDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.ModifierSet;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.ReferenceType;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.unalarabe.urpc.converter.ConverterException;
import com.unalarabe.urpc.converter.prepare.PreClass;
import com.unalarabe.urpc.converter.prepare.PreField;
import com.unalarabe.urpc.converter.prepare.PreMethod;
import com.unalarabe.urpc.converter.prepare.PreParameter;
import com.unalarabe.urpc.converter.prepare.PreService;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class Extractor {

    private static final Logger LOGGER = Logger.getLogger(Extractor.class.getName());

    public static List<PreField> extractFields(PreClass preClass) {
        if (preClass.classIntance != null) {
            return extractFieldsFromClass(preClass);
        } else if (preClass.classFile != null) {
            return extractFieldsFromFile(preClass);
        } else {
            throw new IllegalStateException();
        }
    }

    public static List<PreField> extractFieldsFromFile(PreClass preClass) {
        File javaFile = preClass.classFile;
        String clazz = preClass.name;
        List<PreField> result = new ArrayList<>();
        final Map<String, String> getterTypes = new LinkedHashMap<>();
        final Map<String, String> setterTypes = new LinkedHashMap<>();
        final Set<String> requiredFields = new HashSet<>();
        final Map<String, Set<String>> ignoredFileds = new HashMap<>();

        Set<String> imports;
        try {
            imports = extractImports(javaFile);
        } catch (ConverterException ex) {
            throw new IllegalStateException(ex);
        }

        Map<String, ClassOrInterfaceDeclaration> decalrations = extractClassDecalrations(javaFile);
        final ClassOrInterfaceDeclaration declaration = decalrations.get(clazz);
        if (declaration != null) {
            VoidVisitorAdapter<Object> adapter = new VoidVisitorAdapter<Object>() {
                @Override
                public void visit(MethodDeclaration method, Object arg) {
                    System.out.println("method = " + method);
                    if (Util.isGetter(method)) {
                        String methodType = method.getType().toString();
                        String type = Util.normalizeType(methodType);
                        String methodName = method.getName();
                        String propertyName = Util.getPropertyName(methodName);
                        String fullTypeName = Util.findFullClassName(type, preClass.imports);
                        if (fullTypeName != null) {
                            type = fullTypeName;
                        }
                        if (Util.isGeneric(methodType)) {
                            List<String> generics = Util.extractGenerics(methodType);
                            type += "<" + String.join(",", generics) + ">";
                        }
                        getterTypes.put(propertyName, type);
                        if (Util.hasRequiredAnnotation(declaration, method, imports) || Util.hasRequiredAnnotation(declaration, propertyName, imports)) {
                            requiredFields.add(propertyName);
                        }
                        if (!ignoredFileds.containsKey(propertyName)) {
                            ignoredFileds.put(propertyName, new HashSet<>());
                        }
                        Set<String> ignored = Util.getIgnoredGenerators(declaration, method, imports);
                        ignoredFileds.get(propertyName).addAll(ignored);
                    }
                    if (Util.isSetter(method)) {
                        String parameterType = method.getParameters().get(0).getType().toString();
                        String type = Util.normalizeType(parameterType);
                        String methodName = method.getName();
                        String propertyName = Util.getPropertyName(methodName);
                        String fullTypeName = Util.findFullClassName(type, preClass.imports);
                        if (fullTypeName != null) {
                            type = fullTypeName;
                        }
                        if (Util.isGeneric(parameterType)) {
                            List<String> generics = Util.extractGenerics(parameterType);
                            type += "<" + String.join(",", generics) + ">";
                        }
                        setterTypes.put(propertyName, type);
                        if (Util.hasRequiredAnnotation(declaration, method, imports) || Util.hasRequiredAnnotation(declaration, propertyName, imports)) {
                            requiredFields.add(propertyName);
                        }
                        if (!ignoredFileds.containsKey(propertyName)) {
                            ignoredFileds.put(propertyName, new HashSet<>());
                        }
                        Set<String> ignored = Util.getIgnoredGenerators(declaration, method, imports);
                        ignoredFileds.get(propertyName).addAll(ignored);
                    }
                }
            };
            adapter.visit(declaration, null);
        }

        Set<String> getters = getterTypes.keySet();
        Set<String> setters = setterTypes.keySet();
        Set<String> intersection = new LinkedHashSet<>(getters);
        intersection.retainAll(setters);
        for (String propertyName : intersection) {
            String getterType = getterTypes.get(propertyName);
            String setterType = setterTypes.get(propertyName);
            if (getterType != null && getterType.equals(setterType)) {
                PreField preField = new PreField();
                preField.name = propertyName;
                PreClass fieldClass = new PreClass(preClass.service);
                if (Util.isArray(setterType)) {
                    fieldClass.name = Util.extractArrayType(setterType);
                    fieldClass.isArray = true;
                } else if (Util.isGeneric(setterType)) {
                    fieldClass.name = Util.extractGenericType(setterType);
                    fieldClass.isGeneric = true;
                    List<String> extractGenerics = Util.extractGenerics(setterType);
                    for (String generic : extractGenerics) {
                        PreClass genericClass = new PreClass(preClass.service);
                        genericClass.context = preClass;
                        genericClass.name = generic;
                        fieldClass.generics.add(genericClass);
                    }
                } else {
                    fieldClass.name = setterType;
                }
                fieldClass.context = preClass;
                preField.type = fieldClass;
                preField.required = requiredFields.contains(propertyName);
                preField.ignoredGenerators = ignoredFileds.get(propertyName);
                result.add(preField);
            }
        }

        for (PreClass classExtend : preClass.classExtends) {
            if (classExtend.nameResolved) {
                List<PreField> extraFields = new ArrayList<>();
                List<PreField> parentFields = extractFields(classExtend);
                for (PreField field : parentFields) {
                    if (ignoredFileds.containsKey(field.name)) {
                        field.ignoredGenerators = ignoredFileds.get(field.name);
                    }
                    if (requiredFields.contains(field.name)) {
                        field.required = true;
                    }
                    extraFields.add(field);
                }
                result.addAll(0, extraFields);
            }
        }
        return result;
    }

    public static List<PreField> extractFieldsFromClass(PreClass preClass) {
        Class clazz = preClass.classIntance;
        List<PreField> result = new ArrayList<>();
        Map<String, String> getterTypes = new LinkedHashMap<>();
        Map<String, String> setterTypes = new LinkedHashMap<>();
        Set<String> requiredFields = new HashSet<>();
        Map<String, Set<String>> ignoredFileds = new HashMap<>();
        Method[] methods = clazz.getMethods();
        if (methods != null) {
            for (Method method : methods) {
                String methodName = method.getName();
                if (Util.isGetter(method)) {
                    String propertyName = Util.getPropertyName(methodName);
                    Class<?> returnType = method.getReturnType();
                    String genericString = method.toGenericString();
                    if (Util.isGeneric(genericString)) {
                        List<String> extractGenerics = Util.extractGenerics(genericString);
                        getterTypes.put(propertyName, returnType.getCanonicalName() + "<" + String.join(",", extractGenerics) + ">");
                    } else {
                        getterTypes.put(propertyName, returnType.getCanonicalName());
                    }
                    if (Util.hasRequiredAnnotation(clazz, method)) {
                        requiredFields.add(methodName);
                    }
                    if (!ignoredFileds.containsKey(propertyName)) {
                        ignoredFileds.put(propertyName, new HashSet<>());
                    }
                    Set<String> ignored = Util.getIgnoredGenerators(clazz, method);
                    ignoredFileds.get(propertyName).addAll(ignored);
                }
                if (Util.isSetter(method)) {
                    String propertyName = Util.getPropertyName(methodName);
                    Class<?> paramType = method.getParameterTypes()[0];
                    String genericString = method.toGenericString();
                    if (Util.isGeneric(genericString)) {
                        List<String> extractGenerics = Util.extractGenerics(genericString);
                        setterTypes.put(propertyName, paramType.getCanonicalName() + "<" + String.join(",", extractGenerics) + ">");
                    } else {
                        setterTypes.put(propertyName, paramType.getCanonicalName());
                    }
                    if (Util.hasRequiredAnnotation(clazz, method)) {
                        requiredFields.add(methodName);
                    }
                    if (!ignoredFileds.containsKey(propertyName)) {
                        ignoredFileds.put(propertyName, new HashSet<>());
                    }
                    Set<String> ignored = Util.getIgnoredGenerators(clazz, method);
                    ignoredFileds.get(propertyName).addAll(ignored);
                }
            }
        }
        Set<String> getters = getterTypes.keySet();
        Set<String> setters = setterTypes.keySet();
        Set<String> intersection = new LinkedHashSet<>(getters);
        intersection.retainAll(setters);
        for (String propertyName : intersection) {
            String getterType = getterTypes.get(propertyName);
            String setterType = setterTypes.get(propertyName);
            if (getterType != null && getterType.equals(setterType)) {
                PreField preField = new PreField();
                preField.name = propertyName;
                PreClass fieldClass = new PreClass(preClass.service);
                fieldClass.context = preClass;
                String canonicalName = setterType;
                if (Util.isArray(setterType)) {
                    fieldClass.name = Util.extractArrayType(canonicalName);
                    fieldClass.isArray = true;
                } else if (Util.isGeneric(canonicalName)) {
                    fieldClass.name = Util.extractGenericType(canonicalName);
                    List<String> generics = Util.extractGenerics(canonicalName);
                    for (String generic : generics) {
                        PreClass pc = new PreClass(preClass.service);
                        pc.name = generic;
                        pc.context = preClass;
                        fieldClass.generics.add(pc);
                    }
                    fieldClass.isGeneric = true;
                } else {
                    fieldClass.name = canonicalName;
                }
                preField.type = fieldClass;
                preField.required = requiredFields.contains(propertyName);
                preField.ignoredGenerators = ignoredFileds.get(propertyName);
                result.add(preField);
            }
        }
        return result;
    }

    public static List<PreClass> extractExtends(final PreClass preClass) {
        if (preClass.classFile == null) {
            throw new IllegalArgumentException();
        }
        if (!preClass.nameResolved) {
            throw new IllegalArgumentException();
        }
        List<PreClass> result = new ArrayList<>();
        Map<String, ClassOrInterfaceDeclaration> classDecalrations = extractClassDecalrations(preClass.classFile);
        ClassOrInterfaceDeclaration classDeclaration = classDecalrations.get(preClass.name);
        if (classDeclaration != null) {
            List<ClassOrInterfaceType> aExtends = classDeclaration.getExtends();
            if (aExtends != null) {
                for (ClassOrInterfaceType aExtend : aExtends) {
                    String className = aExtend.getName();
                    PreClass pc = new PreClass(preClass.service);
                    pc.name = Util.normalizeType(className);
                    pc.context = preClass;
                    result.add(pc);
                }
            }
        }
        return result;
    }

    public static Map<String, EnumDeclaration> extractEnumDecalrations(File javaFile) {
        Map<String, EnumDeclaration> declarations = new HashMap<>();
        try {
            CompilationUnit cu = JavaParser.parse(javaFile);
            final PackageDeclaration aPackage = cu.getPackage();
            VoidVisitorAdapter<Map<String, EnumDeclaration>> adapter = new VoidVisitorAdapter<Map<String, EnumDeclaration>>() {
                @Override
                public void visit(EnumDeclaration n, Map<String, EnumDeclaration> arg) {
                    String name = n.getName();
                    String packageName = null;
                    if (aPackage != null) {
                        packageName = aPackage.getPackageName();
                    }
                    arg.put(packageName != null ? packageName + "." + name : name, n);
                }
            };
            adapter.visit(cu, declarations);
        } catch (ParseException | IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return declarations;
    }

    public static Map<String, ClassOrInterfaceDeclaration> extractClassDecalrations(File javaFile) {
        Map<String, ClassOrInterfaceDeclaration> declarations = new HashMap<>();
        try {
            CompilationUnit cu = JavaParser.parse(javaFile);
            final PackageDeclaration aPackage = cu.getPackage();
            VoidVisitorAdapter<Map<String, ClassOrInterfaceDeclaration>> adapter = new VoidVisitorAdapter<Map<String, ClassOrInterfaceDeclaration>>() {
                @Override
                public void visit(ClassOrInterfaceDeclaration n, Map<String, ClassOrInterfaceDeclaration> arg) {
                    String name = n.getName();
                    String packageName = null;
                    if (aPackage != null) {
                        packageName = aPackage.getPackageName();
                    }
                    arg.put(packageName != null ? packageName + "." + name : name, n);
                }
            };
            adapter.visit(cu, declarations);
        } catch (ParseException | IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return declarations;
    }

    public static List<PreClass> extractClasses(PreMethod method) {
        List<PreClass> result = new ArrayList<>();
        for (PreParameter parameter : method.parameters) {
            PreClass preClass = new PreClass(method.service);
            if (Util.isGeneric(parameter.typeName)) {
                preClass.name = Util.extractGenericType(parameter.typeName);
                preClass.isGeneric = true;
                List<String> extractGenerics = Util.extractGenerics(parameter.typeName);
                for (String generic : extractGenerics) {
                    PreClass genericClass = new PreClass(method.service);
                    genericClass.context = preClass;
                    genericClass.name = generic;
                    preClass.generics.add(genericClass);
                }
            } else {
                preClass.name = Util.normalizeType(parameter.typeName);
            }
            parameter.type = preClass;
            result.add(preClass);
        }
        PreClass preClass = new PreClass(method.service);
        if (Util.isGeneric(method.typeName)) {
            preClass.name = Util.extractGenericType(method.typeName);
            preClass.isGeneric = true;
            List<String> extractGenerics = Util.extractGenerics(method.typeName);
            for (String generic : extractGenerics) {
                PreClass genericClass = new PreClass(method.service);
                genericClass.context = preClass;
                genericClass.name = generic;
                preClass.generics.add(genericClass);
            }
        } else {
            preClass.name = Util.normalizeType(method.typeName);
        }
        result.addAll(method.exceptions);
        method.type = preClass;
        result.add(preClass);
        return result;
    }

    public static List<PreClass> extractClasses(List<PreMethod> methods) {
        List<PreClass> result = new ArrayList<>();
        for (PreMethod method : methods) {
            result.addAll(extractClasses(method));
        }
        Map<String, PreClass> classes = new HashMap<>();
        for (PreClass preClass : result) {
            classes.put(preClass.getFullName(), preClass);
        }
        return result;
    }

    public static Set<String> getSiblings(File classFile) throws ParseException, IOException {
        Set<String> result = new HashSet<>();
        String[] files = classFile.getParentFile().list((File dir, String name) -> name.endsWith(".java") && !classFile.getName().equals(name));
        CompilationUnit cu = JavaParser.parse(classFile);
        PackageDeclaration packageDeclaration = cu.getPackage();
        if (packageDeclaration != null) {
            String packageName = packageDeclaration.getPackageName();
            for (String file : files) {
                result.add(packageName + "." + file.substring(0, file.length() - 5));
            }
        }
        return result;
    }

    public static Set<String> extractImports(File classFile) throws ConverterException {
        try {
            Set<String> imports = new HashSet<>(Constants.JAVA_LANG_TYPES);
            imports.addAll(getSiblings(classFile));
            CompilationUnit cu = JavaParser.parse(classFile);
            VoidVisitorAdapter<Set<String>> importVisitor = new VoidVisitorAdapter<Set<String>>() {
                @Override
                public void visit(ImportDeclaration n, Set<String> arg) {
                    if (!n.isStatic()) {
                        if (n.isAsterisk()) {
                            arg.add(String.valueOf(n.getName()) + ".*");
                        } else {
                            arg.add(String.valueOf(n.getName()));
                        }
                    }
                }
            };
            importVisitor.visit(cu, imports);

            return imports;
        } catch (ParseException | IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            throw new ConverterException(ex);
        }
    }

    public static List<PreMethod> extractMethods(PreService preService) throws ConverterException {
        try {
            List<PreMethod> methods = new ArrayList<>();
            CompilationUnit cu = JavaParser.parse(preService.javaFile);
            VoidVisitorAdapter<List<PreMethod>> method;
            final Set<String> imports = extractImports(preService.javaFile);

            method = new VoidVisitorAdapter<List<PreMethod>>() {
                @Override
                public void visit(MethodDeclaration n, List<PreMethod> arg) {
                    boolean isPublic = ModifierSet.isPublic(n.getModifiers());
                    if (isPublic && Util.hasMethodAnnotation(n, imports)) {
                        PreMethod preMethod = new PreMethod(preService);
                        preMethod.name = n.getName();
                        preMethod.typeName = n.getType().toString();
                        preMethod.parameters.addAll(extractParameters(n.getParameters(), imports));
                        preMethod.exceptions.addAll(extractExceptions(preService, n.getThrows(), imports));
                        preMethod.ignoredGenerators.addAll(Util.getIgnoredGenerators(n, imports));
                        arg.add(preMethod);
                    }
                }
            };
            method.visit(cu, methods);

            return methods;
        } catch (ParseException | IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            throw new ConverterException(ex);
        }
    }

    public static List<PreParameter> extractParameters(Collection<Parameter> parameters, Set<String> imports) {
        List<PreParameter> result = new ArrayList<>();
        if (parameters != null) {
            for (Parameter parameter : parameters) {
                result.add(convertNativeParameter(parameter, imports));
            }
        }
        return result;
    }

    public static List<PreClass> extractExceptions(PreService preService, List<ReferenceType> aThrows, Set<String> imports) {
        List<PreClass> result = new ArrayList<>();
        if (aThrows != null) {
            for (ReferenceType aThrow : aThrows) {
                result.add(convertNativeException(preService, aThrow, imports));
            }
        }
        return result;
    }

    public static PreParameter convertNativeParameter(Parameter parameter, Set<String> imports) {
        PreParameter preParameter = new PreParameter();
        preParameter.name = parameter.getName();
        preParameter.typeName = parameter.getType().toString();
        preParameter.required = Util.hasRequiredAnnotation(parameter, imports);
        return preParameter;
    }

    public static PreClass convertNativeException(PreService preService, ReferenceType aThrow, Set<String> imports) {
        PreClass result = new PreClass(preService);
        result.name = Util.normalizeType(aThrow.getType().toStringWithoutComments());
        System.out.println("convertNativeException: " + result);
        return result;
    }
}
