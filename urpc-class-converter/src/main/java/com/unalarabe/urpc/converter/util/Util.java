package com.unalarabe.urpc.converter.util;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.ModifierSet;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.ArrayInitializerExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MarkerAnnotationExpr;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.github.javaparser.ast.type.Type;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import com.unalarabe.urpc.annotations.Ignore;
import com.unalarabe.urpc.annotations.Required;
import com.unalarabe.urpc.annotations.Service;
import com.unalarabe.urpc.converter.ConverterException;
import com.unalarabe.urpc.converter.prepare.PreClass;
import org.codehaus.plexus.util.DirectoryScanner;

/**
 *
 * @author petr
 */
public class Util {

    private static final Logger LOGGER = Logger.getLogger(Util.class.getName());

    public static final Function<PreClass, String> NAME_EXTRACTOR = (PreClass preClass) -> {
        return preClass.name;
    };
    public static final Function<PreClass, String> FULL_NAME_EXTRACTOR = (PreClass preClass) -> {
        return preClass.getFullName();
    };

    public static final List<String> extractNames(List<PreClass> classes) {
        return classes.stream().map(Util.NAME_EXTRACTOR).collect(Collectors.toList());
    }

    public static final List<String> extractFullNames(List<PreClass> classes) {
        return classes.stream().map(Util.FULL_NAME_EXTRACTOR).collect(Collectors.toList());
    }

    public static String[] getSortedIncludeFiles(DirectoryScanner ds) throws ConverterException {
        String[] includedFiles = ds.getIncludedFiles();
        Arrays.sort(includedFiles);
        return includedFiles;
    }

    public static String extractArrayType(String name) {
        int indexOfBrakets = name.indexOf("[]");
        if (indexOfBrakets < 0) {
            throw new IllegalArgumentException(name + " is not array");
        }
        return name.substring(0, indexOfBrakets);
    }

    public static String extractGenericType(String name) {
        int indexOfBrakets = name.indexOf("<");
        if (indexOfBrakets < 0) {
            throw new IllegalArgumentException(name + " is not generic");
        }
        return name.substring(0, indexOfBrakets);
    }

    public static String extractArrayType(Class clazz) {
        if (!clazz.isArray()) {
            throw new IllegalArgumentException(clazz + " is not array");
        }
        return extractArrayType(clazz.getCanonicalName());
    }

    public static List<String> extractGenerics(String type) {
        List<String> resut = new ArrayList<>();
        if (!isGeneric(type)) {
            throw new IllegalArgumentException();
        }
        String normalizedType = normalizeType(type);
        int start = normalizedType.indexOf("<");
        int end = normalizedType.indexOf(">");
        String[] parts = normalizedType.substring(start + 1, end).split(",");
        for (int i = 0; i < parts.length; i++) {
            resut.add(parts[i]);
        }
        return resut;
    }

    public static String normalizeType(String type) {
        return type.replaceAll("\\s+", "");
    }

    public static boolean isArray(String type) {
        return type.endsWith("[]");
    }

    public static boolean isGeneric(String type) {
        return type.contains("<");
    }

    public static <T> boolean isSetEquals(Set<T> a, Set<T> b) {
        return a == null && b == null || a != null && b != null && a.containsAll(b) && b.containsAll(a);
    }

    public static <T> boolean contains(Collection<T> list, T object) {
        for (T t : list) {
            if (t == object) {
                return true;
            }
        }
        return false;
    }

    public static boolean isGetter(Method method) {
        int modifiers = method.getModifiers();
        boolean isPublic = Modifier.isPublic(modifiers);
        if (isPublic) {
            String methodName = method.getName();
            Class<?> returnType = method.getReturnType();
            String prefix = "get";
            if (returnType == null) {
                return false;
            }
            if (method.getParameters().length > 0) {
                return false;
            }
            if (returnType.isPrimitive() && "boolean".equals(returnType.getName())) {
                prefix = "is";
            }
            if (methodName.length() > prefix.length()
                && methodName.startsWith(prefix)
                && methodName.matches(prefix + "[A-Z]{1}.*")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isGetter(MethodDeclaration methodDeclaration) {
        int modifiers = methodDeclaration.getModifiers();
        boolean isPublic = ModifierSet.isPublic(modifiers);
        if (isPublic) {
            String methodName = methodDeclaration.getName();
            Type type = methodDeclaration.getType();
            boolean returnValue = !"void".equals(String.valueOf(type));
            boolean isBool = "boolean".equals(String.valueOf(type));
            boolean hasParameters = null == methodDeclaration.getParameters();
            String prefix = "get";
            if (!returnValue) {
                return false;
            }
            if (hasParameters) {
                return false;
            }
            if (isBool) {
                prefix = "is";
            }
            if (methodName.length() > prefix.length()
                && methodName.startsWith(prefix)
                && methodName.matches(prefix + "[A-Z]{1}.*")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSetter(Method method) {
        int modifiers = method.getModifiers();
        boolean isPublic = Modifier.isPublic(modifiers);
        if (isPublic) {
            if (method.getReturnType() != Void.TYPE || method.getParameters().length != 1) {
                return false;
            }
            String methodName = method.getName();
            String prefix = "set";
            if (methodName.length() > prefix.length()
                && methodName.startsWith(prefix)
                && methodName.matches(prefix + "[A-Z]{1}.*")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSetter(MethodDeclaration methodDeclaration) {
        int modifiers = methodDeclaration.getModifiers();
        boolean isPublic = Modifier.isPublic(modifiers);
        if (isPublic) {
            List<Parameter> parameters = methodDeclaration.getParameters();
            int parametersCount = parameters != null ? parameters.size() : 0;
            if (!"void".equals(String.valueOf(methodDeclaration.getType())) || parametersCount != 1) {
                return false;
            }
            String methodName = methodDeclaration.getName();
            String prefix = "set";
            if (methodName.length() > prefix.length()
                && methodName.startsWith(prefix)
                && methodName.matches(prefix + "[A-Z]{1}.*")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isList(PreClass type) {
        Class clazz = findClassInstance(type);
        return clazz != null && clazz.isAssignableFrom(List.class);
    }

    public static boolean isSet(PreClass type) {
        Class clazz = findClassInstance(type);
        return clazz != null && clazz.isAssignableFrom(Set.class);
    }

    public static boolean isMap(PreClass type) {
        Class clazz = findClassInstance(type);
        return clazz != null && clazz.isAssignableFrom(Map.class);
    }

    public static Class findClassInstance(PreClass type) {
        while (type != null) {
            if (type.classIntance != null) {
                return type.classIntance;
            }
            if (type.classExtends.isEmpty()) {
                type = null;
            } else {
                type = type.classExtends.get(0);
            }
        }
        return null;
    }

    public static String getPropertyName(String methodName) {
        String prefix = "get";
        if (!methodName.startsWith(prefix)) {
            prefix = "set";
        }
        if (!methodName.startsWith(prefix)) {
            prefix = "is";
        }
        if (!methodName.startsWith(prefix)) {
            return null;
        }
        if (methodName.length() > prefix.length()
            && methodName.startsWith(prefix)
            && methodName.matches(prefix + "[A-Z]{1}.*")) {
            return methodName.substring(prefix.length(), prefix.length() + 1).toLowerCase() + methodName.substring(prefix.length() + 1);
        }
        return null;
    }

    public static String resolveClassName(PreClass preClass, Set<String> imports) {
        String resolvedClass = null;
        if ("void".equals(preClass.name)) {
            return "void";
        }
        if (preClass.context != null) {
            resolvedClass = findFullClassName(preClass.getFullName(), preClass.context.imports);
        }
        if (resolvedClass == null) {
            resolvedClass = findFullClassName(preClass.name, imports);
        }
        return resolvedClass;
    }

    public static String findFullClassName(String className, Collection<String> imports) {
        String search = className.contains(".") ? className : "." + className;
        boolean isArray = className.contains("[");
        boolean isGeneric = className.contains("<");
        if (isArray) {
            int indexOfBracket = search.indexOf("[");
            search = search.substring(0, indexOfBracket);
        }
        if (isGeneric) {
            int indexOfBracket = search.indexOf("<");
            search = search.substring(0, indexOfBracket);
        }
        for (String resolvedClass : imports) {
            if (resolvedClass.endsWith(search)) {
                if (isArray) {
                    resolvedClass += "[]";
                }
                return resolvedClass;
            }
        }
        return null;
    }

    public static String getPackage(String fullServiceClassName) {
        int lastIndex = fullServiceClassName.lastIndexOf(".");
        if (lastIndex >= 0) {
            return fullServiceClassName.substring(0, lastIndex);
        }
        return "";
    }

    public static String getFullClassName(File classFile) throws ConverterException {
        try {
            CompilationUnit cu = JavaParser.parse(classFile);
            PackageDeclaration aPackage = cu.getPackage();
            StringBuilder result = new StringBuilder();
            if (aPackage != null) {
                result.append(aPackage.getPackageName());
                result.append('.');
            }
            result.append(classFile.getName());
            int lastIndex = result.lastIndexOf(".java");
            return result.substring(0, lastIndex);
        } catch (ParseException | IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            throw new ConverterException(ex);
        }
    }

    public static boolean hasMethodAnnotation(MethodDeclaration declaration, Set<String> imports) {
        List<AnnotationExpr> annotations = declaration.getAnnotations();
        if (annotations != null) {
            for (AnnotationExpr annotation : annotations) {
                String name = findFullClassName(annotation.getName().getName(), imports);
                if (name.equals(com.unalarabe.urpc.annotations.Method.class.getCanonicalName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean hasServiceAnnotation(File sourceFile, String fullServiceClassName) throws ConverterException {
        Map<String, ClassOrInterfaceDeclaration> classDecalrations = Extractor.extractClassDecalrations(sourceFile);
        ClassOrInterfaceDeclaration declaration = classDecalrations.get(fullServiceClassName);
        if (declaration != null) {
            Set<String> imports = Extractor.extractImports(sourceFile);
            List<AnnotationExpr> annotations = declaration.getAnnotations();
            for (AnnotationExpr annotation : annotations) {
                String name = findFullClassName(annotation.getName().getName(), imports);
                if (name.equals(Service.class.getCanonicalName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean hasRequiredAnnotation(Class clazz, Method method) {
        boolean required = method.getAnnotation(Required.class) != null;
        if (!required) {
            String propertyName = Util.getPropertyName(method.getName());
            try {
                Field field = clazz.getDeclaredField(propertyName);
                required = field.getAnnotation(Required.class) != null;
            } catch (NoSuchFieldException | SecurityException ex) {
                LOGGER.log(Level.FINEST, null, ex);
            }
        }
        return required;
    }

    public static Set<String> getIgnoredGenerators(Class clazz, Method method) {
        Ignore ignore = method.getAnnotation(Ignore.class);
        Set<String> result = new HashSet<>();
        if (ignore != null) {
            result.addAll(Arrays.asList(ignore.generators()));
        }
        String propertyName = Util.getPropertyName(method.getName());
        try {
            Field field = clazz.getDeclaredField(propertyName);
            Ignore fieldIgnore = field.getAnnotation(Ignore.class);
            if (fieldIgnore != null) {
                result.addAll(Arrays.asList(fieldIgnore.generators()));
            }
        } catch (NoSuchFieldException | SecurityException ex) {
            LOGGER.log(Level.FINEST, null, ex);
        }
        return result;
    }

    public static boolean hasRequiredAnnotation(ClassOrInterfaceDeclaration declaration, MethodDeclaration method, Set<String> imports) {
        List<AnnotationExpr> annotations = method.getAnnotations();
        if (annotations != null) {
            for (AnnotationExpr annotation : annotations) {
                String name = findFullClassName(annotation.getName().getName(), imports);
                if (Required.class.getCanonicalName().equals(name)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean hasRequiredAnnotation(ClassOrInterfaceDeclaration declaration, String propertyName, Set<String> imports) {
        FieldDeclaration[] result = new FieldDeclaration[]{null};
        VoidVisitorAdapter<FieldDeclaration[]> adapter = new VoidVisitorAdapter<FieldDeclaration[]>() {
            @Override
            public void visit(FieldDeclaration n, FieldDeclaration[] arg) {
                if (arg[0] == null) {
                    String varName = n.getVariables().get(0).getId().getName();
                    if (propertyName.equals(varName)) {
                        arg[0] = n;
                    }
                }
            }
        };
        adapter.visit(declaration, result);
        FieldDeclaration fieldDeclaration = result[0];
        if (fieldDeclaration != null) {
            return findAnnotation(fieldDeclaration.getAnnotations(), imports, Required.class) != null;
        }
        return false;
    }

    public static AnnotationExpr findAnnotation(List<AnnotationExpr> annotations, Set<String> imports, Class annotationClass) {
        if (annotations != null) {
            for (AnnotationExpr annotation : annotations) {
                String name = findFullClassName(annotation.getName().getName(), imports);
                if (annotationClass.getCanonicalName().equals(name)) {
                    return annotation;
                }
            }
        }
        return null;
    }

    public static Set<String> getIgnoreAnnotationValues(AnnotationExpr ignoreAnnotation) {
        Set<String> result = new HashSet<>();
        if (ignoreAnnotation != null) {
            VoidVisitorAdapter<Set<String>> adapter = new VoidVisitorAdapter<Set<String>>() {
                @Override
                public void visit(ArrayInitializerExpr n, Set<String> set) {
                    List<Expression> values = n.getValues();
                    if (values == null) {
                        set.add("all");
                    } else {
                        for (Expression value : values) {
                            String strValue = value.toStringWithoutComments();
                            set.add(strValue.substring(1, strValue.length() - 1));
                        }
                    }
                }

                @Override
                public void visit(NormalAnnotationExpr n, Set<String> arg) {
                    List<MemberValuePair> pairs = n.getPairs();
                    for (MemberValuePair pair : pairs) {
                        if ("generators".equals(pair.getName())) {
                            String strValue = pair.getValue().toStringWithoutComments();
                            arg.add(strValue.substring(1, strValue.length() - 1));
                        }
                    }
                }

                @Override
                public void visit(MarkerAnnotationExpr n, Set<String> set) {
                    set.add("all");
                }

            };
            if (ignoreAnnotation instanceof SingleMemberAnnotationExpr) {
                adapter.visit((SingleMemberAnnotationExpr) ignoreAnnotation, result);
            } else if (ignoreAnnotation instanceof MarkerAnnotationExpr) {
                adapter.visit((MarkerAnnotationExpr) ignoreAnnotation, result);
            } else if (ignoreAnnotation instanceof NormalAnnotationExpr) {
                adapter.visit((NormalAnnotationExpr) ignoreAnnotation, result);
            } else {
                throw new IllegalStateException();
            }
        }
        return result;
    }

    public static boolean hasVariable(List<VariableDeclarator> variables, String name) {
        if (variables != null && name != null) {
            if (variables.stream().anyMatch((var) -> (name.equals(var.getId().getName())))) {
                return true;
            }
        }
        return false;
    }

    public static Set<String> getIgnoredGenerators(ClassOrInterfaceDeclaration declaration, MethodDeclaration method, Set<String> imports) {
        Set<String> result = new HashSet<>();
        final String propertyName = getPropertyName(method.getName());
        VoidVisitorAdapter<Set<String>> adapter = new VoidVisitorAdapter<Set<String>>() {
            @Override
            public void visit(FieldDeclaration n, Set<String> set) {
                if (!ModifierSet.isStatic(n.getModifiers()) && hasVariable(n.getVariables(), propertyName)) {
                    AnnotationExpr ignoreAnnotation = findAnnotation(n.getAnnotations(), imports, Ignore.class);
                    Set<String> values = getIgnoreAnnotationValues(ignoreAnnotation);
                    set.addAll(values);
                }
            }

        };
        adapter.visit(declaration, result);
        List<AnnotationExpr> annotations = method.getAnnotations();
        AnnotationExpr ignoreAnnotation = findAnnotation(annotations, imports, Ignore.class);
        if (ignoreAnnotation != null) {
            Set<String> values = getIgnoreAnnotationValues(ignoreAnnotation);
            result.addAll(values);
        }
        return result;
    }

    public static Set<String> getIgnoredGenerators(MethodDeclaration method, Set<String> imports) {
        Set<String> result = new HashSet<>();
        List<AnnotationExpr> annotations = method.getAnnotations();
        AnnotationExpr ignoreAnnotation = findAnnotation(annotations, imports, Ignore.class);
        if (ignoreAnnotation != null) {
            Set<String> values = getIgnoreAnnotationValues(ignoreAnnotation);
            result.addAll(values);
        }
        return result;
    }

    public static boolean hasRequiredAnnotation(Parameter parameter, Set<String> imports) {
        AnnotationExpr requierdAnnotation = findAnnotation(parameter.getAnnotations(), imports, Required.class);
        return requierdAnnotation != null;
    }

}
