package com.unalarabe.urpc.converter.prepare;

/**
 *
 * @author petr
 */
public class PreParameter {

    public String typeName;
    public PreClass type;
    public String name;
    public boolean required;

    @Override
    public String toString() {
        return "\nPreParameter{" + "\n\ttypeName=" + typeName + ",\n\tname=" + name + ",\n\ttype=" + type + "\n}";
    }

}
