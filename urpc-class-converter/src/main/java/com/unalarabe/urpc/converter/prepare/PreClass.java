package com.unalarabe.urpc.converter.prepare;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import com.unalarabe.urpc.converter.util.Util;
import com.unalarabe.urpc.converter.structure.ExportClass;

/**
 *
 * @author petr
 */
public class PreClass {

    public final String id = UUID.randomUUID().toString();

    public final PreService service;
    public String name;
    public PreClass context;
    public final List<String> imports = new ArrayList<>();
    public final List<PreClass> classExtends = new ArrayList<>();
    public final List<PreClass> generics = new ArrayList<PreClass>() {
        @Override
        public boolean addAll(Collection<? extends PreClass> c) {
            return super.addAll(c); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void clear() {
            super.clear(); //To change body of generated methods, choose Tools | Templates.
        }

    };
    public final List<PreField> fields = new ArrayList<>();
    public final List<String> entries = new ArrayList<>();
    public boolean nameResolved;
    public boolean fieldsResolved;
    public boolean genericsResolved;
    public boolean extendsResolved;
    public boolean isArray;
    public boolean isGeneric;
    public boolean isSimple;
    public boolean isEnum;
    public File classFile;
    public Class classIntance;
    public ExportClass exportClass;

    public PreClass(PreService service) {
//        System.out.println("NEW PRECLASS");
        this.service = service;
    }

    @Override
    public String toString() {
        List<String> extendsNames = Util.extractNames(classExtends);
        List<String> genericsNames = Util.extractNames(generics);
        return "\nPreClass{" + "\n\tname=" + name + "\n\tcontext=" + (context != null ? context.name : null) + ",\n\tisEnum=" + isEnum + ",\n\timports=" + imports + ",\n\tgenerics= " + genericsNames + ",\n\tclassExtends=" + extendsNames + ",\n\tfields= " + fields + ",\n\tentries= " + entries + ",\n\tnameResolved=" + nameResolved + ",\n\tclassFile=" + classFile + ",\n\tclassIntance=" + classIntance + ",\n\texportClass=" + exportClass + ",\n\tfieldsResolved=" + fieldsResolved + "\n}";
    }

    public String getFullName() {
        if (isArray) {
            return name + "[]";
        } else if (isGeneric) {
            return name + "<" + String.join(",", Util.extractNames(generics)) + ">";
        } else {
            return name;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.service);
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + (this.nameResolved ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PreClass other = (PreClass) obj;
        if (this.nameResolved != other.nameResolved) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.service, other.service)) {
            return false;
        }
        return true;
    }

    public static void copyFields(PreClass source, PreClass target) {
        if (source != target) {
            target.fields.clear();
            target.fields.addAll(source.fields);
        }
    }

    public static void copyImports(PreClass source, PreClass target) {
        if (source != target) {
            target.imports.clear();
            target.imports.addAll(source.imports);
        }
    }

    public static void copyExtends(PreClass source, PreClass target) {
        if (source != target) {
            target.classExtends.clear();
            target.classExtends.addAll(source.classExtends);
        }
    }

    public static void copyGenerics(PreClass source, PreClass target) {
        if (source != target) {
            target.isGeneric = source.isGeneric;
            target.generics.clear();
            target.generics.addAll(source.generics);
        }
    }

    public static void copy(PreClass source, PreClass target) {
        if (!source.service.equals(target.service)) {
            throw new IllegalArgumentException("Classes from different services");
        }
        if (source != target) {
//            System.out.println("COPY " + source.id + " -> " + target.id);

            copyExtends(source, target);
            target.classFile = source.classFile;
            target.classIntance = source.classIntance;
            target.context = source.context;
            copyFields(source, target);
            copyImports(source, target);
            if (source.isGeneric == target.isGeneric) {
                copyGenerics(source, target);
            }
            target.name = source.name;
            target.nameResolved = source.nameResolved;
            target.isArray = source.isArray;
            target.isSimple = source.isSimple;
            target.isEnum = source.isEnum;
            target.fieldsResolved = source.fieldsResolved;
        }
    }

}
