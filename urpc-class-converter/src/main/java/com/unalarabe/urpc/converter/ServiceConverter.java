package com.unalarabe.urpc.converter;

import com.unalarabe.urpc.converter.util.Util;
import com.github.javaparser.ast.body.EnumConstantDeclaration;
import com.github.javaparser.ast.body.EnumDeclaration;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.unalarabe.urpc.converter.prepare.PreClass;
import com.unalarabe.urpc.converter.prepare.PreField;
import com.unalarabe.urpc.converter.prepare.PreMethod;
import com.unalarabe.urpc.converter.prepare.PreParameter;
import com.unalarabe.urpc.converter.prepare.PreService;
import com.unalarabe.urpc.converter.structure.ExportArray;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportCollection;
import com.unalarabe.urpc.converter.structure.ExportEnum;
import com.unalarabe.urpc.converter.structure.ExportField;
import com.unalarabe.urpc.converter.structure.ExportList;
import com.unalarabe.urpc.converter.structure.ExportMap;
import com.unalarabe.urpc.converter.structure.ExportNativeClass;
import com.unalarabe.urpc.converter.structure.ExportService;
import com.unalarabe.urpc.converter.structure.ExportSet;
import com.unalarabe.urpc.converter.util.Constants;
import com.unalarabe.urpc.converter.util.Converter;
import com.unalarabe.urpc.converter.util.Extractor;
import org.apache.commons.lang.ClassUtils;

/**
 *
 * @author petr
 */
public class ServiceConverter {

    public static final Logger LOGGER = Logger.getLogger(ServiceConverter.class.getName());
    public static final String SEPARATOR = System.getProperty("file.separator");

    private final ClassLoader classLoader;
    private final File servicedir;
    private final File basedir;

    private final Map<String, PreClass> resolvedClasses = new HashMap<>();
    private final Map<String, PreClass> fullResolvedClasses = new HashMap<>();

    public ServiceConverter(ClassLoader classLoader, File servicedir, File basedir) {
        this.classLoader = classLoader;
        this.servicedir = servicedir;
        this.basedir = basedir;
    }

    public ExportService export(String serviceClass) throws ConverterException {
        File sourceFile = getJavaFile(serviceClass);
        if (sourceFile == null) {
            return null;
        }
        PreService preService = new PreService(sourceFile);
        String fullServiceClassName = Util.getFullClassName(sourceFile);
        if (!Util.hasServiceAnnotation(sourceFile, fullServiceClassName)) {
            return null;
        }
        preService.name = fullServiceClassName;
        preService.className = fullServiceClassName;
        Set<String> imports = Extractor.extractImports(sourceFile);
        imports.addAll(Constants.JAVA_LANG_TYPES);
        List<PreMethod> methods = Extractor.extractMethods(preService);
        preService.methods.addAll(methods);
        List<PreClass> preClasses = Extractor.extractClasses(methods);
        final List<PreClass> additionalClasses = new ArrayList<PreClass>(preClasses);
        final Set<String> lastAdditionalClasses = new HashSet<>();
        int lastAdditionalClassesLength = 0;
        int iteration = 0;
        int maxIteration = 100;

        PreClass objectPreClass = createResolvedClass(preService, Object.class);
        PreClass voidPreClass = createResolvedClass(preService, Void.class);
        voidPreClass.name = "void";
        PreClass datePreClass = createResolvedClass(preService, Date.class);
        PreClass exceptionPreClass = createResolvedClass(preService, Exception.class);
        PreClass runtimeExceptionPreClass = createResolvedClass(preService, RuntimeException.class);
        PreClass throwablePreClass = createResolvedClass(preService, Throwable.class);

        addToResolved("void", voidPreClass);
        addToResolved(objectPreClass.getFullName(), objectPreClass);
        addToResolved(datePreClass.getFullName(), datePreClass);
        addToResolved(exceptionPreClass.getFullName(), exceptionPreClass);
        addToResolved(runtimeExceptionPreClass.getFullName(), runtimeExceptionPreClass);
        addToResolved(throwablePreClass.getFullName(), throwablePreClass);

        boolean tryWithJavaLang = false;

        while (!additionalClasses.isEmpty()) {
            Set<String> newAdditionalClasses = new HashSet<>(Util.extractFullNames(additionalClasses));
            int newAdditionalClassesLength = additionalClasses.size();
            iteration++;
            if (iteration > maxIteration) {
                throw new IllegalStateException("More than " + maxIteration + " iterations:\n Can't resolve " + newAdditionalClasses);
            }
            if (Util.isSetEquals(lastAdditionalClasses, newAdditionalClasses) && newAdditionalClassesLength == lastAdditionalClassesLength) {
                if (tryWithJavaLang) {
                    throw new IllegalStateException("Can't resolve " + newAdditionalClasses);
                } else {
                    tryWithJavaLang = true;
                }
            } else {
                tryWithJavaLang = false;
            }
            lastAdditionalClasses.clear();
            lastAdditionalClasses.addAll(newAdditionalClasses);
            preClasses.clear();
            preClasses.addAll(additionalClasses);
            additionalClasses.clear();
            lastAdditionalClassesLength = newAdditionalClassesLength;
            for (PreClass preClass : preClasses) {
                resolveImports(preClass, additionalClasses);
                if (preClass.context != null) {
                    resolveImports(preClass.context, additionalClasses);
                }

                if (!preClass.nameResolved) {
                    resolveClass(preClass, imports, additionalClasses, tryWithJavaLang);
                }
            }
            for (PreClass preClass : preClasses) {
                if (preClass.nameResolved) {
                    if (!preClass.extendsResolved) {
                        resolveExtends(preClass, additionalClasses);
                    }
                    if (!preClass.fieldsResolved && preClass.extendsResolved) {
                        List<PreClass> unresolvedExtends = getUnresolvedNameExtends(preClass);
                        if (unresolvedExtends.isEmpty()) {
                            resolveFields(preClass, additionalClasses);
                        }
                    }
                    if (preClass.isGeneric && !preClass.genericsResolved) {
                        resolveGenerics(preClass, additionalClasses);
                        if (!preClass.genericsResolved) {
                            additionalClasses.add(preClass);
                        }
                    }
                    PreClass resolved = resolvedClasses.get(preClass.getFullName());
                    if (resolved != null) {
                        if (!preClass.isGeneric || preClass.genericsResolved) {
                            if (preClass.fieldsResolved && !resolved.fieldsResolved) {
                                PreClass.copyFields(preClass, resolved);
                            }
                            if (preClass.extendsResolved && !resolved.extendsResolved) {
                                PreClass.copyExtends(preClass, resolved);
                            }
                        }
                        if (preClass.isGeneric && preClass.genericsResolved) {
                            PreClass.copyGenerics(preClass, resolved);
                            updateResolved(preClass);
                        }
                        if (!resolved.isGeneric || resolved.genericsResolved) {
                            if (resolved.fieldsResolved && !preClass.fieldsResolved) {
                                PreClass.copyFields(resolved, preClass);
                            }
                            if (resolved.extendsResolved && !preClass.extendsResolved) {
                                PreClass.copyExtends(resolved, preClass);
                            }
                        }
                        if (resolved.isGeneric && resolved.genericsResolved) {
                            PreClass.copyGenerics(resolved, preClass);
                            updateResolved(resolved);
                        }
                    }
                    if (!preClass.isGeneric || preClass.genericsResolved) {
                        if (!resolvedClasses.containsKey(preClass.getFullName())) {
                            resolvedClasses.put(preClass.getFullName(), preClass);
                        }
                    }
                } else {
                    additionalClasses.add(preClass);
                }
                if (preClass.isGeneric && !resolvedClasses.containsKey(preClass.name)) {
                    PreClass noGenericClass = new PreClass(preClass.service);
                    noGenericClass.name = preClass.name;
                    noGenericClass.imports.addAll(preClass.imports);
                    noGenericClass.context = preClass.context;
                    noGenericClass.classExtends.addAll(preClass.classExtends);
                    additionalClasses.add(noGenericClass);
                }
            }
            for (PreClass preClass : resolvedClasses.values()) {
                if (preClass.nameResolved && (!preClass.isGeneric || preClass.genericsResolved) && preClass.extendsResolved && preClass.fieldsResolved) {
                    fullResolvedClasses.put(preClass.getFullName(), preClass);
                    updateResolved(preClass);
                } else {
                    additionalClasses.add(preClass);
                }
            }
        }

        resolveEnumns(fullResolvedClasses.values());

        for (PreMethod method : methods) {
            if (!method.type.nameResolved) {
                throw new IllegalStateException("Unresolved method return type: " + method.type.name);
            }
            method.typeName = method.type.name;
            for (PreParameter parameter : method.parameters) {
                if (!parameter.type.nameResolved) {
                    throw new IllegalStateException("Unresolved method parameter type: " + method.type.name);
                }
                parameter.typeName = parameter.type.getFullName();
            }
        }
        LOGGER.info("=======fullResolvedClasses========");
        for (Map.Entry<String, PreClass> entry : fullResolvedClasses.entrySet()) {
            LOGGER.log(Level.INFO, "{0} => {1}", new Object[]{entry.getKey(), entry.getValue()});
        }
        LOGGER.info("===============");
        Map<String, ExportClass> classMap = createClassMap(fullResolvedClasses);
        resolveClassFields(classMap);
        LOGGER.info("=======classMap========");
        for (Map.Entry<String, ExportClass> entry : classMap.entrySet()) {
            LOGGER.log(Level.INFO, "{0} => {1}", new Object[]{entry.getKey(), entry.getValue()});
        }
        LOGGER.info("===============");

        ExportService service = Converter.convertService(preService, classMap);
        LOGGER.log(Level.INFO, "service={0}", service);

        return service;
    }

    protected void addToResolved(String name, PreClass preClass) {
        resolvedClasses.put(preClass.name, preClass);
        fullResolvedClasses.put(preClass.name, preClass);
    }

    protected PreClass createResolvedClass(PreService preService, Class clazz) {
        PreClass result = new PreClass(preService);
        result.name = clazz.getCanonicalName();
        result.nameResolved = true;
        result.classIntance = loadClass(result.name);
        result.fieldsResolved = true;
        result.genericsResolved = true;
        result.extendsResolved = true;
        return result;
    }

    protected Class tryResolveFromJavaLang(String className) {
        String fullName = "java.lang." + className;
        return loadClass(fullName);
    }

    protected void resolveImports(PreClass preClass, final List<PreClass> additionalClasses) throws ConverterException {
        if (preClass.classFile != null && preClass.imports.isEmpty()) {
            Set<String> classImports = Extractor.extractImports(preClass.classFile);
            preClass.imports.addAll(classImports);
        }
    }

    protected void resolveGenerics(PreClass preClass, final List<PreClass> additionalClasses) {
        boolean genericsResolved = true;
        for (PreClass genericClass : preClass.generics) {
            if (!resolvedClasses.containsKey(genericClass.getFullName())) {
                genericsResolved = false;
                additionalClasses.add(genericClass);
            }
        }
        preClass.genericsResolved = genericsResolved;
    }

    protected void resolveFields(PreClass preClass, final List<PreClass> additionalClasses) {
        if (preClass.fields.isEmpty()) {
            List<PreField> fields = Extractor.extractFields(preClass);
            if (fields.isEmpty()) {
                preClass.fieldsResolved = true;
            } else {
                preClass.fields.addAll(fields);
            }
        }
        if (!preClass.fieldsResolved) {
            boolean fieldsResolved = true;
            for (PreField field : preClass.fields) {
                if (!resolvedClasses.containsKey(field.type.getFullName())) {
                    fieldsResolved = false;
                    additionalClasses.add(field.type);
                }
            }
            preClass.fieldsResolved = fieldsResolved;
        }
    }

    protected void resolveClass(PreClass preClass, Set<String> imports, final List<PreClass> additionalClasses, boolean tryFromLang) throws ConverterException, IllegalStateException {
        String resolvedName = Util.resolveClassName(preClass, imports);

        if (resolvedName == null) {
            resolvedName = getFullClassName(preClass);
            if (resolvedName == null && tryFromLang) {
                Class clazz = tryResolveFromJavaLang(preClass.name);
                if (clazz != null) {
                    resolvedName = clazz.getCanonicalName();
                }
            }
        }
        if (resolvedName == null) {
            Class clazz = loadClass(preClass.name);
            if (clazz != null) {
                resolvedName = clazz.getCanonicalName();
            }
        }
        if (resolvedName != null) {
            preClass.nameResolved = true;
            if (!resolvedClasses.containsKey(resolvedName)) {
                preClass.classIntance = loadClass(resolvedName);
                preClass.name = Util.normalizeType(resolvedName);
                if (Util.isArray(preClass.name)) {
                    preClass.name = Util.extractArrayType(preClass.name);
                }
                if (preClass.classIntance == null) {
                    preClass.classFile = resolveClassFile(preClass);
                    if (preClass.classFile == null) {
                        throw new IllegalStateException();
                    }
                }
                if (preClass.isArray && resolvedClasses.containsKey(preClass.name)) {
                    PreClass pc = resolvedClasses.get(preClass.name);
                    File classFile = preClass.classFile;
                    Class classInstance = preClass.classIntance;
                    PreClass.copy(pc, preClass);
                    preClass.isArray = true;
                    preClass.classIntance = classInstance;
                    preClass.classFile = classFile;
                    resolvedClasses.put(preClass.getFullName(), preClass);
                }
            } else {
                PreClass pc = resolvedClasses.get(resolvedName);
                PreClass.copy(pc, preClass);
            }
        }
    }

    protected void resolveExtends(PreClass preClass, final List<PreClass> additionalClasses) {
        if (preClass.classExtends.isEmpty() && preClass.classFile != null) {
            List<PreClass> classExtends = Extractor.extractExtends(preClass);
            boolean resolved = true;
            for (PreClass classExtend : classExtends) {
                preClass.classExtends.add(classExtend);
                classExtend.context = preClass;
                if (!resolvedClasses.containsKey(classExtend.getFullName())) {
                    additionalClasses.add(classExtend);
                    resolved = false;
                }
            }
            preClass.extendsResolved = resolved;
        } else {
            preClass.extendsResolved = true;
        }
    }

    protected void resolveEnumns(Collection<PreClass> classes) {
        for (PreClass preClass : classes) {
            resolveEnumn(preClass);
        }
    }

    protected void resolveEnumn(PreClass preClass) {
        if ("void".equals(preClass.getFullName())) {
            //do nothing
        } else if (preClass.classFile != null) {
            Map<String, EnumDeclaration> enumDecalrations = Extractor.extractEnumDecalrations(preClass.classFile);
            EnumDeclaration enumDeclaration = enumDecalrations.get(preClass.name);
            if (enumDeclaration != null) {
                preClass.isEnum = true;
                List<EnumConstantDeclaration> entries = enumDeclaration.getEntries();
                if (entries != null) {
                    for (EnumConstantDeclaration entry : entries) {
                        preClass.entries.add(entry.getName());
                    }
                }
            }
        } else if (preClass.classIntance != null) {
            boolean isEnum = preClass.classIntance.isEnum();
            if (isEnum) {
                Object[] enumConstants = preClass.classIntance.getEnumConstants();
                for (Object enumConstant : enumConstants) {
                    preClass.entries.add(String.valueOf(enumConstant));
                }
            }
        } else {
            throw new IllegalStateException();
        }
    }

    protected File getJavaFile(String className) {
        File file = new File(servicedir.getAbsolutePath() + SEPARATOR + className.replaceAll("\\.", SEPARATOR) + ".java");
        if (file.exists()) {
            return file;
        } else {
            return null;
        }
    }

    protected File resolveClassFile(PreClass preClass) {
        String classPackage;
        String className;
        String classPath;
        if (preClass.nameResolved) {
            classPackage = Util.getPackage(preClass.name);
            className = preClass.name.substring(classPackage.length() + 1);
        } else {
            String servicePackage = Util.getPackage(preClass.service.className);
            className = preClass.name;
            classPackage = servicePackage;
            if (className.contains(".")) {
                String path = className;
                boolean findPath = false;
                while (!findPath && path.contains(".")) {
                    int lastDotIndex = path.lastIndexOf(".");
                    path = path.substring(0, lastDotIndex);
                    int pathStart = servicePackage.indexOf(path);
                    int pathEnd = pathStart + path.length();
                    if (pathStart == 0) {
                        if (pathEnd == servicePackage.length()) {
                            classPackage = "";
                            findPath = true;
                        } else if (servicePackage.startsWith(path + ".")) {
                            classPackage = "";
                            findPath = true;
                        }
                    } else if (pathStart > 0) {
                        if (servicePackage.charAt(pathStart - 1) == '.') {
                            if (pathEnd == servicePackage.length()) {
                                classPackage = servicePackage.substring(0, pathStart);
                                findPath = true;
                            }
                        }
                    }
                }
            }
        }
        classPath = classPackage.replaceAll("\\.", SEPARATOR) + SEPARATOR;
        String classNamePath = className.replaceAll("\\.", SEPARATOR);
        String fullPath = basedir + SEPARATOR + classPath + classNamePath + ".java";
        File file = new File(fullPath);
        return file.isFile() ? file : null;
    }

    protected String getFullClassName(PreClass preClass) {
        String result = null;
        File classFile = resolveClassFile(preClass);
        if (classFile != null && classFile.exists()) {
            String absolutePath = classFile.getAbsolutePath();
            if (!absolutePath.startsWith(basedir.getAbsolutePath())) {
                throw new IllegalStateException();
            }
            if (!absolutePath.endsWith(".java")) {
                throw new IllegalStateException();
            }
            result = absolutePath.substring(basedir.getAbsolutePath().length() + 1, absolutePath.length() - 5);
            result = result.replaceAll(SEPARATOR, ".");
            preClass.classFile = classFile;
        }
        return result;
    }

    protected Class loadClass(String resolved) {
        try {
            return ClassUtils.getClass(classLoader, resolved);
        } catch (ClassNotFoundException ex) {
            LOGGER.log(Level.FINE, ex.getMessage(), ex);
        }
        return null;
    }

//    public static  List<ExportMethod> convertMethods(List<PreMethod> methods) {
//        boolean fullyConverted = false;
//        boolean importsChanged = true;
//        while (!fullyConverted && importsChanged) {
//            fullyConverted = true;
//            int sizeBefore = importsMap.size();
//            for (PreMethod method : methods) {
//                ExportMethod exportMethod = convertMethod(method);
//                if (exportMethod != null) {
//                    result.add(exportMethod);
//                } else {
//                    fullyConverted = false;
//                }
//            }
//            int sizeAfter = importsMap.size();
//            importsChanged = sizeAfter != sizeBefore;
//        }
//        if (!importsChanged && !fullyConverted) {
//            throw new IllegalStateException("Can't resolve classes");
//        }
//        return result;
//    }
    public static Map<String, ExportClass> createClassMap(Map<String, PreClass> imports) {
        Map<String, ExportClass> result = new HashMap<>();
        for (Map.Entry<String, PreClass> entry : imports.entrySet()) {
            result.put(entry.getKey(), createExportType(entry.getValue()));
        }
        for (PreClass preClass : imports.values()) {
            if (preClass.exportClass == null) {
                ExportClass exportClass = result.get(preClass.getFullName());
                for (String aImport : preClass.imports) {
                    exportClass.imports.add(aImport);
                }
                preClass.exportClass = exportClass;
            }
        }
        return result;
    }

    public static ExportClass createExportType(PreClass type) {
        if ("void".equals(type.getFullName())) {
            return new ExportClass("void");
        } else if (type.isArray) {
            return new ExportArray(type.name);
        } else if (Util.isList(type)) {
            String genericType;
            if (type.generics.isEmpty()) {
                genericType = "java.lang.Object";
            } else {
                genericType = type.generics.get(0).getFullName();
            }
            ExportList ec = new ExportList(genericType);
            ec.collectionClass = type.name;
            return ec;
        } else if (Util.isSet(type)) {
            String genericType;
            if (type.generics.isEmpty()) {
                genericType = "java.lang.Object";
            } else {
                genericType = type.generics.get(0).getFullName();
            }
            ExportSet ec = new ExportSet(genericType);
            ec.collectionClass = type.name;
            return ec;
        } else if (Util.isMap(type)) {
            String keyType;
            String valueType;
            if (type.generics.isEmpty()) {
                keyType = "java.lang.Object";
                valueType = "java.lang.Object";
            } else {
                keyType = type.generics.get(0).getFullName();
                valueType = type.generics.get(1).getFullName();
            }
            ExportMap ec = new ExportMap(keyType, valueType);
            ec.collectionClass = type.name;
            return ec;
        } else if (type.isGeneric) {
            String genericType = type.generics.get(0).getFullName();
            ExportCollection ec = new ExportCollection(genericType);
            ec.collectionClass = type.name;
            return ec;
        } else if (type.isEnum) {
            ExportEnum exportEnum = new ExportEnum(type.getFullName(), false);
            exportEnum.entries.addAll(type.entries);
            return exportEnum;
        } else if (type.classIntance != null) {
            return new ExportNativeClass(type.classIntance);
        } else if (type.classFile != null) {
            return new ExportClass(type.getFullName());
        } else {
            throw new IllegalArgumentException("Class not resolved " + type);
        }
    }

    protected void resolveClassFields(Map<String, ExportClass> classMap) {
        for (ExportClass exportClass : classMap.values()) {
            PreClass preClass = resolvedClasses.get(exportClass.name);
            if (preClass == null) {
                throw new IllegalArgumentException("PreClass " + exportClass.name + " not resolved");
            }
            if (!preClass.fieldsResolved) {
                throw new IllegalArgumentException("PreClass " + preClass + " fields not resolved");
            }
            for (PreField preField : preClass.fields) {
                ExportField exportField = new ExportField();
                exportField.name = preField.name;
                exportField.required = preField.required;
                exportField.ignoredGenerators = preField.ignoredGenerators;
                ExportClass preFieldType = classMap.get(preField.type.getFullName());
                if (preFieldType == null) {
                    throw new IllegalStateException("Can't find ExportClass for " + preField.type.name);
                }
                exportField.type = preFieldType;
                exportClass.fields.add(exportField);
            }
        }
    }

    public static List<PreClass> getUnresolvedNameExtends(PreClass preClass) {
        List<PreClass> result = new ArrayList<>();
        for (PreClass classExtend : preClass.classExtends) {
            result.addAll(getUnresolvedNameExtends(classExtend));
        }
        return result;
    }

    protected void updateResolvedRecoursive(PreClass preClass, List<PreClass> processed, String prefix) {
        prefix = "    " + prefix;
        if (Util.contains(processed, preClass)) {
            return;
        }
        processed.add(preClass);
        PreClass resolved = resolvedClasses.get(preClass.getFullName());
        if (resolved != null) {
            PreClass.copy(resolved, preClass);
        }
        if (preClass.context != null) {
            updateResolvedRecoursive(preClass.context, processed, prefix);
        }
        for (PreClass classExtend : preClass.classExtends) {
            updateResolvedRecoursive(classExtend, processed, prefix);
        }
        for (PreClass generic : preClass.generics) {
            updateResolvedRecoursive(generic, processed, prefix);
        }
        for (PreField field : preClass.fields) {
            updateResolvedRecoursive(field.type, processed, prefix);
        }
    }

    protected void updateResolved(PreClass preClass) {
        updateResolvedRecoursive(preClass, new ArrayList<>(), "");
    }

}
