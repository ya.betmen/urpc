package com.unalarabe.urpc.converter.util;

import com.unalarabe.urpc.converter.prepare.PreClass;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import com.unalarabe.urpc.converter.prepare.PreMethod;
import com.unalarabe.urpc.converter.prepare.PreParameter;
import com.unalarabe.urpc.converter.prepare.PreService;
import com.unalarabe.urpc.converter.structure.ExportClass;
import com.unalarabe.urpc.converter.structure.ExportException;
import com.unalarabe.urpc.converter.structure.ExportField;
import com.unalarabe.urpc.converter.structure.ExportMethod;
import com.unalarabe.urpc.converter.structure.ExportParameter;
import com.unalarabe.urpc.converter.structure.ExportService;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class Converter {

    public static ExportMethod convertMethod(PreMethod preMethod, Map<String, ExportClass> classMap) {
        ExportMethod result = new ExportMethod();
        result.name = preMethod.name;
        result.parameters.addAll(converParameters(preMethod.parameters, classMap));
        result.type = classMap.get(preMethod.type.getFullName());
        result.exceptions.addAll(convertExceptions(preMethod.exceptions, classMap));
        result.ignoredGenerators.addAll(preMethod.ignoredGenerators);
        return result;
    }

    public static Collection<? extends ExportMethod> convertMethods(List<PreMethod> preMethods, Map<String, ExportClass> classMap) {
        ArrayList<ExportMethod> result = new ArrayList<>();
        for (PreMethod preMethod : preMethods) {
            result.add(convertMethod(preMethod, classMap));
        }
        return result;
    }

    public static ExportService convertService(PreService preService, Map<String, ExportClass> classMap) {
        ExportService result = new ExportService(preService.name);
        Collection<? extends ExportMethod> methods = convertMethods(preService.methods, classMap);
        for (ExportMethod method : methods) {
            result.addMethod(method.name, method);
        }
        return result;
    }

    public static Collection<? extends ExportParameter> converParameters(List<PreParameter> parameters, Map<String, ExportClass> classMap) {
        ArrayList<ExportParameter> result = new ArrayList<>();
        for (PreParameter parameter : parameters) {
            result.add(convertParameter(parameter, classMap));
        }
        return result;
    }

    public static Collection<? extends ExportException> convertExceptions(List<PreClass> exeptions, Map<String, ExportClass> classMap) {
        ArrayList<ExportException> result = new ArrayList<>();
        for (PreClass preClass : exeptions) {
            result.add(convertException(preClass, classMap));
        }
        return result;
    }

    public static ExportParameter convertParameter(PreParameter parameter, Map<String, ExportClass> classMap) {
        ExportParameter result = new ExportParameter();
        result.name = parameter.name;
        result.type = classMap.get(parameter.type.getFullName());
        result.required = parameter.required;
        for (ExportField field : result.type.fields) {
            System.out.println(field.name + " => " + field.type);
        }
        return result;
    }

    public static ExportException convertException(PreClass preClass, Map<String, ExportClass> classMap) {
        ExportClass result = classMap.get(preClass.getFullName());
        if (result == null) {
            throw new IllegalStateException("Can't resolve " + preClass.getFullName());
        }
        return new ExportException(result.name);
    }
}
