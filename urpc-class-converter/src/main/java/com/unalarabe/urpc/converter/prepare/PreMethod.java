package com.unalarabe.urpc.converter.prepare;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author petr
 */
public class PreMethod {

    public final PreService service;
    public String typeName;
    public PreClass type;
    public String name;
    public final List<PreParameter> parameters = new ArrayList<>();
    public final List<PreClass> exceptions = new ArrayList<>();
    public final Set<String> ignoredGenerators = new HashSet<>();

    public PreMethod(PreService service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "\nPreMethod{" + "\n\ttypeName=" + typeName + ",\n\tname=" + name + ",\n\tparameters=" + parameters + ",\n\ttype=" + type + ",\n\texceptions=" + exceptions + "\n}";
    }

}
