package com.unalarabe.urpc.converter.structure;

import java.util.Objects;

/**
 *
 * @author petr
 */
public class ExportCollection extends ExportClass {

    public String collectionClass;

    public ExportCollection(String of) {
        super(of);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.collectionClass);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExportCollection other = (ExportCollection) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.collectionClass, other.collectionClass)) {
            return false;
        }
        return true;
    }
}
