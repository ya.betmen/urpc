package com.unalarabe.urpc.factory;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public interface ObjectFactory {

    public <T> T createObject(Class<T> clazz) throws CreationException;

    public <T, L extends java.util.Collection<T>> L createCollection(Class<L> collectionClazz, Class<T> clazz) throws CreationException;

    public <K, V, M extends java.util.Map<K, V>> M createMap(Class<M> mapClazz, Class<K> keyClazz, Class<V> valueClazz) throws CreationException;

}
