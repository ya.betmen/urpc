package com.unalarabe.urpc.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petr Zalyautdinov<ya.betmen@gmail.com>
 */
public class DefaultObjectFactory implements ObjectFactory {

    private static final Logger LOG = Logger.getLogger(DefaultObjectFactory.class.getName());

    @Override
    public <T> T createObject(Class<T> clazz) throws CreationException {
        try {
            Constructor<T> constructor = clazz.getConstructor();
            return constructor.newInstance();
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new CreationException();
        }
    }

    @Override
    public <T, L extends Collection<T>> L createCollection(Class<L> collectionClazz, Class<T> clazz) throws CreationException {
        if (collectionClazz.isInterface()) {
            if (collectionClazz.isAssignableFrom(Set.class)) {
                return (L) new HashSet<>();
            } else {
                return (L) new ArrayList<>();
            }
        }
        return createObject(collectionClazz);
    }

    @Override
    public <K, V, M extends Map<K, V>> M createMap(Class<M> mapClazz, Class<K> keyClazz, Class<V> valueClazz) throws CreationException {
        if (mapClazz.isInterface()) {
            return (M) new HashMap<>();
        }
        return createObject(mapClazz);
    }

}
